//
//  LP+Extensions.swift
//  ALCQC
//
//  Created by mac-216 on 7/18/18.
//  Copyright © 2018 Sunrise Software Solutions. All rights reserved.
//

import UIKit
import LPInfra

extension UIStatusBarStyle {
    
    public init(rawValue: String) {
        if rawValue.lowercased() == "lightContent" {
            self = .lightContent
        } else {
            self = .default
        }
    }    
}

extension CheckmarksState {
    
    public init(rawValue: String) {
        if rawValue.lowercased() == "sentOnly" {
            self = .sentOnly
        } else if rawValue.lowercased() == "sentAndAccepted" {
            self = .sentAndAccepted
        } else {
            self = .all
        }
    }

}

extension LPUrlPreviewStyle {
    
    public init(rawValue: String) {
        if rawValue.lowercased() == "large" {
            self = .large
        } else {
            self = .slim
        }
    }
    
}
