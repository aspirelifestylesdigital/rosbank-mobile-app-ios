//
//  LPChatProvider.swift
//  ALC
//
//  Created by mac-216 on 6/22/18.
//  Copyright © 2018 Sunrise Software Solutions. All rights reserved.
//

import UIKit
import LPMessagingSDK
import LPAMS
import LPInfra
import LPMonitoring
import UserNotifications

@objc protocol LPChatProviderRemoteNotificationsDelegate {
    @objc optional func lpChatProviderNotificationTapped()
}

@objc class LPChatProvider: NSObject {
    
    static let shared = LPChatProvider()
    private final let configurationFilename = "lpconfiguration.csv"
    
    var chatScreen: ConversationViewController?
    var campaignInfo: LPCampaignInfo?
    var delegate: LPChatProviderRemoteNotificationsDelegate?
    let consumerID = ""
    
    private override init() {
        
        super.init()
        
        do {
            try LPMessagingSDK.instance.initialize(self.brandID, monitoringInitParams: LPMonitoringInitParams(appInstallID: self.appInstallID))
        } catch  {
            print(error.localizedDescription)
        }
        
        
        LPMessagingSDK.instance.subscribeLogEvents(LogLevel.trace) { (log) -> () in
            print("LPMessagingSDK log: \(String(describing: log.text))")
        }
        
        LPMessagingSDK.instance.delegate = self
        
        self.setConfiguration()
        
        let version: String = LPMessagingSDK.instance.getSDKVersion() ?? "Undefined"
        print("LPMessagingSDK version: " + version)
    }
    
    private var brandID: String {
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
        let url = URL(fileURLWithPath: path)
        let dict = Dictionary<String, Any>.contentsOf(path: url)
        let brandID = dict["LivePersonBrandID"] as! String
        return brandID
    }
    
    private var appInstallID: String {
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
        let url = URL(fileURLWithPath: path)
        let dict = Dictionary<String, Any>.contentsOf(path: url)
        let brandID = dict["LivePersonAppInstallID"] as! String
        return brandID
    }
    
    public func showScreen(_ navigationController: UINavigationController, startSetupCallback: (()->())?, finishSetupCallback: ((_ success: Bool)->())?){
        
        if let campaignInfo = self.campaignInfo {
            self.displayConversationWindow(navigationController, campaignInfo: campaignInfo)
        } else {
            startSetupCallback?()
            
            let monitoringParams = getMonitoringParams(navigationController)
            let identity = LPMonitoringIdentity(consumerID: self.consumerID, issuer: "")
            
            LPMonitoringAPI.instance.getEngagement(identities: [identity],
                                                   monitoringParams: monitoringParams,
                                                   completion: { [weak self] getEngagementResponse in
                                                    
                                                    print("received get engagement response with pageID: \(String(describing: getEngagementResponse.pageId))," +
                                                        "campaignID: \(String(describing: getEngagementResponse.engagementDetails?.first?.campaignId))," +
                                                        "engagementID: \(String(describing: getEngagementResponse.engagementDetails?.first?.engagementId))")
                                                    
                                                    if let campaignID = getEngagementResponse.engagementDetails?.first?.campaignId, let engagementID = getEngagementResponse.engagementDetails?.first?.engagementId, let contextID = getEngagementResponse.engagementDetails?.first?.contextId
                                                    {
                                                        self?.campaignInfo = LPCampaignInfo(campaignId: campaignID, engagementId: engagementID, contextId: contextID)
                                                    }
                                                    finishSetupCallback?(true)
                                                    self?.displayConversationWindow(navigationController, campaignInfo: self?.campaignInfo)
                }, failure: { [weak self] error  in
                    finishSetupCallback?(false)
                    self?.displayErrorMessage(navigationController)
            })
        }
    }
    
    private func getMonitoringParams(_ navigationController: UINavigationController) -> LPMonitoringParams? {
        let entryPoints = ["sec://rosbank"]
        let monitoringParams = LPMonitoringParams(entryPoints: entryPoints, engagementAttributes: nil)
        return monitoringParams
    }
    
    private func displayErrorMessage(_ navigationController: UINavigationController,
                                     message: String = NSLocalizedString("Chat initialization error", comment: "")){
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (alert: UIAlertAction) -> Void in }
        let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: message, preferredStyle: .alert)
        alertController.addAction(cancelAction)
        navigationController.present(alertController, animated: true, completion: nil)
    }
    
    private func displayConversationWindow(_ navigationController: UINavigationController, campaignInfo: LPCampaignInfo?){
        let conversationQuery: ConversationParamProtocol? = LPMessagingSDK.instance.getConversationBrandQuery(self.brandID, campaignInfo: campaignInfo)
        guard conversationQuery != nil else {
            displayErrorMessage(navigationController)
            return
        }
        if self.chatScreen == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.chatScreen = storyboard.instantiateViewController(withIdentifier: "ConversationViewController") as? ConversationViewController
            guard self.chatScreen != nil || conversationQuery != nil else {
                displayErrorMessage(navigationController)
                return
            }
        }
        self.chatScreen?.account = self.brandID
        self.chatScreen?.conversationQuery = conversationQuery!
        let conversationViewParams = LPConversationViewParams(conversationQuery: conversationQuery!,
                                                              containerViewController: self.chatScreen,
                                                              isViewOnly: false)
        LPMessagingSDK.instance.showConversation(conversationViewParams, authenticationParams: nil)
        navigationController.pushViewController(self.chatScreen!, animated: true)
    }
    
    private func setConfiguration(){
        
        guard let filepath = Bundle.main.path(forResource: configurationFilename, ofType: nil) else {
            print("LPChatProvider error: File of configuration \(configurationFilename) hasn't found" )
            return
        }
        do {
            var csvContents = try String(contentsOfFile: filepath, encoding: .utf8)
            csvContents = csvContents.replacingOccurrences(of: "\r", with: "\n")
            csvContents = csvContents.replacingOccurrences(of: "\n\n", with: "\n")
            let configurations = LPConfig.defaultConfiguration
            csvContents.components(separatedBy:"\n").forEach { csvString in
                print(csvString)
                if let itemConfig = LPConfigItem(csv: csvString){
                    if itemConfig.type == .lpLanguage, let strValue = itemConfig.value as? String {
                        configurations.language = LPLanguage(rawValue: strValue)
                    }
                    else if itemConfig.type == .uiFontTextStyle , let strValue = itemConfig.value as? String {
                        if itemConfig.name == "conversationSeparatorFontSize" {
                            configurations.conversationSeparatorFontSize = UIFontTextStyle(strValue)
                        } else if itemConfig.name == "dateSeparatorFontSize" {
                            configurations.dateSeparatorFontSize = UIFontTextStyle(strValue)
                        }
                    }
                    else if itemConfig.type == .uiStatusBarStyle, let strValue = itemConfig.value as? String {
                        configurations.conversationStatusBarStyle = UIStatusBarStyle(rawValue: strValue)
                    }
                    else if itemConfig.type == .checkmarksState, let strValue = itemConfig.value as? String {
                        configurations.checkmarkVisibility = CheckmarksState(rawValue: strValue)
                    }
//                    else if itemConfig.type == .urlPreviewStyle, let strValue = itemConfig.value as? String {
//                    }
                    else {
                        configurations.setValue(itemConfig.value, forKey: itemConfig.name)
                    }
                }
                
            }
        } catch {
            print("LPChatProvider error: Content of file \(filepath) hasn't valid")
            return
        }
        
        LPConfig.defaultConfiguration.enableAudioSharing = true
        
    }
    
    func setUserProfile(firstName: String? = nil,
                        lastName: String? = nil,
                        nickName: String? = nil,
                        profileImageURL: String? = nil,
                        phoneNumber: String? = nil,
                        email: String? = nil
                        )
    {
        var contactField = ""
        contactField += phoneNumber ?? ""
        if let email = email {
            contactField += "|\(email)"
        }        
        let user = LPUser(firstName: firstName,
                          lastName: lastName,
                          nickName: nickName,
                          uid: nil,
                          profileImageURL: profileImageURL,
                          phoneNumber: contactField,
                          employeeID: nil)
        LPMessagingSDK.instance.setUserProfile(user, brandID: self.brandID)
    }
    
    fileprivate func setAgent(_ agent: LPUser?){
        let title = (agent?.nickName?.length ?? 0 != 0) ? agent!.nickName! : NSLocalizedString("Chat", comment: "").uppercased()
        self.chatScreen?.title = title
        LPConfig.defaultConfiguration.brandName = title
    }
    
    fileprivate func setChatScreenTitle(_ title: String){
        let title = NSLocalizedString(title, comment: "").uppercased()
        self.chatScreen?.title = title
        LPConfig.defaultConfiguration.brandName = title
    }
    
    public func registerRemoteNotifications(){
        let application = UIApplication.shared
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        } else if #available(iOS 9, *) {
            let notificationSettings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
        }
        application.registerForRemoteNotifications()
    }
    
    public func unregisterRemoteNotifications(){
        let application = UIApplication.shared
        application.unregisterForRemoteNotifications()
    }
}

//MARK:- LPMessagingSDKDelegate
extension LPChatProvider: LPMessagingSDKdelegate {
    
    /**
     This delegate method is required.
     It is called when authentication process fails
     */
    func LPMessagingSDKAuthenticationFailed(_ error: NSError) {
        NSLog("Error: \(error)");
    }
    
    /**
     This delegate method is required.
     It is called when the SDK version you're using is obselete and needs an update.
     */
    func LPMessagingSDKObseleteVersion(_ error: NSError) {
        NSLog("Error: \(error)");
    }
    
    /**
     This delegate method is optional.
     It is called each time the SDK receives info about the agent on the other side.
     
     Example:
     You can use this data to show the agent details on your navigation bar (in view controller mode)
     */
    func LPMessagingSDKAgentDetails(_ agent: LPUser?) {
        guard self.chatScreen != nil else {
            return
        }
        setAgent(agent)
    }
    
    /**
     This delegate method is optional.
     It is called each time the SDK menu is opened/closed.
     */
    func LPMessagingSDKActionsMenuToggled(_ toggled: Bool) {
        
    }
    
    /**
     This delegate method is optional.
     It is called each time the agent typing state changes.
     */
    func LPMessagingSDKAgentIsTypingStateChanged(_ isTyping: Bool) {
        
    }
    
    /**
     This delegate method is optional.
     It is called after the customer satisfaction page is submitted with a score.
     */
    func LPMessagingSDKCSATScoreSubmissionDidFinish(_ accountID: String, rating: Int) {
        setAgent(nil)
    }
    
    /**
     This delegate method is optional.
     If you set a custom button, this method will be called when the custom button is clicked.
     */
    func LPMessagingSDKCustomButtonTapped() {
        
    }
    
    /**
     This delegate method is optional.
     It is called whenever an event log is received.
     */
    func LPMessagingSDKDidReceiveEventLog(_ eventLog: String) {
        
    }
    
    /**
     This delegate method is optional.
     It is called when the SDK has connections issues.
     */
    func LPMessagingSDKHasConnectionError(_ error: String?) {
        
    }
    
    /**
     This delegate method is required.
     It is called when the token which used for authentication is expired
     */
    func LPMessagingSDKTokenExpired(_ brandID: String) {
        
    }
    
    /**
     This delegate method is required.
     It lets you know if there is an error with the sdk and what this error is
     */
    func LPMessagingSDKError(_ error: NSError) {
        
    }
    
    /**
     This delegate method is optional.
     It is called when the conversation view controller removed from its container view controller or window.
     */
    func LPMessagingSDKConversationViewControllerDidDismiss() {
        
    }
    
    /**
     This delegate method is optional.
     It is called when a new conversation has started, from the agent or from the consumer side.
     */
    func LPMessagingSDKConversationStarted(_ conversationID: String?) {
        
    }
    
    /**
     This delegate method is optional.
     It is called when a conversation has ended, from the agent or from the consumer side.
     */
    func LPMessagingSDKConversationEnded(_ conversationID: String?) {
        
    }
    
    /**
     This delegate method is optional.
     It is called when the customer satisfaction survey is dismissed after the user has submitted the survey/
     */
    func LPMessagingSDKConversationCSATDismissedOnSubmittion(_ conversationID: String?) {
        
    }
    
    /**
     This delegate method is optional.
     It is called each time connection state changed for a brand with a flag whenever connection is ready.
     Ready means that all conversations and messages were synced with the server.
     */
    func LPMessagingSDKConnectionStateChanged(_ isReady: Bool, brandID: String) {
        print("isReady \(isReady)")
    }
    
    /**
     This delegate method is optional.
     It is called when the user tapped on the agent’s avatar in the conversation and also in the navigation bar within window mode.
     */
    func LPMessagingSDKAgentAvatarTapped(_ agent: LPUser?) {
        
    }
    
    /**
     This delegate method is optional.
     It is called when the Conversation CSAT did load
     */
    func LPMessagingSDKConversationCSATDidLoad(_ conversationID: String?) {
        setChatScreenTitle("")
    }
    
    /**
     This delegate method is optional.
     It is called when the Conversation CSAT skipped by the consumer
     */
    func LPMessagingSDKConversationCSATSkipped(_ conversationID: String?) {
        
    }
    
    /**
     This delegate method is optional.
     It is called when the user is opening photo sharing gallery/camera and the persmissions denied
     */
    func LPMessagingSDKUserDeniedPermission(_ permissionType: LPPermissionTypes) {
        
    }
}

//MARK:- LPChatNotificationsProtocol
extension LPChatProvider: LPChatNotificationsProtocol, LPMessagingSDKNotificationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {

    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        LPMessagingSDK.instance.registerPushNotifications(token: deviceToken, notificationDelegate: self)
        //print("didRegisterForRemoteNotificationsWithDeviceToken \(deviceToken.map { String(format: "%02.2hhx", $0) }.joined())")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        LPMessagingSDK.instance.handlePush(userInfo)
        if application.applicationState != .active {
            delegate?.lpChatProviderNotificationTapped?()
        }
        completionHandler(.newData)
    }
    
    func LPMessagingSDKNotification(notificationTapped notification: LPNotification) {
        delegate?.lpChatProviderNotificationTapped?()
    }
}
