//
//  ConversationViewController.swift
//  SampleApp-Swift
//
//  Created by Nimrod Shai on 2/23/16.
//  Copyright © 2016 LivePerson. All rights reserved.
//

import Foundation
import LPMessagingSDK
import LPAMS
import LPInfra

class ConversationViewController: UIViewController {

    var account: String? = nil
    var conversationQuery: ConversationParamProtocol?    
    @IBOutlet var leftBarButtonItem: UIBarButtonItem!
    @IBOutlet var rightBarButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftBarButtonItem.title = NSLocalizedString("Back", comment: "")
        rightBarButtonItem.title = NSLocalizedString("Menu", comment: "")
        self.title = NSLocalizedString("Chat", comment: "").uppercased()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        /*
         guard let vc = UIApplication.shared.windows.firstObject()!.rootViewController?.presentedViewController else {
         return
         }
         
         if String(describing: type(of: vc.self)) == "LPImagePreviewViewController" {
         for v in vc.view.subviews {
         if String(describing: type(of: v.self)) == "InputTextView" {
         v.isHidden = true
         return
         }
         }
         }
         */
    }

    
    @IBAction func backButtonPressed() {
        if self.account != nil {
            self.conversationQuery = LPMessagingSDK.instance.getConversationBrandQuery(self.account!)
            if self.conversationQuery != nil {
                LPMessagingSDK.instance.removeConversation(self.conversationQuery!)
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func menuButtonPressed() {
        
        let alertController = UIAlertController(title: NSLocalizedString("Menu", comment: ""),
                                                message: NSLocalizedString("Choose an option", comment: ""), preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (alert: UIAlertAction) -> Void in }
        
        /**
        is how to resolve a conversation
        */
        let resolveAction = UIAlertAction(title: NSLocalizedString("Resolve the conversation", comment: ""), style: .default) { [weak self] (alert: UIAlertAction) -> Void in
            self?.showResolveConversationMessageBox(title: NSLocalizedString("Resolve the conversation", comment: ""),
                                                    message: NSLocalizedString("Are you sure this topic is resolved?", comment: ""))
        }
        resolveAction.isEnabled = LPMessagingSDK.instance.checkActiveConversation(self.conversationQuery!)
        
        /**
        This is how to manage the urgency state of the conversation
        */
        let urgentTitle = NSLocalizedString( LPMessagingSDK.instance.isUrgent(self.conversationQuery!) ? "Dismiss urgency" : "Mark as urgent", comment: "")
        let urgentAction = UIAlertAction(title: urgentTitle, style: .default) { [weak self] (alert: UIAlertAction) -> Void in
            self?.showUrgentMessageBox()
        }
        urgentAction.isEnabled = LPMessagingSDK.instance.checkActiveConversation(self.conversationQuery!)
        
        /**
         This is how to clear the history of the conversation
         */
        let clearHistoryAction = UIAlertAction(title: NSLocalizedString("Clear history", comment: ""), style: .default) { [weak self] (alert: UIAlertAction) -> Void in
            self?.showClearHistoryMessageBox()
        }
        
        //alertController.addAction(urgentAction)
        alertController.addAction(clearHistoryAction)
        alertController.addAction(resolveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) { () -> Void in
        }
    }
    
    private func showMessageBox(title: String? = nil, message: String? = nil, actions: [UIAlertAction]){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { alertController.addAction($0)}
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showResolveConversationMessageBox(title: String? = nil, message: String? = nil){
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (alert: UIAlertAction) -> Void in }
        let okAction = UIAlertAction(title: NSLocalizedString("Resolve", comment: ""), style: .default) {(alert: UIAlertAction) -> Void in
            if self.conversationQuery != nil {
                LPMessagingSDK.instance.resolveConversation(self.conversationQuery!)
            }
        }
        
        self.showMessageBox(title: title,
                            message: message,
                            actions: [okAction, cancelAction])
    }
    
    private func showUrgentMessageBox(){
        let urgentTitle = NSLocalizedString( LPMessagingSDK.instance.isUrgent(self.conversationQuery!) ? "Dismiss urgency" : "Mark as urgent", comment: "")
        let urgentMessage = NSLocalizedString(LPMessagingSDK.instance.isUrgent(self.conversationQuery!) ? "Are you sure you want to mark this conversation as not urgent?" : "Are you sure you want to mark this conversation as urgent?", comment: "")
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (alert: UIAlertAction) -> Void in }
        let okAction = UIAlertAction(title: urgentTitle, style: .default) {(alert: UIAlertAction) -> Void in
            if LPMessagingSDK.instance.isUrgent(self.conversationQuery!) {
                LPMessagingSDK.instance.dismissUrgent(self.conversationQuery!)
            } else {
                LPMessagingSDK.instance.markAsUrgent(self.conversationQuery!)
            }
        }
        
        self.showMessageBox(title: urgentTitle,
                            message: urgentMessage,
                            actions: [okAction, cancelAction])
    }
    
    private func showClearHistoryMessageBox(){
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (alert: UIAlertAction) -> Void in }
        let okAction = UIAlertAction(title: NSLocalizedString("Clear", comment: ""), style: .default) {(alert: UIAlertAction) -> Void in
            if LPMessagingSDK.instance.checkActiveConversation(self.conversationQuery!) {
                self.showResolveConversationMessageBox(title: NSLocalizedString("Clear history", comment: ""), message: NSLocalizedString("Please resolve the conversation first", comment: ""))
            } else {
                try? LPMessagingSDK.instance.clearHistory(self.conversationQuery!)
            }
        }
        
        self.showMessageBox(title: NSLocalizedString("Clear history", comment: ""),
                            message: NSLocalizedString("All of your existing conversation history will be lost. Are you sure?", comment: ""),
                            actions: [okAction, cancelAction])
    }
}
