//
//  WSB2CGetUserDetails.h
//  ALC
//
//  Created by Anh Tran on 11/3/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "UserObject.h"
@interface WSB2CGetUserDetails : WSB2CBase
@property (strong, nonatomic) UserObject* userDetails;
-(void)getUserDetails;
@end
