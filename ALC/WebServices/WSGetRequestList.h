//
//  WSGetRequestList.h
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSGetRequestList : WSB2CBase
@property (strong, nonatomic) NSMutableArray* myUpcomingRequests;
@property (strong, nonatomic) NSMutableArray* myHistoryRequests;
-(id)initWithPageSize:(NSInteger)size;
-(void)getMyRequest:(NSMutableDictionary*) dict;
-(void) nextPage;
-(BOOL) hasNextItem;
@end
