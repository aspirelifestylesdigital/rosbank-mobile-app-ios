//
//  WSGetGourmetRecommend.h
//  ALC
//
//  Created by Hai NguyenV on 10/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "GouetmetRecommendDetailsObj.h"
@interface WSGetGourmetRecommend : WSB2CBase
{
    NSString* bookingID;
}
@property (strong, nonatomic) GouetmetRecommendDetailsObj* bookingDetails;

-(void) getRequestDetails:(NSString*) bookingId;

@end
