//
//  WSGetGourmetDetails.m
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HGWGetBaseRequest.h"
#import "WSGetGourmetDetails.h"
#import "BaseResponseObject.h"

@implementation WSGetGourmetDetails
@synthesize restaurantId;
-(id)init{
    self = [super init];
    return self;
}

-(void)getGourmetDetails:(NSString*) restId{
    self.restaurantId = restId;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_GOURMET_DETAILS;
    subTask = WS_ST_NONE;
    
    HGWGetBaseRequest *request = [[HGWGetBaseRequest alloc]init];
    request.dictKeyValues = [self buildRequestParams];
    request.requestUrl = [NSString stringWithFormat:@"%@%@", API_URL, GetRestaurantDetailsUrl];
    
    [service invokeMethod:kHTTPGET andObject:request forTask:task forSubTask:subTask];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            NSDictionary* data = [jsonResult dictionaryForKey:@"Data"];
            self.restaurantDetails = [[GourmetObject alloc] initFromDict:data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:restaurantId forKey:@"RestaurantId"];
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        [dictKeyValues setObject:((UserObject*)[appdele getLoggedInUser]).userId  forKey:@"UserID"];
    }
    
    return dictKeyValues;
}
@end
