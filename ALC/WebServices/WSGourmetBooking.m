//
//  WSGourmetBooking.m
//  ALC
//
//  Created by Hai NguyenV on 10/5/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGourmetBooking.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@implementation WSGourmetBooking


-(void)bookingGourmet:(GourmetRequestObject*)gourmetRequest{
    requestObject = gourmetRequest;
    [self checkAuthenticate];
}
-(void)startAPIAfterAuthenticate{
    task = WS_BOOKING_GOURMET;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(requestObject.BookingId){
        url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    } else {
        url = [B2C_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }
    [self POST:url withParams:[self buildRequestParams]];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(RESTAURANT_REQUEST_TYPE, RESTAURANT_BOOK);
    
    [dictKeyValues setObject:RESTAURANT_BOOK_SITUATION forKey:@"Situation"];

    if(requestObject){
        if(requestObject.BookingId){
            [dictKeyValues setObject:requestObject.BookingId  forKey:@"TransactionID"];
            //[dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];
        }
        
        if(requestObject.EmailAddress.length > 0){
            [dictKeyValues setObject:requestObject.EmailAddress  forKey:@"EmailAddress1"];
            [dictKeyValues setObject:requestObject.EmailAddress  forKey:@"Email1"];
        }
        
        if(requestObject.MobileNumber){
            [dictKeyValues setObject:requestObject.MobileNumber  forKey:@"PhoneNumber"];
        }
        
        /*if(requestObject.isContactBoth){
            [dictKeyValues setObject:@"Both" forKey:@"PrefResponse"];
        } else */if(requestObject.isContactPhone){
            [dictKeyValues setObject:@"Phone" forKey:@"PrefResponse"];
        } else if(requestObject.isContactEmail){
            [dictKeyValues setObject:@"Email" forKey:@"PrefResponse"];
        }
        
        [dictKeyValues setObject:requestObject.EditType forKey:@"EditType"];

        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.adultPax]  forKey:@"NumberofAdults"];
        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.kidsPax]  forKey:@"NumberOfKids"];
        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.kidsPax]  forKey:@"NumberOfChildren"];
        [dictKeyValues setObject:requestObject.City  forKey:@"City"];
        [dictKeyValues setObject:getCountryCodeByName(requestObject.Country)  forKey:@"Country"];
        [dictKeyValues setObject:[requestObject.reservationDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.reservationTime]]  forKey:@"EventDate"];
        
        dictKeyValues = removeSpecialCharactersfromDict(dictKeyValues);
        
        //Add values to Request Details
        NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
        
        [detailsValues setObject:requestObject.reservationName  forKey:RESERVATION_NAME_REQUIREMENT];
        [detailsValues setObject:requestObject.restaurantName  forKey:RESTAURANT_NAME_REQUIREMENT];
        [detailsValues setObject:requestObject.Country  forKey:COUNTRY_REQUIREMENT];
        
        if(requestObject.Occasion){
            NSString *value = convertArrayToString(requestObject.Occasion);
            [detailsValues setObject:value forKey:OCCASION_REQUIREMENT];
        }
        
        if(requestObject.foodAllergies){
            [detailsValues setObject:requestObject.foodAllergies forKey:FoodAllergies];
        }
        
        if(requestObject.SpecialRequirements){
            requestObject.SpecialRequirements = [[requestObject.SpecialRequirements componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            [detailsValues setObject:requestObject.SpecialRequirements  forKey:SPECIAL_REQUIREMENT];
        }
        
        if(requestObject.State){
            [detailsValues setObject:requestObject.State  forKey:StateRequirement];
        }        
        
        if(requestObject.privilegeId){
            [detailsValues setObject:requestObject.privilegeId  forKey:PrivilegeId];
        }
        
        if(requestObject.fullAddress){
            [detailsValues setObject:requestObject.fullAddress  forKey:FullAddress];
        }
        
        detailsValues = removeSpecialCharactersfromDict(detailsValues);
        ///end Request Details
        
        [dictKeyValues setObject:convertRequestDetailsToString(detailsValues) forKey:@"RequestDetails"];
       
    }
    return dictKeyValues;
}

@end
