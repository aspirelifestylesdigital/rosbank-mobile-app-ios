//
//  WSCreateRentalRequest.m
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateRentalRequest.h"

#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@interface WSCreateRentalRequest (){
    CarCreateRequestObject* requestObject;
}

@end

@implementation WSCreateRentalRequest

-(void)bookingRental:(CarCreateRequestObject*) item{
    requestObject = item;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_BOOKING_CAR_RENTAL;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(requestObject.bookingId){
        url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    } else {
        url = [B2C_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }

    [self POST:url withParams:[self buildRequestParams]];
    
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(CAR_RENTAL_REQUEST_TYPE, CAR_RENTAL_FUNCTION);
    
    if(requestObject){
        if(requestObject.bookingId){
            [dictKeyValues setObject:requestObject.bookingId  forKey:@"TransactionID"];
            //[dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];
        }
        
        if(requestObject.email.length > 0){
            [dictKeyValues setObject:requestObject.email  forKey:@"EmailAddress1"];
            [dictKeyValues setObject:requestObject.email  forKey:@"Email1"];
        }
        
        if(requestObject.mobileNumber){
            [dictKeyValues setObject:requestObject.mobileNumber  forKey:@"PhoneNumber"];
        }
        
        /*if(requestObject.isContactBoth){
            [dictKeyValues setObject:@"Both" forKey:@"PrefResponse"];
        } else*/ if(requestObject.isContactPhone){
            [dictKeyValues setObject:@"Phone" forKey:@"PrefResponse"];
        } else if(requestObject.isContactEmail){
            [dictKeyValues setObject:@"Email" forKey:@"PrefResponse"];
        }
        
        [dictKeyValues setObject:requestObject.EditType forKey:@"EditType"];
        
        
        [dictKeyValues setObject:[requestObject.pickUpDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.pickUpTime]]  forKey:@"PickUp"];
        [dictKeyValues setObject:[requestObject.pickUpDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.pickUpTime]]  forKey:@"PickupDate"];
        
        dictKeyValues = removeSpecialCharactersfromDict(dictKeyValues);
        
        //Start add Request Details
        NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
        
        [detailsValues setObject:requestObject.pickUpLocation  forKey:PickLocation];
        
        [detailsValues setObject:[requestObject.dropOffDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.dropOffTime]]  forKey:DropoffDate];
        [detailsValues setObject:requestObject.dropOffLocation  forKey:DropLocation];
        
        [detailsValues setObject:requestObject.driverName  forKey:DriverName];
        [detailsValues setObject:(requestObject.isInternationalLicense?@"yes":@"no")  forKey:IntLicense];
        [detailsValues setObject:[NSNumber numberWithInteger:requestObject.driverAge]  forKey:DriverAge];
        [detailsValues setObject:requestObject.reservationName  forKey:RESERVATION_NAME_REQUIREMENT];
        
        if(requestObject.preferredVerhicle && requestObject.preferredVerhicle.count>0){
            [detailsValues setObject:convertArrayToString(requestObject.preferredVerhicle) forKey:PrefVehicle];
        }
        
        if(requestObject.preferredCarRentals && requestObject.preferredCarRentals.count>0){
            [detailsValues setObject:convertArrayToString(requestObject.preferredCarRentals) forKey:PrefCarRentalComp];
        }
        
        if(requestObject.maximumPrice && requestObject.maximumPrice.length>0){
            [detailsValues setObject:requestObject.maximumPrice  forKey:MAX_BUDGET_RENTAL];
        }
        
        
        if(requestObject.specialMessage){
            requestObject.specialMessage = [[requestObject.specialMessage componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            [detailsValues setObject:requestObject.specialMessage  forKey:SPECIAL_REQUIREMENT];
        }
        detailsValues = removeSpecialCharactersfromDict(detailsValues);
        //end
        
        [dictKeyValues setObject:convertRequestDetailsToString(detailsValues) forKey:@"RequestDetails"];
    }
    
    return dictKeyValues;
}

@end
