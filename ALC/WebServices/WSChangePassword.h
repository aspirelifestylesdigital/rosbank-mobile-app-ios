//
//  WSChangePassword.h
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSChangePassword : WSB2CBase
-(void) changePassword:(NSString*) old toPassword:(NSString*)newP;
@end
