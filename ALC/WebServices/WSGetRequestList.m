//
//  WSGetRequestList.m
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGetRequestList.h"
#import "BaseResponseObject.h"
#import "MyRequestObject.h"
#import "ALCQC-Swift.h"

@interface WSGetRequestList(){
    NSInteger total;
    NSInteger pageIndex;
    NSInteger pageSize;
    NSMutableDictionary *options;
    BOOL maybeHaveNextItems;
    
    NSMutableArray *existedTransactionId;
}

@end

@implementation WSGetRequestList
-(id)init{
    self = [super init];
    pageSize = 10;
    pageIndex = 1;
    self.myUpcomingRequests = [NSMutableArray array];
    self.myHistoryRequests = [NSMutableArray array];
    return self;
}

-(id)initWithPageSize:(NSInteger)size{
    self = [super init];
    pageSize = size;
    pageIndex = 1;
    self.myUpcomingRequests = [NSMutableArray array];
    self.myHistoryRequests = [NSMutableArray array];
    return self;
}


-(void)getMyRequest:(NSMutableDictionary*) dict{
    options = dict;
    pageIndex = 1;
    self.myUpcomingRequests = [NSMutableArray array];
    self.myHistoryRequests = [NSMutableArray array];
    existedTransactionId = [NSMutableArray array];
    [self checkAuthenticate];
}


-(void) nextPage{
    self.myUpcomingRequests = [NSMutableArray array];
    self.myHistoryRequests = [NSMutableArray array];
    if(!existedTransactionId){
        existedTransactionId = [NSMutableArray array];
    }
    if(maybeHaveNextItems){
        pageIndex++;
        [self checkAuthenticate];
    } else {
        [self.delegate loadDataDoneFrom:self];
    }
}

-(BOOL) hasNextItem{
    return maybeHaveNextItems;
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_REQUEST_LIST;
    subTask = WS_ST_NONE;
    
    NSString* url = [B2C_API_URL stringByAppendingString:GetMyRequestListUrl];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(self && jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] init];
        response.task = self.task;
        
        if(self.delegate && [jsonResult isKindOfClass:[NSArray class]])
        {
            NSArray* rests =  (NSArray*)jsonResult;
            maybeHaveNextItems = rests.count > 0;
            
            self.myUpcomingRequests = [NSMutableArray array];
            self.myHistoryRequests = [NSMutableArray array];
            MyRequestObject *item;
            NSInteger count = rests.count;
            for (int i = 0; i < count; i++) {
                if(![existedTransactionId containsObject:item.requestId]){
                    item = [[MyRequestObject alloc] initFromDict:[rests objectAtIndex:i]];
                    if(item.isHistory){
                        [self.myHistoryRequests addObject:item];
                    } else {
                        [self.myUpcomingRequests addObject:item];
                    }
                }
            }
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self && self.delegate)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [NSMutableDictionary dictionary];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        UserObject *user = (UserObject*)[appdele getLoggedInUser];
        [dictKeyValues setObject:@"GetRecentRequests" forKey:@"Functionality"];
        [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
        [dictKeyValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
        [dictKeyValues setObject:user.userId  forKey:@"OnlineMemberId"];
    }
    
    [dictKeyValues setObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(pageIndex-1)*pageSize]] forKey:@"RowStart"];
    [dictKeyValues setObject:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:pageIndex*pageSize-1]] forKey:@"RowEnd"];
    return dictKeyValues;
}

@end
