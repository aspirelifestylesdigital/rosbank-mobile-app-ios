//
//  WSBase.m
//  MobileMap
//
//  Created by Huy Tran on 5/10/12.
//  Copyright (c) 2012 S3Corp. All rights reserved.
//

#import "WSBase.h"
#import "HGWPostBaseRequest.h"
#import "HGWPostRequest.h"
#import "BaseResponseObject.h"
#import "BaseResponseObject.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@interface WSBase(){

}

@end
@implementation WSBase


@synthesize delegate;
@synthesize task, subTask;


-(id) init
{
    self = [super init];
    
    service = [[HGWWebService alloc] init];
    service.delegate = self;
    // for new API
    jsonParser = [[SBJsonParser alloc] init];
    // end for new API
    return self;
}
-(void)startAPIAfterAuthenticate{
}

-(void) dealloc
{
    delegate = nil;
    if(self.servicetask && self.servicetask.state == NSURLSessionTaskStateRunning){
        [self.servicetask cancel];
        self.servicetask = nil;
    }
}

-(void)cancelRequest
{
    if(self.servicetask && self.servicetask.state == NSURLSessionTaskStateRunning){
        [self.servicetask cancel];
        self.servicetask = nil;
    }
}

+(BOOL)authenticatedHGWAPI
{
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    return (appdele.AuthToken != nil && appdele.AuthExpDate != nil
            && NSOrderedDescending == [appdele.AuthExpDate compare:[NSDate date]]);
}
-(void)checkAuthenticate{
    /*AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele getUserEmail]!=nil && [appdele getUserPassword]!=nil){
        if (![WSBase authenticatedHGWAPI])
        {
            [self authenticateHGWAPI:task];
        } else {
            [self startAPIAfterAuthenticate];
        }
    }*/
}

-(void)authenticateHGWAPI:(enum WSTASK)aTask
{   /*
    task = aTask;
    subTask = WS_AUTHENTICATION;
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    // Create data to request
    HGWPostRequest *request = [[HGWPostRequest alloc]init];
    request.dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // Set data for request
    [request.dictKeyValues setObject:[appdele getUserEmail] forKey:@"UserID"];
    [request.dictKeyValues setObject:[appdele getUserPassword] forKey:@"Password"];
    
    request.contentType = RCT_JSON;
    request.requestUrl = AuthenticationUrl;
    
    // Send request
    [service invokeMethod:kHTTPPOST andObject:request forTask:task forSubTask:subTask];*/
}

-(BOOL)parseHGWAuthentication:(NSDictionary*)dict
{
    /*NSNumber *resultCode = [dict objectForKey:@"Status"];
    if (resultCode && [resultCode integerValue] == kSTATUSOK)
    {
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        NSDictionary* data = [dict objectForKey:@"Data"];
        appdele.AuthToken = [data objectForKey:@"AuthorizeToken"];
        appdele.AuthExpDate = [NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"ExpiredAt"] doubleValue]];
        
        return YES;
    }
    else
    {
        if (self.delegate != nil) {
            BaseResponseObject *result = [[BaseResponseObject alloc] initFromDict:dict];
            [self.delegate loadDataFailFrom:result withErrorCode:[resultCode integerValue]];
        }
        return NO;
    }*/
    return NO;
}
#pragma mark - 
#pragma mark HGW web service delegate method
-(void) processResults:(NSData*)data forTask:(NSInteger)localTask forSubTask:(NSInteger)localSubTask returnFormat:(NSInteger)returnFormat
{
    
    if(data){
        NSString *json = [[NSString alloc]
                          initWithBytes: [data bytes]
                          length:[data length]
                          encoding:NSUTF8StringEncoding];
        NSLog(@"Response: %@",json);
        if([json isEqualToString:@"[]"]){
            [self processDataResultWithError:nil];
            return;
        }
        NSError *jsonError = nil;
        NSDictionary* dict = [jsonParser objectWithString:json error:&jsonError];
        if(dict){
            if(localSubTask == WS_AUTHENTICATION )
            {
                if ([self parseHGWAuthentication:dict])
                {
                    [self startAPIAfterAuthenticate];
                }
            } else {
                BaseResponseObject *result = [[BaseResponseObject alloc] initFromDict:dict];
                if(result.status == kSTATUSTOKENEXPIRE){
                    [self authenticateHGWAPI:(int)localTask];
                }else if(result.isSuccess){
                    [self processDataResults:dict forTask:localTask forSubTask:localSubTask returnFormat:returnFormat];
                }else{
                    jsonError = [NSError errorWithDomain:@"Error" code:result.status userInfo:dict];
                    [self processDataResultWithError:jsonError];
                }
            }
        } else {
            [self processDataResultWithError:nil];
        }
    } else {
        [self processDataResultWithError:nil];
    }
}

-(void)processResultWithError:(NSError *)errorCode
{
    
}

-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params{
    NSLog(@"Request:%@%@",url,params);
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer =  requestSerializer;
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    self.servicetask = [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        NSLog(@"Response:%@%@",url,responseObject);
        [self processDataResults:responseObject forTask:self.task forSubTask:subTask returnFormat:400];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Response:%@ error",url);
        if(!isNetworkAvailable()){
            showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
            NSError *err = [[NSError alloc] initWithDomain:@"" code:kNO_INTERNET_CONNECTION userInfo:nil];
            [self processDataResultWithError:err];
        } else {
            [self processDataResultWithError:error];
        }
    }];
}

-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params{
    NSLog(@"Request:%@%@",url,params);
    /*if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
        
        [self processDataResultWithError:nil];
    }else{*/
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    self.servicetask = [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        NSLog(@"Response:%@%@",url,responseObject);
        if([self respondsToSelector:@selector(processDataResults:forTask:forSubTask:returnFormat:)]) {
            [self processDataResults:responseObject forTask:self.task forSubTask:subTask returnFormat:400];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Response:%@ error",url);
        if(!isNetworkAvailable()){
            showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
            NSError *err = [[NSError alloc] initWithDomain:@"" code:kNO_INTERNET_CONNECTION userInfo:nil];
            [self processDataResultWithError:err];
        }
    }];
    //}
    
}

@end
