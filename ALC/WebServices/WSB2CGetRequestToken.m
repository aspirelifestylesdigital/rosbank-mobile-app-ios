//
//  WSB2CGetRequestToken.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetRequestToken.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "BaseResponseObject.h"
#import "ALCQC-Swift.h"

@interface WSB2CGetRequestToken (){
}

@end

@implementation WSB2CGetRequestToken
-(void) getRequestToken{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_REQUEST_TOKEN;
    /*if(!isNetworkAvailable())
     {
     showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
     [self processDataResultWithError:nil];
     }else{*/
    NSString* url = [B2C_API_URL stringByAppendingString:GetRequestToken];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:[AppConfig shared].consumerKey forHTTPHeaderField:@"ConsumerKey"];
    [manager.requestSerializer setValue:[AppConfig shared].consumerSecret forHTTPHeaderField:@"ConsumerSecret"];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    self.servicetask = [manager GET:url parameters:[self buildRequestParams] progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        [self processDataResults:responseObject forTask:self.task forSubTask:subTask returnFormat:400];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Response:%@ error",url);
        if(!isNetworkAvailable()){
            showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
            NSError *err = [[NSError alloc] initWithDomain:@"" code:kNO_INTERNET_CONNECTION userInfo:nil];
            [self processDataResultWithError:err];
        }
    }];
    // }
    
    
    
    
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        NSString *token = [jsonResult stringForKey:@"RequestToken"];
        if(token!=nil && token.length > 0){
            _requestToken = token;

            if(self.delegate)
                [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = task;
    result.status = error.code;
    [self.delegate loadDataFailFrom:result withErrorCode:error.code];
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    UserObject* user =[appdele getLoggedInUser];
    if(user){
        [dictKeyValues setObject:user.userId forKey:@"OnlineMemberId"];
        [dictKeyValues setObject:[AppConfig shared].callBackURL forKey:@"callbackUrl"];
        [dictKeyValues setObject:B2C_DeviceId forKey:@"MemberDeviceId"];
    }
    
    return dictKeyValues;
}

@end
