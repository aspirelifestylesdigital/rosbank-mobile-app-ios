//
//  WSCreateHotelRequest.m
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateHotelRequest.h"

#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@interface WSCreateHotelRequest (){
    HotelRequestObject* requestObject;
}

@end

@implementation WSCreateHotelRequest

-(void)bookingHotel:(HotelRequestObject*) item{
    requestObject = item;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_BOOKING_HOTEL;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(requestObject.bookingId){
        url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    } else {
        url = [B2C_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }
    [self POST:url withParams:[self buildRequestParams]];
    
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(HOTEL_REQUEST_TYPE,  HOTEL_BOOK);
    
    if(requestObject){
        if(requestObject.bookingId){
            [dictKeyValues setObject:requestObject.bookingId  forKey:@"TransactionID"];
            //[dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];
        }
        
        if(requestObject.email.length > 0){
            [dictKeyValues setObject:requestObject.email  forKey:@"EmailAddress1"];
            [dictKeyValues setObject:requestObject.email  forKey:@"Email1"];
        }
        
        if(requestObject.mobileNumber){
            [dictKeyValues setObject:requestObject.mobileNumber  forKey:@"PhoneNumber"];
        }
        
        /*if(requestObject.isContactBoth){
            [dictKeyValues setObject:@"Both" forKey:@"PrefResponse"];
        } else*/ if(requestObject.isContactPhone){
            [dictKeyValues setObject:@"Phone" forKey:@"PrefResponse"];
        } else if(requestObject.isContactEmail){
            [dictKeyValues setObject:@"Email" forKey:@"PrefResponse"];
        }
        
        [dictKeyValues setObject:requestObject.EditType forKey:@"EditType"];
        
        
        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.adultPax]  forKey:@"NumberofAdults"];
        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.kidsPax]  forKey:@"NumberOfKids"];
        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.kidsPax]  forKey:@"NumberofChildren"];
        [dictKeyValues setObject:requestObject.city  forKey:@"City"];
        [dictKeyValues setObject:getCountryCodeByName(requestObject.country)  forKey:@"Country"];
        [dictKeyValues setObject:requestObject.checkInDate  forKey:@"CheckIn"];
        [dictKeyValues setObject:requestObject.checkInDate  forKey:@"Checkindate"];
        [dictKeyValues setObject:requestObject.checkInDate  forKey:@"StartDate"];
        [dictKeyValues setObject:requestObject.checkOutDate  forKey:@"CheckOut"];
        [dictKeyValues setObject:requestObject.checkOutDate  forKey:@"Checkoutdate"];
        [dictKeyValues setObject:requestObject.checkOutDate  forKey:@"EndDate"];
        
        dictKeyValues = removeSpecialCharactersfromDict(dictKeyValues);
        
        // Start add RequestDetails
        NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
        
        [detailsValues setObject:(requestObject.isSmoking?@"yes":@"no")  forKey:SMOKING_PREF_REQUIREMENT];
        
        [detailsValues setObject:requestObject.hotelName  forKey:HOTEL_NAME_REQUIREMENT];
        [detailsValues setObject:requestObject.country  forKey:COUNTRY_REQUIREMENT];
        [detailsValues setObject:requestObject.reservationName  forKey:GUEST_NAME_REQUIREMENT];
        
        if(requestObject.roomType){
            [detailsValues setObject:requestObject.roomType  forKey:ROOM_PREF_REQUIREMENT];
        }
        if(requestObject.specialMessage){
            requestObject.specialMessage = [[requestObject.specialMessage componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            [detailsValues setObject:requestObject.specialMessage  forKey:SPECIAL_REQUIREMENT];
        }
        if(requestObject.anyRequest){
            [detailsValues setObject:requestObject.anyRequest forKey:AdditionalPreference];
        }
        if(requestObject.loyyaltyProgram){
            [detailsValues setObject:requestObject.loyyaltyProgram  forKey:MEMBER_SHIP_REQUIREMENT];
        }
        if(requestObject.membershipNo){
            [detailsValues setObject:requestObject.membershipNo  forKey:MEMBER_NO_REQUIREMENT];
        }
        
        if(requestObject.State){
            [detailsValues setObject:requestObject.State  forKey:StateRequirement];
        }
        
        if(requestObject.privilegeId){
            [detailsValues setObject:requestObject.privilegeId  forKey:PrivilegeId];
        }
        
        if(requestObject.fullAddress){
            [detailsValues setObject:requestObject.fullAddress  forKey:FullAddress];
        }
        
        /*if(requestObject.isWithWifi){
            [detailsValues setObject:(requestObject.isWithWifi?@"true":@"false")  forKey:WithWifi];
        }
        
        if(requestObject.isWithBreakfast){
            [detailsValues setObject:(requestObject.isWithBreakfast?@"true":@"false")  forKey:WithBreakfast];
        }
        
        if(requestObject.noOfRooms > 0){
            [detailsValues setObject:[NSNumber numberWithInteger:requestObject.noOfRooms]  forKey:NumberOfRooms];
        }*/

        detailsValues = removeSpecialCharactersfromDict(detailsValues);
        //end
        
        [dictKeyValues setObject:convertRequestDetailsToString(detailsValues) forKey:@"RequestDetails"];
    }
    
    return dictKeyValues;
}

@end
