//
//  WSDCRBase.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "WSDCRBase.h"
#import "BaseResponseObject.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
@interface WSDCRBase(){
}

@end

@implementation WSDCRBase

-(void)checkAuthenticate{
    [self startAPIAfterAuthenticate];
}


-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params{
    NSLog(@"Request:%@%@",url,params);
    /*if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
        [self processDataResultWithError:nil];
    }else{*/
        
        // add Russia languague
        [params setObject:@"ru-RU" forKey:@"language"];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.securityPolicy.allowInvalidCertificates = YES;
        manager.requestSerializer =  [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [manager.requestSerializer setValue:DCR_SUBCRIPTION_VALUE forHTTPHeaderField:DCR_SUBCRIPTION_KEY];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        self.servicetask = [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
            NSLog(@"Response:%@%@",url,responseObject);
            [self processDataResults:responseObject forTask:self.task forSubTask:subTask returnFormat:400];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Response:%@ error",url);
            if(!isNetworkAvailable()){
                showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
                [self processDataResultWithError:nil];
            }
        }];
    //}
}

-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params{
    NSLog(@"Request:%@%@",url,params);
    /*if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
        
        [self processDataResultWithError:nil];
    }else{*/
        
        // add Russia languague
        [params setObject:@"ru-RU" forKey:@"language"];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        AFSecurityPolicy* policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        [policy setValidatesDomainName:NO];
        [policy setAllowInvalidCertificates:YES];
        manager.securityPolicy=policy;

        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        [manager.requestSerializer setValue:DCR_SUBCRIPTION_VALUE forHTTPHeaderField:DCR_SUBCRIPTION_KEY];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        self.servicetask = [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
            NSLog(@"Response:%@%@",url,responseObject);
            if([self respondsToSelector:@selector(processDataResults:forTask:forSubTask:returnFormat:)]) {
                [self processDataResults:responseObject forTask:self.task forSubTask:subTask returnFormat:400];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Response:%@ error",url);
            if(!isNetworkAvailable()){
                showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
                [self processDataResultWithError:nil];
            }
        }];
    //}
    
}

-(void) dealloc
{
    delegate = nil;
    if(self.servicetask && self.servicetask.state == NSURLSessionTaskStateRunning){
        [self.servicetask cancel];
        self.servicetask = nil;
    }
}

-(void)cancelRequest
{
    if(self.servicetask && self.servicetask.state == NSURLSessionTaskStateRunning){
        [self.servicetask cancel];
        self.servicetask = nil;
    }
}


@end
