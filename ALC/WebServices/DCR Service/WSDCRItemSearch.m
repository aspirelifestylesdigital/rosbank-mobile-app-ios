//
//  WSDCRItemSearch.m
//  ALC
//
//  Created by Anh Tran on 3/13/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "WSDCRItemSearch.h"
#import "DCRItemSearchResponse.h"
#import "BaseDCRObject.h"
@interface WSDCRItemSearch(){
    NSInteger total;
    NSInteger pageIndex;
    NSInteger pageSize;
    BOOL maybeHaveNextItems;
    
    enum DCR_ITEM_TYPE filterType;
    NSString* filterSubType;
}

@end
@implementation WSDCRItemSearch

-(void) searchItemType:(enum DCR_ITEM_TYPE) itemType andSubType:(NSString*)subType{
    filterType = itemType;
    filterSubType = subType;
    pageSize = 20;
    pageIndex = 1;
    [self checkAuthenticate];
}

-(void) nextPage{
    self.items = [NSMutableArray array];
    if([self hasNextItem]){
        pageIndex++;
        [self checkAuthenticate];
    } else {
        [self.delegate loadDataDoneFrom:self];
    }
}

-(BOOL) hasNextItem{
    return maybeHaveNextItems;
}

-(void)startAPIAfterAuthenticate{
    task = WS_DCR_ITEM_SEARCH;
    subTask = WS_ST_NONE;
    
    NSString* url = [DCR_API_URL stringByAppendingString:DCR_ITEM_SEARCH_API];
    
    [self GET:url withParams:[self buildRequestParams]];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        DCRItemSearchResponse *response = [[DCRItemSearchResponse alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response success]){
            _items = [response data];
            maybeHaveNextItems = pageSize*pageIndex < response.totalRecord;
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            BaseResponseObject* res = [[BaseResponseObject alloc] initFromDCR:response];
            [self.delegate loadDataFailFrom:res withErrorCode:res.b2cStatus];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    if(filterType > 0 && filterType <= DCR_ITEM_TYPE_VALUE.count){
        [dictKeyValues setObject:DCR_ITEM_TYPE_VALUE[filterType-1] forKey:@"item_type"];
    }
    
    if(isValidValue(filterSubType)){
        [dictKeyValues setObject:DCR_ITEM_TYPE_VALUE[filterType-1] forKey:@"filter_type"];
    }
    
    [dictKeyValues setObject:@"Name" forKey:@"sort"];
    [dictKeyValues setObject:[NSNumber numberWithInteger:pageIndex] forKey:@"page"];
    [dictKeyValues setObject:[NSNumber numberWithInteger:pageSize] forKey:@"size"];
    
    return dictKeyValues;
}



@end
