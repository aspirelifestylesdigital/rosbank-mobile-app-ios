//
//  WSCreateEventRequest.m
//  ALC
//
//  Created by Hai NguyenV on 11/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateEventRequest.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@implementation WSCreateEventRequest

-(void)bookingEvent:(EventRequestObj*)eventRequest{
    requestObject = eventRequest;
    [self checkAuthenticate];
}
-(void)startAPIAfterAuthenticate{
    task = WS_BOOKING_ENTERTAINMENT;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(requestObject.BookingId){
        url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    } else {
        url = [B2C_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }
    [self POST:url withParams:[self buildRequestParams]];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(EVENT_REQUEST_TYPE, EVENT_BOOK_FUNCTION);
    
    if(requestObject){
        if(requestObject.BookingId){
            [dictKeyValues setObject:requestObject.BookingId  forKey:@"TransactionID"];
            //[dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];
        }
        
        if(requestObject.EmailAddress.length > 0){
            [dictKeyValues setObject:requestObject.EmailAddress  forKey:@"EmailAddress1"];
            [dictKeyValues setObject:requestObject.EmailAddress  forKey:@"Email1"];
        }
        
        if(requestObject.MobileNumber){
            [dictKeyValues setObject:requestObject.MobileNumber  forKey:@"PhoneNumber"];
        }
        
        /*if(requestObject.isContactBoth){
            [dictKeyValues setObject:@"Both" forKey:@"PrefResponse"];
        } else */if(requestObject.isContactPhone){
            [dictKeyValues setObject:@"Phone" forKey:@"PrefResponse"];
        } else if(requestObject.isContactEmail){
            [dictKeyValues setObject:@"Email" forKey:@"PrefResponse"];
        }
        
        [dictKeyValues setObject:EVENT_BOOK_SITUATION forKey:@"Situation"];
        [dictKeyValues setObject:requestObject.EditType forKey:@"EditType"];
        
        [dictKeyValues setObject:requestObject.City  forKey:@"City"];
        [dictKeyValues setObject:getCountryCodeByName(requestObject.Country)  forKey:@"Country"];
        [dictKeyValues setObject:[requestObject.reservationDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.reservationTime]]  forKey:@"Delivery"];
        [dictKeyValues setObject:[requestObject.reservationDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.reservationTime]]  forKey:@"EventDate"];
        [dictKeyValues setObject:requestObject.EventName  forKey:@"EventName"];
        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.adultPax]  forKey:@"NumberofAdults"];
        
        dictKeyValues = removeSpecialCharactersfromDict(dictKeyValues);
        
        //Start Add RequestDetails
        NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
        
        
        [detailsValues setObject:[NSNumber numberWithInteger:requestObject.adultPax]  forKey:NoofTickets];
        [detailsValues setObject:requestObject.Country  forKey:COUNTRY_REQUIREMENT];
        [detailsValues setObject:requestObject.reservationName  forKey:RESERVATION_NAME_REQUIREMENT];
        [detailsValues setObject:[requestObject.reservationDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.reservationTime]] forKey:PreferedDate];
        [detailsValues setObject:requestObject.EventName forKey:Event_Name];
        [detailsValues setObject:requestObject.reservationDate forKey:EventDate];
        [detailsValues setObject:requestObject.reservationTime forKey:EventTime];
        
        
        if(requestObject.SpecialRequirements){
            requestObject.SpecialRequirements = [[requestObject.SpecialRequirements componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            [detailsValues setObject:requestObject.SpecialRequirements  forKey:SPECIAL_REQUIREMENT];
        }
        
        if(requestObject.State){
            [detailsValues setObject:requestObject.State  forKey:StateRequirement];
        }
        
        detailsValues = removeSpecialCharactersfromDict(detailsValues);
        //end
        
        [dictKeyValues setObject:convertRequestDetailsToString(detailsValues) forKey:@"RequestDetails"];
        
    }
    
    return dictKeyValues;
}

@end
