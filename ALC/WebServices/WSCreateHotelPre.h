//
//  WSCreateHotelPre.h
//  ALC
//
//  Created by Hai NguyenV on 9/16/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSCreateHotelPre : WSB2CBase

@property(nonatomic, strong)NSMutableArray* BedTypes;
@property(nonatomic, strong)NSMutableArray* RoomTypes;
@property(nonatomic, strong)NSString* Ratings;
@property(nonatomic, strong)NSString* loyaltyMem;
@property(nonatomic, strong)NSString* loyaltyNo;
@property(nonatomic, strong)NSString* SmokingRoom;
@property(nonatomic, strong)NSString* preferenceID;
@property(nonatomic, strong)NSString* anyRequest;

-(void)createHotelPre;

@end
