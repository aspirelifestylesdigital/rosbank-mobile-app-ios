//
//  WSCreateHotelPre.m
//  ALC
//
//  Created by Hai NguyenV on 9/16/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateHotelPre.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "ALCQC-Swift.h"

@implementation WSCreateHotelPre

-(void)createHotelPre{
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_CREATE_HOTELPRE;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(_preferenceID){
        url = [B2C_API_URL stringByAppendingString:UpdateMyPreference];
    } else {
        url = [B2C_API_URL stringByAppendingString:AddMyPreference];
    }
    [self POST:url withParams:[self buildRequestParams]];
    
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *memValues = [[NSMutableDictionary alloc]init];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        UserObject *user = (UserObject*)[appdele getLoggedInUser];
        [memValues setObject:@"Preferences" forKey:@"Functionality"];
        [memValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
        [memValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
        [memValues setObject:user.userId  forKey:@"OnlineMemberId"];
    }
    [dictKeyValues setObject:memValues forKey:@"Member"];
    
    NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
   // if(_Ratings>0)
        [detailsValues setObject:_Ratings  forKey:@"Preferredstarrating"];
  //  if(_RoomTypes)
        [detailsValues setObject:removeSpecialCharacters(convertArrayToString(_RoomTypes))  forKey:@"RoomPreference"];
  //  if(_BedTypes)
        [detailsValues setObject:removeSpecialCharacters(convertArrayToString(_BedTypes))  forKey:@"BedPreference"];
    [detailsValues setObject:removeSpecialCharacters(_SmokingRoom)  forKey:@"SmokingPreference"];
 //   if(_loyaltyMem)
        [detailsValues setObject:removeSpecialCharacters(_loyaltyMem)  forKey:@"NameofLoyaltyProgram"];
//    if(_loyaltyNo)
        [detailsValues setObject:removeSpecialCharacters(_loyaltyNo)  forKey:@"MembershipNo"];
    [detailsValues setObject:Preference_Hotel  forKey:@"Type"];
    [detailsValues setObject:removeSpecialCharacters(_anyRequest)  forKey:@"Value6"];
    [detailsValues setObject:@"false"  forKey:@"Delete"];
    if(_preferenceID){
        [detailsValues setObject:_preferenceID  forKey:@"MyPreferencesId"];
    }
    NSArray* arr= [[NSArray alloc] initWithObjects:detailsValues, nil];
    [dictKeyValues setObject:arr forKey:@"PreferenceDetails"];
    return dictKeyValues;
}

@end
