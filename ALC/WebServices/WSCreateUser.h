//
//  WSCreateUser.h
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"
#import "UserObject.h"
@interface WSCreateUser : WSBase <DataLoadDelegate>
@property (strong, nonatomic) UserObject* userDetails;
-(void)createUser:(NSMutableDictionary*) userDict;
@end
