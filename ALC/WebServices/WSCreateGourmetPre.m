//
//  WSCreateGourmetPre.m
//  ALC
//
//  Created by Hai NguyenV on 9/16/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateGourmetPre.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "ALCQC-Swift.h"

@implementation WSCreateGourmetPre

-(void)createGourmet{
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_CREATE_GOURMETPRE;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(_preferenceID){
        url = [B2C_API_URL stringByAppendingString:UpdateMyPreference];
    } else {
        url = [B2C_API_URL stringByAppendingString:AddMyPreference];
    }
    [self POST:url withParams:[self buildRequestParams]];
    
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *memValues = [[NSMutableDictionary alloc]init];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        UserObject *user = (UserObject*)[appdele getLoggedInUser];
        [memValues setObject:@"Preferences" forKey:@"Functionality"];
        [memValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
        [memValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
        [memValues setObject:user.userId  forKey:@"OnlineMemberId"];
    }
    [dictKeyValues setObject:memValues forKey:@"Member"];
    
    NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
    if(_otherCuisine)
        [detailsValues setObject:_otherCuisine  forKey:@"OtherCuisine"];
    if(_arrCuisine)
        [detailsValues setObject:convertArrayToString(_arrCuisine)  forKey:@"CuisinePreferences"];
    
    [detailsValues setObject:removeSpecialCharacters(_foodAllergies)  forKey:@"Value2"];
    
    [detailsValues setObject:Preference_Dining  forKey:@"Type"];
    [detailsValues setObject:@"false"  forKey:@"Delete"];
    if(_preferenceID){
        [detailsValues setObject:_preferenceID  forKey:@"MyPreferencesId"];
    }
    NSArray* arr= [[NSArray alloc] initWithObjects:detailsValues, nil];
    [dictKeyValues setObject:arr forKey:@"PreferenceDetails"];
    return dictKeyValues;
}

@end
