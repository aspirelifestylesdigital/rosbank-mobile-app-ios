//
//  WSB2CBase.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "WSB2CRefreshToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
@interface WSB2CBase(){
    WSB2CRefreshToken *wsRefreshToken;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetAccessToken *wsGetAccessToken;
    UserObject *loggedInUser;
}

@end

@implementation WSB2CBase

-(void)checkAuthenticate{
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        loggedInUser = [appdele getLoggedInUser];
    }
    if(appdele.ACCESS_TOKEN!=nil){
        if ([appdele.B2C_ExpiredAt compare:[NSDate date]] == NSOrderedAscending)
        {
            [self refreshTokenAPI:task];
        } else {
            [self startAPIAfterAuthenticate];
        }
    } else if([appdele isLoggedIn]){
        [self requestToken];
    } else {
        NSError *err = [[NSError alloc] init];
        [self processDataResultWithError:err];
    }
}

-(void)refreshTokenAPI:(enum WSTASK)aTask
{
    wsRefreshToken = [[WSB2CRefreshToken alloc] init];
    wsRefreshToken.delegate = self;
    [wsRefreshToken refreshAccessToken];
}

-(void)requestToken
{
    if(loggedInUser){
        wsRequestToken = [[WSB2CGetRequestToken alloc] init];
        wsRequestToken.delegate = self;
        [wsRequestToken getRequestToken];
    } else {
        [self userHaventLoggedIn];
    }
}

-(void)requestAccessToken:(NSString*) token{
    if(loggedInUser){
        wsGetAccessToken = [[WSB2CGetAccessToken alloc] init];
        wsGetAccessToken.delegate = self;
        [wsGetAccessToken requestAccessToken:token member:loggedInUser.userId];
    } else {
        [self userHaventLoggedIn];
     }
}

-(void) userHaventLoggedIn{
    NSError* err = [NSError errorWithDomain:@"Error" code:401 userInfo:nil];
    [self processDataResultWithError:err];

}


-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_REFRESH_ACCESS_TOKEN){
        // after refresh token, call next request
        [self startAPIAfterAuthenticate];
    }else if(ws.task == WS_GET_REQUEST_TOKEN){
        [self requestAccessToken:wsRequestToken.requestToken];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        [self startAPIAfterAuthenticate];
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    NSError* err = [NSError errorWithDomain:@"Error" code:errorCode userInfo:nil];
    if(result && [result.b2cErrorCode isEqualToString:B2C_INVALID_ACCESS_TOKEN]){
        // access token is invalid, have to request new token, then request access token again
        [self requestToken];
    } else {
        [self processDataResultWithError:err];
    }
}

-(void)cancelRequest{
    
    if(wsRequestToken){
        [wsRequestToken cancelRequest];
    }
    
    if(wsGetAccessToken){
        [wsGetAccessToken cancelRequest];
        
    }
    
    if(wsRefreshToken){
        [wsRefreshToken cancelRequest];
    }
    
    [super cancelRequest];
}

- (void)dealloc
{
    if(wsRequestToken){
        [wsRequestToken cancelRequest];
    }
    
    if(wsGetAccessToken){
        [wsGetAccessToken cancelRequest];
        
    }
    
    if(wsRefreshToken){
        [wsRefreshToken cancelRequest];
    }
    
    [super cancelRequest];
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = task;
    if(error!=nil){
        result.status = error.code;
    }
    [delegate loadDataFailFrom:result withErrorCode:error.code];
}


@end
