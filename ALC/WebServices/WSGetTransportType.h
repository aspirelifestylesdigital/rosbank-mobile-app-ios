//
//  WSGetTransportType.h
//  ALC
//
//  Created by Anh Tran on 10/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"

@interface WSGetTransportType : WSBase

@property (strong, nonatomic) NSMutableArray* transportList;
@property (strong, nonatomic) NSMutableArray* transportFullList;
-(void)getList;

@end
