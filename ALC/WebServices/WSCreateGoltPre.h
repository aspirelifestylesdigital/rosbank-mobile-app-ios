//
//  WSCreateGoltPre.h
//  ALC
//
//  Created by Hai NguyenV on 9/16/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSCreateGoltPre : WSB2CBase
@property(nonatomic, strong)NSString* courseName;
@property(nonatomic, strong)NSMutableArray* arrTeeTimes;
@property(nonatomic, strong)NSString* preferenceID;
@property(nonatomic, strong)NSString* handicap;

-(void)createGolf;

@end
