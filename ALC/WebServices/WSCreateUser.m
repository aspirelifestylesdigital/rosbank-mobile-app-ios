//
//  WSCreateUser.m
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.

//  '- Step1: 2.2.1 Registration (API Endpoint: Registration)
//     Input:
//        All user information,
//        a ConsumerKey is stored on app,
//        VerificationCode: is whatever, cause app don’t have verification module
//  - Step2: Endpoint: AuthoriseRequestToken
//  - Step3: Endpoint: GetAccessToken
//  - Step 4: Endpoint: GetUserDetails
//

#import "WSCreateUser.h"
#import "BaseResponseObject.h"
#import "StoreUserData.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetUserDetails.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "Constant.h"
#import "NSString+AESCrypt.h"
#import "ALCQC-Swift.h"


@interface WSCreateUser (){
    NSMutableDictionary* user;
    NSString *onlineMemberId;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
}

@end

@implementation WSCreateUser

-(void)createUser:(NSMutableDictionary*) userDict{
    user = userDict;
    [self startAPIAfterAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_CREATE_USER;
    subTask = WS_ST_NONE;
    
    
    NSString* url = [B2C_API_URL stringByAppendingString:PostRegistration];
    [self POST:url withParams:[self buildRequestParams]]; 
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)ntask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        result.task = ntask;
        if(result.b2cStatus){
            onlineMemberId = [jsonResult stringForKey:@"OnlineMemberID"];
            storeUserOnlineMemberId(onlineMemberId);
            // call Request token
            wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
            wsB2CGetRequestToken.delegate = self;
            [wsB2CGetRequestToken getRequestToken];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }

    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = task;
    result.status = error.code;
    [delegate loadDataFailFrom:result withErrorCode:error.code];
}


#pragma mark - DataLoadDelegate

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_REQUEST_TOKEN){
        wsB2CGetAccessToek = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToek.delegate = self;
        [wsB2CGetAccessToek requestAccessToken:wsB2CGetRequestToken.requestToken member:onlineMemberId];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        //TODO: get user details
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    } else if(ws.task == WS_B2C_GET_USER_DETAILS){
        if(wsGetUser.userDetails != nil){
            self.userDetails = wsGetUser.userDetails;
            [self.delegate loadDataDoneFrom:self];
        } else {
            BaseResponseObject *result = [[BaseResponseObject alloc] init];
            result.task = task;
            result.status = 400;
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    result.task = task;
    [self.delegate loadDataFailFrom:result withErrorCode:result.status];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
//    NSString* userPass = [user objectForKey:USER_SECRET];
    
    NSMutableDictionary* memberDic = [[NSMutableDictionary alloc] init];
    [memberDic setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
    [memberDic setObject:[user objectForKey:@"EmailAddress"] forKey:@"Email"];
    [memberDic setObject:[user objectForKey:@"FirstName"] forKey:@"FirstName"];
    [memberDic setObject:[user objectForKey:@"LastName"] forKey:@"LastName"];
    [memberDic setObject:@"Registration" forKey:@"Functionality"];
    [memberDic setObject:B2C_DeviceId forKey:@"MemberDeviceId"];
    [memberDic setObject:[user objectForKey:@"MobileNumber"] forKey:@"MobileNumber"];

    // For Veracode
    NSString *keySecret = [keyCurrentSecret AES256DecryptWithKey:EncryptKey];
    [memberDic setObject:[user objectForKey:keySecret] forKey:keySecret];
    
    [memberDic setObject:convertSalutationToEnglish([user objectForKey:@"Salutation"]) forKey:@"Salutation"];
    //[memberDic setObject:[user objectForKey:@"PostalCode"] forKey:@"PostalCode"];
    
    
    [dictKeyValues setObject:memberDic forKey:@"Member"];
    
    NSMutableDictionary *verificationDict = [[NSMutableDictionary alloc] init];
    [verificationDict setObject:[AppConfig shared].verificationCode forKey:@"VerificationCode"];
    NSMutableArray *memberDetails = [[NSMutableArray alloc] init];
    [memberDetails addObject:verificationDict];
    
    [dictKeyValues setObject:memberDetails forKey:@"MemberDetails"];
    return dictKeyValues;
}

@end
