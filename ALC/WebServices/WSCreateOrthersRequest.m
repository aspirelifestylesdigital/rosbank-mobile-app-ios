//
//  WSCreateOrthersRequest.m
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateOrthersRequest.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@implementation WSCreateOrthersRequest

-(void)bookingOrthers:(OrthersRequestObj*)ortherRequest{
    requestObject = ortherRequest;
    [self checkAuthenticate];
}
-(void)startAPIAfterAuthenticate{
    task = WS_BOOKING_ORTHERS;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(requestObject.BookingId){
        url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    } else {
        url = [B2C_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }
    [self POST:url withParams:[self buildRequestParams]];
    
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(OTHER_REQUEST_TYPE, OTHER_RESERVATION);
    if(requestObject){
        if(requestObject.BookingId){
            [dictKeyValues setObject:requestObject.BookingId  forKey:@"TransactionID"];
            //[dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];
        }
        
        if(requestObject.EmailAddress.length > 0){
            [dictKeyValues setObject:requestObject.EmailAddress  forKey:@"EmailAddress1"];
        }
        
        if(requestObject.MobileNumber){
            [dictKeyValues setObject:requestObject.MobileNumber  forKey:@"PhoneNumber"];
        }
        
        /*if(requestObject.isContactBoth){
            [dictKeyValues setObject:@"Both" forKey:@"PrefResponse"];
        } else */if(requestObject.isContactPhone){
            [dictKeyValues setObject:@"Phone" forKey:@"PrefResponse"];
        } else if(requestObject.isContactEmail){
            [dictKeyValues setObject:@"Email" forKey:@"PrefResponse"];
        }
        [dictKeyValues setObject:requestObject.EditType forKey:@"EditType"];
        
        //Start add Request Details
        NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
        [detailsValues setObject:requestObject.reservationName  forKey:RESERVATION_NAME_REQUIREMENT];
                
        if(requestObject.ortherString){
            requestObject.ortherString = [[requestObject.ortherString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            [detailsValues setObject:requestObject.ortherString  forKey:SPECIAL_REQUIREMENT];
        }
        
        if(requestObject.spaName){
            [detailsValues setObject:requestObject.spaName  forKey:SpaName];
        }        
        
        if(requestObject.privilegeId){
            [detailsValues setObject:requestObject.privilegeId  forKey:PrivilegeId];
        }
        
        if(requestObject.fullAddress){
            [detailsValues setObject:requestObject.fullAddress  forKey:FullAddress];
        }
        
        if(requestObject.City){
            [detailsValues setObject:requestObject.City  forKey:CITY_GOLF_REQUIREMENT];
        }if(requestObject.Country){
            [detailsValues setObject:requestObject.Country  forKey:COUNTRY_REQUIREMENT];
        }
        if(requestObject.State){
            [detailsValues setObject:requestObject.State  forKey:StateRequirement];
        }
        
        detailsValues = removeSpecialCharactersfromDict(detailsValues);
        //end
        
        [dictKeyValues setObject:convertRequestDetailsToString(detailsValues) forKey:@"RequestDetails"];
        
    }
    return dictKeyValues;
}

@end
