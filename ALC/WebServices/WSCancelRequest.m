//
//  WSCancelRequest.m
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCancelRequest.h"
#import "HGWPostRequest.h"
#import "BaseResponseObject.h"

@interface WSCancelRequest (){
    NSString* requestId;
}

@end

@implementation WSCancelRequest

-(void) cancelRequestBooking:(NSString*) bookingId{
    requestId = bookingId;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_CANCEL_REQUEST;
    subTask = WS_ST_NONE;
    
    NSString* url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task1 forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}



-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(CAR_RENTAL_REQUEST_TYPE, CAR_RENTAL_FUNCTION);
    [dictKeyValues setObject:requestId  forKey:@"TransactionID"];
    //[dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];    
    
    return dictKeyValues;
}
@end
