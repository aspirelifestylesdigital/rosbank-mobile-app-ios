//
//  WSGetHotelRecommendDetails.m
//  ALC
//
//  Created by Anh Tran on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGetHotelRecommendDetails.h"
#import "BaseResponseObject.h"
#import "ALCQC-Swift.h"

@interface  WSGetHotelRecommendDetails(){
    NSString* bookingID;
}

@end

@implementation WSGetHotelRecommendDetails

-(void) getRequestDetails:(NSString*) bookingId{
    bookingID = bookingId;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_HOTEL_RECOMMEND_REQUEST;
    subTask = WS_ST_NONE;
    
    NSString* url = [B2C_API_URL stringByAppendingString:GetConciergeRequestDetailsUrl];
    [self POST:url withParams:[self buildRequestParams]];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] init];
        response.task = self.task;
        if(self.delegate != nil && jsonResult)
        {
            if(jsonResult){
                _bookingDetails = [[HotelRequestDetailsObject alloc] initFromDict:jsonResult];
            }
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [NSMutableDictionary dictionary];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        UserObject *user = (UserObject*)[appdele getLoggedInUser];
        [dictKeyValues setObject:@"GetRecentRequests" forKey:@"Functionality"];
        [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
        [dictKeyValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
        [dictKeyValues setObject:user.userId  forKey:@"OnlineMemberId"];
    }
    [dictKeyValues setObject:@"" forKey:@"EPCCaseID"];
    [dictKeyValues setObject:bookingID forKey:@"TransactionID"];
    return dictKeyValues;
}
@end
