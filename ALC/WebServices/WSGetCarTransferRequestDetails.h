//
//  WSGetCarTransferRequestDetails.h
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "CarTransferDetailsObject.h"
@interface WSGetCarTransferRequestDetails : WSB2CBase
@property (strong, nonatomic) CarTransferDetailsObject* bookingDetails;
-(void) getRequestDetails:(NSString*) bookingId;
@end
