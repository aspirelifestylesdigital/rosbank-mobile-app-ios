//
//  WSCreateEventRequest.h
//  ALC
//
//  Created by Hai NguyenV on 11/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "EventRequestObj.h"

@interface WSCreateEventRequest : WSB2CBase
{
    EventRequestObj* requestObject;
}
-(void)bookingEvent:(EventRequestObj*)eventRequest;

@end
