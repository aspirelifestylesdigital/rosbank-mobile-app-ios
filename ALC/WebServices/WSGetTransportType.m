//
//  WSGetTransportType.m
//  ALC
//
//  Created by Anh Tran on 10/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGetTransportType.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"

#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@implementation WSGetTransportType
-(void)getList{
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_TRANSPORT_TYPE;
    subTask = WS_ST_NONE;
    
    HGWGetBaseRequest *request = [[HGWGetBaseRequest alloc]init];
    request.requestUrl = [API_URL stringByAppendingString:GetTransportType];
    
    [service invokeMethod:kHTTPGET andObject:request forTask:task forSubTask:subTask];
    
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            NSArray* data = [jsonResult arrayForKey:@"Data"];
            self.transportList = [NSMutableArray array];
            NSMutableDictionary *item;
            for (int i = 0; i < data.count; i++) {
                item = [data objectAtIndex:i];
                [self.transportList addObject:[item stringForKey:@"Value"]];
            }
            self.transportFullList = [[NSMutableArray alloc] initWithArray:data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    return dictKeyValues;
}
@end
