//
//  WSCreateOrthersRequest.h
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "OrthersRequestObj.h"

@interface WSCreateOrthersRequest : WSB2CBase
{
    OrthersRequestObj* requestObject;
}
-(void)bookingOrthers:(OrthersRequestObj*)ortherRequest;

@end
