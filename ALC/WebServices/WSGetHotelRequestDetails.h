//
//  WSGetHotelRequestDetails.h
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "HotelRequestDetailsObject.h"

@interface WSGetHotelRequestDetails : WSB2CBase
@property (strong, nonatomic) HotelRequestDetailsObject* bookingDetails;

-(void) getRequestDetails:(NSString*) bookingId;

@end
