//
//  WSGetGourmetDetails.h
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"
#import "GourmetObject.h"
@interface WSGetGourmetDetails : WSBase
@property (strong, nonatomic) NSString* restaurantId;
@property (strong, nonatomic) NSString* userId;
@property (strong, nonatomic) GourmetObject* restaurantDetails;
-(void)getGourmetDetails:(NSString*) restId;
@end
