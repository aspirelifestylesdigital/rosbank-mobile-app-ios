//
//  WSForgotPassword.m
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSForgotPassword.h"
#import "HGWPostRequest.h"
#import "BaseResponseObject.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "ALCQC-Swift.h"

@interface WSForgotPassword(){
    NSString *userEmail;
}

@end

@implementation WSForgotPassword

-(void)forgotPassword:(NSString*) email{
    userEmail = email;
    [self startAPIAfterAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_FORGOT_PASSWORD;
    subTask = WS_ST_NONE;
    
    NSString* url = [B2C_API_URL stringByAppendingString:PostForgotPasswordUrl];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        if(result.b2cStatus){
            [self.delegate loadDataDoneFrom:self];
        } else {
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = task;
    result.status = error.code;
    [delegate loadDataFailFrom:result withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:@"ForgotPassword" forKey:@"Functionality"];
    [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
    [dictKeyValues setObject:userEmail forKey:@"Email2"];
    return dictKeyValues;
}
@end
