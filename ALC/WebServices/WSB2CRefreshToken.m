//
//  WSB2CRefreshToken.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CRefreshToken.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "ALCQC-Swift.h"

@implementation WSB2CRefreshToken
-(void) refreshAccessToken{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_REFRESH_ACCESS_TOKEN;
    
    NSString* url = [B2C_API_URL stringByAppendingString:RefreshAccessToken];
    [self GET:url withParams:[self buildRequestParams]];
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)ntask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        result.task = ntask;
        if(result.b2cStatus){
            //update expired time
            AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
            appdele.B2C_ExpiredAt = [[NSDate date] dateByAddingTimeInterval:3599];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            
            
            [self.delegate loadDataFailFrom:result withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = task;
    result.status = error.code;
    [delegate loadDataFailFrom:result withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [dictKeyValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
    [dictKeyValues setObject:appdele.REFRESH_TOKEN forKey:@"RefreshToken"];
    [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
    return dictKeyValues;
}

@end
