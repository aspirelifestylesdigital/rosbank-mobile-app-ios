//
//  WSChangePassword.m
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSChangePassword.h"
#import "BaseResponseObject.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "ALCQC-Swift.h"

@interface WSChangePassword(){
    NSString* oldPass;
    NSString* newPass;
}

@end

@implementation WSChangePassword

-(void) changePassword:(NSString*) old toPassword:(NSString*)newP{
    oldPass = old;
    newPass = newP;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_CHANGE_PASSWORD;
    subTask = WS_ST_NONE;
        
    NSString* url = [B2C_API_URL stringByAppendingString:PostChangePasswordUrl];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        result.task = self.task;
        if([result isSuccess]){
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:oldPass forKey:@"OldPassword"];
    [dictKeyValues setObject:newPass forKey:@"NewPassword"];
    [dictKeyValues setObject:@"ChangePassword" forKey:@"Functionality"];
    [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        [dictKeyValues setObject:((UserObject*)[appdele getLoggedInUser]).email  forKey:@"Email2"];
    }
    
    return dictKeyValues;
}
@end
