//
//  WSCreateCarPre.h
//  ALC
//
//  Created by Hai NguyenV on 9/16/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSCreateCarPre : WSB2CBase

@property(nonatomic, strong)NSString* arrCompany;
@property(nonatomic, strong)NSMutableArray* arrVehicle;
//@property(nonatomic, strong)NSString* otherCompany;
@property(nonatomic, strong)NSString* preferenceID;
@property(nonatomic, strong)NSString* loyaltyMem;
@property(nonatomic, strong)NSString* loyaltyNo;

-(void)createCarPre;

@end
