//
//  WSB2CLogin.h
//  ALC
//
//  Created by Hai NguyenV on 11/4/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "StoreUserData.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetUserDetails.h"

@interface WSB2CLogin : WSBase <DataLoadDelegate>
{
    NSString* userEmail;
    NSString* userPassword;
    NSString *onlineMemberId;
    BOOL hasForgotPassword;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
}
-(void)onLogin:(NSString*)email andPass:(NSString*)password;

@end
