//
//  WSB2CLogin.m
//  ALC
//
//  Created by Hai NguyenV on 11/4/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CLogin.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "StoreUserData.h"
#import "NSString+AESCrypt.h"
#import "ALCQC-Swift.h"

@implementation WSB2CLogin
-(void)onLogin:(NSString*)email andPass:(NSString*)password{
    userEmail = email;
    userPassword =  password;
    [self startAPIAfterAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    self.task = WS_AUTHENTICATION_LOGIN;
    self.subTask = WS_ST_NONE;
    NSString* url = [B2C_API_URL stringByAppendingString:PostUserLogin];
    [self POST:url withParams:[self buildRequestParams]];    
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)ptask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        result.task = ptask;
        if(result.b2cStatus){
            onlineMemberId = [jsonResult stringForKey:@"OnlineMemberID"];
            hasForgotPassword  = [jsonResult boolForKey:@"HasForgotPassword"];
            setHasForgotPassword(hasForgotPassword);
            storeUserOnlineMemberId(onlineMemberId);;
            // call Request token
            wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
            wsB2CGetRequestToken.delegate = self;
            [wsB2CGetRequestToken getRequestToken];
        } else if(self.delegate != nil){
            BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
            result.task = ptask;
            [self.delegate loadDataFailFrom:result withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = task;
    result.status = error.code;
    [delegate loadDataFailFrom:result withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:@"Login" forKey:@"Functionality"];
    [dictKeyValues setObject:B2C_DeviceId forKey:@"MemberDeviceId"];
    [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
    
    // For Veracode 
    NSString *keySecret = [keyCurrentSecret AES256DecryptWithKey:EncryptKey];
    [dictKeyValues setObject:userPassword forKey:keySecret];
    
    [dictKeyValues setObject:userEmail forKey:@"Email2"];
    return dictKeyValues;
}
#pragma mark - DataLoadDelegate

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_REQUEST_TOKEN){
        wsB2CGetAccessToek = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToek.delegate = self;
        [wsB2CGetAccessToek requestAccessToken:wsB2CGetRequestToken.requestToken member:onlineMemberId];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        //TODO: get user details
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    } else if(ws.task == WS_B2C_GET_USER_DETAILS){
        if(wsGetUser.userDetails != nil){
          //  self.userDetails = wsGetUser.userDetails;
            [self.delegate loadDataDoneFrom:self];
        } else {
            BaseResponseObject *result = [[BaseResponseObject alloc] init];
            result.task = task;
            result.status = 400;
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    result.task = task;
    [self.delegate loadDataFailFrom:result withErrorCode:result.status];
}

@end
