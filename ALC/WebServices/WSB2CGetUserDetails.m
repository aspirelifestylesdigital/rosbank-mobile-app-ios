//
//  WSB2CGetUserDetails.m
//  ALC
//
//  Created by Anh Tran on 11/3/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetUserDetails.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "StoreUserData.h"
#import "ALCQC-Swift.h"

@implementation WSB2CGetUserDetails
-(void) getUserDetails{
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_B2C_GET_USER_DETAILS;
    subTask = WS_ST_NONE;
    
    NSString* url = [B2C_API_URL stringByAppendingString:GetUserDetailsUrl];
    [self POST:url withParams:[self buildRequestParams]]; 

}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)ptask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        NSDictionary *member = [jsonResult dictionaryForKey:@"Member"];
        if(member){
            self.userDetails = [[UserObject alloc] initFromDict:member];
            NSArray* array=[jsonResult arrayForKey:@"MemberDetails"];
            if(array.count>0){
                NSDictionary *memberDetails = [array objectAtIndex:0];
                self.userDetails.ONLINEMEMBERDETAILID = [memberDetails stringForKey:@"ONLINEMEMBERDETAILID"];
                self.userDetails.VERIFICATIONCODE = [memberDetails stringForKey:@"VERIFICATIONCODE"];
            }
            storeUserDetails(self.userDetails);
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
            result.task = ptask;
            [self.delegate loadDataFailFrom:result withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}



-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [dictKeyValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
    [dictKeyValues setObject:@"GetUserDetails" forKey:@"Functionality"];
    [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
    [dictKeyValues setObject:appdele.ONLINE_MEMBER_ID forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:B2C_MemberRefNo forKey:@"MemberRefNo"];
    return dictKeyValues;
}

@end
