//
//  WSCreateCarTransferRequest.m
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateCarTransferRequest.h"

#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@interface WSCreateCarTransferRequest (){
    CarCreateRequestObject* requestObject;
}

@end

@implementation WSCreateCarTransferRequest

-(void)bookingTransfer:(CarCreateRequestObject*) item{
    requestObject = item;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_BOOKING_CAR_TRANSFER;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(requestObject.bookingId){
        url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    } else {
        url = [B2C_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }
    [self POST:url withParams:[self buildRequestParams]];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(TRANSFER_LIMO_REQUEST_TYPE, CAR_TRANSFER_FUNCTION);
    
    if(requestObject){
        
        if(requestObject.bookingId){
            [dictKeyValues setObject:requestObject.bookingId  forKey:@"TransactionID"];
           // [dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];
        }
        
        if(requestObject.email.length > 0){
            [dictKeyValues setObject:requestObject.email  forKey:@"EmailAddress1"];
            [dictKeyValues setObject:requestObject.email  forKey:@"Email1"];
        }
        
        if(requestObject.mobileNumber){
            [dictKeyValues setObject:requestObject.mobileNumber  forKey:@"PhoneNumber"];
        }
        
        /*if(requestObject.isContactBoth){
            [dictKeyValues setObject:@"Both" forKey:@"PrefResponse"];
        } else */if(requestObject.isContactPhone){
            [dictKeyValues setObject:@"Phone" forKey:@"PrefResponse"];
        } else if(requestObject.isContactEmail){
            [dictKeyValues setObject:@"Email" forKey:@"PrefResponse"];
        }
        
        [dictKeyValues setObject:requestObject.EditType forKey:@"EditType"];
        [dictKeyValues setObject:[NSNumber numberWithInteger:requestObject.nbOfPassengers]  forKey:@"NumberOfAdults"];
        
        
        [dictKeyValues setObject:[requestObject.pickUpDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.pickUpTime]]  forKey:@"PickUp"];
        [dictKeyValues setObject:[requestObject.pickUpDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.pickUpTime]]  forKey:@"PickupDate"];
        dictKeyValues = removeSpecialCharactersfromDict(dictKeyValues);
        
        //Start add request details
        NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
        
        [detailsValues setObject:requestObject.pickUpLocation  forKey:PickLocation];
        [detailsValues setObject:requestObject.dropOffLocation  forKey:DropLocation];
        [detailsValues setObject:requestObject.transportType    forKey:TransportType];
        [detailsValues setObject:requestObject.reservationName  forKey:RESERVATION_NAME_REQUIREMENT];
        [detailsValues setObject:[NSNumber numberWithInteger:requestObject.nbOfPassengers]  forKey:NumberPassengers];
    
        if(requestObject.specialMessage){
            requestObject.specialMessage = [[requestObject.specialMessage componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            [detailsValues setObject:requestObject.specialMessage  forKey:SPECIAL_REQUIREMENT];
        }
        detailsValues = removeSpecialCharactersfromDict(detailsValues);
        // end
        
        [dictKeyValues setObject:convertRequestDetailsToString(detailsValues) forKey:@"RequestDetails"];
    }
    
    return dictKeyValues;
}

-(NSString*) requestTypeValue:(NSString*) type{
    if([type isEqualToString:@"Bus"]){
        return TRANSFER_BUS_REQUEST_TYPE;
    } else if([type isEqualToString:@"Train"]){
        return TRANSFER_TRAIN_REQUEST_TYPE;
    } else if([type isEqualToString:@"Taxi"]){
        return TRANSFER_TAXI_REQUEST_TYPE;
    } else if([type isEqualToString:@"Private car/Limo"]){
        return TRANSFER_LIMO_REQUEST_TYPE;
    } else {
        return TRANSFER_OTHER_REQUEST_TYPE;
    }
}

@end
