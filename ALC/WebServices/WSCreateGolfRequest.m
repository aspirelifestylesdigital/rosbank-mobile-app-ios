//
//  WSCreateGolfRequest.m
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSCreateGolfRequest.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "HGWPostRequest.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"

@implementation WSCreateGolfRequest

-(void)bookingGolf:(GolfRequestObj*)golfRequest{
    requestObject = golfRequest;
    [self checkAuthenticate];
}
-(void)startAPIAfterAuthenticate{
    task = WS_BOOKING_GOLF;
    subTask = WS_ST_NONE;
    
    NSString* url;
    if(requestObject.BookingId){
        url = [B2C_API_URL stringByAppendingString:CancelRequestUrl];
    } else {
        url = [B2C_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }
    [self POST:url withParams:[self buildRequestParams]];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = createCommonBookingObject(GOLF_REQUEST_TYPE, GOLF_RESERVATION);
    if(requestObject){
        if(requestObject.BookingId){
            [dictKeyValues setObject:requestObject.BookingId  forKey:@"TransactionID"];
           // [dictKeyValues setObject:[NSNull null]  forKey:@"AttachmentPath"];
        }
        
        if(requestObject.EmailAddress.length > 0){
            [dictKeyValues setObject:requestObject.EmailAddress  forKey:@"EmailAddress1"];
            [dictKeyValues setObject:requestObject.EmailAddress  forKey:@"Email1"];
        }
        
        if(requestObject.MobileNumber){
            [dictKeyValues setObject:requestObject.MobileNumber  forKey:@"PhoneNumber"];
        }
        
        /*if(requestObject.isContactBoth){
            [dictKeyValues setObject:@"Both" forKey:@"PrefResponse"];
        } else */if(requestObject.isContactPhone){
            [dictKeyValues setObject:@"Phone" forKey:@"PrefResponse"];
        } else if(requestObject.isContactEmail){
            [dictKeyValues setObject:@"Email" forKey:@"PrefResponse"];
        }
        
        [dictKeyValues setObject:requestObject.EditType forKey:@"EditType"];
        
        [dictKeyValues setObject:requestObject.noOfBaler  forKey:@"NumberofAdults"];
        
        [dictKeyValues setObject:requestObject.City  forKey:@"City"];
        [dictKeyValues setObject:getCountryCodeByName(requestObject.Country)  forKey:@"Country"];
        [dictKeyValues setObject:[requestObject.GolfDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.GolfTime]]  forKey:@"StartDate"];
        [dictKeyValues setObject:[requestObject.GolfDate stringByAppendingString:[NSString stringWithFormat:@" %@",requestObject.GolfTime]]  forKey:@"EventDate"];
        dictKeyValues = removeSpecialCharactersfromDict(dictKeyValues);
       
        
        //Start add Request details
        NSMutableDictionary *detailsValues = [[NSMutableDictionary alloc]init];
        if(requestObject.SpecialRequirements){
            requestObject.SpecialRequirements = [[requestObject.SpecialRequirements componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
            [detailsValues setObject:requestObject.SpecialRequirements  forKey:SPECIAL_REQUIREMENT];
        }
        [detailsValues setObject:removeSpecialCharacters(requestObject.GolfCourse)  forKey:GolfCourseName];
        [detailsValues setObject:requestObject.noOfBaler  forKey:NoofBallers];
        [detailsValues setObject:requestObject.hole  forKey:Holes];
        [detailsValues setObject:requestObject.City  forKey:CITY_GOLF_REQUIREMENT];
        [detailsValues setObject:requestObject.Country  forKey:COUNTRY_REQUIREMENT];
        [detailsValues setObject:requestObject.reservationName  forKey:GUEST_NAME_REQUIREMENT];
        [detailsValues setObject:requestObject.GolfDate forKey:GolfDateOfPlay];
        [detailsValues setObject:requestObject.GolfTime forKey:TeeTime];
        
        if(requestObject.Handicap)
            [detailsValues setObject:requestObject.Handicap  forKey:PreferredGolfHandicap];
        
        if(requestObject.State){
            [detailsValues setObject:requestObject.State  forKey:StateRequirement];
        }

        detailsValues = removeSpecialCharactersfromDict(detailsValues);
        //end
        
        [dictKeyValues setObject:convertRequestDetailsToString(detailsValues) forKey:@"RequestDetails"];       

        
    }
    return dictKeyValues;
}

@end
