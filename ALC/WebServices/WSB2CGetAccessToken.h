//
//  WSB2CGetAccessToken.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"

@interface WSB2CGetAccessToken : WSBase
-(void) requestAccessToken:(NSString*) token member:(NSString*) memberId;
@end
