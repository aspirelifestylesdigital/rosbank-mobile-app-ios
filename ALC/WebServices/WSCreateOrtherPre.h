//
//  WSCreateOrtherPre.h
//  ALC
//
//  Created by Hai NguyenV on 9/16/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSCreateOrtherPre : WSB2CBase
@property(nonatomic, strong)NSString* others;
@property(nonatomic, strong)NSString* preferenceID;
@property(nonatomic, strong)NSMutableArray* arrLoyalty;

-(void)createOthersPre;
@end
