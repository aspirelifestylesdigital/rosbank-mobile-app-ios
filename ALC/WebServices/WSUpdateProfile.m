//
//  WSUpdateProfile.m
//  ALC
//
//  Created by Hai NguyenV on 9/20/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSUpdateProfile.h"
#import "HGWPostRequest.h"
#import "BaseResponseObject.h"
#import "StoreUserData.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "ALCQC-Swift.h"

@implementation WSUpdateProfile

-(void)updateUser:(NSMutableDictionary*) userDict{
    user = userDict;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_UPDATE_USER;
    subTask = WS_ST_NONE;
    
    NSString* url = [B2C_API_URL stringByAppendingString:UpdateRegistration];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)atask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromB2CDict:jsonResult];
        result.task = atask;
        if(result.b2cStatus){
            id value = [user objectForKey:@"Mobile"];
            [user setValue:value forKey:@"MobileNumber"];
            [user removeObjectForKey:@"Mobile"];
            updateUserDetails(user);
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
        
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [user setObject:@"Registration" forKey:@"Functionality"];
    [user setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
    [user setObject:appdele.ONLINE_MEMBER_ID forKey:@"OnlineMemberID"];
    [user setObject:B2C_DeviceId forKey:@"MemberDeviceId"];
    [dictKeyValues setObject:user forKey:@"Member"];
    
    NSMutableDictionary* dict= getUserDetailsObj();
    NSMutableArray* array= [[NSMutableArray alloc]  init];
    [array addObject:dict];
    
    [dictKeyValues setObject:array forKey:@"MemberDetails"];
    return dictKeyValues;
}
@end
