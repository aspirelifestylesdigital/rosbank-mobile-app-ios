//
//  WSCreateCarTransferRequest.h
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "CarCreateRequestObject.h"
@interface WSCreateCarTransferRequest : WSB2CBase
-(void)bookingTransfer:(CarCreateRequestObject*) item;
@end
