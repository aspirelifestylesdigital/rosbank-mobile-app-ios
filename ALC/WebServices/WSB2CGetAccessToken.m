//
//  WSB2CGetAccessToken.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetAccessToken.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "BaseResponseObject.h"
#import "ALCQC-Swift.h"

@interface WSB2CGetAccessToken (){
    NSString* onlineMemberId;
    NSString* requestToken;
}

@end

@implementation WSB2CGetAccessToken
-(void) requestAccessToken:(NSString*) token member:(NSString*) memberId{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_ACCESS_TOKEN;
    
    onlineMemberId = memberId;
    requestToken = token;
    if(requestToken && onlineMemberId){
        NSString* url = [B2C_API_URL stringByAppendingString:GetAccessToken];
        [self GET:url withParams:[self buildRequestParams]];
    }
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        NSString *accesstoken = [jsonResult stringForKey:@"AccessToken"];
        NSString *refeshToken = [jsonResult stringForKey:@"RefreshToken"];
        if(accesstoken!=nil && accesstoken.length > 0){
            //TODO: save to app delegate
            AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
            [appdele saveAccessToken:accesstoken refreshToken:refeshToken expired:[jsonResult stringForKey:@"ExpirationTime"]];

            if(self.delegate != nil)
                [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = task;
    result.status = error.code;
    [delegate loadDataFailFrom:result withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"consumerKey"];
    [dictKeyValues setObject:[AppConfig shared].callBackURL forKey:@"callbackUrl"];
    [dictKeyValues setObject:onlineMemberId forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:requestToken forKey:@"RequestToken"];
    return dictKeyValues;
}

@end
