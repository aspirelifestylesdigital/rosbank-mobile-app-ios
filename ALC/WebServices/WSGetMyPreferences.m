//
//  WSGetMyPreferences.m
//  ALC
//
//  Created by Hai NguyenV on 9/15/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGetMyPreferences.h"
#import "HGWGetBaseRequest.h"
#import "HGWPostBaseRequest.h"
#import "BaseResponseObject.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "ALCQC-Swift.h"

@implementation WSGetMyPreferences

-(void)getMyPreference{
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_MY_PREFERENCE;
    subTask = WS_ST_NONE;
    
    NSString* url = [B2C_API_URL stringByAppendingString:GetMyPreference];
    [self POST:url withParams:[self buildRequestParams]];
    
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] init];
        response.task = self.task;
        
        if(self.delegate && [jsonResult isKindOfClass:[NSArray class]])
        {
            NSArray* rests =  (NSArray*)jsonResult;

            self.pData = [[NSMutableDictionary alloc] init];
            
            NSInteger count = rests.count;
            for (int i = 0; i < count; i++) {
                NSDictionary* dict= [rests objectAtIndex:i];
                NSString* type = [dict objectForKey:MY_PRE_TYPE];
                if([type isEqualToString:Preference_Dining]){
                    [self.pData setValue:dict forKey:Preference_Dining];
                }else if([type isEqualToString:Preference_Hotel]){
                    [self.pData setValue:dict forKey:Preference_Hotel];
                }else if([type isEqualToString:Preference_Golf]){
                    [self.pData setValue:dict forKey:Preference_Golf];
                }else if([type isEqualToString:Preference_Other]){
                    [self.pData setValue:dict forKey:Preference_Other];
                }else if([type isEqualToString:Preference_Car_Rental]){
                    [self.pData setValue:dict forKey:Preference_Car_Rental];
                }
            }
            
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        UserObject *user = (UserObject*)[appdele getLoggedInUser];
        [dictKeyValues setObject:@"GetPreference" forKey:@"Functionality"];
        [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
        [dictKeyValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
        [dictKeyValues setObject:user.userId  forKey:@"OnlineMemberId"];
    }
    return dictKeyValues;
}

@end
