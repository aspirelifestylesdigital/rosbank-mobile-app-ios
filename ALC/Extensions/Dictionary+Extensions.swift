//
//  Dictionary+Extensions.swift
//  ALC
//
//  Created by mac-216 on 6/25/18.
//  Copyright © 2018 Sunrise Software Solutions. All rights reserved.
//

import Foundation

extension Dictionary {
    
    static func contentsOf(path: URL) -> Dictionary<String, Any> {
        let data = try! Data(contentsOf: path)
        let plist = try! PropertyListSerialization.propertyList(from: data, options: .mutableContainers, format: nil)
        return plist as! [String: Any]
    }
    
}
