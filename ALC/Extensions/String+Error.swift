//
//  String+Error.swift
//  Alley
//
//  Created by mac-216 on 4/5/18.
//  Copyright © 2018 itechart. All rights reserved.
//

import Foundation

extension String: Error {}

extension Error
{
    var message: String
    {
        var message: String?
        switch self {
        case let error as NSError: // should be last one because Error is always NSError
            message = error.userInfo["message"] as? String
        case let text as String:
            message = text
        default:
            message = nil
        }
        return message ?? localizedDescription
    }
}
