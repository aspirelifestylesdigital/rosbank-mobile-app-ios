//
//  CarRentalTransferView.m
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CarRentalTransferView.h"
#import "CustomTextField.h"
#import "BaseResponseObject.h"
#import "WSGetTransportType.h"

#define TRANSPORT_HINT_TEXT NSLocalizedString(@"Please select type of transport", nil)

@interface  CarRentalTransferView(){
    enum REQUEST_TYPE requestType;
    AppDelegate* appDelegate;
    UIDatePicker *datepickerPickUp;
    UIDatePicker *datepickerDropOff;
    UIDatePicker *timepickerPickUp;
    UIDatePicker *timepickerDropOff;
    NSString* selectedTransportKey;
    NSMutableDictionary* dictIconErrorManager;
    NSInteger normalPax;
}
@property (strong, nonatomic) IBOutlet CustomTextField *txtDriverName;
@property (strong, nonatomic) IBOutlet UIButton *btTransportType;
@property (strong, nonatomic) IBOutlet CustomTextField *txtAge;
@property (strong, nonatomic) IBOutlet UISwitch *swLiciense;
@property (strong, nonatomic) IBOutlet CustomTextField *txtRentalPickUpDate;
@property (strong, nonatomic) IBOutlet CustomTextField *txtRentalPickUpTime;
@property (strong, nonatomic) IBOutlet CustomTextField *txtPickUpDate;
@property (strong, nonatomic) IBOutlet CustomTextField *txtPickUpTime;
@property (strong, nonatomic) IBOutlet CustomTextField *txtPickUpLocation;
@property (strong, nonatomic) IBOutlet CustomTextField *txtDropOffDate;
@property (strong, nonatomic) IBOutlet CustomTextField *txtDropOffTime;
@property (strong, nonatomic) IBOutlet CustomTextField *txtDropOffLocation;
@property (strong, nonatomic) IBOutlet CustomTextField *txtNumberPassengers;
@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;

@property (strong, nonatomic) IBOutlet ErrorToolTip *icDriverNameError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icTransportError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icAgeError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icRentalPickupTimeError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icPickupTimeError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icPickUpLocationError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icDropOffTimeError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icDropOffLocationError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icPassengersError;
//@property (strong, nonatomic) IBOutlet ErrorToolTip *icNbPassengerError;

@property (strong, nonatomic) IBOutlet UILabel *lbTitleDropOffLocation;
@property (strong, nonatomic) IBOutlet UILabel *lbTitlePickUpDate;
@property (strong, nonatomic) IBOutlet UILabel *lbTitlePickupTime;
@property (strong, nonatomic) IBOutlet UILabel *lbTitlePickUpLocation;

@property (weak, nonatomic) IBOutlet UILabel *lbDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lbAge;
@property (weak, nonatomic) IBOutlet UILabel *lbSwitchLiciense;
@property (weak, nonatomic) IBOutlet UILabel *lbDropOffDate;
@property (weak, nonatomic) IBOutlet UILabel *lbDropOffTime;
@property (weak, nonatomic) IBOutlet UILabel *lbPickUpDate;
@property (weak, nonatomic) IBOutlet UILabel *lbPickupTime;
@property (weak, nonatomic) IBOutlet UILabel *lbNumberPassengers;
@property (weak, nonatomic) IBOutlet UILabel *lbTransportType;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;


@end

@implementation CarRentalTransferView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"CarRentalTransferView" owner:self options:nil]
                 objectAtIndex:0];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
        [self setUpView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        [self setUpView];
        return self;
    }
    
    return self;
}

-(void) setUpView{
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    
    [self setUpDateTimePicker];
    _txtDriverName.delegate = self;
    _txtNumberPassengers.delegate = self;
    _txtAge.delegate = self;
    _txtDropOffLocation.delegate = self;
    _txtPickUpLocation.delegate = self;
    
    //Display Optional Details View
    _optionalDetailsView.delegate = self;
    //end
    
    [self displayUserInfo];
    
    [self collectErrorIcons];
    
    [self setTextForView];
}

-(void) setUpDateTimePicker{
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    
    _txtPickUpDate.inputAccessoryView = toolbar;
    _txtPickUpTime.inputAccessoryView = toolbar;
    _txtRentalPickUpDate.inputAccessoryView = toolbar;
    _txtRentalPickUpTime.inputAccessoryView = toolbar;
    _txtDropOffDate.inputAccessoryView = toolbar;
    _txtDropOffTime.inputAccessoryView = toolbar;
    
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
    //Pick up date picker
    datepickerPickUp = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerPickUp setDatePickerMode:UIDatePickerModeDate];
    [datepickerPickUp setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [datepickerPickUp addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerPickUp.date = datepickerPickUp.date;
    
    
    //end
    
    //Pick up time picker
    timepickerPickUp = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [timepickerPickUp setDatePickerMode:UIDatePickerModeTime];
    [timepickerPickUp addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    timepickerPickUp.minuteInterval = 5;
    timepickerPickUp.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*10];
    
    [self setUpPickUpDateTime];
   
    //end
    
    //Drop off date picker
    datepickerDropOff = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerDropOff setDatePickerMode:UIDatePickerModeDate];
    [datepickerDropOff setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [datepickerDropOff addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerDropOff.tag = _txtDropOffDate.tag*10;
    _txtDropOffDate.inputView = datepickerDropOff;
    _txtDropOffDate.delegate = self;
    datepickerDropOff.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    _txtDropOffDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerDropOff.date, DATE_FORMAT)];
    //end
    
    //Drop off time picker
    timepickerDropOff = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [timepickerDropOff setDatePickerMode:UIDatePickerModeTime];
    [timepickerDropOff addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    timepickerDropOff.tag = _txtDropOffTime.tag*10;
    _txtDropOffTime.inputView = timepickerDropOff;
    _txtDropOffTime.delegate = self;
    timepickerDropOff.minuteInterval = 5;
    timepickerDropOff.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*10];
    _txtDropOffTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerDropOff.date, TIME_FORMAT)];
    //end
    
}
- (IBAction)actionCancel:(id)sender {
    [[self currentNavigationController] popViewControllerAnimated:YES];
}
-(void) setUpPickUpDateTime{
    if(requestType == CAR_TRANSFER_REQUEST){
        datepickerPickUp.tag = _txtPickUpDate.tag*10;
        timepickerPickUp.tag = _txtPickUpTime.tag*10;
        _txtPickUpDate.inputView = datepickerPickUp;
        _txtPickUpDate.delegate = self;
        _txtPickUpDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerPickUp.date, DATE_FORMAT)];
        _txtPickUpTime.inputView = timepickerPickUp;
        _txtPickUpTime.delegate = self;
        _txtPickUpTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerPickUp.date, TIME_FORMAT)];
    } else {
        datepickerPickUp.tag = _txtRentalPickUpDate.tag*10;
        timepickerPickUp.tag = _txtRentalPickUpTime.tag*10;
        _txtRentalPickUpDate.inputView = datepickerPickUp;
        _txtRentalPickUpDate.delegate = self;
        _txtRentalPickUpDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerPickUp.date, DATE_FORMAT)];
        _txtRentalPickUpTime.inputView = timepickerPickUp;
        _txtRentalPickUpTime.delegate = self;
        _txtRentalPickUpTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerPickUp.date, TIME_FORMAT)];
    }
}

-(void) displayUserInfo{
    if(!appDelegate){
        appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    
    UserObject *user = [appDelegate getLoggedInUser];
    if(user){
       // _txtDriverName.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    }
}

-(SlideNavigationController*) currentNavigationController{
    if(!appDelegate){
        appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    return appDelegate.rootVC;
}

-(void) applyRequestType:(enum REQUEST_TYPE) type{
    normalPax =1;
    requestType = type;
    [_optionalDetailsView applyRequestType:type];
    [self setUpViewBaseOnRequestType:type];
    
    [self setUpPickUpDateTime];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void) setUpViewBaseOnRequestType:(enum REQUEST_TYPE) type{
    switch (type) {
        case CAR_TRANSFER_REQUEST:
            _viewDriverName.hidden = YES;
            _viewAgeOfDriver.hidden = YES;
            _viewInternationalLiciense.hidden =YES;
            _viewDropOffDate.hidden = YES;
            _viewPickupDate.hidden = YES;
            _viewPickupTranfer.hidden = NO;
            _pickupLocationTop.constant = 20;
            // This is specific case for Russia
            [_lbTitleDropOffLocation setAttributedText:[self addRedStarToString:NSLocalizedString(@"Drop off location car_transfer",nil)]];
            [_lbPickUpDate setAttributedText:[self addRedStarToString:NSLocalizedString(@"Pick up date car_transfer",nil)]];
            [_lbPickupTime setAttributedText:[self addRedStarToString:NSLocalizedString(@"Pick up time car_transfer",nil)]];
            _icDropOffLocationError.errorMessage = NSLocalizedString(@"Please enter drop-off address car_transfer", nil);
            _icPickUpLocationError.errorMessage = NSLocalizedString(@"Please enter pickup address transfer car_transfer", nil);
            [_txtPickUpLocation setPlaceholder:NSLocalizedString(@"Please enter pickup address car_transfer", nil)];
            [_txtDropOffLocation setPlaceholder:NSLocalizedString(@"Please enter drop-off address car_transfer", nil)];
            break;
        case CAR_RENTAL_REQUEST:
            normalPax = 1;
            _viewTransportType.hidden = YES;
            _viewNumberPassengers.hidden =YES;
            _viewPickupTranfer.hidden = YES;
            _viewPickupDate.hidden = NO;
            
            [_lbTitleDropOffLocation setAttributedText:[self setRequireString:NSLocalizedString(@"Drop off location",nil)]];
            [_lbTitlePickUpDate setAttributedText:[self setRequireString:NSLocalizedString(@"Pick up date",nil)]];
            [_lbTitlePickupTime setAttributedText:[self setRequireString:NSLocalizedString(@"Pick up time",nil)]];
            [_txtPickUpLocation setPlaceholder:NSLocalizedString(@"Please enter pickup address", nil)];
            [_txtDropOffLocation setPlaceholder:NSLocalizedString(@"Please enter drop-off address", nil)];
            
            break;
        default:
            break;
    }
    
    
}


-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+216;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

- (IBAction)actionTransportType:(id)sender {
    _icTransportError.hidden = YES;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = TRANSPORT;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.isCloseAfterClick = YES;
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btTransportType.currentTitle, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedTransportKey, nil];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}


- (IBAction)actionSubmit:(id)sender {
    [self closeAllKeyboardAndPicker];
    CarCreateRequestObject* request = [self validateAndCollectData];
    if(request && _delegate){
        [_delegate submitRequest:request forType:requestType];
    }
}

-(void) closeAllKeyboardAndPicker{
    [_txtAge resignFirstResponder];
    [_txtDriverName resignFirstResponder];
    [_txtPickUpLocation resignFirstResponder];
    [_txtDropOffLocation resignFirstResponder];
    [_txtNumberPassengers resignFirstResponder];
    [_txtPickUpTime endEditing:YES];
    [_txtRentalPickUpTime endEditing:YES];
    [_txtDropOffTime endEditing:YES];
    [_txtPickUpDate endEditing:YES];
    [_txtRentalPickUpDate endEditing:YES];
    [_txtDropOffDate endEditing:YES];
    
    [self adjustDropOffDate];
}


-(void) dateTextField:(id)sender
{
    if(((UIView*)sender).tag == ((requestType==CAR_TRANSFER_REQUEST) ? _txtPickUpDate.tag : _txtRentalPickUpDate.tag)*10){
        [self setPickUpText];
        
    } else if(((UIView*)sender).tag == _txtDropOffDate.tag*10){
        _txtDropOffDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerDropOff.date, DATE_FORMAT)];
    } else if(((UIView*)sender).tag == ((requestType==CAR_TRANSFER_REQUEST) ? _txtPickUpTime.tag : _txtRentalPickUpTime.tag)*10){
        [self setPickUpText];
        
    } else if(((UIView*)sender).tag == _txtDropOffTime.tag*10){
        _txtDropOffTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerDropOff.date, TIME_FORMAT)];
    }
    
    if(!isValidDateTimeWithin24h(datepickerPickUp.date, timepickerPickUp.date)){
        timepickerPickUp.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*5];
        if(requestType == CAR_TRANSFER_REQUEST){
            _icPickupTimeError.hidden = NO;
            _txtPickUpTime.text = [self getTimebyDate:timepickerPickUp.date];
        } else {
            _icRentalPickupTimeError.hidden = NO;
            _txtRentalPickUpTime.text = [self getTimebyDate:timepickerPickUp.date];
        }
    }
    
}

-(void) adjustDropOffDate{
    if(datepickerDropOff){
        if([datepickerDropOff.date compare:datepickerPickUp.date] == NSOrderedAscending){
        datepickerDropOff.date = datepickerPickUp.date;
        _txtDropOffDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerDropOff.date, DATE_FORMAT)];
        }
        [datepickerDropOff setMinimumDate:datepickerPickUp.date];
    }
}

-(void) setPickUpText{
    if(requestType == CAR_TRANSFER_REQUEST){
         _txtPickUpDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerPickUp.date, DATE_FORMAT)];
         _txtPickUpTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerPickUp.date, TIME_FORMAT)];
    } else {
        _txtRentalPickUpDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerPickUp.date, DATE_FORMAT)];
        _txtRentalPickUpTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerPickUp.date, TIME_FORMAT)];
    }
}

-(NSString*)getTimebyDate:(NSDate*)pdate{
    NSString* time = @"";
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    [outputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *temp = [outputFormatter stringFromDate:pdate];
    
    NSDate *date = [outputFormatter dateFromString:temp];
    
    outputFormatter.dateFormat = @"hh:mm a";
    time = [outputFormatter stringFromDate:date];
    
    return  time;
}

- (void)dismiss
{
    [self closeAllKeyboardAndPicker];

}

#pragma textField
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self closeAllKeyboardAndPicker];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField.tag == 4 || textField.tag == 5 || textField.tag==7 || textField.tag==8){
        return NO;
    }
    return  YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self updateScrollViewToSelectedView:textField];
    
    UIView *errorIcon = [self.view viewWithTag:100+textField.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon) setErrorHidden:YES];
    }
}

-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + [self scrollViewPositionInSuperView] - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}

-(NSInteger) scrollViewPositionInSuperView{
    return _scrollView.frame.origin.y + countRightPositionInScrollView([_scrollView superview]) - 30;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma OptionalDetailsDelegate
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
    [self closeAllKeyboardAndPicker];
}

-(void) textFieldBeginEdit:(UIView*) selectedView{
    [self updateScrollViewToSelectedView:selectedView];
}

#pragma Sub filter delegate
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(key && values && key.count>0 && values.count>0){
        if(type == TRANSPORT){
            NSString* transport = [key objectAtIndex:0];
            selectedTransportKey = transport;
            [_btTransportType setSelected:NO];
            [_btTransportType setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        }
    } else {
        if(type == TRANSPORT){
            selectedTransportKey = nil;
            [_btTransportType setSelected:YES];
            [_btTransportType setTitle:TRANSPORT_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}
#pragma end

-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_icDriverNameError forKey:[NSNumber numberWithInteger:_icDriverNameError.tag]];
    [dictIconErrorManager setObject:_icTransportError forKey:[NSNumber numberWithInteger:_icTransportError.tag]];
    [dictIconErrorManager setObject:_icAgeError forKey:[NSNumber numberWithInteger:_icAgeError.tag]];
    [dictIconErrorManager setObject:_icPickupTimeError forKey:[NSNumber numberWithInteger:_icPickupTimeError.tag]];
    [dictIconErrorManager setObject:_icPickUpLocationError forKey:[NSNumber numberWithInteger:_icPickUpLocationError.tag]];
    [dictIconErrorManager setObject:_icDropOffTimeError forKey:[NSNumber numberWithInteger:_icDropOffTimeError.tag]];
    [dictIconErrorManager setObject:_icDropOffLocationError forKey:[NSNumber numberWithInteger:_icDropOffLocationError.tag]];
    [dictIconErrorManager setObject:_icPassengersError forKey:[NSNumber numberWithInteger:_icPassengersError.tag]];
    for (UIView *item in [dictIconErrorManager allValues]) {
        if([item isKindOfClass:[ErrorToolTip class]]){
           ((ErrorToolTip*)item).delegate = self;
        }
    }
    
}

- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            if([[dictIconErrorManager objectForKey:icTag] isKindOfClass:[ErrorToolTip class]]){
                [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
            }
        }
    }
}

-(CarCreateRequestObject*) validateAndCollectData{
    for (UIView *item in [dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }
    
    [_optionalDetailsView exportValues];
    
    CarCreateRequestObject* result = [[CarCreateRequestObject alloc] init];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.bookingId = _bookingId;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    if(requestType == CAR_RENTAL_REQUEST){
        value = _txtDriverName.text;
        if(isValidValue(value)){
            if (isNotIncludedSpecialCharacters(value)) {
                result.driverName = value;
            } else {
                [_icDriverNameError setHidden:NO];
                isEnoughInfo = NO;
                _icDriverNameError.errorMessage = INCLUDE_UNSUPPORT_CHAR_ERR;
            }
        } else {
            [_icDriverNameError setHidden:NO];
            isEnoughInfo = NO;
            if(value.length==0){
                _icDriverNameError.errorMessage = MANDATORY_BLANK;
            }
        }
        
        value = _txtAge.text;
        if(isValidValue(value)){
            result.driverAge = [value integerValue];
        } else {
            [_icAgeError setHidden:NO];
            isEnoughInfo = NO;
            if(value.length==0){
                _icAgeError.errorMessage = MANDATORY_BLANK;
            }
        }
        
        result.dropOffDate = formatDate(datepickerDropOff.date, DATE_SEND_REQUEST_FORMAT);
        result.dropOffTime = formatDate(timepickerDropOff.date, TIME_SEND_REQUEST_FORMAT);
        if ([formatDateToString(datepickerPickUp.date, DATE_FORMAT) isEqualToString:formatDateToString(datepickerDropOff.date, DATE_FORMAT)] && [timepickerDropOff.date compare:timepickerPickUp.date] == NSOrderedAscending)  {
            _icDropOffTimeError.errorMessage = DROP_OFF_TIME_ERR;
            [_icDropOffTimeError setHidden:NO];
            isEnoughInfo = NO;
            }
        
        if ([_swLiciense isOn]) {
            result.isInternationalLicense = YES;
        } else {
            result.isInternationalLicense = NO;
        }
        
        result.preferredVerhicle = _optionalDetailsView.selectedPreferredVerhicleKeys;
        result.preferredCarRentals =  _optionalDetailsView.selectedPreferredCompanyKeys;

        if(_optionalDetailsView.maximumPrice && _optionalDetailsView.maximumPrice.length>0){
            result.maximumPrice = _optionalDetailsView.maximumPrice;
        }
    } else if (requestType == CAR_TRANSFER_REQUEST){
        value = selectedTransportKey;
        if(isValidValue(value)){
            result.transportType = value;
        } else {
            [_icTransportError setHidden:NO];
            if(value.length==0){
                _icTransportError.errorMessage = MANDATORY_BLANK;
            }
            isEnoughInfo = NO;
        }
        
        value = _txtNumberPassengers.text;
        if(isValidValue(value)){
            result.nbOfPassengers = [value integerValue];
        } else {
            isEnoughInfo = NO;
            [_icPassengersError setHidden:NO];
            _icPassengersError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    result.pickUpDate = formatDate(datepickerPickUp.date, DATE_SEND_REQUEST_FORMAT);
    result.pickUpTime = formatDate(timepickerPickUp.date, TIME_SEND_REQUEST_FORMAT);
    if (!isValidDateTimeWithin24h(datepickerPickUp.date, timepickerPickUp.date)) {
        if(requestType == CAR_TRANSFER_REQUEST){
            _icPickupTimeError.hidden = NO;
        } else {
            _icRentalPickupTimeError.hidden = NO;
        }
        if(value.length==0){
            _icPickupTimeError.errorMessage = MANDATORY_BLANK;
            [self bringSubviewToFront:_icPickupTimeError];
        }
        isEnoughInfo = NO;
    }
    
    value = _txtPickUpLocation.text;
    if(isValidValue(value)){
        result.pickUpLocation = value;
    } else {
        [_icPickUpLocationError setHidden:NO];
        if(value.length==0){
            _icPickUpLocationError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    value = _txtDropOffLocation.text;
    if(isValidValue(value)){
        result.dropOffLocation = value;
    } else {
        [_icDropOffLocationError setHidden:NO];
        if(value.length==0){
            _icDropOffLocationError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.mobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.email = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.rentalCompany;
    if(isValidValue(value)){
        result.preferredCarRentals = [[NSMutableArray alloc] initWithObjects:value, nil];
    }
    
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.specialMessage = value;
        } else {
            isEnoughInfo = NO;
        }
    }
    
    
    
    result.photo = _optionalDetailsView.selectedPhoto;
    
    if(isEnoughInfo){
        return result;
    } else {
        CarCreateRequestObject *temp;
        return temp;
    }
}

-(void) touchinDetailsView{
    [self closeAllKeyboardAndPicker];
}
-(void) setMyPreference:(NSDictionary*)dict{
    NSString* vehicle = [dict stringForKey:VALUE];
    NSString* compa = [dict stringForKey:VALUE1];
    [_optionalDetailsView setCarPreference:vehicle withNo:compa];
}
-(void) showRentalDetails:(CarRentalDetailsObject*) rentalDetails{
    _bookingId = rentalDetails.bookingId;
    _txtDriverName.text = rentalDetails.driverName;
    _txtAge.text = rentalDetails.driverAge>0 ? [NSString stringWithFormat:@"%ld", rentalDetails.driverAge] : @"";
    _swLiciense.on = rentalDetails.isInternationalLicense;
    datepickerPickUp.date = rentalDetails.pickUpDate;
    
    datepickerDropOff.date = rentalDetails.dropOffDate;
    datepickerDropOff.minimumDate = datepickerPickUp.date;
    
    if(rentalDetails.pickUpTime){
        timepickerPickUp.date = rentalDetails.pickUpTime;
        _txtRentalPickUpTime.text = formatDateToString(timepickerPickUp.date, TIME_FORMAT);
    }
    
    if(rentalDetails.dropOffTime){
        timepickerDropOff.date = rentalDetails.dropOffTime;
        _txtDropOffTime.text = formatDateToString(timepickerDropOff.date, TIME_FORMAT);
    }
    
    _txtRentalPickUpDate.text = formatDateToString(datepickerPickUp.date, DATE_FORMAT);
    _txtDropOffDate.text = formatDateToString(datepickerDropOff.date, DATE_FORMAT);
    
    
    _txtPickUpLocation.text = rentalDetails.pickUpLocation;
    _txtDropOffLocation.text = rentalDetails.dropOffLocation;
    
    [_optionalDetailsView showCarRentalDetails:rentalDetails];
   
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
}
-(void)closAllkeyboard{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}
-(void) showTransferDetails:(CarTransferDetailsObject*) transferDetails{
    _bookingId = transferDetails.bookingId;
    selectedTransportKey =[transferDetails transportKey];
    [_btTransportType setSelected:NO];
    [_btTransportType setTitle:[transferDetails transportValue] forState:UIControlStateNormal];
    
    datepickerPickUp.date = transferDetails.pickUpDate;
    if(transferDetails.pickUpTime){
        timepickerPickUp.date = transferDetails.pickUpTime;
        _txtPickUpTime.text = formatDateToString(timepickerPickUp.date, TIME_FORMAT);
    }
    
    _txtPickUpDate.text = formatDateToString(datepickerPickUp.date, DATE_FORMAT);
    
    
    _txtPickUpLocation.text = transferDetails.pickUpLocation;
    _txtDropOffLocation.text = transferDetails.dropOffLocation;
    
    if(transferDetails.nbPassengers < 1){
        transferDetails.nbPassengers = 1;
    }
    normalPax = transferDetails.nbPassengers;
    _txtNumberPassengers.text = [NSString stringWithFormat:@"%ld", transferDetails.nbPassengers];
    
    [_optionalDetailsView showDetailsView:transferDetails];
    
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
}
- (IBAction)actionDecreasePax:(id)sender {
    if(normalPax>1){
        [self displayPaxNumber:normalPax-1];
    }
}

- (IBAction)actionIncreasePax:(id)sender {
    [self displayPaxNumber:normalPax+1];
}
-(void) displayPaxNumber:(NSInteger) pax{
    normalPax = pax;
    _txtNumberPassengers.text = [NSString stringWithFormat:@"%ld ", normalPax];
}

-(BOOL) isValidDateTime{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:timepickerPickUp.date];
    NSInteger hour = [components1 hour];
    NSInteger minute = [components1 minute];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: datepickerPickUp.date];
    [components setHour: hour];
    [components setMinute: minute];
    [components setSecond: 0];
    NSDate *fullReservation = [gregorian dateFromComponents: components];
    
    if([fullReservation compare:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]] == NSOrderedAscending){
        return NO;
    }
    return YES;
}

-(void) resetTimeValue{
    timepickerPickUp.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*10];
    if(requestType == CAR_TRANSFER_REQUEST){
        _txtPickUpTime.text = formatDateToString(timepickerPickUp.date, TIME_FORMAT);
    } else {
        _txtRentalPickUpTime.text = formatDateToString(timepickerPickUp.date, TIME_FORMAT);
        timepickerDropOff.date = timepickerPickUp.date;
        _txtDropOffTime.text = formatDateToString(timepickerDropOff.date, TIME_FORMAT);
    }
}

-(NSAttributedString*) addRedStarToString:(NSString*)value{
    value = [value stringByAppendingString:@"<font color=\"red\">*</font>"];
    NSString *addHtml = [NSString stringWithFormat:@"<html> \n"
                         "<head> \n"
                         "<style type=\"text/css\"> \n"
                         "body {font-family: \"%@\"; font-size: %f; color:#555555}\n"
                         "</style> \n"
                         "</head> \n"
                         "<body>%@</body> \n"
                         "</html>", @"AvenirNextLTPro-Medium", 16*SCREEN_SCALE, value];
    return [[NSAttributedString alloc] initWithData:[addHtml dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
}


-(void)setTextForView {
    _lbDriverName.attributedText = [self setRequireString:NSLocalizedString(@"Driver's name", nil)];
    _txtDriverName.placeholder = NSLocalizedString(@"Please enter driver's name", nil);
    _lbAge.attributedText = [self setRequireString:NSLocalizedString(@"Driver's age", nil)];
    _txtAge.placeholder = NSLocalizedString(@"Please enter driver's age", nil);
    _lbSwitchLiciense.attributedText = [self setRequireString:NSLocalizedString(@"I have international license", nil)];
    _lbDropOffDate.attributedText = [self setRequireString:NSLocalizedString(@"Drop off date", nil)];
    _lbDropOffTime.attributedText = [self setRequireString:NSLocalizedString(@"Drop off time", nil)];
   // _lbPickUpDate.attributedText = [self setRequireString:NSLocalizedString(@"Pick up date",nil)];

   
    _lbTransportType.attributedText = [self setRequireString:NSLocalizedString(@"Transport type", nil)];
    _lbNumberPassengers.attributedText = [self setRequireString:NSLocalizedString(@"Number of passengers", nil)];

    
    
    [_lbTitleDropOffLocation setAttributedText:[self setRequireString:NSLocalizedString(@"Drop off location",nil)]];
    [_lbTitlePickUpDate setAttributedText:[self setRequireString:NSLocalizedString(@"Pick up date",nil)]];
    //[_lbTitlePickupTime setAttributedText:[self setRequireString:NSLocalizedString(@"Pick up time",nil)]];
    [_lbTitlePickUpLocation setAttributedText:[self setRequireString:NSLocalizedString(@"Pick up location_rental",@"")]];
    //_lbTitlePickUpLocation.text = @"1321313123";
    _icDropOffLocationError.errorMessage = NSLocalizedString(@"Please enter drop-off address", nil);
    _icPickUpLocationError.errorMessage = NSLocalizedString(@"Please enter pickup address transfer", nil);
    [_txtPickUpLocation setPlaceholder:NSLocalizedString(@"Please enter pickup address", nil)];
    [_txtDropOffLocation setPlaceholder:NSLocalizedString(@"Please enter drop-off address", nil)];
    [_btTransportType setTitle:NSLocalizedString(@"Please select type of transport", nil) forState:UIControlStateNormal];
    
    [_btCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}
@end
