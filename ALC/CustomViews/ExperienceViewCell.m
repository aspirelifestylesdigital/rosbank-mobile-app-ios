//
//  ExperienceViewCell.m
//  ALC
//
//  Created by Anh Tran on 8/22/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ExperienceViewCell.h"

@implementation ExperienceViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
