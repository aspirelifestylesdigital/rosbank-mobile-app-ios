//
//  HomeGourmetItem.h
//  ALC
//
//  Created by Hai NguyenV on 8/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageViewAligned.h"
//#import "WSAddItemToWishList.h"
//#import "WSRemoveItemfromWishList.h"

@protocol HomeItemDelegate <NSObject>

@optional

-(void)onFaviotesClick:(int)position;
-(void)onFaviotesDone:(int)position withValues:(BOOL)isLike;
-(void)onFaviotesFail;
-(void)onItemClick:(int)position;

@end

@interface HomeGourmetItem : UIView //<DataLoadDelegate>
{
    //WSRemoveItemfromWishList *wsRemoveItemfromWishList;
    //WSAddItemToWishList *wsAddItemToWishList;
}
@property (strong, nonatomic) IBOutlet UIView *view;
@property (assign, nonatomic) id<HomeItemDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFaviote;
@property (weak, nonatomic) IBOutlet UIImageViewAligned *imvView;
@property (weak, nonatomic) IBOutlet UILabel *lbRestaurantName;
@property (strong, nonatomic) NSString* restaurantID;

- (IBAction)btnFaviote:(id)sender;
- (IBAction)btnMoveDetails:(id)sender;

@end
