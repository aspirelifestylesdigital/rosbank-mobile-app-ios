//
//  HomeGourmetItem.m
//  ALC
//
//  Created by Hai NguyenV on 8/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HomeGourmetItem.h"
#import "BaseResponseObject.h"

@implementation HomeGourmetItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"HomeGourmetItem" owner:self options:nil] firstObject];
    [self.view setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [self addSubview:self.view];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)btnFaviote:(id)sender {
    if(self.delegate!=nil){
        [self.delegate onFaviotesClick:(int)self.tag];
    }
    /*if(_btnAddFaviote.isSelected){
        //remove
        wsRemoveItemfromWishList = [[WSRemoveItemfromWishList alloc] init];
        wsRemoveItemfromWishList.delegate = self;
        [wsRemoveItemfromWishList removeItem:self.restaurantID];
    } else {
        //add
        wsAddItemToWishList = [[WSAddItemToWishList alloc] init];
        wsAddItemToWishList.delegate = self;
        [wsAddItemToWishList addItem:self.restaurantID];
    }*/
}

- (IBAction)btnMoveDetails:(id)sender {
    if(self.delegate!=nil){
        [self.delegate onItemClick:(int)self.tag];
    }
}

/*
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_GOURMET_DETAILS){
       
    } else if (ws.task == WS_ADD_ITEM_TO_WISHLIST){
        [_btnAddFaviote setSelected:YES];
        if(self.delegate!=nil){
            [self.delegate onFaviotesDone:(int)self.tag withValues:YES];
        }
    } else if (ws.task == WS_REMOVE_ITEM_FROM_WISHLIST){
        [_btnAddFaviote setSelected:NO];
        if(self.delegate!=nil){
            [self.delegate onFaviotesDone:(int)self.tag withValues:NO];
        }
    }
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(self.delegate!=nil){
        [self.delegate onFaviotesFail];
    }
}*/
@end
