//
//  CityGuideCategoryView.h
//  ALC
//
//  Created by Anh Tran on 9/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CategoryDelegate;
@interface CityGuideCategoryView : UIView
@property (unsafe_unretained) id<CategoryDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIImageView *imvCategory;
@property (strong, nonatomic) IBOutlet UILabel *lbCategoryName;
-(void) showView:(NSInteger) index withTitle:(NSString*) title andImage:(NSString*) imageUrl;
@end

@protocol CategoryDelegate <NSObject>

- (void) clickOnCategory:(NSInteger)index;

@end