//
//  CellView_Bot.m
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import "CellView_Bot.h"

#define SPACE_X 8
#define SPACE_Y 8

@implementation CellView_Bot

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) loadView
{
    [self layoutIfNeeded];
    currentBeginX = 0;
    currentBeginY = 0;
    
    [self.vwFrameAvatar.layer setCornerRadius:self.vwFrameAvatar.frame.size.height/2.0];
    [self.vwFrameContent.layer setCornerRadius:4.0];
    
    NSInteger fistWidth = self.lblContent.frame.size.width;
    
    [self.lblContent setText:self.currentChat.valueContent];
    
    [self.lblContent sizeToFit];
    
    self.layoutHeightContent.constant = self.lblContent.frame.size.height;
    
    NSInteger spaceBetweenWidth = self.lblContent.frame.size.width - fistWidth;
    
    NSInteger currentHeight = self.lblContent.frame.origin.y + self.layoutHeightContent.constant + 5;
    self.layoutFrameCOntent.constant = currentHeight;
    
    [self layoutIfNeeded];
    
    if ([self.currentChat.listButtons count] > 0)
    {
        for (int i = 0; i < [self.currentChat.listButtons count]; i++)
        {
            ChatButtonObject* itemButton = [self.currentChat.listButtons objectAtIndex:i];
            [self addButtonToSpecialViewWithObjectChatButton:itemButton atIndex:i];
        }
        
        [self layoutIfNeeded];
        
        if (self.lblContent.frame.size.width <= self.vwShowSpecial.frame.size.width)
        {
            spaceBetweenWidth = self.vwShowSpecial.frame.size.width - fistWidth;
        }
        self.layoutLeadingWithView.constant = self.layoutLeadingWithView.constant - spaceBetweenWidth;
        
        [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width + spaceBetweenWidth, self.vwFrameContent.frame.origin.y*2 + self.layoutFrameCOntent.constant)];
    }
    else
    {
        
        [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width + spaceBetweenWidth, self.vwFrameContent.frame.origin.y*2 + currentHeight)];

    }
    
}


- (void) addButtonToSpecialViewWithObjectChatButton:(ChatButtonObject*)currentButton atIndex:(NSInteger) currentIndexButton
{
    CustomButtonView* vwButton = (CustomButtonView*)[self createNewCustomObjectViewFromNibName:@"CustomButtonView" owner:self withClass:[CustomButtonView class]];
    vwButton.valueText = currentButton.textButton;
    vwButton.currentButtonObject = currentButton;
    vwButton.delegate = self;
    [vwButton setFrame:CGRectMake(0, 0, self.vwShowSpecial.frame.size.width,vwButton.frame.size.height)];
    [self.vwShowSpecial addSubview:vwButton];
    [vwButton loadView];
    
    
    if (currentBeginX + SPACE_X + vwButton.frame.size.width + SPACE_X > self.vwShowSpecial.frame.size.width )
    {
        currentBeginY = SPACE_Y + self.layoutHeightShowSpecial.constant;
        currentBeginX = 0;
    }
    else
    {
        if (currentIndexButton != 0)
        {
            currentBeginX += SPACE_X;
        }
    }
    
    [vwButton setFrame:CGRectMake(currentBeginX, currentBeginY, vwButton.frame.size.width, vwButton.frame.size.height)];
//    [self.vwShowSpecial addSubview:btnTemp];
    
    self.layoutHeightShowSpecial.constant = currentBeginY + vwButton.frame.size.height;
    
    self.layoutFrameCOntent.constant = self.vwShowSpecial.frame.origin.y + self.layoutHeightShowSpecial.constant + 5;
    
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.vwFrameContent.frame.origin.y*2 + self.layoutFrameCOntent.constant)];
    
    currentBeginX += vwButton.frame.size.width;
}

- (id) createNewCustomObjectViewFromNibName:(NSString*)nibName owner:(id)owner withClass:(Class)classView
{
    
    NSArray* nibContents = [[NSBundle mainBundle]
                            loadNibNamed:nibName owner:owner options:NULL];
    
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    NSObject* nibItem = nil;
    
    while ( (nibItem = [nibEnumerator nextObject]) != nil) {
        
        if ( [nibItem isKindOfClass: classView])
        {
            return nibItem;
        }
    }
    return nil;
}

#pragma mark - Delegate Button View

- (void) clickAtButtonObject:(ChatButtonObject *)currentButton
{
    
    for (CustomButtonView* itemView in self.vwShowSpecial.subviews)
    {
        if (![itemView.currentButtonObject.textButton isEqualToString:currentButton.textButton])
        {
            [itemView returnNormal];
        }
    }
    
    [self lockView];
    
    if (self.delegate)
    {
        if ([self.delegate respondsToSelector:@selector(activeClickToButton:)]) {
            [self.delegate activeClickToButton:currentButton];
        }
    }
}

- (void) lockView
{
    [self.vwShowSpecial setUserInteractionEnabled:NO];
}
@end
