//
//  CellView_Restaurant.h
//  demoChatBot
//
//  Created by Nhat Huy Truong  on 9/12/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GourmetObject.h"
//#import "WSAddItemToWishList.h"
//#import "WSRemoveItemfromWishList.h"
#import <MessageUI/MessageUI.h>

@interface CellView_Restaurant : UIView<MFMailComposeViewControllerDelegate>
{
    //WSRemoveItemfromWishList *wsRemoveItemfromWishList;
    //WSAddItemToWishList *wsAddItemToWishList;
}

@property (strong, nonatomic) IBOutlet UILabel *lblRating;
@property (strong, nonatomic) GourmetObject* currentGourmet;
@property (strong, nonatomic) IBOutlet UIView *vwFrameContent;
@property (strong, nonatomic) IBOutlet UIImageView *imvRestaurant;
@property (strong, nonatomic) IBOutlet UILabel *lblRestaurantName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress_street;
@property (strong, nonatomic) IBOutlet UILabel *lblAddres_number;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress_city_phone;
@property (strong, nonatomic) IBOutlet UILabel *lblNumberRating;
@property (strong, nonatomic) IBOutlet UIButton *btnFarvorite;

- (IBAction)clickImage:(id)sender;
- (IBAction)clickTitle:(id)sender;

- (void) loadView;
- (IBAction)clickReadMore:(id)sender;
- (IBAction)clickBookNew:(id)sender;
- (IBAction)clickFarvorite:(id)sender;
- (IBAction)clickShare:(id)sender;
@end
