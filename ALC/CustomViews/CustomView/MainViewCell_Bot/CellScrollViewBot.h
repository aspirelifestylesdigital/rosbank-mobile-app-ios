//
//  CellScrollViewBot.h
//  demoChatBot
//
//  Created by Nhat Huy on 9/11/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "CellView_Restaurant.h"
#import "ChatBotObject.h"
#import "CellView_Image.h"

@interface CellScrollViewBot : UIView<iCarouselDataSource, iCarouselDelegate>
{
    NSInteger currentIndex;
}
@property (assign, nonatomic) BOOL isShowImages;
@property (strong, nonatomic) NSMutableArray* arrayListShow;

@property (strong, nonatomic) ChatBotObject* currentItemChat;
@property (strong, nonatomic) IBOutlet UIView *vwAvatar;
@property (strong, nonatomic) IBOutlet iCarousel *vwShow;
@property (strong, nonatomic) IBOutlet UIButton *btnLeft;
@property (strong, nonatomic) IBOutlet UIButton *btnRight;


- (void) loadView;
- (IBAction)clickLeft:(id)sender;
- (IBAction)clickRight:(id)sender;
- (void) lockView;

@end
