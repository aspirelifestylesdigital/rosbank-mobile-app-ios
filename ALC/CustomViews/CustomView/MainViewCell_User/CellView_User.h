//
//  CellView_User.h
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ChatBotObject.h"

@interface CellView_User : UIView

@property (strong, nonatomic) IBOutlet UIView *vwFrameAvatar;
@property (strong, nonatomic) IBOutlet UIView *vwFrameContent;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutHeightContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutFrameCOntent;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UIImageView *imvAvartar;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutLeadingWithView;
@property (strong,nonatomic) ChatBotObject* currentChat;

- (void) loadView;
@end
