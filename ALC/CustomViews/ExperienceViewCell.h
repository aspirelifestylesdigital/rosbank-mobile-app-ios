//
//  ExperienceViewCell.h
//  ALC
//
//  Created by Anh Tran on 8/22/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExperienceViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbExperienceName;
@property (strong, nonatomic) IBOutlet UIImageView *imvImage;

@end
