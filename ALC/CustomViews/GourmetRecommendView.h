//
//  GourmetRecommendView.h
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionalDetailsView.h"
#import "SubFilterController.h"
#import "GourmetRecommendRequestObj.h"
#import "GouetmetRecommendDetailsObj.h"
#import "ErrorToolTip.h"
#import "CustomTextField.h"
@protocol GourmetRecommendViewDelegate <NSObject>

@optional
-(void) submitRequest:(GourmetRecommendRequestObj*) requestObject forType:(enum REQUEST_TYPE) type;
@end

@interface GourmetRecommendView : UIView<OptionalDetailsViewDelegate,  SubFilterControllerDelegate,ErrorToolTipDelegate,UITextFieldDelegate>
{
    NSString* selectedCountry;
    NSString* selectedCity;
    NSString* selectedState;
    NSMutableArray *selectedCuisines;
    NSMutableArray *selectedCuisinesKey;
    
    UIDatePicker *datepickerReservation;
    UIDatePicker *timepickerReservation;
    
    NSMutableArray *selectedOccasion;
    NSMutableArray *selectedOccasionKey;

}

@property (strong, nonatomic) NSString* bookingId;

@property (unsafe_unretained) id<GourmetRecommendViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCity;
@property (strong, nonatomic) IBOutlet CustomTextField *txtMaximumBudget;
@property (strong, nonatomic) IBOutlet UIButton *btCountry;
@property (strong, nonatomic) IBOutlet UIButton *btCuisine;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationDate;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationTime;
@property (strong, nonatomic) IBOutlet UIButton *btOccasion;
@property (weak, nonatomic) IBOutlet UIView *pvState;
@property (strong, nonatomic) IBOutlet UIButton *btState;

- (IBAction)actionCountry:(id)sender;
- (IBAction)actionCuisine:(id)sender;
- (IBAction)actionSubmitRequest:(id)sender;


-(void) showRecommendDetails:(GouetmetRecommendDetailsObj*) recommendDetails;
-(void) setMyPreferencce:(NSString*)cuisine withOther:(NSString*)other;

-(void) setTimeToCurrentTime;
@end
