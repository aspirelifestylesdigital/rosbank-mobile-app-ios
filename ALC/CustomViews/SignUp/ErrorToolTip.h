//
//  ErrorToolTip.h
//  ALC
//
//  Created by Anh Tran on 12/21/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorToolTip.h"
#import "PaddingUILabel.h"
@protocol ErrorToolTipDelegate ;
@interface ErrorToolTip : UIView
@property (nonatomic) IBInspectable NSString* errorMessage;
@property (unsafe_unretained) id<ErrorToolTipDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIButton *btError;
@property (strong, nonatomic) IBOutlet UIView *tooltipView;
@property (strong, nonatomic) IBOutlet PaddingUILabel *lbErrorMsg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightErrorMsg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthErrorMsg;
- (void) toggleToolTips:(Boolean) isShow;
- (void) setErrorHidden:(BOOL)hidden;
@end

@protocol ErrorToolTipDelegate <NSObject>
@optional
- (void) clickedOnErrorIcon:(NSInteger) tag;

@end