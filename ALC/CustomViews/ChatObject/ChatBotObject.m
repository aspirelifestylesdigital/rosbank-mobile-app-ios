//
//  ChatBotObject.m
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import "ChatBotObject.h"

@implementation ChatBotObject

- (id) init
{
    self = [super init];
    if (self)
    {
        self.listRestaurant = [[NSMutableArray alloc] init];
        self.listButtons    = [[NSMutableArray alloc] init];
        self.listImageURL    = [[NSMutableArray alloc] init];
    }
    return self;
}
@end
