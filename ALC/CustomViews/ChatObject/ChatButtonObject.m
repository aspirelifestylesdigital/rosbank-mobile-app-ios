//
//  ChatButtonObject.m
//  ALC
//
//  Created by Nhat Huy Truong  on 9/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ChatButtonObject.h"

@implementation ChatButtonObject

+ (id) createObjectWithDic:(NSDictionary*)dicData
{
    ChatButtonObject* itemButton = nil;
    if (dicData)
    {
        itemButton = [[ChatButtonObject alloc] init];
        itemButton.textButton = stringFromObject([dicData objectForKey:@"name"]);
        itemButton.type = stringFromObject([dicData objectForKey:@"type"]);
        itemButton.action = stringFromObject([dicData objectForKey:@"action"]);
        itemButton.phone = stringFromObject([dicData objectForKey:@"action"]);
        itemButton.linkeURL = stringFromObject([dicData objectForKey:@"action"]);
    }
    return itemButton;
}
@end
