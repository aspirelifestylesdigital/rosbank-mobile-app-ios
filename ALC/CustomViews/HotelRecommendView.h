//
//  HotelRecommendView.h
//  ALC
//
//  Created by Anh Tran on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubFilterController.h"
#import "OptionalDetailsView.h"
#import "HotelRequestObject.h"

@protocol HotelRecommendViewDelegate <NSObject>

@optional
-(void) submitRequest:(HotelRequestObject*) requestObject forType:(enum REQUEST_TYPE) type;
@end


@interface HotelRecommendView : UIView <UITextFieldDelegate,  UIPickerViewDelegate, OptionalDetailsViewDelegate,  SubFilterControllerDelegate,ErrorToolTipDelegate>
{
    NSString* selectedState;
}
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (weak, nonatomic) IBOutlet UIView *pvState;
@property (strong, nonatomic) IBOutlet UIButton *btState;
@property (strong, nonatomic) NSString* anyRequest;

@property (strong, nonatomic) NSString* bookingId;
@property (unsafe_unretained) id<HotelRecommendViewDelegate> delegate;
-(void) showRecommendDetails:(HotelRequestDetailsObject*) details;
-(void) setMyPreference:(NSDictionary*)dict;
@end
