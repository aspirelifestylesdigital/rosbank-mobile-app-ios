//
//  MyRequestCell.h
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSCancelRequest.h"
#import "MyRequestObject.h"

@protocol MyRequestCellDelegate;

@interface MyRequestCell : UITableViewCell<DataLoadDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lbTitleStatus;
@property (weak, nonatomic) IBOutlet  UIImageView *icRequestType;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
//@property (strong, nonatomic) IBOutlet UILabel *lbAddress;
//@property (strong, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UIView *amendcancelView;
@property (weak, nonatomic) IBOutlet UIButton *btAmend;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
@property (weak, nonatomic) IBOutlet UIButton *btCalendar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintAmend;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintCancel;
@property (unsafe_unretained) id<MyRequestCellDelegate> delegate;

-(void) showData:(MyRequestObject*) item;
- (IBAction)actionAddCalendar:(id)sender;

@end


@protocol MyRequestCellDelegate <NSObject>

@optional
- (void) cancelingRequest:(MyRequestObject*)item;
- (void) failCancelRequest:(MyRequestObject*)item;
- (void) didCancelRequest:(MyRequestObject*)item;
- (void) amendRequest:(MyRequestObject*)item;

@end