//
//  HotelRecommendView.m
//  ALC
//
//  Created by Anh Tran on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HotelRecommendView.h"

#define COUNTRY_HINT_TEXT NSLocalizedString(@"Please select country of preferred hotel", nil)
#define CITY_HINT_TEXT NSLocalizedString(@"Please select city of preferred hotel", nil)
#define STATE_HINT_TEXT NSLocalizedString(@"Please select state of preferred hotel", nil)

@interface HotelRecommendView(){
    NSString* selectedCountry;
    NSString* selectedCity;
    AppDelegate* appDelegate;
    NSMutableDictionary* dictIconErrorManager;
}
@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCity;
@property (strong, nonatomic) IBOutlet UIButton *btCountry;
@property (strong, nonatomic) IBOutlet CustomTextField *txtMaximumPrice;

@property (strong, nonatomic) IBOutlet ErrorToolTip *icMaximumError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCityError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCountryError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icStateError;


@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbMaxPrice;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;

@end

@implementation HotelRecommendView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"HotelRecommendView" owner:self options:nil]
                 objectAtIndex:0];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
        [self setUpView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        [self setUpView];
        return self;
    }
    
    return self;
}

-(void) setUpView{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self addGestureRecognizer:tapGesture];
    
    [_txtCity setPlaceholder:CITY_HINT_TEXT];
    selectedCountry = NSLocalizedString(@"Russia", nil);
    [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    _pvState.hidden = YES;
    [_btCountry setSelected:NO];
    [_btCountry setTitle:selectedCountry forState:UIControlStateNormal];
    
    _txtMaximumPrice.delegate = self;
    _txtCity.delegate  =self;
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    _optionalDetailsView.delegate = self;
    [_optionalDetailsView applyRequestType:HOTEL_RECOMMEND_REQUEST];
    
    [self collectErrorIcons];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self setTextForViews];
}

-(SlideNavigationController*) currentNavigationController{
    if(!appDelegate){
        appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    return appDelegate.rootVC;
}


-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) closeAllKeyboardAndPicker{
    [_txtMaximumPrice resignFirstResponder];
    [_txtCity resignFirstResponder];
}


#pragma textField
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self closeAllKeyboardAndPicker];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self updateScrollViewToSelectedView:textField];
    
    UIView *errorIcon = [self.view viewWithTag:100+textField.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon) setErrorHidden:YES];
    }
}

-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + [self scrollViewPositionInSuperView] - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}

-(NSInteger) scrollViewPositionInSuperView{
    return _scrollView.frame.origin.y + countRightPositionInScrollView([_scrollView superview]) - 30;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma OptionalDetailsDelegate
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
    [self closeAllKeyboardAndPicker];
}

-(void) textFieldBeginEdit:(UIView*) selectedView{
    [self updateScrollViewToSelectedView:selectedView];
}
- (IBAction)actionCancel:(id)sender {
    [[self currentNavigationController] popViewControllerAnimated:YES];
}
#pragma Sub filter delegate
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(key && values && key.count>0 && values.count>0){
        if(type == COUNTRY){
            //_btCity.enabled = YES;
            NSString* newCountry = [key objectAtIndex:0];
            if([newCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [newCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
                _pvState.hidden = NO;
            }else{
                _pvState.hidden = YES;
            }
            selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
            
            selectedCountry = newCountry;
            [_btCountry setSelected:NO];
            [_btCountry setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        } else if(type == STATE){
            NSString* newState = [key objectAtIndex:0];
            selectedState = newState;
            [_btState setSelected:NO];
            [_btState setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        }
    } else {
        if(type == COUNTRY){
            //_btCity.enabled = NO;
            selectedCity = nil;
            selectedCountry = nil;
            _pvState.hidden = YES;
            [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
            [_btCountry setSelected:YES];
        } else if(type == STATE){
            selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}
#pragma end
- (IBAction)actionCountry:(id)sender {
    [_icCountryError setHidden:YES];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = COUNTRY;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.isCloseAfterClick = YES;
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btCountry.currentTitle, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedCountry, nil];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}
- (IBAction)actionStateClicked:(id)sender {
    [_icStateError setHidden:YES];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = STATE;
    controller.delegate = self;
    controller.maxCount = 1;
    if([selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        controller.countryType = USA;
    }else if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)]){
        controller.countryType = CANADA;
    }
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btState.currentTitle, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedState, nil];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}
-(void) setMyPreference:(NSDictionary*)dict{
    NSString* value = [dict stringForKey:VALUE3];
    if(isEqualIgnoreCase(value,@"yes")){
        _optionalDetailsView.swSmoking.on = YES;
    }else{
        _optionalDetailsView.swSmoking.on = NO;
    }
    _anyRequest = [dict stringForKey:VALUE6];
    
    NSString* room = [dict stringForKey:VALUE1];
    [_optionalDetailsView setRoomPreferen:room];
    [_optionalDetailsView setLoyalty:[dict stringForKey:VALUE4] withNo:[dict stringForKey:VALUE5]];
    
}

- (IBAction)actionSubmit:(id)sender {
    [self closeAllKeyboardAndPicker];
    HotelRequestObject* request = [self validateAndCollectData];
    if(request && _delegate){
        [_delegate submitRequest:request forType:HOTEL_RECOMMEND_REQUEST];
    }
}

-(HotelRequestObject*) validateAndCollectData{
    for (ErrorToolTip *item in [dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }
    
    [_optionalDetailsView exportValues];
    
    HotelRequestObject* result = [[HotelRequestObject alloc] init];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.bookingId = _bookingId;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    if(isValidValue(_anyRequest)){
        result.anyRequest = _anyRequest;
    }
    
    value = _txtMaximumPrice.text;
    if(isValidValue(value)){
        result.maximumPrice = value;
    } else {
        [_icMaximumError setHidden:NO];
        if(value.length ==0){
            _icMaximumError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.mobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.email = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    if(_optionalDetailsView.checkInDate && _optionalDetailsView.checkOutDate){
        result.checkInDate = _optionalDetailsView.checkInDate;
        result.checkOutDate = _optionalDetailsView.checkOutDate;
    }
    
    value = _optionalDetailsView.roomType;
    if(isValidValue(value)){
        result.roomType = value;
    }else {
        isEnoughInfo = NO;
    }
    
    if(_optionalDetailsView.adultPax + _optionalDetailsView.normalPax > 0){
        result.adultPax = _optionalDetailsView.adultPax;
        result.kidsPax = _optionalDetailsView.kidsPax;
    }
    
    value = _txtCity.text;
    if(isValidValue(value)){
        result.city = value;
    } else {
        if(value.length==0){
            _icCityError.errorMessage = MANDATORY_BLANK;
        }
        [_icCityError setHidden:NO];
        isEnoughInfo = NO;
    }
    
    value = selectedCountry;
    if(isValidValue(value)){
        result.country = value;
    } else {
        [_icCountryError setHidden:NO];
        if(value.length==0){
            _icCountryError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        value = selectedState;
        if(isValidValue(value)){
            result.State = value;
        }else{
            isEnoughInfo = NO;
            if(value.length==0){
                _icStateError.errorMessage = MANDATORY_BLANK;
            }
            [_icStateError setHidden:NO];
        }
    }
    
    value= _optionalDetailsView.loyaltyMembershipProgram;
    if(isValidValue(value))
    {
        result.loyyaltyProgram = value;
    }
    
    value= _optionalDetailsView.loyaltyMembershipNo;
    if(isValidValue(value))
    {
        result.membershipNo = value;
    }
    
    result.isSmoking = _optionalDetailsView.isSmoking;
    result.isWithBreakfast = _optionalDetailsView.isWithBreakfast;
    result.isWithWifi = _optionalDetailsView.isWithWifi;
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.specialMessage = value;
        } else {
            isEnoughInfo = NO;
        }
    }
    
    result.photo = _optionalDetailsView.selectedPhoto;
    
    if(isEnoughInfo){
        return result;
    } else {
        HotelRequestObject *temp;
        return temp;
    }
}



-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_icMaximumError forKey:[NSNumber numberWithInteger:_txtMaximumPrice.tag]];
    [dictIconErrorManager setObject:_icCountryError forKey:[NSNumber numberWithInteger:_btCountry.tag]];
    [dictIconErrorManager setObject:_icCityError forKey:[NSNumber numberWithInteger:_txtCity.tag]];
    for (ErrorToolTip *item in [dictIconErrorManager allValues]) {
        item.delegate = self;
    }
    
}

- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
        }
    }
}
-(void) touchinDetailsView{
    [self closeAllKeyboardAndPicker];
}
-(void) showRecommendDetails:(HotelRequestDetailsObject*) details{
    
    [_optionalDetailsView showHotelRecommendDetails:details];
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
    _anyRequest = details.anyRequest;
    _txtMaximumPrice.text = details.maximumPrice;
    _bookingId =details.bookingId;
    selectedCountry = details.country;
    selectedCity = details.city;
    _txtCity.text = details.city;
    
    if([details.country isEqualToString:NSLocalizedString(@"Canada", nil)] || [details.country isEqualToString:NSLocalizedString(@"United States", nil)]){
        _pvState.hidden = NO;
        [_btState setSelected:NO];
        selectedState = details.stateValue;
        [_btState setTitle:details.stateValue forState:UIControlStateNormal];
    }else{
        _pvState.hidden = YES;
    }
    
    [_btCountry setSelected:NO];
    [_btCountry setTitle:(details.countryValue ? details.countryValue : selectedCountry) forState:UIControlStateNormal];
    
}

-(void)setTextForViews {
    
    _lbCountry.attributedText = [self setRequireString:NSLocalizedString(@"Country", nil)];
    //[_btCountry setTitle:NSLocalizedString(@"Please select country of preferred restaurant", nil) forState:UIControlStateNormal];
    
    
    _lbCity.attributedText = [self setRequireString:NSLocalizedString(@"City", nil)];
    _txtCity.placeholder = NSLocalizedString(@"Please enter city of preferred hotel recommend", nil);
    
    _lbState.attributedText = [self setRequireString: NSLocalizedString(@"State", nil)];
    [_btState setTitle:NSLocalizedString(@"Please enter state of preferred hotel recommend", nil) forState:UIControlStateNormal];
    
    
   
    _lbMaxPrice.attributedText = [self setRequireString: NSLocalizedString(@"Maximum budget", nil)];
    _txtMaximumPrice.placeholder = NSLocalizedString(@"Please enter your maximum budget", nil);
    
    
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", @"") forState:UIControlStateNormal];
    [_btCancel setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
    
    
    
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}

@end
