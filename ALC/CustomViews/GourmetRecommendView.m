//
//  GourmetRecommendView.m
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetRecommendView.h"

#define COUNTRY_HINT_TEXT NSLocalizedString(@"Please select country of preferred restaurant", nil)
#define CITY_HINT_TEXT NSLocalizedString(@"Please select city of preferred restaurant", nil)
#define CUISINE_HINT_TEXT NSLocalizedString(@"Please select your preferred cuisine", nil)
#define OCCASION_HINT_TEXT NSLocalizedString(@"Please select occasion", nil)
#define STATE_HINT_TEXT NSLocalizedString(@"Please select state of preferred restaurant", nil)
#define DEFAULT_CUISINE @"Русская"

@interface GourmetRecommendView(){
    NSMutableDictionary* dictIconErrorManager;
}
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCuisineError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCityError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCountryError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icStateError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icTimeError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icOccasionError;


@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbMaxBudget;
@property (weak, nonatomic) IBOutlet UILabel *lbCuisine;
@property (weak, nonatomic) IBOutlet UILabel *lbReservationDate;
@property (weak, nonatomic) IBOutlet UILabel *lbReservationTime;
@property (weak, nonatomic) IBOutlet UILabel *lBOccasion;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;


@end

@implementation GourmetRecommendView

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        [self setupView];
        return self;
    }
    
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setupView{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self addGestureRecognizer:tapGesture];
    
    [_txtCity setPlaceholder:CITY_HINT_TEXT];
    selectedCountry = NSLocalizedString(@"Russia", nil);;
    [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    _pvState.hidden = YES;
    [_btCountry setSelected:NO];
    [_btCountry setTitle:selectedCountry forState:UIControlStateNormal];
    _txtCity.delegate = self;
    
    _optionalDetailsView.delegate = self;
    [_optionalDetailsView applyRequestType:GOURMET_RECOMMEND_REQUEST];
    
    _txtCity.delegate = self;
    [_btCuisine setTitle:DEFAULT_CUISINE forState:UIControlStateNormal];
    [_btCuisine setSelected:NO];
    selectedCuisines = [NSMutableArray arrayWithObject:DEFAULT_CUISINE];
    selectedCuisinesKey = [NSMutableArray arrayWithObject:DEFAULT_CUISINE];
    
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
    _txtMaximumBudget.delegate = self;
    
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    
    _txtReservationDate.inputAccessoryView = toolbar;
    _txtReservationTime.inputAccessoryView = toolbar;
    
    datepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerReservation setDatePickerMode:UIDatePickerModeDate];
    datepickerReservation.datePickerMode = UIDatePickerModeDate;
    [datepickerReservation setMinimumDate:[NSDate date]];
    [datepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerReservation.tag = _txtReservationDate.tag*10;
    _txtReservationDate.inputView = datepickerReservation;
    _txtReservationDate.delegate = self;
    datepickerReservation.date = [NSDate date];
    _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    //end
    
    //Check out date picker
    timepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [timepickerReservation setDatePickerMode:UIDatePickerModeTime];
    timepickerReservation.datePickerMode = UIDatePickerModeTime;
    // [timepickerReservation setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [timepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    timepickerReservation.tag = _txtReservationTime.tag*10;
    _txtReservationTime.inputView = timepickerReservation;
    _txtReservationTime.delegate = self;
    timepickerReservation.minuteInterval = 5;
    timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval:60*30];
    _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    
    [self collectErrorIcons];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self setTextForViews];
}
- (void)dismiss
{
    [self closeAllKeyboardAndPicker];
}
-(void) dateTextField:(id)sender
{
    if(((UIView*)sender).tag == _txtReservationDate.tag*10){
        _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    } else if(((UIView*)sender).tag == _txtReservationTime.tag*10){
        _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    }
    
    if(!isValidDateTimeWithinHalfAnHour(datepickerReservation.date, timepickerReservation.date)){
       // _icTimeError.hidden = NO;
        timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval: 60*30];
        _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    }
}
-(NSString*)getTimebyDate:(NSDate*)pdate{
    NSString* time = @"";
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    [outputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *temp = [outputFormatter stringFromDate:pdate];
    
    NSDate *date = [outputFormatter dateFromString:temp];
    
    outputFormatter.dateFormat = @"hh:mm a";
    time = [outputFormatter stringFromDate:date];
    
    return  time;
}

-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}

-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + [self scrollViewPositionInSuperView] - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}
-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(NSInteger) scrollViewPositionInSuperView{
    return _scrollView.frame.origin.y + countRightPositionInScrollView([_scrollView superview]) - 30;
}
#pragma OptionalDetailsDelegate
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
  
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self closeAllKeyboardAndPicker];
    return YES;
}

-(void) textFieldBeginEdit:(UIView*) selectedView{
    [self updateScrollViewToSelectedView:selectedView];
    
    UIView *errorIcon = [self.view viewWithTag:100+selectedView.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon) setErrorHidden:YES];
    }
}
#pragma mark ---------- UITextField Delegate --------------------
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return  YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self updateScrollViewToSelectedView:textField];
    
    UIView *errorIcon = [self.view viewWithTag:100+textField.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon) setErrorHidden:YES];
    }
}
-(void) closeAllKeyboardAndPicker{
    [_txtCity resignFirstResponder];
    [_txtReservationTime endEditing:YES];
    [_txtReservationDate endEditing:YES];
    [_txtMaximumBudget resignFirstResponder];
}

#pragma Sub filter delegate
- (void) pickOption:(NSMutableArray*) values forType:(NSInteger) type{
    if (type == OCCASION){
        selectedOccasion = values;
    }
}
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(key && values && key.count>0 && values.count>0){
        if(type == COUNTRY){
           // _btCity.enabled = YES;
            NSString* newCountry = [key objectAtIndex:0];
            if([newCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [newCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
                _pvState.hidden = NO;
            }else{
                _pvState.hidden = YES;
            }
            selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
            [_btCountry setSelected:NO];
            selectedCountry = newCountry;
            [_btCountry setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        } else if(type == STATE){
            NSString* newState = [key objectAtIndex:0];
            selectedState = newState;
            [_btState setSelected:NO];
            [_btState setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        } else if(type == CUISINES){
            selectedCuisines = values;
            selectedCuisinesKey = key;
            [_btCuisine setSelected:NO];
            [self.btCuisine setTitle:convertArrayToString(values) forState:UIControlStateNormal];
            if(self.btCuisine.currentTitle.length == 0){
                [_btCuisine setSelected:YES];
                [self.btCuisine setTitle:CUISINE_HINT_TEXT forState:UIControlStateNormal];
            }
        }else if(type == OCCASION){
            selectedOccasion = values;
            selectedOccasionKey = key;
            [_btOccasion setSelected:NO];
            [self.btOccasion setTitle:convertArrayToString(values) forState:UIControlStateNormal];
            if(self.btOccasion.currentTitle.length == 0){
                [_btOccasion setSelected:YES];
                [self.btOccasion setTitle:OCCASION_HINT_TEXT forState:UIControlStateNormal];
            }
        }
    } else {
        if(type == COUNTRY){
           // _btCity.enabled = NO;
            selectedCity = nil;
            selectedCountry = nil;
            _pvState.hidden = YES;
            [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
            [_btCountry setSelected:YES];
        } else if(type == STATE){
            selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
        }else if(type == CUISINES){
            selectedCuisines = nil;
            selectedCuisinesKey = nil;
            [_btCuisine setSelected:YES];
            [_btCuisine setTitle:CUISINE_HINT_TEXT forState:UIControlStateNormal];
        }else if(type == OCCASION){
            selectedOccasion = nil;
            selectedOccasionKey = nil;
            [_btOccasion setSelected:YES];
            [_btOccasion setTitle:OCCASION_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}

#pragma end
- (IBAction)actionOccasion:(id)sender{
    _icOccasionError.hidden = YES;
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = OCCASION;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.isCloseAfterClick = YES;
    controller.selectedValues =[[NSMutableArray alloc] initWithArray:selectedOccasion] ;
    [[self currentNavigationController] pushViewController:controller animated:YES];
}
- (IBAction)actionCancel:(id)sender {
    [[self currentNavigationController] popViewControllerAnimated:YES];
}
- (IBAction)actionCountry:(id)sender {
    [_icCountryError setHidden:YES];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = COUNTRY;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.isCloseAfterClick = YES;
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btCountry.currentTitle, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedCountry, nil];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}

- (IBAction)actionStateClicked:(id)sender {
    [_icStateError setHidden:YES];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = STATE;
    controller.delegate = self;
    controller.maxCount = 1;
    if([selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        controller.countryType = USA;
    }else if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)]){
        controller.countryType = CANADA;
    }
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btState.currentTitle, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedState, nil];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}

-(SlideNavigationController*) currentNavigationController{
    AppDelegate* appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    return appDelegate.rootVC;
}

- (IBAction)actionCuisine:(id)sender{
    _icCuisineError.hidden = YES;
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = CUISINES;
    controller.delegate = self;
    controller.isCloseAfterClick = YES;
    controller.selectedValues =[[NSMutableArray alloc] initWithArray:selectedCuisines] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithArray:selectedCuisinesKey];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}
-(void) showRecommendDetails:(GouetmetRecommendDetailsObj*) recommendDetails{
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_btCountry setSelected:NO];
    [_btCountry setTitle:recommendDetails.country forState:UIControlStateNormal];
    selectedCountry = recommendDetails.country;
    selectedCity = recommendDetails.city;
    _txtCity.text = recommendDetails.city;
    _bookingId = recommendDetails.bookingId;
    if([recommendDetails.country isEqualToString:NSLocalizedString(@"Canada", nil)] || [recommendDetails.country isEqualToString:NSLocalizedString(@"United States", nil)]){
        _pvState.hidden = NO;
        [_btState setSelected:NO];
        selectedState = recommendDetails.stateValue;
        [_btState setTitle:recommendDetails.stateValue forState:UIControlStateNormal];
    }else{
        _pvState.hidden = YES;
    }
    
    datepickerReservation.date = recommendDetails.reservationDate;
    timepickerReservation.date = recommendDetails.reservationTime;
    
    _txtMaximumBudget.text = recommendDetails.MaximumPrice;
    _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    _txtReservationTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerReservation.date, TIME_FORMAT)];
    
    selectedOccasion = [recommendDetails occasionValues];
    selectedOccasionKey = [recommendDetails occasionKeys];
    selectedCuisinesKey = [recommendDetails cuisineKeys];
    selectedCuisines = [recommendDetails cuisineValues];
    
    [self.btOccasion setTitle:convertArrayToString(selectedOccasion) forState:UIControlStateNormal];
    [_btOccasion setSelected:NO];
    if(self.btOccasion.currentTitle.length == 0){
        [_btOccasion setSelected:YES];
        [self.btOccasion setTitle:OCCASION_HINT_TEXT forState:UIControlStateNormal];
    }
    
    [self.btCuisine setTitle:convertArrayToString(selectedCuisines) forState:UIControlStateNormal];
    [_btCuisine setSelected:NO];
    if(self.btCuisine.currentTitle.length == 0){
        [_btCuisine setSelected:YES];
        [self.btCuisine setTitle:CUISINE_HINT_TEXT forState:UIControlStateNormal];
    }
    
    [_optionalDetailsView showGourmetRecommendDetails:recommendDetails];
}
-(void) setMyPreferencce:(NSString*)cuisine withOther:(NSString*)other{
    NSMutableArray* prefValues = [NSMutableArray array];
    NSArray* arr= convertStringToArray(cuisine);
    if(arr.count>0 && cuisine.length>0){
        prefValues = [[NSMutableArray alloc] initWithArray:arr];
    }
    if(other.length>0){
        [prefValues addObject:other];
    }

    if(prefValues.count > 0){
        selectedCuisines = prefValues;
        selectedCuisinesKey = [NSMutableArray arrayWithArray:prefValues];
    }
    [self.btCuisine setTitle:convertArrayToString(selectedCuisines) forState:UIControlStateNormal];
    [_btCuisine setSelected:NO];
    if(self.btCuisine.currentTitle.length == 0){
        [_btCuisine setSelected:YES];
        [self.btCuisine setTitle:CUISINE_HINT_TEXT forState:UIControlStateNormal];
    }
}
- (IBAction)actionSubmitRequest:(id)sender {
    [sender setUserInteractionEnabled:false];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
        [sender setUserInteractionEnabled:true];
    });
    GourmetRecommendRequestObj* object = [self validateAndCollectData];
    if(object && _delegate){
        [_delegate submitRequest:object forType:GOURMET_RECOMMEND_REQUEST];
    }else {
        //showAlertOneButton(self,@"Warning", @"Please input correct values.", @"OK");
    }
}

-(GourmetRecommendRequestObj*) validateAndCollectData{
    for (UIView *item in [dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }

    [_optionalDetailsView exportValues];
    
    GourmetRecommendRequestObj* result = [[GourmetRecommendRequestObj alloc] init];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.BookingId = _bookingId;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.MobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.EmailAddress = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    result.reservationDate = formatDate(datepickerReservation.date, DATE_SEND_REQUEST_FORMAT);
    result.reservationTime = formatDate(timepickerReservation.date, TIME_SEND_REQUEST_FORMAT);
    if (!isValidDateTimeWithinHalfAnHour(datepickerReservation.date, timepickerReservation.date)) {
        //_icTimeError.hidden = NO;
        //isEnoughInfo = NO;
    }
    result.MaximumPrice = _txtMaximumBudget.text;
    
    if(selectedCuisinesKey.count>0)
        result.Cuisines = selectedCuisinesKey;
    else
    {
        isEnoughInfo = NO;
        _icCuisineError.hidden = NO;
        if(selectedCuisinesKey.count==0){
            _icCuisineError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    if(selectedOccasionKey.count>0){
        result.Occasion = selectedOccasionKey;
    }else{
        //isEnoughInfo = NO;
        //_icOccasionError.hidden = NO;
    }
    
    result.adultPax = _optionalDetailsView.adultPax;
    result.kidsPax = _optionalDetailsView.kidsPax;
    
    value = _txtCity.text;
    if(isValidValue(value)){
        result.City = value;
    } else {
        if(value.length==0){
            _icCityError.errorMessage = MANDATORY_BLANK;
        }
        [_icCityError setHidden:NO];
        isEnoughInfo = NO;
    }
    
    value = selectedCountry;
    if(isValidValue(value)){
        result.Country = value;
    } else {
        isEnoughInfo = NO;
        _icCountryError.hidden = NO;
        if(value.length==0){
            _icCountryError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        value = selectedState;
        if([self isValidValue:value]){
            result.State = value;
        }else{
            isEnoughInfo = NO;
            if(value.length==0){
                _icStateError.errorMessage = MANDATORY_BLANK;
            }
            [_icStateError setHidden:NO];
        }
    }
    
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.SpecialRequirements = value;
        } else {
            isEnoughInfo = NO;
        }
    }
    
    
    result.photo = _optionalDetailsView.selectedPhoto;
    
    if(isEnoughInfo){
        return result;
    } else {
        GourmetRecommendRequestObj *temp;
        return temp;
    }
}

-(BOOL) isValidValue:(NSString*) text{
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(text==nil || text.length == 0){
        return NO;
    }
    return YES;
}
-(void) touchinDetailsView{
  
}

-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_icCuisineError forKey:[NSNumber numberWithInteger:_icCuisineError.tag]];
    [dictIconErrorManager setObject:_icCountryError forKey:[NSNumber numberWithInteger:_icCountryError.tag]];
    [dictIconErrorManager setObject:_icOccasionError forKey:[NSNumber numberWithInteger:_icOccasionError.tag]];
    [dictIconErrorManager setObject:_icTimeError forKey:[NSNumber numberWithInteger:_icTimeError.tag]];
    [dictIconErrorManager setObject:_icCityError forKey:[NSNumber numberWithInteger:_icCityError.tag]];
    for (UIView *item in [dictIconErrorManager allValues]) {
        if([item isKindOfClass:[ErrorToolTip class]]){
            ((ErrorToolTip*)item).delegate = self;
        }
    }
}

- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
        }
    }
}

-(void) setTimeToCurrentTime{
    timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval: 60*30];
    _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
}


-(void)setTextForViews {
   
    _lbCountry.attributedText = [self setRequireString:NSLocalizedString(@"Country", nil)];
    //[_btCountry setTitle:NSLocalizedString(@"Please select country of preferred restaurant", nil) forState:UIControlStateNormal];
    
    
    _lbCity.attributedText = [self setRequireString:NSLocalizedString(@"City", nil)];
    _txtCity.placeholder = NSLocalizedString(@"Please enter city of preferred restaurant", nil);
    
    _lbState.attributedText = [self setRequireString: NSLocalizedString(@"State", @"")];
    [_btState setTitle:NSLocalizedString(@"Please enter state of preferred restaurant", nil) forState:UIControlStateNormal];
    
    
    _lbReservationDate.attributedText = [self setRequireString:NSLocalizedString(@"Reservation date book_dinging", nil)];
    _lbReservationTime.attributedText = [self setRequireString:NSLocalizedString(@"Reservation time", nil)];
    
    _lBOccasion.text = NSLocalizedString(@"Occasion", nil);
    
    [_btOccasion setTitle:NSLocalizedString(@"Please select occasion book_dinging", nil) forState:UIControlStateNormal];
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_btCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    
    _lbCuisine.attributedText = [self setRequireString:NSLocalizedString(@"Cuisine", nil)];
    [_btCuisine setTitle:NSLocalizedString(@"Please select your preferred cuisine", nil) forState:UIControlStateNormal];
    
    
    _lbMaxBudget.text = NSLocalizedString(@"Maximum budget per person", nil);
    _txtMaximumBudget.placeholder = NSLocalizedString(@"Please enter your maximum budget per person", nil);
    
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}

@end
