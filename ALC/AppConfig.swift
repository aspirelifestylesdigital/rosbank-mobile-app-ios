//
//  AppConfig.swift
//  ALCQC
//
//  Created by mac-216 on 7/5/18.
//  Copyright © 2018 Sunrise Software Solutions. All rights reserved.
//

import Foundation
import UIKit

enum AppConfigType: String {
    
    case rba
    case rpb
    
    init?(conciergeCode: String){
        switch conciergeCode {
        case "230385": self = .rba
        case "090284": self = .rpb
        default: return nil
        }
    }
    
    var consumerKey: String {
        return self.rawValue.uppercased()
    }
    
    var consumerSecret: String {
        switch self {
            case .rba: return "RBA-CD32DD06-785"
            case .rpb: return "RPB-CD32DD06-785"
        }
    }
    
    var callBackURL: String {
        switch self {
        case .rba: return "urn:ietf:wg:oauth:2.0:oob"
        case .rpb: return "urn:ietf:wg:oauth:2.0:oob"
        }
    }
    
    var verificationCode: String {
        switch self {
        case .rba: return "mowRBA1r"
        case .rpb: return "mowRPB1R"
        }
    }
    
    var clientCode: String {
        return self.rawValue.uppercased()
    }
    
    var programCode: String {
        switch self {
        case .rba: return "RBA CONC"
        case .rpb: return "RPB CONCIERGE L HERMITAGE MED LEG"
        }
    }
    
    var companyLightLogo: String {
        switch self {
        case .rba: return "company_light_logo"
        case .rpb: return "lhermitage_logo"
        }
    }
    
    var companyDarkLogo: String {
        switch self {
        case .rba: return "company_dark_logo"
        case .rpb: return "lhermitage_logo"
        }
    }
    
    var companyPhoneNumber: String {
        switch self {
        case .rba: return "+74995004411"
        case .rpb: return "+74995004414"
        }
    }
    
    var conciergeServiceName: String {
        switch self {
        case .rba: return NSLocalizedString("ROSBANK", comment: "")
        case .rpb: return NSLocalizedString("L'Hermitage", comment: "")
        }
    }
    
    var conciergePhoneNumber: String {
        return "+74995004423"
    }
    
}

@objc class AppConfig: NSObject {
    
    static let shared = AppConfig()
    
    private let consumerKeyValue = "ConsumerKey"
    
    private let lock = NSLock()
    private var config: AppConfigType
    
    override init() {
        let defaults = UserDefaults.standard
        var newConfig: AppConfigType?
        if let consumerKeyValue = defaults.string(forKey: consumerKeyValue) {
            newConfig = AppConfigType(rawValue: consumerKeyValue)
        }
        if newConfig != nil {
            config = newConfig!
        } else {
            config = .rba
        }
    }
    
    public func setConciergeCode(_ code: String){
        lock.lock()
        defer { lock.unlock() }
        guard let newConfig = AppConfigType(conciergeCode: code) else {
            return
        }
        config = newConfig
        let defaults = UserDefaults.standard
        defaults.set(config.rawValue, forKey: consumerKeyValue)
        defaults.synchronize()
    }
    
    var consumerKey: String {
        return config.consumerKey
    }
    
    var consumerSecret: String {
        return config.consumerSecret
    }
    
    var callBackURL: String {
        return config.callBackURL
    }
    
    var verificationCode: String {
        return config.verificationCode
    }
    
    var clientCode: String {
        return config.clientCode
    }
    
    var programCode: String {
        return config.programCode
    }
    
    var companyLightLogo: UIImage? {
        return UIImage(named: config.companyLightLogo)
    }
    
    var companyDarkLogo: UIImage? {
        return UIImage(named: config.companyDarkLogo)
    }
    
    var companyPhoneNumber: String {
        return config.companyPhoneNumber
    }
    
    var conciergePhoneNumber: String {
        return config.conciergePhoneNumber
    }
    
    var conciergeServiceName: String {
        return config.conciergeServiceName
    }
    
    public func reset(){
        config = .rba
        let defaults = UserDefaults.standard
        defaults.set(config.rawValue, forKey: consumerKeyValue)
        defaults.synchronize()
    }
    
    public func isConciergeCodeValid(_ code: String) -> Bool {
        return AppConfigType(conciergeCode: code) != nil
    }
}
