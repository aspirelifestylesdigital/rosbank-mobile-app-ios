//
//  MyRequestObject.m
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MyRequestObject.h"


#define DISPLAY_DATETIME [NSString stringWithFormat:@"dd MMM yyyy %@ hh:mm a", NSLocalizedString(@"@", nil)]

//#define DISPLAY_DATETIME @"dd MMM yyyy @ hh:mm a"
#define DISPLAY_DATE @"dd MMM yyyy"
#define FULFILLED_STATUS @"Closed"
#define OPEN_STATUS @"Open"
#define IN_PROGRESS_STATUS @"In progress"
#define CANCELED @"CANCEL"

@implementation MyRequestObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    _requestId = [dict stringForKey:@"TransactionID"];
    _bookingItemId = [dict stringForKey:@"TransactionID"];
    _bookingTypeGroup = [dict stringForKey:@"REQUESTTYPE"];
    _bookingType = [dict stringForKey:@"REQUESTTYPE"];
    _functionality = [dict stringForKey:@"FUNCTIONALITY"];
    _requestDetails = convertRequestDetailsStringToDict([dict stringForKey:@"REQUESTDETAILS"]);
    
    _city = [dict stringForKey:@"CITY"];
    _country = [_requestDetails stringForKey:COUNTRY_REQUIREMENT];
    _stateValue = [_requestDetails stringForKey:StateRequirement];
    
    //Check some special value for DMA api
    NSString* tempValue = [dict stringForKey:@"EVENTNAME"];
    if(tempValue && tempValue.length>0){
        [_requestDetails setValue:tempValue forKey:Event_Name];
    }
    
    if(!_city || _city.length == 0){
        _city = [_requestDetails stringForKey:CITY_GOLF_REQUIREMENT];
    }
    //end
    
    if(_city && _country){
        _address = [NSString stringWithFormat:@"%@, %@", _city, _country ];
    }
    
    _parsedRequestType = [self parsedRequestType: _bookingType functionality:_functionality];
    _requestStatus = [dict stringForKey:@"REQUESTSTATUS"];
    _requestMode = [dict stringForKey:@"REQUESTMODE"];
    _isDeleted = isEqualIgnoreCase(_requestMode, @"cancel") ? true : false;
    
    _itemTitle = [self setDefaultItemTitle];    
    
    
    
    [self parseDateForRequestType:_parsedRequestType from:dict];
    
    if(_happenDateTime){
        _isHistory = (_isDeleted || (!((_parsedRequestType == OTHER_REQUEST) || (_parsedRequestType == SPA_REQUEST)) && ([_happenDateTime compare:[NSDate date]] == NSOrderedAscending)));
        
        _notDisplayEditCancel = _isHistory? YES:_isDeleted;
    } else {
        _isHistory = YES;
        _notDisplayEditCancel = YES;
    }
    
    _fullDescription = [self createFullDescription];
    return self;
}

-(NSString*) setDefaultItemTitle{
    switch (_parsedRequestType) {
        case HOTEL_REQUEST:
            return [_requestDetails stringForKey:HOTEL_NAME_REQUIREMENT];
        case HOTEL_RECOMMEND_REQUEST:
            return NSLocalizedString(@"Get hotel recommendation", nil);
        case GOURMET_RECOMMEND_REQUEST:
            return NSLocalizedString(@"Get dining recommendation", nil);
        case GOURMET_REQUEST:
            return [_requestDetails stringForKey:RESTAURANT_NAME_REQUIREMENT];
        case GOLF_REQUEST:
            return [_requestDetails stringForKey:GolfCourseName];
        case SPA_REQUEST:
            return [_requestDetails stringForKey:SpaName];
        case CAR_RENTAL_REQUEST:
            return NSLocalizedString(@"Car rental", nil);
        case CAR_TRANSFER_REQUEST:
            return [NSLocalizedString(@"Transfer by ", nil) stringByAppendingString:[_requestDetails stringForKey:TransportType]];
        case OTHER_REQUEST:
            return NSLocalizedString(@"Other requests request_cell", nil);
        case ENTERTAINMENT_REQUEST:
            return [_requestDetails stringForKey:Event_Name];
        case ENTERTAINMENT_RECOMMEND_REQUEST:
            return NSLocalizedString(@"Get entertainment recommendation", nil);
        default:
            return @"";
    }
}


- (void) parseDateForRequestType:(enum REQUEST_TYPE) type from:(NSDictionary*) dict{
    _createDate = convertStringToDateWithTimeZone([dict stringForKey:@"CREATEDDATE"],@"yyyy-MM-dd'T'HH:mm:ss.SSS", UTC_TIMEZONE);
    _happenDateTime = _createDate;
    switch (type) {
        case GOURMET_REQUEST:
        case GOURMET_RECOMMEND_REQUEST:
            _reservationDateTime = convertStringToDate([dict stringForKey:@"DELIVERY"], DATETIME_SEND_REQUEST_FORMAT);
            _happenDateTime = _reservationDateTime;
            break;
        case ENTERTAINMENT_REQUEST:
            _eventDateTime = convertStringToDate([dict stringForKey:@"DELIVERY"], DATETIME_SEND_REQUEST_FORMAT);
            _happenDateTime = _eventDateTime;
            break;
        case ENTERTAINMENT_RECOMMEND_REQUEST:
            _eventDateTime = convertStringToDate([dict stringForKey:@"DELIVERY"], DATETIME_SEND_REQUEST_FORMAT);
            _happenDateTime = _eventDateTime;
            break;
        case HOTEL_REQUEST:
        case HOTEL_RECOMMEND_REQUEST:
            _checkInDate = convertStringToDate([dict stringForKey:@"CHECKIN"], DATETIME_SEND_REQUEST_FORMAT);
            _checkOutDate = convertStringToDate([dict stringForKey:@"CHECKOUT"], DATETIME_SEND_REQUEST_FORMAT);
            _happenDateTime = _checkInDate;
            break;
        case CAR_RENTAL_REQUEST:
            _pickUpDateTime = convertStringToDate([dict stringForKey:@"PICKUP"], DATETIME_SEND_REQUEST_FORMAT);
            if([_requestDetails objectForKey:DropoffDate]){
                _dropOffDateTime = convertStringToDate([NSString stringWithFormat:@"%@ %@",[_requestDetails stringForKey:DropoffDate], [_requestDetails stringForKey:DropoffTime]], DATETIME_SEND_REQUEST_FORMAT);
            }
            _happenDateTime = _pickUpDateTime;
            break;
        case CAR_TRANSFER_REQUEST:
            _pickUpDateTime = convertStringToDate([dict stringForKey:@"PICKUP"], DATETIME_SEND_REQUEST_FORMAT);
            _happenDateTime = _pickUpDateTime;
            break;
        case GOLF_REQUEST:
            _golfCourseDateTime =  convertStringToDate([NSString stringWithFormat:@"%@ %@",[_requestDetails stringForKey:GolfDateOfPlay], [_requestDetails stringForKey:TeeTime]], DATETIME_SEND_REQUEST_FORMAT);
            _happenDateTime = _golfCourseDateTime;
            break;
        case SPA_REQUEST:
        case OTHER_REQUEST:
            _happenDateTime = convertStringToDate([dict stringForKey:@"CREATEDDATE"],@"yyyy-MM-dd'T'HH:mm:ss.SSS");
            break;
            
        default:
            break;
    }
}


-(enum REQUEST_TYPE) parsedRequestType:(NSString*)type functionality:(NSString*)functionality {
    if(isEqualIgnoreCase(type, RESTAURANT_REQUEST_TYPE)){
        return [_requestDetails objectForKey:RESTAURANT_NAME_REQUIREMENT]
        ? GOURMET_REQUEST
        : GOURMET_RECOMMEND_REQUEST;
    } else if(isEqualIgnoreCase(type, GOLF_REQUEST_TYPE) && [_requestDetails stringForKey:GolfCourseName].length > 0){
        return GOLF_REQUEST;
    } else if(isEqualIgnoreCase(type, OTHER_REQUEST_TYPE) && [_requestDetails stringForKey:SpaName].length > 0){
        return SPA_REQUEST;
    } else if(isEqualIgnoreCase(type, CAR_RENTAL_REQUEST_TYPE)){
        return CAR_RENTAL_REQUEST;
    } else if(isEqualIgnoreCase(type, TRANSFER_LIMO_REQUEST_TYPE)){
        return CAR_TRANSFER_REQUEST;
    }  else if(isEqualIgnoreCase(type, HOTEL_REQUEST_TYPE)){
        return [_requestDetails objectForKey:HOTEL_NAME_REQUIREMENT]
        ? HOTEL_REQUEST
        : HOTEL_RECOMMEND_REQUEST;
    } else if(isEqualIgnoreCase(type, EVENT_REQUEST_TYPE)){
        return  [functionality isEqual:EVENT_BOOK_FUNCTION] ? ENTERTAINMENT_REQUEST : ENTERTAINMENT_RECOMMEND_REQUEST;
    }
    return OTHER_REQUEST;
}

-(NSString*) createFullDescription{
    switch (_parsedRequestType) {
        case GOURMET_RECOMMEND_REQUEST:
        case GOURMET_REQUEST:{
            NSLog(@"%@", [self concateAllDescription:[NSMutableArray arrayWithObjects:_address,
                                                     [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Reservation date", nil), formatDateToString(_reservationDateTime, DISPLAY_DATETIME)], nil]]);
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:_address,
                                                [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Reservation date", nil), formatDateToString(_reservationDateTime, DISPLAY_DATETIME)], nil]];
        }
        case HOTEL_RECOMMEND_REQUEST:
        case HOTEL_REQUEST:{
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:_address,
                                                [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Check-in date", nil),  formatDateToString(_checkInDate, DISPLAY_DATE)],
                                                [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Check-out date", nil), formatDateToString(_checkOutDate, DISPLAY_DATE)],nil]];
        }
        case GOLF_REQUEST:
        {
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:_address,
                                                [NSString stringWithFormat:@"%@: %@",
                                                 NSLocalizedString(@"Date of play golf_request", nil), formatDateToString(_golfCourseDateTime, DISPLAY_DATETIME)],
                                                nil]];
        }
        case CAR_RENTAL_REQUEST:{
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:
                                                [NSString stringWithFormat:@"%@ @ %@",
                                                 NSLocalizedString(@"Pick up", nil),  [_requestDetails stringForKey:PickLocation]],
                                                [NSString stringWithFormat:@"%@: %@",
                                                 NSLocalizedString(@"Pick up date car_rental_request", nil),  formatDateToString(_pickUpDateTime, DISPLAY_DATETIME)],
                                                [NSString stringWithFormat:@"%@ @ %@",
                                                 NSLocalizedString(@"Drop off", nil),  [_requestDetails stringForKey:DropLocation]],
                                                [NSString stringWithFormat:@"%@: %@",
                                                 NSLocalizedString(@"Drop off date car_rental_request", nil),  formatDateToString(_dropOffDateTime, DISPLAY_DATETIME)],
                                                nil]];
        }
        case CAR_TRANSFER_REQUEST:{
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:
                                                [NSString stringWithFormat:@"%@ @ %@",
                                                 NSLocalizedString(@"Pick up", nil),
                                                 [_requestDetails stringForKey:PickLocation]],
                                                [NSString stringWithFormat:@"%@: %@",
                                                 NSLocalizedString(@"Pick up date car_rental_request", nil), formatDateToString(_pickUpDateTime, DISPLAY_DATETIME)],
                                                [NSString stringWithFormat:@"%@ @ %@",
                                                 NSLocalizedString(@"Drop off", nil),
                                                 [_requestDetails stringForKey:DropLocation]],
                                                nil]];
        }
        case OTHER_REQUEST:
            return [_requestDetails stringForKey:SPECIAL_REQUIREMENT];
            
        case ENTERTAINMENT_REQUEST:
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:
                                                _address,
                                                [NSString stringWithFormat:@"%@: %@",
                                                 NSLocalizedString(@"Event date request", nil), formatDateToString(_eventDateTime, DISPLAY_DATETIME)],
                                                nil]];
        case ENTERTAINMENT_RECOMMEND_REQUEST:{
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:
                                                _address,
                                                [NSString stringWithFormat:@"%@: %@",
                                                 NSLocalizedString(@"Event date", nil), formatDateToString(_eventDateTime, DISPLAY_DATE)],
                                                nil]];
        }
        case SPA_REQUEST:{
            return [self concateAllDescription:[NSMutableArray arrayWithObjects:_address, nil       ]];
        }
        default:
            return @"";
    }
}

-(NSString*) concateAllDescription:(NSMutableArray*) fields{
    NSString* result = @"";
    for (NSString* value in fields) {
        if(!value || value.length == 0){
            continue;
        }
        if(result.length>0){
            result = [result stringByAppendingString:@"\n"];
        }
        result = [result stringByAppendingString:value];
    }
    return result;
}

-(BOOL) notAllowEditCancel{
    if(_parsedRequestType == HOTEL_REQUEST || _parsedRequestType == HOTEL_RECOMMEND_REQUEST){
        _isHistory = ![formatDateToString(_happenDateTime, DATE_FORMAT) isEqualToString:formatDateToString([NSDate date], DATE_FORMAT)]
        && ([_happenDateTime compare:[NSDate date]] == NSOrderedAscending);
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: _happenDateTime];
        [components setHour: 23];
        [components setMinute: 59];
        [components setSecond: 59];
        NSDate *adjustedHotelCheckin = [gregorian dateFromComponents: components];
        
        return (isWithin24h(adjustedHotelCheckin, [NSDate date]));
    } else {
        return (!((_parsedRequestType == OTHER_REQUEST)
                  || (_parsedRequestType == SPA_REQUEST))&& isWithin24h(_happenDateTime, [NSDate date]));
    }
}
@end
