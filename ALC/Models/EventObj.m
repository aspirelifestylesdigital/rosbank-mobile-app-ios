//
//  EventObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/25/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "EventObj.h"

@implementation EventObj

@synthesize bookingID;
@synthesize identify;


-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:bookingID forKey:@"bookingID"];
    [encoder encodeObject:identify forKey:@"identify"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self.bookingID = [decoder decodeObjectForKey:@"bookingID"];
    self.identify = [decoder decodeObjectForKey:@"identify"];
    return self;
}

@end
