//
//  UserObject.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserObject : NSObject
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* password;
@property (strong, nonatomic) NSString* userId;
@property (strong, nonatomic) NSString* avatarUrl;
@property (strong, nonatomic) NSString* salutation;
@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSString* fullName;
@property (strong, nonatomic) NSString* dateOfBirth;
@property (strong, nonatomic) NSString* countryOfBirth;
@property (strong, nonatomic) NSString* mobileNumber;
@property (strong, nonatomic) NSString* zipCode;
@property (strong, nonatomic) NSString* interestMusics;
@property (strong, nonatomic) NSString* interestCuisines;
@property (strong, nonatomic) NSString* interestBrands;
@property (strong, nonatomic) NSString* nameOnCard;
@property (strong, nonatomic) NSString* memberCardId;
@property (strong, nonatomic) NSString* cardExpirydDate;
@property (strong, nonatomic) NSString* currentCountry;
@property (strong, nonatomic) NSString* currentCity;
@property (strong, nonatomic) NSString* currentRegionCode;
@property (strong, nonatomic) NSString* countryCode;
@property (strong, nonatomic) NSString* shortMobileNumber;
@property (strong, nonatomic) NSString* ONLINEMEMBERDETAILID;
@property (strong, nonatomic) NSString* VERIFICATIONCODE;
-(Boolean) isLoggedIn;
- (id) initFromDict:(NSDictionary*) dict;
-(BOOL)isValidMobileNumber:(NSString*)checkString;
@end
