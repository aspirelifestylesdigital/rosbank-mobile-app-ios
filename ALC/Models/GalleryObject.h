//
//  GalleryObject.h
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GalleryObject : NSObject
@property (strong, nonatomic) NSString* captionDescription;
@property (strong, nonatomic) NSString* captionTitle;
@property (strong, nonatomic) NSString* backgroundImgURL;
@property (nonatomic) Boolean isVideo;
@property (strong, nonatomic) NSString* videoLink;
@property (strong, nonatomic) NSString* imageAlt;
-(id) initFromDict:(NSDictionary*) dict;
@end
