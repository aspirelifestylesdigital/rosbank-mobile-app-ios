//
//  CarRentalDetailsObject.h
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface CarRentalDetailsObject : BaseBookingDetailsObject
@property (nonatomic) NSInteger driverAge;
@property (nonatomic) BOOL isInternationalLicense;
@property (strong, nonatomic) NSDate *pickUpDate;
@property (strong, nonatomic) NSDate* pickUpTime;
@property (strong, nonatomic) NSDate* dropOffDate;
@property (strong, nonatomic) NSDate* dropOffTime;
@property (strong, nonatomic) NSString* driverName;
@property (strong, nonatomic) NSString* pickUpLocation;
@property (strong, nonatomic) NSString* dropOffLocation;
@property (strong, nonatomic) NSArray* prefferedVerhicles;
@property (strong, nonatomic) NSArray* prefferedRentals;
@property (strong, nonatomic) NSString* minimumPrice;
@property (strong, nonatomic) NSString* maximumPrice;


-(id) initFromDict:(NSDictionary*) dict;
-(NSMutableArray*) prefferedRentalKeys;
-(NSMutableArray*) prefferedRentalValues;
-(NSMutableArray*) prefferedVerhicleKeys;
-(NSMutableArray*) prefferedVerhicleValues;
@end
