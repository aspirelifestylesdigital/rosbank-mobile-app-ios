//
//  GourmetMenuObject.m
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetMenuObject.h"

@implementation GourmetMenuObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.menuId = [dict stringForKey:@"ID"];
    self.groupName = [dict stringForKey:@"GroupName"];
    self.name = [dict stringForKey:@"Name"];
    self.menuDescription = [dict stringForKey:@"Description"];
    self.imageURL = [DOMAIN_URL stringByAppendingString:[dict stringForKey:@"ImageUrl"]];
    self.price = [dict stringForKey:@"Price"];
    return self;
}
@end
