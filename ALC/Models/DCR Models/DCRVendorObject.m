//
//  DCRVendorObject.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRVendorObject.h"

@implementation DCRVendorObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.vendorId = [dict stringForKey:@"id"];
    self.name = [dict stringForKey:@"name"];
    self.promotionCode = [dict stringForKey:@"promotion_code"];
    return self;
}

@end
