//
//  DCRImageObject.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRImageObject.h"

@implementation DCRImageObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.url = [dict stringOfURLForKey:@"url"] ;
    self.defaultFlag = [dict boolForKey:@"default"];
    return self;
}
@end
