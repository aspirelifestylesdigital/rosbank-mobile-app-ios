//
//  DCROpeningHourObject.m
//  ALC
//
//  Created by Anh Tran on 3/15/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCROpeningHourObject.h"

@implementation DCROpeningHourObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.openingFromDay = [dict stringForKey:@"opening_from_day"];
    self.openingToDay = [dict stringForKey:@"opening_to_day"];
    self.openingToHour = [[dict stringForKey:@"opening_to_hour"] stringByReplacingOccurrencesOfString:@"24:00" withString:@"23:59"];
    self.openingFromHour = [dict stringForKey:@"opening_from_hour"];
    
    return self;
}

-(NSString*) fullValue{
    return [NSString stringWithFormat:@"%@ - %@: %@ - %@", self.openingFromDay, self.openingToDay, self.openingFromHour, self.openingToHour];
}
@end
