//
//  GouetmetRecommendDetailsObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GouetmetRecommendDetailsObj.h"

@implementation GouetmetRecommendDetailsObj

-(id) initFromDict:(NSDictionary*) dict
{
    self = [super initFromDict:dict];
    
    _MinimumPrice = @"";
    _MaximumPrice = [self.requestDetails stringForKey:MAX_PRICE_REQUIREMENT];
    
    
    _reservationTime = self.reservationDateTime;
    _reservationDate  = self.reservationDateTime;
    _reservationName = self.guestName;
    
    NSString* tempValue = [self.requestDetails stringForKey:CUISINE_REQUIREMENT];
    if([tempValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0){
        _cuisine = convertStringToArray(tempValue);
    } else {
        _cuisine = [NSArray array];
    }
    
    
    tempValue =[self.requestDetails stringForKey:OCCASION_REQUIREMENT];
    if([tempValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0){
        _occasion = convertStringToArray(tempValue);
    } else {
        _occasion = [NSArray array];
    }
    
    _foodAllergies = [self.requestDetails stringForKey:FoodAllergies];
    
    return self;
}
-(NSMutableArray*) cuisineKeys{
    return [NSMutableArray arrayWithArray:_cuisine];
}
-(NSMutableArray*) cuisineValues{
    return [NSMutableArray arrayWithArray:_cuisine];
}
-(NSMutableArray*) occasionKeys{
    return [NSMutableArray arrayWithArray:_occasion];
}
-(NSMutableArray*) occasionValues{
    return [NSMutableArray arrayWithArray:_occasion];
}

@end
