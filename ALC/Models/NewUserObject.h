//
//  NewUserObject.h
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewUserObject : NSObject
@property (strong, nonatomic) NSString* userID;
@property (strong, nonatomic) NSString* salutation;
@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSString* ccNumber;
@property (strong, nonatomic) NSString* phoneNumber;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* password;
@property (strong, nonatomic) NSString* cardNumber;
@property (strong, nonatomic) NSString* postalCode;
@end
