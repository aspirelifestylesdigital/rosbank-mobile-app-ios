//
//  HotelRequestObject.m
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HotelRequestObject.h"

@implementation HotelRequestObject
-(id) initFromDetails:(HotelRequestDetailsObject*)detailsObj{
    self = [super init];
    self.bookingId = self.bookingItemId = detailsObj.bookingId;
    self.reservationName = detailsObj.guestName;
    self.email = detailsObj.email;
    self.mobileNumber = detailsObj.mobileNumber;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    self.specialMessage = detailsObj.specialRequirement;
    self.hotelName  = [detailsObj.requestDetails stringForKey:HOTEL_NAME_REQUIREMENT];
    self.adultPax  = detailsObj.adulsPax;
    self.kidsPax = detailsObj.kidsPax;
    self.checkInDate = formatDate(detailsObj.checkInDate, DATETIME_SEND_REQUEST_FORMAT);
    self.checkOutDate = formatDate(detailsObj.checkOutDate, DATETIME_SEND_REQUEST_FORMAT);
    self.roomType = detailsObj.roomTypeValue;
    self.guestName = detailsObj.guestName;
    self.country = detailsObj.country;
    self.city = detailsObj.city;
    self.State = detailsObj.stateValue;
    self.loyyaltyProgram = detailsObj.loyaltyMembership;
    self.isSmoking = detailsObj.isSmoking;
    self.isWithWifi = detailsObj.isWithWifi;
    self.isWithBreakfast = detailsObj.isWithBreakfast;
    
    self.maximumPrice = detailsObj.maximumPrice;
    self.noOfRooms = detailsObj.noOfRooms;
    [self setPrivilegeData:detailsObj.requestDetails];
    return self;
}

@end
