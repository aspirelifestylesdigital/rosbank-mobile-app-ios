//
//  CurrencyObject.h
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyObject : NSObject
@property (nonatomic, strong) NSString* currencyKey;
@property (nonatomic, strong) NSString* currencyText;
-(id) initFromDict:(NSDictionary*) dict;
-(id) init:(NSString*) key withValue:(NSString*) value;
-(NSString*) fullKeyvalue;
@end
