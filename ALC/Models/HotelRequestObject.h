//
//  HotelRequestObject.h
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoResizedInfo.h"
#import "HotelRequestDetailsObject.h"
#import "BaseBookingRequestObject.h"

@interface HotelRequestObject : BaseBookingRequestObject
@property (strong, nonatomic) NSString* bookingId;
@property (strong, nonatomic) NSString* userId;
@property (strong, nonatomic) NSString* bookingItemId;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* mobileNumber;
@property (strong, nonatomic) NSString* hotelName;
@property ( nonatomic) NSInteger adultPax;
@property ( nonatomic) NSInteger kidsPax;
@property ( nonatomic) NSInteger noOfRooms;
@property (strong, nonatomic) NSString* checkInDate;
@property (strong, nonatomic) NSString* checkOutDate;
@property (strong, nonatomic) NSString* roomType;
@property (strong, nonatomic) NSString* guestName;
@property (strong, nonatomic) NSString* country;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* State;
@property (strong, nonatomic) NSString* loyyaltyProgram;
@property (strong, nonatomic) NSString* membershipNo;
@property (strong, nonatomic) PhotoResizedInfo* photo;
@property ( nonatomic) BOOL isSmoking;
@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;
@property ( nonatomic) BOOL isWithBreakfast;
@property ( nonatomic) BOOL isWithWifi;
@property (strong, nonatomic) NSString* specialMessage;
@property (strong, nonatomic) NSString* maximumPrice;
@property (strong, nonatomic) NSString* reservationName;
@property (strong, nonatomic) NSString* anyRequest;
@property ( nonatomic) BOOL isEdit;
@property (strong, nonatomic) NSString* EditType;

-(id) initFromDetails:(HotelRequestDetailsObject*)detailsObj;
@end
