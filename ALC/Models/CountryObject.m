//
//  CountryObject.m
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CountryObject.h"

@implementation CountryObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    _countryId = [dict stringForKey:@"Id"];
    _countryCode = [dict stringForKey:@"CountryCode"];
    _countryName = [dict stringForKey:@"CountryName"];
    _region = [dict stringForKey:@"RegionOfCountry"];
    return self;
}
@end
