//
//  HotelRequestDetailsObject.m
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HotelRequestDetailsObject.h"

@implementation HotelRequestDetailsObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    _hotelName = [self.requestDetails stringForKey:HOTEL_NAME_REQUIREMENT];
    self.guestName = [self.requestDetails stringForKey:GUEST_NAME_REQUIREMENT];
    
    _minimumPrice = @"";
    _maximumPrice = [self.requestDetails stringForKey:MAX_PRICE_REQUIREMENT];
    _anyRequest = [self.requestDetails stringForKey:AdditionalPreference];
    
    _startRatingKey = @"";
    _startRatingValue = 0;
    self.isWithWifi = [self.requestDetails boolForKey:WithWifi];
    self.isWithBreakfast = [self.requestDetails boolForKey:WithBreakfast];
    self.noOfRooms = [self.requestDetails integerForKey:NumberOfRooms];
    return self;
}

-(NSString*) privilegeName{
    return _hotelName;
}
@end
