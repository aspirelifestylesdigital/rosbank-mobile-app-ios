//
//  HGWPostBaseRequest.m
//  HungryGoWhere
//
//  Created by Linh Le on 1/10/13.
//  Copyright (c) 2013 Linh Le. All rights reserved.
//

#import "HGWPostBaseRequest.h"
#import "Common.h"
#import "NSString+MD5.h"

@implementation HGWPostBaseRequest

@synthesize secret;

- (NSString*)getParametersStringInOrder:(NSMutableDictionary*)keyValues encode:(BOOL)isEncode
{
    if (keyValues == nil || (keyValues != nil && [[keyValues allKeys]count] == 0)) {
        return nil;
    }
    
    NSMutableString *parameters = [[NSMutableString alloc]init];
    NSArray *keys = [keyValues allKeys];
    keys = [keys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [(NSString*)firstObject compare:(NSString*)secondObject];
    }];
    
    for (NSString *key in keys) {
        id value = [keyValues objectForKey:key];
        if ([value isKindOfClass:[NSString class]]) {
            if (isEncode) {
                value = urlencode(value);
            }
            
            if (parameters.length == 0) {
                [parameters appendFormat:@"%@=%@", key, value];
            }
            else
            {
                [parameters appendFormat:@"&%@=%@", key, value];
            }
        }
        else if([value isKindOfClass:[NSArray class]])
        {
            NSArray* arrayValues = (NSArray*)value;
            for (NSString *subValue in arrayValues)
            {
                NSString *newValue = subValue;
                if (isEncode) {
                    newValue = urlencode(subValue);
                }
                
                if (parameters.length == 0) {
                    [parameters appendFormat:@"%@=%@", key, newValue];
                }
                else
                {
                    [parameters appendFormat:@"&%@=%@", key, newValue];
                }
            }
        }
    }
    
    NSLog(@"parameters = %@",parameters);
    
    return parameters;
}

- (NSString*)getParametersString:(NSMutableDictionary*)keyValues inOrder:(BOOL)isInOrder encode:(BOOL)isEncode
{
    if (keyValues == nil || (keyValues != nil && [[keyValues allKeys]count] == 0)) {
        return nil;
    }
    
    NSMutableString *parameters = [[NSMutableString alloc]init];
    NSArray *keys = [keyValues allKeys];
    if (isInOrder) {
        keys = [keys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [(NSString*)firstObject compare:(NSString*)secondObject];
        }];
    }
    
    for (NSString *key in keys) {
        id value = [keyValues objectForKey:key];
        if ([value isKindOfClass:[NSString class]]) {
            if (isEncode) {
                value = urlencode(value);
            }
            
            if (parameters.length == 0) {
                [parameters appendFormat:@"%@=%@", key, value];
            }
            else
            {
                [parameters appendFormat:@"&%@=%@", key, value];
            }
        }
        else if([value isKindOfClass:[NSArray class]])
        {
            NSArray* arrayValues = (NSArray*)value;
            for (NSString *subValue in arrayValues)
            {
                NSString *newValue = subValue;
                if (isEncode) {
                    newValue = urlencode(subValue);
                }
                
                if (parameters.length == 0) {
                    [parameters appendFormat:@"%@=%@", key, newValue];
                }
                else
                {
                    [parameters appendFormat:@"&%@=%@", key, newValue];
                }
            }
        }
    }
    
    NSLog(@"parameters = %@",parameters);
    
    return parameters;
}

- (NSString*)getAPISignature:(NSString*)parameters secret:(NSString*)aSecret
{
    NSString *url = [self.requestUrl stringByReplacingOccurrencesOfString:@"//" withString:@""];
    NSRange firstSlashRange = [url rangeOfString:@"/"];
    NSRange replacedRange = NSMakeRange(0,firstSlashRange.location);
    url = [url stringByReplacingCharactersInRange:replacedRange withString:@""];
    url = [url stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    NSString *concanatedString = [NSString stringWithFormat:@"%@%@%@", aSecret, url, parameters];
    
    NSLog(@"%@",concanatedString);
    
    return [concanatedString MD5];
}

-(NSString*) requestString
{
    if (self.dictKeyValues == nil) {
        return nil;
    }
    
    NSString *parameterInOrder;
    //NSLog(@"search parameter in order = %@", parameterInOrder);
    
    if (self.secret == nil) {
        parameterInOrder = [self getParametersString:self.dictKeyValues inOrder:NO encode:YES];
        return parameterInOrder;
    }
    
    parameterInOrder = [self getParametersString:self.dictKeyValues inOrder:YES encode:YES];
    
    // remove "&" and "=" from parameterInOrder
    NSString *newParameterInOrder = [parameterInOrder stringByReplacingOccurrencesOfString:@"&" withString:@""];
    newParameterInOrder = [newParameterInOrder stringByReplacingOccurrencesOfString:@"=" withString:@""];
    
    NSString *sig = [self getAPISignature:newParameterInOrder secret:self.secret];
    NSLog(@"sig = %@", sig);
    
    // append sig to parameters
    NSString *parameters = [NSString stringWithFormat:@"%@&sig=%@",parameterInOrder, sig];
    
    return parameters;
    
    return parameterInOrder;
}


@end
