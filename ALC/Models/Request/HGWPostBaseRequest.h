//
//  HGWPostBaseRequest.h
//  HungryGoWhere
//
//  Created by Linh Le on 1/10/13.
//  Copyright (c) 2013 Linh Le. All rights reserved.
//

#import "HGWBaseRequest.h"

@interface HGWPostBaseRequest : HGWBaseRequest

@property (strong) NSString *secret;

- (NSString*)getParametersString:(NSMutableDictionary*)keyValues inOrder:(BOOL)isInOrder encode:(BOOL)isEncode;
- (NSString*)getParametersStringInOrder:(NSMutableDictionary*)keyValues encode:(BOOL)isEncode;
- (NSString*)getAPISignature:(NSString*)parameters secret:(NSString*)secret;

@end
