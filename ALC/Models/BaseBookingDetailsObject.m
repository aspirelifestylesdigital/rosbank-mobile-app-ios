//
//  BaseBookingDetailsObject.m
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@implementation BaseBookingDetailsObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    _bookingId = self.bookingItemId;
    _bookingName = self.bookingItemId;
    _firstName = [dict stringForKey:@"FIRSTNAME"];
    _lastName = [dict stringForKey:@"LASTNAME"];
    _guestName = [self.requestDetails stringForKey:RESERVATION_NAME_REQUIREMENT];
    _email = [dict stringForKey:@"EMAILADDRESS1"];
    _mobileNumber = [dict stringForKey:@"PHONENUMBER"];
    
    NSString *tempValue = [[dict stringForKey:@"PREFRESPONSE"] lowercaseString];
    if (tempValue.length == 0){
        tempValue = @"email";
    }
    _isContactBoth = [tempValue isEqualToString:@"both"];
    _isContactEmail = [tempValue isEqualToString:@"email"];
    _isContactPhone = [tempValue isEqualToString:@"phone"];
    if(!_isContactPhone && !_isContactEmail){
        _isContactBoth = YES;
    }
    
    if((tempValue = [self.requestDetails stringForKey:SMOKING_PREF_REQUIREMENT])){
        tempValue = [tempValue lowercaseString];
        _isSmoking = (isEqualIgnoreCase(tempValue,@"yes") || isEqualIgnoreCase(tempValue ,@"true"))? YES : NO;
    }
    
    _loyaltyMembership = [self.requestDetails stringForKey:MEMBER_SHIP_REQUIREMENT];
    _loyaltyMemberNo = [self.requestDetails stringForKey:MEMBER_NO_REQUIREMENT];
    _specialRequirement  = [self.requestDetails stringForKey:SPECIAL_REQUIREMENT];
    _userId = [dict stringForKey:@"UserID"];
    _adulsPax = [dict intWithDefaultMinusOneForKey:@"NUMBEROFADULTS"];
    _kidsPax = [dict integerForKey:@"NUMBEROFKIDS"];
    
    tempValue = [self.requestDetails stringForKey:ROOM_PREF_REQUIREMENT];
    _roomTypeKey = _roomTypeValue = tempValue;
     
    return self;
}

-(NSString*) privilegeName{
    return @"";
}

@end
