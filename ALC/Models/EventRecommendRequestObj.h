//
//  EventRecommendRequestObj.h
//  ALC
//
//  Created by Hai NguyenV on 12/2/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntertainmentRequestDetailsObject.h"
@interface EventRecommendRequestObj : NSObject

@property (strong, nonatomic) NSString* reservationDate;
@property (strong, nonatomic) NSString* reservationName;
@property (strong, nonatomic) NSString* EventCategory;
@property (strong, nonatomic) NSString* BookingId;
@property (strong, nonatomic) NSString* MobileNumber;
@property (strong, nonatomic) NSString* EmailAddress;
@property (strong, nonatomic) NSString* BookingItemId;
@property (strong, nonatomic) NSString* Country;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSString* State;
@property (strong, nonatomic) NSString* SpecialRequirements;
@property (strong, nonatomic) NSString* guestName;

@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;

@property ( nonatomic) BOOL isEdit;
@property (strong, nonatomic) NSString* EditType;
@property ( nonatomic) NSInteger adultPax;

-(id) initFromDetails:(EntertainmentRequestDetailsObject*)detailsObj;
@end
