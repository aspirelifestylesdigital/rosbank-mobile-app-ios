//
//  OrtherRequestDetailsObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface OrtherRequestDetailsObj : BaseBookingDetailsObject

@property (strong, nonatomic) NSString* ortherString;
@property (strong, nonatomic) NSString* spaName;

-(id) initFromDict:(NSDictionary*) dict;

@end
