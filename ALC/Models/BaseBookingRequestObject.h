//
//  BaseBookingRequestObject.h
//  ALC
//
//  Created by Anh Tran on 3/30/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCRItemDetails.h"
@interface BaseBookingRequestObject : NSObject
@property (strong, nonatomic) NSString* privilegeId;
@property (strong, nonatomic) NSString* fullAddress;
- (void) setPrivilegeData:(NSDictionary*) requestDetails;
@end
