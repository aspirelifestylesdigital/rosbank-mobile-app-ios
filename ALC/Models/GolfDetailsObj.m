//
//  GolfDetailsObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GolfDetailsObj.h"

@implementation GolfDetailsObj

-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    self.guestName = [self.requestDetails stringForKey:GUEST_NAME_REQUIREMENT];
    _GolfCourse = [self.requestDetails stringForKey:GolfCourseName];
    _GolfDate = self.golfCourseDateTime;
    _hole = [self.requestDetails stringForKey:Holes];
    _noOfBaler = [self.requestDetails stringForKey:NoofBallers];
    _handicaf = [self.requestDetails stringForKey:PreferredGolfHandicap];
    return self;
}

@end
