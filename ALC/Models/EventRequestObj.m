//
//  EventRequestObj.m
//  ALC
//
//  Created by Hai NguyenV on 11/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "EventRequestObj.h"

@implementation EventRequestObj
-(id) initFromDetails:(EntertainmentRequestDetailsObject*)detailsObj{
    self = [super init];
    self.BookingId = detailsObj.bookingId;
    self.reservationName = detailsObj.guestName;
    self.EmailAddress = detailsObj.email;
    self.MobileNumber = detailsObj.mobileNumber;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    self.SpecialRequirements = detailsObj.specialRequirement;
    
    self.City = detailsObj.city;
    self.Country = detailsObj.country;
    self.State = detailsObj.stateValue;
    
    self.reservationDate = formatDate(detailsObj.eventDateTime, DATE_SEND_REQUEST_FORMAT);
    self.reservationTime = formatDate(detailsObj.eventDateTime, TIME_SEND_REQUEST_FORMAT);
    self.EventName = detailsObj.eventName;
    
    if(detailsObj.noOfTickets.length > 0){
        self.adultPax = [detailsObj.noOfTickets integerValue];
    }
    
    
    return self;
}
@end
