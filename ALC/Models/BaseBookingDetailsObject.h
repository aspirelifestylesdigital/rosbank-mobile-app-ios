//
//  BaseBookingDetailsObject.h
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MyRequestObject.h"

@interface BaseBookingDetailsObject : MyRequestObject
@property (strong, nonatomic) NSString* bookingId;
@property (strong, nonatomic) NSString* bookingName;
@property (strong, nonatomic) NSString* guestName;
@property (strong, nonatomic) NSString* userId;
@property (strong, nonatomic) NSString* specialRequirement;
@property (strong, nonatomic) NSString* countryValue;
@property (nonatomic) BOOL isSmoking;
@property (strong, nonatomic) NSString* loyaltyMembership;
@property (strong, nonatomic) NSString* loyaltyMemberNo;
@property (strong, nonatomic) NSString* attachedPhotoUrl;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* mobileNumber;
@property (nonatomic) BOOL isContactEmail;
@property (nonatomic) BOOL isContactPhone;
@property (nonatomic) BOOL isContactBoth;
@property (nonatomic) NSInteger adulsPax;
@property (nonatomic) NSInteger kidsPax;
@property (strong, nonatomic) NSString* roomTypeKey;
@property (strong, nonatomic) NSString* roomTypeValue;

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property ( nonatomic) BOOL isWithBreakfast;
@property ( nonatomic) BOOL isWithWifi;
@property ( nonatomic) NSInteger noOfRooms;
-(id) initFromDict:(NSDictionary*) dict;
-(NSString*) privilegeName;

@end
