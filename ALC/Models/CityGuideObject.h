//
//  CityGuideObject.h
//  ALC
//
//  Created by Anh Tran on 9/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityGuideObject : NSObject
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* cityName;
@property (strong, nonatomic) NSString* cityCountry;
@property (strong, nonatomic) NSString* cityCode;
@property (strong, nonatomic) NSString* introduction;
@property (strong, nonatomic) NSString* shortDescription;
@property (strong, nonatomic) NSString* linkToCity;
@property (strong, nonatomic) NSString* cityDescription;
@property (strong, nonatomic) NSString* cityId;
@property (nonatomic) Boolean isAddWishList;
@property (strong, nonatomic) NSString* backgroundImageUrl;
@property (strong, nonatomic) NSString* bannerUrl;



-(id) initFromDict:(NSDictionary*) dict;
@end
