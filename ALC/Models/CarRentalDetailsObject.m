//
//  CarRentalDetailsObject.m
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CarRentalDetailsObject.h"

@implementation CarRentalDetailsObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    _driverName = [self.requestDetails stringForKey:DriverName];
    _pickUpLocation = [self.requestDetails stringForKey:PickLocation];
    _dropOffLocation =  [self.requestDetails stringForKey:DropLocation];
    _minimumPrice = @"";
    _maximumPrice = [self.requestDetails stringForKey:MAX_PRICE_REQUIREMENT];
    _driverAge = [self.requestDetails integerForKey:DriverAge];
    
    NSString* tempValue;
    if((tempValue = [self.requestDetails stringForKey:IntLicense])){
        tempValue = [tempValue lowercaseString];
        _isInternationalLicense = (isEqualIgnoreCase(tempValue,@"yes") || isEqualIgnoreCase(tempValue,@"true"))
        ? YES : NO;
    }
    
    _pickUpDate = self.pickUpDateTime;
    _pickUpTime = self.pickUpDateTime;
    _dropOffDate = self.dropOffDateTime;
    _dropOffTime =self.dropOffDateTime;
    
    tempValue =[self.requestDetails stringForKey:PrefVehicle];
    if([tempValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0){
        _prefferedVerhicles = convertStringToArray(tempValue);
    } else {
        _prefferedVerhicles = [NSArray array];
    }
    
    tempValue =[self.requestDetails stringForKey:PrefCarRentalComp];
    if([tempValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0){
        _prefferedRentals = convertStringToArray(tempValue);
    } else {
        _prefferedRentals = [NSArray array];
    }
    
    
    return self;
}
-(NSMutableArray*) prefferedRentalKeys{
    return [NSMutableArray arrayWithArray:_prefferedRentals];
}
-(NSMutableArray*) prefferedRentalValues{
    return [NSMutableArray arrayWithArray:_prefferedRentals];
}
-(NSMutableArray*) prefferedVerhicleKeys{
    return [NSMutableArray arrayWithArray:_prefferedVerhicles];
}
-(NSMutableArray*) prefferedVerhicleValues{
    return [NSMutableArray arrayWithArray:_prefferedVerhicles];
}

@end
