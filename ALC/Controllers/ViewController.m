//
//  ViewController.m
//  ALC
//
//  Created by Hai NguyenV on 8/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ViewController.h"
#import "SplashStep1.h"
#import "SplashStep2.h"
#import "SplashStep3.h"
#import "SplashStep4.h"
#import "LoginViewController.h"

#define SPLASH_PAGE_SIZE 4
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = YES;
    
    pArrayView = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    if([self isStarted] == 1){
        /*UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        LoginViewController* controller= [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self.navigationController pushViewController:controller animated:YES];*/

        setRootViewLogout();

    }else{
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
        [pref setObject:@"0" forKey:FLAG_STARTED];
        [pref synchronize];
    }
    NSLog(@"%f",self.view.frame.size.width);
    
    [self addSplashScroll];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}
-(void) appEnteredForeground {
    if(pArrayView.count>0){
        SplashStep1* step1 = (SplashStep1*)[pArrayView objectAtIndex:0];
        [step1 onResume];
    }
}

-(void) appEnteredBackground {
    if(pArrayView.count>0){
        SplashStep1* step1 = (SplashStep1*)[pArrayView objectAtIndex:0];
        [step1 onPause];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"ViewController ViewWillAppear");

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"ViewController viewDidAppear");
}

-(int)isStarted{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    int isstarted = 0;
    isstarted = [[pref objectForKey:FLAG_STARTED] intValue];
    return isstarted;
}

-(void)addSplashScroll{
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.pScrollView addGestureRecognizer:swiperight];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.pScrollView addGestureRecognizer:swipeleft];
    
    self.pScrollView.contentSize = CGSizeMake(self.view.frame.size.width*SPLASH_PAGE_SIZE, 300);
    for (int i = 0; i<SPLASH_PAGE_SIZE; i++) {
        switch (i) {
            case 0:
            {
                SplashStep1* step1 = [[SplashStep1 alloc] initWithFrame:self.view.frame];
                [self.pScrollView addSubview:step1];
                [pArrayView addObject:step1];
                break;
            }
            case 3:
            {
                CGRect frame = self.view.frame;
                frame.origin.x = self.view.frame.size.width*i;
                SplashStep1* step3 = [[SplashStep1 alloc] initWithFrame:frame];
                [self.pScrollView addSubview:step3];
                break;
            }
           /* case 1:
            {
                CGRect frame = self.view.frame;
                frame.origin.x = self.view.frame.size.width*i;
                SplashStep2* step2 = [[SplashStep2 alloc] initWithFrame:frame];
                [self.pScrollView addSubview:step2];
                break;
            }*/
            case 1:
            {
                CGRect frame = self.view.frame;
                frame.origin.x = self.view.frame.size.width*i;
                SplashStep3* step3 = [[SplashStep3 alloc] initWithFrame:frame];
                [self.pScrollView addSubview:step3];
                break;
            }
            case 2:
            {
                CGRect frame = self.view.frame;
                frame.origin.x = self.view.frame.size.width*i;
                SplashStep4* step4 = [[SplashStep4 alloc] initWithFrame:frame];
                [self.pScrollView addSubview:step4];
                break;
            }
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width);
    if(pageNumber >= SPLASH_PAGE_SIZE - 1){
        pageNumber = 0;
        [self.pScrollView scrollRectToVisible:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height) animated:NO];
    }
    self.pPager.currentPage = pageNumber;
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    int pageNumber = (int) (self.pPager.currentPage + 1);
    [self.pScrollView scrollRectToVisible:CGRectMake((self.pScrollView.frame.size.width)*pageNumber,0,self.view.frame.size.width,self.view.frame.size.height) animated:YES];
    //int pageNumber = round(self.pScrollView.contentOffset.x / self.pScrollView.frame.size.width);
    if(pageNumber >= SPLASH_PAGE_SIZE - 1){
        pageNumber = 0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.pScrollView scrollRectToVisible:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height) animated:NO];
        });
        
    }
    self.pPager.currentPage = pageNumber;
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer{
    if(self.pPager.currentPage > 0){
        int pageNumber = (int) (self.pPager.currentPage - 1);
        [self.pScrollView scrollRectToVisible:CGRectMake((self.pScrollView.frame.size.width)*pageNumber,0,self.view.frame.size.width,self.view.frame.size.height) animated:YES];
        self.pPager.currentPage = pageNumber;
    }
}


- (IBAction)actionStarted:(id)sender {
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref setObject:@"1" forKey:FLAG_STARTED];
    [pref synchronize];
    
    setRootViewLogout();
}

-(void) setTextForViews
{
    [self.getStartedBtn setTitle:NSLocalizedString(@"Get Started", @"") forState:UIControlStateNormal];
    
    NSLog(@"Call set Text Function");
}
@end
