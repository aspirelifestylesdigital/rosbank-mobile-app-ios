//
//  RequestFormBaseController.m
//  ALC
//
//  Created by Anh Tran on 3/16/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "RequestFormBaseController.h"
#import "DCRTopItemDetailsView.h"
#import "ThankYouBookingController.h"
#import "ALCQC-Swift.h"

@interface RequestFormBaseController ()

@end

@implementation RequestFormBaseController
@synthesize selectedCity, selectedState, selectedCountry;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self customButtonBack];
    // Do any additional setup after loading the view.
    
    [self setUpSuperView];
}

-(void) setUpSuperView{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    selectedCountry = NSLocalizedString(@"Russia", nil);
    
    [self setUpSubView];
    
    // If open form privilege
    if(self.privilegeDetails){
        self.hideTopNavigator = YES;
        [self addPrivilegeView:nil];
    } else {
        [self getRequestDetails];
    }
    
    if(self.hideTopNavigator){
        [self hideTopHeader];
    }
}

-(void) addPrivilegeView:(BaseBookingDetailsObject *) bookDetails{
    DCRTopItemDetailsView* topView = [[DCRTopItemDetailsView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 535)];
    NSString* headerTitle = @"";
    if([self parentStack]){
        [[self parentStack] insertArrangedSubview:topView atIndex:0];
        if(_privilegeDetails){
            [topView showItemDetails:_privilegeDetails];
            headerTitle = _privilegeDetails.itemName;
        } else if(bookDetails){
            _privilegeId  = [bookDetails.requestDetails stringForKey:PrivilegeId];
            _privilegeFullAddress = [bookDetails.requestDetails stringForKey:FullAddress];
            [topView showEditItemDetails:bookDetails];
            headerTitle = [bookDetails privilegeName];
        }
        [self addTitleToHeader:headerTitle withColor:HEADER_COLOR];
        [self hideAndFillPrivilegeToForm];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(UIToolbar*) setUpToolBar{
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    
    return toolbar;
}

- (void) hideTopHeader{
    
}

- (void)dismiss
{
    [self closeAllKeyboardAndPicker];
}


-(void)keyboardWillShow {
}

-(void)keyboardWillHide {
}


#pragma mark ------ Optional Detail Delegate ------------------
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
    
}
-(void) textFieldBeginEdit:(UIView*) textField{
    [self updateScrollViewToSelectedView:textField];
}

-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    
}

-(void) touchinDetailsView{
    [self closeAllKeyboardAndPicker];
}

- (void) closeAllKeyboardAndPicker{
    //TODO implement in subview
}


#pragma mark ---------- UITextField Delegate --------------------
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if([self.view viewWithTag:textField.tag+1]){
        if([[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextView class]])
            [(UITextView*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        else if( [[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextField class]]){
            [(UITextField*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        }
        else{
            [textField resignFirstResponder];
        }
    } else {
        [textField resignFirstResponder];
    }
    
    [self closeAllKeyboardAndPicker];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return  YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self updateScrollViewToSelectedView:textField];
    
    UIView *errorIcon = [self.view viewWithTag:100+textField.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon) setErrorHidden:YES];
    }
}

- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [_dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            [((ErrorToolTip*)[_dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
        }
    }
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture{
    
}

#pragma Utils

-(NSString*)getTimebyDate:(NSDate*)pdate{
    NSString* time = @"";
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    [outputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *temp = [outputFormatter stringFromDate:pdate];
    
    NSDate *date = [outputFormatter dateFromString:temp];
    
    outputFormatter.dateFormat = @"hh:mm a";
    time = [outputFormatter stringFromDate:date];
    
    return  time;
}

-(void) openFilterCountry:(id<SubFilterControllerDelegate>)filterDelegate{
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = COUNTRY;
    controller.delegate = filterDelegate;
    controller.maxCount = 1;
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:self.selectedCountry, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:self.selectedCountry, nil];
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void) openFilterState:(id<SubFilterControllerDelegate>)filterDelegate{
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = STATE;
    controller.delegate = filterDelegate;
    controller.maxCount = 1;
    if([self.selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        controller.countryType = USA;
    }else if([self.selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)]){
        controller.countryType = CANADA;
    }
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:self.selectedState, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:self.selectedState, nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (BOOL) isSpecialCountries{
    return [self.selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [self.selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)];
}

- (void) hideAndFillPrivilegeToForm{
    
}

- (void) openThankYouPage{
    [self.navigationController popViewControllerAnimated:NO];
    ThankYouBookingController* controller = (ThankYouBookingController*) [self controllerByIdentifier:@"ThankYouBooking"];
    controller.thankYouMessage = [BOOKING_THANK_YOU_MSG stringByReplacingOccurrencesOfString:@"PHONE_NUMBER_PLACEHOLDER" withString:[AppConfig shared].companyPhoneNumber];
    [appdelegate.rootVC pushViewController:controller animated:YES];
}

// add privilege id and full address
- (BaseBookingRequestObject*) addPrivilegeData:(BaseBookingRequestObject*) object{
    if(_privilegeDetails){
        object.privilegeId = _privilegeDetails.itemId;
        object.fullAddress = [_privilegeDetails fullLocationAddress];
    } else {
        if(_privilegeId){
            object.privilegeId = _privilegeId;
        }
        
        if(_privilegeFullAddress){
            object.fullAddress = _privilegeFullAddress;
        }
    }
    
    return object;
}

-(void) showBaseBookingDetails:(BaseBookingDetailsObject*) object{
    NSString *priId = [object.requestDetails stringForKey:PrivilegeId];
    if(priId && priId.length > 0){
        // edit a booking from Privilege
        [self addPrivilegeView:object];
    }
    [self showBookingDetails:object];
}


- (void)showBookingDetails:(BaseBookingDetailsObject*) details{
    
}

- (UIStackView*) parentStack{
    return nil;
}

- (void) setUpSubView{
    
}

- (void) getRequestDetails{
    
}
@end
