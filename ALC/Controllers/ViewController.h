//
//  ViewController.h
//  ALC
//
//  Created by Hai NguyenV on 8/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperMainViewController.h"

@interface ViewController : SuperMainViewController<UIScrollViewDelegate>
{
    NSMutableArray* pArrayView;
}
@property (weak, nonatomic) IBOutlet UIScrollView *pScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pPager;
@property (weak, nonatomic) IBOutlet UIButton *getStartedBtn;

- (IBAction)actionStarted:(id)sender;
@end

