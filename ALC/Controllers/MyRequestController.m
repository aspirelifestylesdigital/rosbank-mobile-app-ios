//
//  MyRequestController.m
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MyRequestController.h"
#import "BaseResponseObject.h"
#import "MyRequestObject.h"
#import "CancelRequestViewController.h"
#import "WSGetCarRentalRequestDetails.h"
#import "WSGetGolfRequestDetails.h"
#import "WSGetGourmetRequestDetails.h"
#import "WSGetHotelRequestDetails.h"
#import "WSGetOtherRequestDetails.h"
#import "WSGetHotelRecommendDetails.h"
#import "WSGetGourmetRecommend.h"
#import "WSGetCarTransferRequestDetails.h"
#import "WSGetEntertainmentRequestDetails.h"

#define UserID @"UserID"
#define TypeOfRequests @"TypeOfRequests"
#define SortBy @"SortBy"
#define CELL_STATIC_HEIGHT 95

static NSString *const kCancelRequestViewController = @"CancelRequestViewController";

@interface MyRequestController () <CancelRequestViewControllerDelegate> {
    WSGetRequestList* wsGetRequest;
    NSMutableDictionary *filterOptions;
    enum REQUEST_TYPE selectedFilterRequestType;
    NSMutableArray* myUpComingRequests;
    NSMutableArray* myHistoryRequests;
    NSMutableArray* filteredMyRequests;
    BOOL isLoading;
    float scale;
    WSGetCarRentalRequestDetails *wsCarRentalRequestDetails;
    WSGetGolfRequestDetails *wsGoftRequestDetails;
    WSGetGourmetRequestDetails *wsGourmetRequestDetails;
    WSGetHotelRequestDetails *wsHotelRequestDetails;
    WSGetOtherRequestDetails *wsOtherRequestDetails;
    WSGetHotelRecommendDetails *wsHotelRecommend;
    WSGetGourmetRecommend* wsGetGourmetRecommend;
    WSGetCarTransferRequestDetails *wsCarTransferRequestDetails;
    WSGetEntertainmentRequestDetails* wsGetEntertainmentRequestDetails;
}

@property (strong, nonatomic) IBOutlet UIButton *btUpcoming;
@property (strong, nonatomic) IBOutlet UIButton *btHistory;
@property (strong, nonatomic) IBOutlet UIView *topOrangeUcomingBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowUpcoming;
@property (strong, nonatomic) IBOutlet UIView *topOrangeHistoryBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowHistory;
@property (strong, nonatomic) IBOutlet UIButton *btAll;
@property (strong, nonatomic) IBOutlet UIButton *btDinning;
@property (strong, nonatomic) IBOutlet UIButton *btHotel;
@property (strong, nonatomic) IBOutlet UIButton *btEvent;
@property (strong, nonatomic) IBOutlet UIButton *btCar;
@property (strong, nonatomic) IBOutlet UIButton *btGoft;
@property (strong, nonatomic) IBOutlet UIButton *btSpa;
@property (strong, nonatomic) IBOutlet UILabel *lbNoData;
@property (strong, nonatomic) IBOutlet UIButton *btOtherRequest;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollMenu;
@property (strong, nonatomic) IBOutlet UILabel *lbTopViewType;

@property (strong, nonatomic) MyRequestObject *requestSelected;

@end

@implementation MyRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleToHeader:NSLocalizedString(@"MY REQUESTS", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    _lbNoData.hidden = YES;
    selectedFilterRequestType = NO_REQUEST;
    
    filterOptions = [NSMutableDictionary dictionary];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        [filterOptions setObject:((UserObject*)[appdele getLoggedInUser]).userId  forKey:@"UserID"];
        [filterOptions setObject:@"upcoming" forKey:SortBy];
        [self setupView];
    } else {
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didAmendRequest)
                                                 name:NOTIFY_A_REQUEST_IS_AMENDED
                                               object:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupView{
    [_scrollMenu setNeedsLayout];
    [_scrollMenu layoutIfNeeded];
    _scrollMenu.contentSize = CGSizeMake(_scrollMenu.contentSize.width,_scrollMenu.frame.size.height);
    
    [_btUpcoming.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_btHistory.titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [_table setSeparatorColor:[UIColor clearColor]];
    _table.rowHeight = UITableViewAutomaticDimension;
    _table.estimatedRowHeight = 170;
    [self reloadData];
}

- (IBAction)historyClicked:(id)sender {
    [_btUpcoming setSelected:NO];
    [_topOrangeUcomingBar setHidden:YES];
    [_bottomArrowUpcoming setHidden:YES];
    
    [_btHistory setSelected:YES];
    [_topOrangeHistoryBar setHidden:NO];
    [_bottomArrowHistory setHidden:NO];
    
    [self updateViewType];
}

- (IBAction)upcomingClicked:(id)sender {
    [_btUpcoming setSelected:YES];
    [_topOrangeUcomingBar setHidden:NO];
    [_bottomArrowUpcoming setHidden:NO];
    
    [_btHistory setSelected:NO];
    [_topOrangeHistoryBar setHidden:YES];
    [_bottomArrowHistory setHidden:YES];
    
    [self updateViewType];
}

- (IBAction)btAllClicked:(id)sender {
    selectedFilterRequestType = NO_REQUEST;
    [self unselectOtherChoice:((UIButton*)sender).tag];
    [self updateViewType];
}

- (IBAction)btOtherRequestClicked:(id)sender {
    [self unselectOtherChoice:((UIButton*)sender).tag];
    if(_btOtherRequest.isSelected){
        selectedFilterRequestType = OTHER_REQUEST;
    } else {
        selectedFilterRequestType = NO_REQUEST;
    }
    [self updateViewType];
}

- (IBAction)btGoftClicked:(id)sender {
    [self unselectOtherChoice:((UIButton*)sender).tag];
    if(_btGoft.isSelected){
        selectedFilterRequestType = GOLF_REQUEST;
    } else {
        selectedFilterRequestType = NO_REQUEST;
    }
    
    [self updateViewType];
}
- (IBAction)btDinningClicked:(id)sender {
    [self unselectOtherChoice:((UIButton*)sender).tag];
    if(_btDinning.isSelected){
        selectedFilterRequestType = GOURMET_REQUEST;
    } else {
        selectedFilterRequestType = NO_REQUEST;
    }
    [self updateViewType];
}
- (IBAction)btEventClicked:(id)sender {
    [self unselectOtherChoice:((UIButton*)sender).tag];
    if(_btEvent.isSelected){
        selectedFilterRequestType = ENTERTAINMENT_REQUEST;
    } else {
        selectedFilterRequestType = NO_REQUEST;
    }
    
    [self updateViewType];
}
- (IBAction)btCarClicked:(id)sender {
    [self unselectOtherChoice:((UIButton*)sender).tag];
    if(_btCar.isSelected){
        selectedFilterRequestType = CAR_RENTAL_REQUEST;
    } else {
        selectedFilterRequestType = NO_REQUEST;
    }
    [self updateViewType];
}

- (IBAction)btHotelClicked:(id)sender {
    [self unselectOtherChoice:((UIButton*)sender).tag];
    
    if(_btHotel.isSelected){
        selectedFilterRequestType = HOTEL_REQUEST;
    } else {
        selectedFilterRequestType = NO_REQUEST;
    }
    
    [self updateViewType];
}

- (IBAction)btSpaClicked:(id)sender {
    [self unselectOtherChoice:((UIButton*)sender).tag];
    
    if(_btSpa.isSelected){
        selectedFilterRequestType = SPA_REQUEST;
    } else {
        selectedFilterRequestType = NO_REQUEST;
    }
    
    [self updateViewType];
}

-(void) unselectOtherChoice:(NSInteger) tag{
    [_btEvent setSelected:_btEvent.tag == tag];
    [_btDinning setSelected:_btDinning.tag == tag];
    [_btHotel setSelected:_btHotel.tag == tag];
    [_btCar setSelected:_btCar.tag == tag];
    [_btGoft setSelected:_btGoft.tag == tag];
    [_btOtherRequest setSelected:_btOtherRequest.tag == tag];
    [_btAll setSelected:_btAll.tag == tag];
    [_btSpa setSelected:_btSpa.tag == tag];
    
    [filterOptions removeObjectForKey:TypeOfRequests];
}

-(void) addRequestType:(NSString*) type{
    NSMutableArray *types = [filterOptions objectForKey:TypeOfRequests];
    if(types==nil){
        types = [NSMutableArray array];
    }
    
    [types addObject:type];
    [filterOptions setObject:types forKey:TypeOfRequests];
}

-(void) removeRequestType:(NSString*) type{
    NSMutableArray *types = [filterOptions objectForKey:TypeOfRequests];
    if(types==nil){
        types = [NSMutableArray array];
    }
    
    [types removeObject:type];
    [filterOptions setObject:types forKey:TypeOfRequests];
    
    if(types.count == 0){
        [_btAll setSelected:YES];
    }
}

- (void) reloadData{
    myUpComingRequests = [NSMutableArray array];
    myHistoryRequests = [NSMutableArray array];
    [filteredMyRequests removeAllObjects];
    
    [_table reloadData];
    
    [self showToast];
    isLoading = true;
    if(wsGetRequest==nil){
        wsGetRequest = [[WSGetRequestList alloc] init];
    } else {
        [wsGetRequest cancelRequest];
    }
    wsGetRequest.delegate = self;
    [wsGetRequest getMyRequest:filterOptions];
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    
    switch (ws.task) {
        case WS_GET_REQUEST_LIST: {
            [myHistoryRequests addObjectsFromArray:((WSGetRequestList*)ws).myHistoryRequests];
            [myUpComingRequests addObjectsFromArray:((WSGetRequestList*)ws).myUpcomingRequests];
            
            NSMutableArray *tempFiltered = [self filterRequestsList:(_btUpcoming.isSelected ? myUpComingRequests : myHistoryRequests)];
            
            if(filteredMyRequests && filteredMyRequests.count == tempFiltered.count){
                // do nothing
            } else {
                if(filteredMyRequests== nil){
                    filteredMyRequests = [[NSMutableArray alloc] init];
                }
                [filteredMyRequests removeAllObjects];
                [filteredMyRequests addObjectsFromArray:tempFiltered];
                [_table reloadData];
            }
            
            if(filteredMyRequests.count==0){
                _lbNoData.hidden = NO;
            }else{
                _lbNoData.hidden = YES;
            }
            isLoading = false;
        }
            break;
            
        case WS_GET_CAR_RENTAL_REQUEST: {
            [self openCancelRequestScreen:wsCarRentalRequestDetails.bookingDetails andRequestType:CAR_RENTAL_REQUEST];
        }
            break;
        case WS_GET_CAR_TRANSFER_REQUEST: {
            [self openCancelRequestScreen:wsCarTransferRequestDetails.bookingDetails andRequestType:CAR_TRANSFER_REQUEST];
        }
            break;
            
        case WS_GET_GOLF_REQUEST: {
            [self openCancelRequestScreen:wsGoftRequestDetails.bookingDetails andRequestType:GOLF_REQUEST];
        }
            break;
            
        case WS_GET_HOTEL_REQUEST: {
            [self openCancelRequestScreen:wsHotelRequestDetails.bookingDetails andRequestType:HOTEL_REQUEST];
        }
            break;
            
        case WS_GET_GOURMET_REQUEST: {
            [self openCancelRequestScreen:wsGourmetRequestDetails.bookingDetails andRequestType:GOURMET_REQUEST];
        }
            break;
            
        case WS_GET_OTHER_REQUEST: {
            if(wsOtherRequestDetails.bookingDetails.parsedRequestType == SPA_REQUEST){
                [self openCancelRequestScreen:wsOtherRequestDetails.bookingDetails andRequestType:SPA_REQUEST];
            } else {
                [self openCancelRequestScreen:wsOtherRequestDetails.bookingDetails andRequestType:OTHER_REQUEST];
            }
        }
            break;
        case WS_GET_GOURMET_RECOMMEND_REQUEST: {
            [self openCancelRequestScreen:wsGetGourmetRecommend.bookingDetails andRequestType:GOURMET_RECOMMEND_REQUEST];
        }
            break;
        case WS_GET_HOTEL_RECOMMEND_REQUEST: {
            [self openCancelRequestScreen:wsHotelRecommend.bookingDetails andRequestType:HOTEL_RECOMMEND_REQUEST];
        }
            break;
        case WS_GET_BOOKING_ENTERTAINMENT:
        {
            [self openCancelRequestScreen:wsGetEntertainmentRequestDetails.bookingDetails andRequestType:ENTERTAINMENT_REQUEST];
        }
            break;
        case WS_GET_ENTERTAINMENT_RECOMMEND: {
            [self openCancelRequestScreen:wsGetEntertainmentRequestDetails.bookingDetails andRequestType:ENTERTAINMENT_RECOMMEND_REQUEST];
        }
            break;
            
        default:
            break;
    }
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(result.task == WS_GET_REQUEST_LIST){
        if(result!=nil && result.message!=nil){
            // [self showErrorMessage:result.message];
        }
        
        isLoading = false;
    }
    if((_btUpcoming.isSelected ? myUpComingRequests.count : myHistoryRequests.count)==0){
        _lbNoData.hidden = NO;
    }else{
        _lbNoData.hidden = YES;
    }
    [self stopToast];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filteredMyRequests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MyRequestObject *item = ((MyRequestObject*)[filteredMyRequests objectAtIndex:indexPath.row]);
    
    static NSString *simpleTableIdentifier = @"MyRequestCell";
    
    MyRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(!cell){
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"MyRequestCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        resetScaleViewBaseOnScreen(cell);
    }
    cell.delegate = self;
    [cell showData:item];
    
    if (indexPath.row % 2 == 0) {
        [cell setBackgroundColor:[UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0]];
    } else {
        [cell setBackgroundColor:[UIColor  whiteColor]];
    }
    cell.userInteractionEnabled = YES;
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isLoading && indexPath.row == filteredMyRequests.count - 1 ) {
        if(wsGetRequest!=nil && [wsGetRequest hasNextItem]){
            isLoading = true;
            [wsGetRequest nextPage];
        }
    }
}

- (void) cancelingRequest:(MyRequestObject*)item{
    if([item notAllowEditCancel]){
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_CANCEL_MSG, @"OK");
    }else{
        [self showToast];
        self.requestSelected = item;
        switch (item.parsedRequestType) {
            case SPA_REQUEST:
            case OTHER_REQUEST: {
                wsOtherRequestDetails = [[WSGetOtherRequestDetails alloc] init];
                wsOtherRequestDetails.delegate = self;
                [wsOtherRequestDetails getRequestDetails:item.requestId];
                }
                break;
                
            case GOURMET_REQUEST: {
                wsGourmetRequestDetails = [[WSGetGourmetRequestDetails alloc] init];
                wsGourmetRequestDetails.delegate = self;
                [wsGourmetRequestDetails getRequestDetails:item.requestId];
            }
                break;
                
            case CAR_RENTAL_REQUEST: {
                wsCarRentalRequestDetails = [[WSGetCarRentalRequestDetails alloc] init];
                wsCarRentalRequestDetails.delegate = self;
                [wsCarRentalRequestDetails getRequestDetails:item.requestId];
            }
                break;
            case CAR_TRANSFER_REQUEST: {
                wsCarTransferRequestDetails = [[WSGetCarTransferRequestDetails alloc] init];
                wsCarTransferRequestDetails.delegate = self;
                [wsCarTransferRequestDetails getRequestDetails:item.requestId];
            }
                break;
                
            case HOTEL_REQUEST: {
                wsHotelRequestDetails = [[WSGetHotelRequestDetails alloc] init];
                wsHotelRequestDetails.delegate = self;
                [wsHotelRequestDetails getRequestDetails:item.requestId];
            }
                break;
                
            case GOLF_REQUEST: {
                wsGoftRequestDetails = [[WSGetGolfRequestDetails alloc] init];
                wsGoftRequestDetails.delegate = self;
                [wsGoftRequestDetails getRequestDetails:item.requestId];
            }
                break;
            case HOTEL_RECOMMEND_REQUEST: {
                wsHotelRecommend = [[WSGetHotelRecommendDetails alloc] init];
                wsHotelRecommend.delegate = self;
                [wsHotelRecommend getRequestDetails:item.requestId];
            }
                break;
            case GOURMET_RECOMMEND_REQUEST: {
                wsGetGourmetRecommend = [[WSGetGourmetRecommend alloc] init];
                wsGetGourmetRecommend.delegate = self;
                [wsGetGourmetRecommend getRequestDetails:item.requestId];
            }
                break;
            case ENTERTAINMENT_REQUEST: {
                wsGetEntertainmentRequestDetails = [[WSGetEntertainmentRequestDetails alloc] init];
                wsGetEntertainmentRequestDetails.delegate = self;
                [wsGetEntertainmentRequestDetails getRequestDetails:item.requestId withTask:WS_GET_BOOKING_ENTERTAINMENT];
            }
                break;
            case ENTERTAINMENT_RECOMMEND_REQUEST: {
                wsGetEntertainmentRequestDetails = [[WSGetEntertainmentRequestDetails alloc] init];
                wsGetEntertainmentRequestDetails.delegate = self;
                [wsGetEntertainmentRequestDetails getRequestDetails:item.requestId withTask:WS_GET_ENTERTAINMENT_RECOMMEND];
            }
                break;
                
            default:
                self.requestSelected = nil;
                [self stopToast];
                break;
        }
    }
}

- (void)didCancelRequestSuccessfully:(MyRequestObject *)myRequest {
    [filteredMyRequests removeObject:myRequest];
    self.requestSelected = nil;
    [_table reloadData];
}

- (void) failCancelRequest:(MyRequestObject*)item{
    [self stopToast];
}

- (void) didCancelRequest:(MyRequestObject *)item{
    [filteredMyRequests removeObject:item];
    [_table reloadData];
    [self stopToast];
}

- (void) amendRequest:(MyRequestObject*)item{
    //TODO: open Booking details page
    if([item notAllowEditCancel]){
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_ADMEND_MSG, @"OK");
    }
}

-(void)didAmendRequest {
    [self reloadData];
}

- (void)openCancelRequestScreen:(id)requestDetails andRequestType:(NSInteger)requestType {
    CancelRequestViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kCancelRequestViewController];
    vc.requestDetails = requestDetails;
    vc.requestType = requestType;
    vc.myRequest = self.requestSelected;
    vc.delegate = self;
    
    [[SlideNavigationController sharedInstance] pushViewController:vc animated:YES];
}

-(void) updateViewType{
    NSString* typeValue;
    switch (selectedFilterRequestType) {
        case GOURMET_REQUEST:
            typeValue = NSLocalizedString(@"Dining Requests", nil);
            break;
        case HOTEL_REQUEST:
            typeValue = NSLocalizedString(@"Hotel Requests", nil);
            break;
        case GOLF_REQUEST:
            typeValue = NSLocalizedString(@"Golf Requests", nil);
            break;
        case CAR_RENTAL_REQUEST:
            typeValue = NSLocalizedString(@"Transportation Requests", nil);
            break;
        case OTHER_REQUEST:
            typeValue = NSLocalizedString(@"Other Requests", nil);
            break;
        case ENTERTAINMENT_REQUEST:
            typeValue = NSLocalizedString(@"Entertainment Requests", nil);
            break;
        case SPA_REQUEST:
            typeValue = NSLocalizedString(@"Spa Requests", nil);
            break;
        default:
            typeValue = NSLocalizedString(@"All Requests", nil);
            break;
    }
    
    [_lbTopViewType setText:typeValue];
    filteredMyRequests = [self filterRequestsList:(_btUpcoming.isSelected ? myUpComingRequests : myHistoryRequests)];
    _lbNoData.hidden = filteredMyRequests.count > 0;
    [_table reloadData];
}

-(NSMutableArray*) filterRequestsList:(NSMutableArray*) selectedList{
    switch (selectedFilterRequestType) {
        case NO_REQUEST:
            return selectedList;
        default:
            return [self filterRequestByType:selectedFilterRequestType inSelectedList:selectedList];
    }
}

-(NSMutableArray*) filterRequestByType:(enum REQUEST_TYPE) type inSelectedList:(NSMutableArray*) selectedList
{
    NSMutableArray* results = [NSMutableArray array];
    for (MyRequestObject* item in selectedList) {
        switch (type) {
            case HOTEL_REQUEST:
                if(item.parsedRequestType == type
                   || item.parsedRequestType == HOTEL_RECOMMEND_REQUEST){
                    [results addObject:item];
                }
                break;
            case CAR_RENTAL_REQUEST:
                if(item.parsedRequestType == type
                   || item.parsedRequestType == CAR_TRANSFER_REQUEST){
                    [results addObject:item];
                }
                break;
            case GOURMET_REQUEST:
                if(item.parsedRequestType == type
                   || item.parsedRequestType == GOURMET_RECOMMEND_REQUEST){
                    [results addObject:item];
                }
                break;
            case ENTERTAINMENT_REQUEST:
                if(item.parsedRequestType == type
                   || item.parsedRequestType == ENTERTAINMENT_RECOMMEND_REQUEST){
                    [results addObject:item];
                }
                break;
            default:
                if(item.parsedRequestType == type){
                    [results addObject:item];
                }
                break;
        }
        
    }
    return results;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NOTIFY_A_REQUEST_IS_AMENDED
                                                  object:nil];
    if(wsGetRequest){
        [wsGetRequest cancelRequest];
    }
    
    if(wsCarRentalRequestDetails){[wsCarRentalRequestDetails cancelRequest];
    }
    
    if(wsCarTransferRequestDetails){[wsCarTransferRequestDetails cancelRequest];
    }
    
    if(wsGoftRequestDetails){ [wsGoftRequestDetails cancelRequest];
    }
    
    if(wsGourmetRequestDetails){ [wsGourmetRequestDetails cancelRequest];
    }
    
    if(wsHotelRequestDetails){ [wsHotelRequestDetails cancelRequest];
    }
    
    if(wsOtherRequestDetails){[ wsOtherRequestDetails cancelRequest];
    }
    
    if(wsHotelRecommend){[ wsHotelRecommend cancelRequest];
    }
    
    if(wsGetGourmetRecommend){ [wsGetGourmetRecommend cancelRequest];
    }
    
    if(wsGetEntertainmentRequestDetails){ [wsGetEntertainmentRequestDetails cancelRequest];
    }
}

-(void)setTextForViews {
    [_lbTopViewType setText:NSLocalizedString(@"All Requests", nil)];
    [_btUpcoming setTitle:NSLocalizedString(@"Upcoming", @"") forState:UIControlStateNormal];
    [_btUpcoming setTitle:NSLocalizedString(@"Upcoming", @"") forState:UIControlStateSelected];
    [_btHistory setTitle:NSLocalizedString(@"History", @"") forState:UIControlStateNormal];
    [_btHistory setTitle:NSLocalizedString(@"History", @"") forState:UIControlStateSelected];
    [_lbNoData setText:NSLocalizedString(@"You have no requests recorded at the moment. Why not submit new ones? Simply tell us your fancies and our concierge will fulfill your request", nil)];
}




@end
