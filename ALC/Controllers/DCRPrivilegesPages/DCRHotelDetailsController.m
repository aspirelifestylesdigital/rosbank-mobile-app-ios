//
//  DCRHotelDetailsController.m
//  ALC
//
//  Created by Anh Tran on 3/16/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRHotelDetailsController.h"
#import "HotelRequestController.h"
@interface DCRHotelDetailsController ()

@end

@implementation DCRHotelDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:@"DCRItemDetailsController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void) displayItem:(DCRItemDetails*) item{
    [super displayItem:item];
    
    //Add more subview
    [self hideSummaryView];
    
    //Add privilege
    [self addPrivilegeView];
    
    //Add Location view
    [self addLocationView];
    
    //Add Hotel Key view
    //[self addSubView:@"stay" withTitle:@"" value:[NSString stringWithFormat:@"%@<br>", NSLocalizedString(@"HOTEL", nil)] actionOnClicked:nil];    
    
    //Add Web view
    [self addWebSiteView:NSLocalizedString(@"Visit the hotel's website",nil)];
    
    [self addFooterText];
    
    //[self.view layoutIfNeeded];
}


-(void) goToRequestForm:(DCRItemDetails*) item{
    HotelRequestController* controller = (HotelRequestController*) [self controllerByIdentifier:@"HotelRequestController"];
    controller.privilegeDetails = item;
    [self.navigationController pushViewController:controller animated:YES];
}


@end
