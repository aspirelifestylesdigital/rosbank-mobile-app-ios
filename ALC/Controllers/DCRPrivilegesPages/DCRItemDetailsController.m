//
//  DCRItemDetailsController.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemDetailsController.h"
#import "DCRItemDetails.h"
#import "DCRItemGallery.h"
#import "DCRImageObject.h"
#import "DCRDescriptionView.h"
#import "DCRSubItemWithIconView.h"
#import "DCRPrivilegeItemView.h"
#import "DCRRelatedItemObject.h"
#import "DCROpeningHourObject.h"
#import "InAppBrowserViewController.h"
//#import "MapController.h"
#import "PaddingUILabel.h"



@interface DCRItemDetailsController (){
    WSDCRItemDetails* wsItemDetails;
    DCRItemDetails* itemDetails;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollOuterView;
@property (strong, nonatomic) IBOutlet DCRItemGallery *viewGallery;
@property (strong, nonatomic) IBOutlet UIView *viewSummary;
@property (strong, nonatomic) IBOutlet UILabel *lbSummary;
@property (strong, nonatomic) IBOutlet UILabel *lbItemTitle;
@property (strong, nonatomic) IBOutlet UIButton *btBook;
@property (strong, nonatomic) IBOutlet DCRDescriptionView *viewDescription;

@property (strong, nonatomic) IBOutlet UIWebView *wvWebpage;
@property (strong, nonatomic) IBOutlet UILabel *lbImagecopyright;

@end

@implementation DCRItemDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customButtonBack];
    
    [self getItemDetails];
}

- (void) buttonBack{
    if(_wvWebpage.hidden){
        [super buttonBack];
    } else {
        _scrollOuterView.hidden = NO;
        _wvWebpage.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getItemDetails{
    if(_itemId.length <= 2){
        //Load local data from Spa
        NSMutableArray *spaItems = initSpaList();
        int position  = [_itemId intValue];
        if(position >= 0 && position < spaItems.count){
            [self displayItem:[spaItems objectAtIndex:position]];
        }
    } else {
        [self showToast];
        _scrollOuterView.hidden = YES;
        wsItemDetails = [[WSDCRItemDetails alloc] init];
        wsItemDetails.delegate = self;    
        [wsItemDetails findItem:_itemId];
    }
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 11){
        CGRect rect = self.btBook.frame;
        rect.origin.y -= self.navigationController.view.safeAreaInsets.bottom;
        self.btBook.frame = rect;
    }    
}

#pragma API response
-(void)loadDataDoneFrom:(WSBase*)ws{
    // display data to View
    [self displayItem:((WSDCRItemDetails*)ws).item];
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(result!=nil && result.message!=nil){
        // [self showErrorMessage:result.message];
    }
    
    [self stopToast];
}

- (void) displayItem:(DCRItemDetails*) item{
    itemDetails = item;
    _scrollOuterView.hidden = NO;
    [self addTitleToHeader:item.itemName withColor:HEADER_COLOR];
    [_viewGallery showData:item.images];
    [_lbItemTitle setText:[item.itemName uppercaseString]];
    
    if(item.itemSummary.length > 0 ){
        [_lbSummary setAttributedText:[item.itemSummary stringByAddHtml]];
    } else {
        [_viewSummary setHidden:YES];
    }
    
    if(item.description.length > 0){
        [_viewDescription showData:item];
    } else {
        [_viewDescription setHidden:YES];
    }
    
    //TODO: The other sub items are added in each specific item type view
    
}

- (void) addSubView:(NSString*)iconName withTitle:(NSString*)title value:(NSString*)value actionOnClicked:(void(^)())clicked{
    if(value && value.length>0){
        CGRect subViewFrame = CGRectMake(0, 0, SCREEN_WIDTH, 120);
        DCRSubItemWithIconView* subView = [[DCRSubItemWithIconView alloc] initWithFrame:subViewFrame];
        [_stackSubItems addArrangedSubview:subView];
        [subView showView:iconName withTitle:title value:value action:clicked];
    }
}

- (void) addLocationView{
    __unsafe_unretained typeof(self) weakSelf = self;
    [self addSubView:@"pin" withTitle:NSLocalizedString(@"LOCATION",nil) value:[itemDetails fullLocationAddress] actionOnClicked:^(){
                    [weakSelf openGoogleMap];
                    }];
}

- (void) addWebSiteView:(NSString*) text{
    //Add Web view
    if(itemDetails.websiteUrl.length > 0){
        __unsafe_unretained typeof(self) weakSelf = self;
        [self addSubView:@"icon-website" withTitle:@"" value:[NSString stringWithFormat:@"%@%@",text,@"<br>"] actionOnClicked:^(){
            [weakSelf openWebPage];
        }];

    }
}

- (void) addOpenHoursView{
    //Add Opening Hours view
    if(itemDetails.openingHours && itemDetails.openingHours.count > 0){
        NSString* fullHours = [itemDetails fullOpeningHoursDetails];
        [self addSubView:@"icon-hours" withTitle: NSLocalizedString(@"OPERATING HOURS",nil) value:fullHours actionOnClicked:nil];
    }    
}

- (void) addPrivilegeView{
    if(itemDetails.relatedItems && itemDetails.relatedItems.count>0){
        NSString* displayValue = @"";
        NSString* subValue = @"";
        for (DCRRelatedItemObject* relatedItem in itemDetails.relatedItems) {
            if (isEqualIgnoreCase(@"privilege", relatedItem.type.Code)) {
                 subValue = [NSString stringWithFormat:@"<strong>%@</strong>%@%@",relatedItem.Name,
                                          (relatedItem.itemDescription.length > 0 ? @"<br>" : @""),
                                          relatedItem.itemDescription];
                if(subValue.length > 0){
                    displayValue = [displayValue stringByAppendingString:[NSString stringWithFormat:@"%@%@",
                     (displayValue.length > 0 ? @"<br><br>" : @""),
                     subValue]];
                }
            }
        }
        
        if(displayValue.length > 0){
            CGRect subViewFrame = CGRectMake(0, 0, SCREEN_WIDTH, 120);
            DCRPrivilegeItemView* subView = [[DCRPrivilegeItemView alloc] initWithFrame:subViewFrame];
            [_stackSubItems addArrangedSubview:subView];
            [subView showView:@"info" withTitle:NSLocalizedString(@"PRIVILEGES", nil) value:displayValue action:nil];
        }
        
    }
}



- (void) addFooterText{
    [_lbImagecopyright setFont:[UIFont fontWithName:@"AvenirLTStd-Oblique" size:SCREEN_SCALE*14]];
    [_lbImagecopyright setText:[NSString stringWithFormat:@"\n%@ %@ %@\n\n",
                    NSLocalizedString(@"*image is property of", nil), itemDetails.itemName,
                    NSLocalizedString(@"and shall not be reproduced without permission", nil)]];
}


- (IBAction)actionBookClicked:(id)sender {
    //TODO: go to Request form
    UIAlertAction *signInAction = [UIAlertAction actionWithTitle:SIGN_IN
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                               setRootViewLogout();
                                                           }];
    if(!showAlertTopOpenLogin([SlideNavigationController sharedInstance] , signInAction)           && itemDetails){
        [self goToRequestForm:itemDetails];
    }
}


-(void) goToRequestForm:(DCRItemDetails*) item{
    
}

- (void) openWebPage{
    [self showToast];
    [_wvWebpage loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:itemDetails.websiteUrl]]];
    _scrollOuterView.hidden = YES;
    _wvWebpage.hidden = NO;
}

- (void) openGoogleMap{
    if(itemDetails.latitude && itemDetails.latitude.length>0 && itemDetails.longitude && itemDetails.longitude.length > 0){
        NSString* params = [NSString stringWithFormat:@"q=loc:%@,%@&zoom=14", itemDetails.latitude,itemDetails.longitude ];
        /*if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps:"]]) {
         NSString *urlString = [NSString stringWithFormat:@"comgooglemaps://?%@",params];
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
         } else {*/
        NSString *string = [NSString stringWithFormat:@"http://maps.google.com/maps?%@",params];
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
        // Go to In app Map
        NSLog(@"%@", [NSString stringWithFormat:@"q=loc:%@,%@&zoom=14",itemDetails.latitude,itemDetails.longitude]);
        InAppBrowserViewController* controller = [[InAppBrowserViewController alloc] initWithNibName:@"InAppBrowserViewController" bundle:nil];
        controller.urlBooking = string;
        controller.headerTitle =  NSLocalizedString(@"LOCATION", nil);
        controller.isLiveChat = NO;
        [self.navigationController pushViewController:controller animated:YES];
        
        //}
    }
}

- (void) hideSummaryView{
    [_viewSummary setHidden:YES];
}

- (void)dealloc
{
    if(wsItemDetails){ [wsItemDetails cancelRequest];
    }
}


#pragma mark - WebView Controller

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopToast];
}

//- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
//{
//    [self stopToast];
//}

//- (void)webViewDidFinishLoad:(UIWebView *)webView {
//    [self stopToast];
//}
/*
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *requestString = [[[request URL] absoluteString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSLog(requestString);
    
    if ([requestString containsString:@"snapwidget.com"]) {
        return NO;
    }
    return YES;
    
}*/


/// Spa Local Data
NSMutableArray* initSpaList(){
    NSMutableArray* spaItems = [NSMutableArray array];
    DCRCommonObject *commmonObject;
    DCRImageObject* image;
    DCROpeningHourObject *hour;
    NSMutableArray* imagesArray;
    NSMutableArray* openHoursArray;
    
    DCRItemDetails *item = [[DCRItemDetails alloc] init];
    item.itemId = @"2";
    item.itemName =@"Float Studio";
    item.paredItemType = SPA_ITEM;
    item.itemDescription = @"Флоатинг – это уникальный терапевтический метод достижения глубинной релаксации в специальной флоат-комнате. Процедура проходит в бассейне с солевым раствором, который позволяет телу «парить» на поверхности. Флоатинг активно используется не только в качестве релакс-процедуры, он благотворно влияет на весь организм целиком и имеет мощный косметологический эффект. В то время как каждая мышца тела отдыхает, ваше давление и сердечный ритм приходят в норму, органы насыщаются кислородом, а состояние кожи заметно улучшается.<br><br>Yслуги - Уход за лицом, Комплексный уход за лицом/телом, Мануальная терапия, Оздоровительные процедуры";
    item.spaFullfillmentInstruction = @"";
    item.addressLine1 = @"ул. Большая Татарская, д. 7";
    item.addressLine2 = @"";
    item.addressNeighbourhood = @"";
    item.addressPostalCode = @"115184";
    commmonObject = [[DCRCommonObject alloc] init];
    commmonObject.Id = @"";
    //Moscow
    commmonObject.Name = @"Москва";
    item.addressCity = commmonObject;
    commmonObject = [[DCRCommonObject alloc] init];
    commmonObject.Id = @"";
    // Russian Fedaration
    commmonObject.Name = @"Россия";
    item.addressCountry = commmonObject;
    item.phoneNumber = @"+7 499 110 52 10";
    item.websiteUrl = @"http://floatstudio.ru/";
    item.longitude = @"37.633522";
    item.latitude = @"55.740572";
    //item.spaService = @"Facials, Treatment Packages, Massage Therapies, Wellness Offerings";
    item.spaService = @"Уход за лицом, Комплексный уход за лицом/телом, Мануальная терапия, Оздоровительные процедуры";
    item.spaTreatment = @"";
    item.spaHighlight = @"Флоат Студия - это современное пространство расслабления и оздоровления в самом центре Москвы.";
    item.itemSummary = item.spaHighlight;
    imagesArray = [NSMutableArray array];
    image = [[DCRImageObject alloc] init];
    image.url = @"Float Studio1.jpg";
    [imagesArray addObject:image];
    image = [[DCRImageObject alloc] init];
    image.url = @"Float Studio2.jpg";
    [imagesArray addObject:image];
    item.images = imagesArray;
    
    openHoursArray = [NSMutableArray array];
    hour = [[DCROpeningHourObject alloc] init];
    hour.openingFromDay = @"Пн";
    hour.openingFromHour = @"10:00";
    hour.openingToHour = @"22:00";
    hour.openingToDay = @"Вс";
    [openHoursArray addObject:hour];
    item.openingHours = openHoursArray;
    
    DCRRelatedItemObject *p = [[DCRRelatedItemObject alloc] init];
    DCRCommonObject *type = [[DCRCommonObject alloc] init];
    type.Name = @"privilege";
    type.Code = @"privilege";
    p.type = type;
    p.Name = @"Скидка 10% на все услуги";
    p.itemDescription = [NSString stringWithFormat:@"<br>%@<br><br>%@",  NSLocalizedString(@"Fulfillment instructions", nil), @"Для того, чтобы воспользоваться скидкой, пожалуйста, обратитесь в Консьерж-службу"];
    
    item.relatedItems = [NSArray arrayWithObjects:p, nil];
    
    [spaItems addObject:item];

    ///////////////////////////////////////
    item = [[DCRItemDetails alloc] init];
    item.itemId = @"1";
    item.itemName =@"Mahash SPA в отеле Intercontinental";
    item.paredItemType = SPA_ITEM;
    item.itemSummary = @"";
    item.itemDescription = @"MAHASH – это всегда шанс оторваться от действительности, чтобы найти себя или вернуться к себе в душевном состоянии, в образе или в стиле. Место, где можно расслабиться и отдохнуть. Источник восстановления внутренних сил человека, целебной энергетики и мудрости, накопленных веками аюрведических и восточных практик, слитых воедино в эстетике растительной косметики.<br><br>Yслуги - услугУход за лицом, Комплексный уход за лицом/телом, Обертывание, Мануальная терапия";
    item.spaFullfillmentInstruction = @"";
    item.addressLine1 = @"ул. Тверская, д. 22, гостиница Intercontinental, -1 этаж";
    item.addressLine2 = @"";
    item.addressNeighbourhood = @"";
    item.addressPostalCode = @"127006";
    commmonObject = [[DCRCommonObject alloc] init];
    commmonObject.Id = @"";
    //Moscowdev
    commmonObject.Name = @"Москва";
    item.addressCity = commmonObject;
    commmonObject = [[DCRCommonObject alloc] init];
    commmonObject.Id = @"";
    //Russian Fedaration
    commmonObject.Name = @"Россия";
    item.addressCountry = commmonObject;
    item.phoneNumber = @"+7 499 951 95 03";
    item.websiteUrl = @"http://www.mahash.ru/mahash/index.php";
    item.longitude = @"37.601051";
    item.latitude = @"55.767371";
    item.spaService = @"Уход за лицом, Комплексный уход за лицом/телом, Обертывание, Мануальная терапия";
    //item.spaService = @"Facials, Treatment Packages, Body Treatments, Massage Therapies";
    item.spaTreatment = @"";
    item.spaHighlight = @"MAHASH – это ведущий международный бренд, представляющий СПА салоны, салоны красоты и собственную органическую марку косметики в Москве и Европе.";
    item.itemSummary = item.spaHighlight;
    imagesArray = [NSMutableArray array];
    image = [[DCRImageObject alloc] init];
    image.url = @"Mahash SPA1.jpg";
    [imagesArray addObject:image];
    image = [[DCRImageObject alloc] init];
    image.url = @"Mahash SPA2.jpg";
    [imagesArray addObject:image];
    item.images = imagesArray;
    
    openHoursArray = [NSMutableArray array];
    hour = [[DCROpeningHourObject alloc] init];
    hour.openingFromDay = @"Пн";
    hour.openingFromHour = @"10:00";
    hour.openingToHour = @"23:59";
    hour.openingToDay = @"Вс";
    [openHoursArray addObject:hour];
    item.openingHours = openHoursArray;
    
    DCRRelatedItemObject *p1 = [[DCRRelatedItemObject alloc] init];
    p1.type = type;
    p1.Name = @"Скидка 30%  на все СПА-процедуры для участников программы MAHASH Corporate Wellness";
    p1.itemDescription = [NSString stringWithFormat:@"<br>%@<br><br>%@",  NSLocalizedString(@"Fulfillment instructions", nil), @"Для того, чтобы воспользоваться скидкой, пожалуйста, обратитесь в Консьерж-службу"];
    
    DCRRelatedItemObject *p2 = [[DCRRelatedItemObject alloc] init];
    p2.type = type;
    p2.Name = @"Скидка 15% на все услуги салона при единоразовом посещении";
    p2.itemDescription = [NSString stringWithFormat:@"<br>%@<br><br>%@<br>",  NSLocalizedString(@"Fulfillment instructions", nil), @"Для того, чтобы воспользоваться скидкой, пожалуйста, обратитесь в Консьерж-службу"];
    
    item.relatedItems = [NSArray arrayWithObjects:p1, p2, nil];
    
    [spaItems addObject:item];
    
    return spaItems;
}

@end
