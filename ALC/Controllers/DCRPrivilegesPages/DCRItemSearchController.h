//
//  DCRItemSearchController.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "WSDCRItemSearch.h"

@interface DCRItemSearchController : MainViewController<DataLoadDelegate,UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) enum DCR_ITEM_TYPE filterType;

@end
