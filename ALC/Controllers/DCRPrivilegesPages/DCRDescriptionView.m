//
//  DCRDescriptionView.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRDescriptionView.h"
#import "NSString+HTML.h"
@interface DCRDescriptionView(){
    NSLayoutConstraint *heightDescriptionConstraint;
}
@end
@implementation DCRDescriptionView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"DCRDescriptionView" owner:self options:nil] firstObject];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
        [self setupSubview];
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        self.view.translatesAutoresizingMaskIntoConstraints = false;
        
        [self addSubview:self.view];
        [self setupSubview];
        
        return self;
    }
    
    return self;
}

-(void)setupSubview {
    [[self.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:0.0] setActive:YES];
    [[self.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:0.0] setActive:YES];
    [[self.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:0.0] setActive:YES];
    [[self.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:0.0] setActive:YES];
}

- (IBAction)actionClickedReadMore:(id)sender {
    [_btReadmore setSelected:!_btReadmore.isSelected];
    //Selected is Read less => collapse label
    if(_btReadmore.isSelected){
        [_lbDescription setNumberOfLines:0];
        //[self removeHeightConstraint];
    } else {
        [_lbDescription setNumberOfLines:10];
        //[self addHeightContrainstToDescription];
    }
}

- (IBAction)actionClickedTermsOfUse:(id)sender {
     [_btTermsOfUse setSelected:!_btTermsOfUse.isSelected];
    _lbTermsOfUse.hidden = !_btTermsOfUse.isSelected;
}

-(void) showData:(DCRItemDetails*) item{
    [_lbDescription setAttributedText:[item.itemDescription stringByAddHtml]];
    
    if(item.termsAndConditions.length > 0){
        [_lbTermsOfUse setAttributedText:[item.termsAndConditions stringByAddHtml]];
    } else {
        _viewTermsOfUse.hidden = YES;
    }
    
    _btReadmore.hidden = [self limitEqualmaxForLabelDescription];
    
}


- (BOOL) limitEqualmaxForLabelDescription {
    BOOL isEqual = YES;
    UILabel* label = [[UILabel alloc] initWithFrame:_lbDescription.frame];
    label.font = _lbDescription.font;
    label.attributedText = _lbDescription.attributedText;
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    label.numberOfLines = 0;
    
    UILabel* label1 = [[UILabel alloc] initWithFrame:_lbDescription.frame];
    label1.font = _lbDescription.font;
    label1.attributedText = _lbDescription.attributedText;
    [label1 setLineBreakMode:NSLineBreakByWordWrapping];
    label1.numberOfLines = 10;
    
    int unlimitHeight = 0;
    int limitHeight = 0;
    CGSize textSize = CGSizeMake(label.frame.size.width, MAXFLOAT);
    unlimitHeight = (int) lroundf([label sizeThatFits:textSize].height);
    
    limitHeight = (int) lroundf([label1 sizeThatFits:textSize].height);
    
    isEqual = unlimitHeight - limitHeight < _lbDescription.font.lineHeight*SCREEN_SCALE;
    
    return isEqual;
}



@end
