//
//  DCRPrivilegeItemView.m
//  ALC
//
//  Created by Anh Tran on 3/29/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRPrivilegeItemView.h"

@interface DCRPrivilegeItemView(){
    
}
@property (nonatomic, strong) void(^completionClicked)();
@end

@implementation DCRPrivilegeItemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"DCRPrivilegeItemView" owner:self options:nil] firstObject];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    resetScaleViewBaseOnScreen(self);
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        resetScaleViewBaseOnScreen(self);
        return self;
    }
    
    return self;
}

- (IBAction)actionClickedReadMore:(id)sender {
    [_btReadmore setSelected:!_btReadmore.isSelected];
    //Selected is Read less => collapse label
    if(_btReadmore.isSelected){
        [_lbValue setNumberOfLines:0];
    } else {
        [_lbValue setNumberOfLines:10];
    }
}

-(void) showView:(NSString*)iconName withTitle:(NSString*) title value:(NSString*)value action:(void(^)())clicked{
    
    if(iconName){
        [_icon setImage:[UIImage imageNamed:iconName]];
        
    }
    if(title){
        [_lbTite setText:title];
    }
    if(value){
         [_lbValue setAttributedText:[value stringByAddHtml]];
    }
    _viewReadMore.hidden = [self limitEqualmaxForLabelDescription];
}

- (BOOL) limitEqualmaxForLabelDescription {
        BOOL isEqual = YES;
        UILabel* label = [[UILabel alloc] initWithFrame:_lbValue.frame];
        label.font = _lbValue.font;
        label.attributedText = _lbValue.attributedText;
        [label setLineBreakMode:NSLineBreakByWordWrapping];
        label.numberOfLines = 0;
        
        UILabel* label1 = [[UILabel alloc] initWithFrame:_lbValue.frame];
        label1.font = _lbValue.font;
        label1.attributedText = _lbValue.attributedText;
        [label1 setLineBreakMode:NSLineBreakByWordWrapping];
        label1.numberOfLines = 10;
        
        int unlimitHeight = 0;
        int limitHeight = 0;
        CGSize textSize = CGSizeMake(label.frame.size.width, MAXFLOAT);
        unlimitHeight = (int) lroundf([label sizeThatFits:textSize].height);
        
        limitHeight = (int) lroundf([label1 sizeThatFits:textSize].height);
        
        isEqual = unlimitHeight - limitHeight < _lbValue.font.lineHeight*SCREEN_SCALE;
        
        return isEqual;
}


@end
