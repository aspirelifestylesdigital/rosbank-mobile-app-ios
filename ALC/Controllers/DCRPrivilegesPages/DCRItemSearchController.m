//
//  DCRItemSearchController.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemSearchController.h"

#import "GourmetLandingViewCell.h"
#import "BaseResponseObject.h"
#import "DCRItemObject.h"
#import "UIImageView+AFNetworking.h"
#import "DCRDiningDetailsController.h"
#import "DCRHotelDetailsController.h"
#import "DCRSpaDetailsController.h"

@interface IndexItem : NSObject
@property (nonatomic) NSInteger index;
@property (nonatomic) NSInteger count;
@end
@implementation IndexItem

@end

@interface DCRItemSearchController (){
    int cellHeight;
    NSMutableArray *itemResults;
    BOOL isLoading;
    IBOutlet UITableView *table;
    WSDCRItemSearch *wsItemSearch;
    AppDelegate* appdele;
    NSMutableDictionary *selectedFilterOptions;
    NSMutableDictionary* alphabetIndex;
}

@end

@implementation DCRItemSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customButtonBack];
    
    [self setHeaderTitle];
    
    cellHeight = CATEGORY_LIST_CELL_HEIGHT*SCREEN_SCALE;
    
    
    appdele =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [self searchItems];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setHeaderTitle{
    NSString* title = @"";
    switch (_filterType) {
        case DINING_ITEM:
            title = NSLocalizedString(@"PREFERRED RESTAURANT PROGRAM", nil);
            break;
        case ACCOMMODATION_ITEM:
            title = NSLocalizedString(@"PREFERRED HOTEL PROGRAM", nil);
            break;
        case SPA_ITEM:
            title = NSLocalizedString(@"PREFERRED SPA PROGRAM", nil);
            break;
        default:
            break;
    }
    [self addTitleToHeader:title withColor:HEADER_COLOR];
}

- (void) searchItems{
    [self searchItemsWithOptions:nil];
}

-(void) searchItemsWithOptions:(NSMutableDictionary *)options{
    itemResults = [NSMutableArray array];
    alphabetIndex = [self createCharacterIndex];
    [table reloadData];
    
    if(_filterType != SPA_ITEM){
        [self showToast];
        isLoading = true;
        wsItemSearch = [[WSDCRItemSearch alloc] init];
        wsItemSearch.delegate = self;
    
        [wsItemSearch searchItemType:_filterType andSubType:nil];
    } else {
        itemResults = initSpaList();
        [table reloadData];
    }
}

#pragma API response
-(void)loadDataDoneFrom:(WSBase*)ws{
    // display data to View
    [self indexItems:((WSDCRItemSearch*)ws).items fromIndex:itemResults.count];
    [itemResults addObjectsFromArray:((WSDCRItemSearch*)ws).items];
    
    [table reloadData];
    
    isLoading = false;
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(result!=nil && result.message!=nil){
        // [self showErrorMessage:result.message];
    }
    
    isLoading = false;
    [self stopToast];
}

#pragma Table Delegate
/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(itemResults.count > 10){
        return [self availableIndexValue];
    }
    return [NSArray array];
}*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemResults count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"GourmetLandingViewCell";
    
    GourmetLandingViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(!cell){
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"GourmetLandingViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        resetScaleViewBaseOnScreen(cell);
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    DCRItemObject *item = ((DCRItemObject*)[itemResults objectAtIndex:indexPath.row]);
    NSString *modifiedString = [item.itemName stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    [cell showTitle:modifiedString andImage:[item getFirstImageUrl]];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //TODO: open Gourmet details
    DCRItemObject *item = ((DCRItemObject*)[itemResults objectAtIndex:indexPath.row]);
    switch (item.paredItemType) {
        case DINING_ITEM:{
            DCRDiningDetailsController* controller = [[DCRDiningDetailsController alloc] init];
            controller.itemId = item.itemId;
            [self.navigationController pushViewController:controller animated:YES];
         }
            break;
        case ACCOMMODATION_ITEM:{
            DCRHotelDetailsController* controller = [[DCRHotelDetailsController alloc] init];
            controller.itemId = item.itemId;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case SPA_ITEM:{
            DCRSpaDetailsController* controller = [[DCRSpaDetailsController alloc] init];
            //controller.itemId = item.itemId;
            
            //Load SPA local data
            controller.itemId =  [NSString stringWithFormat:@"%ld", indexPath.row];
            //end local
            
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        default:
            break;
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isLoading && indexPath.row == itemResults.count - 1) {
        if(wsItemSearch!=nil && [wsItemSearch hasNextItem]){
            isLoading = true;
            [wsItemSearch nextPage];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if(![[alphabetIndex objectForKey:title] isEqual:[NSNull null]]){
        IndexItem* indexItem = [alphabetIndex objectForKey:title];
        if(indexItem){
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexItem.index inSection:0];
            [table scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionTop
                                 animated:NO];
        }
    }
    return 1;
}


-(void) selectedFilterOptions:(NSMutableDictionary*) options{
    selectedFilterOptions = options;
    
    [self searchItemsWithOptions:selectedFilterOptions];
}

#pragma SetUp Index

-(NSMutableDictionary*) createCharacterIndex{
    /*return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            [NSNull null], @"A",
            [NSNull null],@"B",
            [NSNull null],
            @"C",[NSNull null],
            @"D",[NSNull null],
            @"E",[NSNull null],
            @"F",[NSNull null],
            @"G",[NSNull null],
            @"H",[NSNull null],
            @"I",[NSNull null],
            @"J",[NSNull null],
            @"K",[NSNull null],
            @"L",[NSNull null],
            @"M",[NSNull null],
            @"N",[NSNull null],
            @"O",[NSNull null],
            @"P",[NSNull null],
            @"Q",[NSNull null],
            @"R",[NSNull null],
            @"S",[NSNull null],
            @"T",[NSNull null],
            @"U",[NSNull null],
            @"V",[NSNull null],
            @"W",[NSNull null],
            @"X",[NSNull null],
            @"Y",[NSNull null],
            @"Z",[NSNull null],
            @"#",nil
            ];*/
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            [NSNull null], @"А",
            [NSNull null],@"Б",
            [NSNull null],
            @"В",[NSNull null],
            @"Г",[NSNull null],
            @"Д",[NSNull null],
            @"Е",[NSNull null],
            @"Ё",[NSNull null],
            @"Ж",[NSNull null],
            @"З",[NSNull null],
            @"И",[NSNull null],
            @"Й",[NSNull null],
            @"К",[NSNull null],
            @"Л",[NSNull null],
            @"М",[NSNull null],
            @"Н",[NSNull null],
            @"О",[NSNull null],
            @"П",[NSNull null],
            @"Р",[NSNull null],
            @"С",[NSNull null],
            @"Т",[NSNull null],
            @"У",[NSNull null],
            @"Ф",[NSNull null],
            @"Х",[NSNull null],
            @"Ц",[NSNull null],
            @"Ч",[NSNull null],
            @"Ш",[NSNull null],
            @"Щ",[NSNull null],
            @"Ъ",[NSNull null],
            @"Ы",[NSNull null],
            @"Ь",[NSNull null],
            @"Э",[NSNull null],
            @"Ю",[NSNull null],
            @"Я",[NSNull null],
            @"#",nil
            ];
}

-(void) indexItems:(NSArray*)newList fromIndex:(NSInteger)startIndex{
    NSInteger len = newList.count;
    NSString* firstChar = @"";
    IndexItem *index;
    for (int i = 0; i < len; i++) {
        firstChar = [[((DCRItemObject*)[newList objectAtIndex:i]).itemName substringToIndex:1] uppercaseString];
        if(![self isAlphabetCharacter:firstChar]){
            firstChar = @"#";
        }
        
        index = [alphabetIndex objectForKey:firstChar];
        if(![index isEqual:[NSNull null]]){
            index.count += 1;
        } else {
            index = [[IndexItem alloc] init];
            index.count = 1;
            index.index = i + startIndex;
            [alphabetIndex setObject:index forKey:firstChar];
        }
    }
}

-(NSArray*) availableIndexValue{
   return @[ @"\u0410",
                                 @"Б",
                               @"\u0412",
                               @"Г",
                               @"Д",
                               @"Е",
                               @"Ё",
                               @"Ж",
                               @"З",
                               @"И",
                               @"Й",
                               @"К",
                               @"Л",
                               @"М",
                               @"Н",
                               @"О",
                               @"П",
                               @"Р",
                               @"С",
                               @"Т",
                               @"У",
                               @"Ф",
                               @"Х",
                               @"Ц",
                               @"Ч",
                               @"Ш",
                               @"Щ",
                               @"Ъ",
                               @"Ы",
                               @"Ь",
                               @"Э",
                               @"Ю",
                               @"Я",
                               @"#"
                               ];
    /*[NSMutableArray array];
    for (NSString* character in [alphabetIndex allKeys]) {
        //if (((IndexItem*)[alphabetIndex objectForKey:character]).count >= 1){
            [results addObject:character];
        //}
    }
    results = [NSMutableArray arrayWithArray:[results sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    if([results containsObject:@"#"]){
        //Whereever # place, move it to the last position
        [results removeObject:@"#"];
        [results addObject:@"#"];
    }*/
    //return results;
}

-(BOOL) isAlphabetCharacter:(NSString*) character{
    //Create character set
    NSCharacterSet *validChars = [NSCharacterSet letterCharacterSet];
    
    //Invert the set
    validChars = [validChars invertedSet];
    
    //Check against that
    NSRange  range = [character rangeOfCharacterFromSet:validChars];
    if (NSNotFound != range.location) {
        return NO;
    }
    return YES;
}


- (void)dealloc
{
    if(wsItemSearch){ [wsItemSearch cancelRequest];
    }
}
@end


