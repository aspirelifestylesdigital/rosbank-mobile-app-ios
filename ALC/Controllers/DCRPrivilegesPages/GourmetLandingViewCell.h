//
//  GourmetLandingViewCell.h
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageViewAligned.h"
@interface GourmetLandingViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbSubTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbDistance;
@property (strong, nonatomic) IBOutlet UIView *viewSubTitle;
@property (strong, nonatomic) IBOutlet UIImageViewAligned *imvMainImg;

-(void) showTitle:(NSString*)title andImage:(NSString*)img;
@end
