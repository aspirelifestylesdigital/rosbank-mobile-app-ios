//
//  AccountController.h
//  ALC
//
//  Created by Hai NguyenV on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountController : MainViewController

- (IBAction)actionProfile:(id)sender;
@end
