//
//  HomeViewController.m
//  ALC
//
//  Created by Hai NguyenV on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HomeViewController.h"
#import "StoreUserData.h"
#import "UserObject.h"
#import "ChatBotViewController.h"
#import "BaseResponseObject.h"
#import "GourmetObject.h"
#import "HomeGourmetItem.h"
#import "UIImageView+AFNetworking.h"
#import "ThankYouBookingController.h"
#import "LocationPopUpViewController.h"
#import "MyRequestController.h"
#import "ChangePasswordController.h"
#import "HambugerController.h"
#import "ExperienceItem.h"
#import "GourmetRequestController.h"
#import "EventRequestController.h"
#import "HotelRequestController.h"
#import "GolfRequestController.h"
#import "OrthersBookingController.h"
#import "CarRentalTransferRequestController.h"
#import "DCRItemSearchController.h"
#import "ALCQC-Swift.h"

#define PLACEHOLDER_TEXTS @[NSLocalizedString(@"Can you recommend a private guide?",nil), NSLocalizedString(@"Book me a private jet to Paris",nil), NSLocalizedString(@"Book a private chef's table in Melbourne",nil), NSLocalizedString(@"Are there any hidden treasures you'd recommend?", nil)]

@interface HomeViewController (){
    WSCreateOrthersRequest* wsCreateOtherRequest;
    NSTimer* placeHolderTimer;
    int placeHolderTextIndex;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  //  [self addTitleToHeader:NSLocalizedString(@"HOME",nil) withColor:HEADER_COLOR];
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRollback)
                                                 name:NOTIFY_CLICK_HOME
                                               object:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
   
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
        appdelegate.socialView.hidden = YES;
        [appdelegate.socialMenu itemsWillDisapearIntoButton:appdelegate.socialButton];
        
    }];
    
    UserObject* user = getUser();
    if(isUserLoggedIn()){
        
        // Force user change password after reseting
        if(isUserForgot()){
            ChangePasswordController* controller = (ChangePasswordController*) [self controllerByIdentifier:@"ChangePasswordController"];
            controller.isForceChange = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        self.pLbUserName.text = user.fullName;
        self.pLbWelcome.text = NSLocalizedString(@"Welcome, ", @"");
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        appdele.ONLINE_MEMBER_ID = user.userId;
        
    }
    else{
        self.pLbUserName.text = @"";
        self.pLbWelcome.text = NSLocalizedString(@"Welcome Guest", @"");
    }
 
    self.pPositionY.constant = 213*SCREEN_SCALE;
    
    self.pHeightMain.constant =672*SCREEN_SCALE;
    
    self.pPositionRecommen.constant = 672*SCREEN_SCALE;
    self.pPositionArrow.constant = 15*SCREEN_SCALE;
    
    //Set animation for bottom Arrow
    [self.btnArrow setImage:[UIImage imageNamed:@"icon_scrolldown.png"] forState:UIControlStateNormal];
    self.btnArrow.imageView.animationImages =[NSArray arrayWithObjects:
                                              [UIImage imageNamed:@"icon_scrolldown.png"],
                                              [UIImage imageNamed:@"icon_scrolldown1.png"],
                                              [UIImage imageNamed:@"icon_scrolldown3.png"],
                                              [UIImage imageNamed:@"icon_scrolldown4.png"],
                                              [UIImage imageNamed:@"icon_scrolldown2.png"],nil];
    self.btnArrow.imageView.animationDuration = 1.2;
    [self.btnArrow setNeedsLayout];
    // end Arrow

    //icon_home_logo
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 93, 43)];
    [button setImage:[AppConfig shared].companyLightLogo forState:UIControlStateNormal];
    [button addTarget:self action:@selector(logoClick) forControlEvents:UIControlEventTouchDownRepeat];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = rightBarButtonItem;
    
    pRecommendedViews = [[NSMutableArray alloc] init];
    [self addFakeRecommendItem:[self createFakeExperienceRecommend]];

    /*
    wsGourmetList = [[WSGourmerList alloc] init];
    wsGourmetList.delegate = self;
    wsGourmetList.pageSize = 3;
    if(user!=nil){
        wsGourmetList.userID = user.userId;
    }
    [wsGourmetList getGourmetListWithOptions:nil];
    positionY = 0;
     */
}

-(void) setTextForViews
{
    self.greetingMsgLbl.text = NSLocalizedString(@"Hello! How can we be of assistance today?", @"");
    self.lbPlaceHolder.text = NSLocalizedString(@"Can you recommend a private guide?", @"");
    [self.askUsBtn setTitle:NSLocalizedString(@"ASK US", @"") forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
    self.introduceMsgLbl.text = NSLocalizedString(@"Let us help you with the following", nil);
}


-(void)didRollback {
    [_pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [_icRequestMsgError setHidden:YES];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture{
    [_tfChatAsk resignFirstResponder];
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UserObject* user = getUser();
    if(isUserLoggedIn()){
        self.pLbUserName.text = user.fullName;
        self.pLbWelcome.text = NSLocalizedString(@"Welcome, ", @"");
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        appdele.ONLINE_MEMBER_ID = user.userId;
        
    }
    else{
        self.pLbUserName.text = @"";
        self.pLbWelcome.text = NSLocalizedString(@"Welcome Guest", @"");
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [((HambugerController*)appdelegate.rootVC.rightMenu) redisplayHome];
    
   /* BOOL didOpenLocationPopUp = [[NSUserDefaults standardUserDefaults] boolForKey:DID_OPEN_LOCATION_POP_UP];
    if (!didOpenLocationPopUp) {
        LocationPopUpViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_LOCATION_POP_UP_VIEW_CONTROLLER];
        [vc willAddLocationPopupViewControllerInViewController:[SlideNavigationController sharedInstance]];
     
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DID_OPEN_LOCATION_POP_UP];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }*/
    
    // open conciegre button and tooltip at the first time
    
    NSString* flagShow = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_SHOWTOOLTIP];
    if (![flagShow isEqualToString:@"0"])
    {
        [self showConciergeButtonToolTip];
    }
    
    [self startFlingArrow];
    [self startFlingPlaceHolder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self stopFlingArrow];
}

-(void)logoClick{
    [self setShowSplashScreen];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionShowRecom:(id)sender{
    
    [self.pScrollView setContentOffset:CGPointMake(0, 75+195*SCREEN_SCALE+(195*SCREEN_SCALE)/2) animated:YES];
    self.btnArrow.hidden = YES;
}

-(void)addRecommendItem:(NSMutableArray*)array{
    int index= 0;
    index = (int)(pArrRecommended.count - array.count);
    
    int height =195*SCREEN_SCALE;
    
    for (int i = 0; i<array.count; i++) {
        GourmetObject* gourmet = [array objectAtIndex:i];
        
        HomeGourmetItem* homeItem = [[HomeGourmetItem alloc] initWithFrame:CGRectMake(0, positionY, SCREEN_WIDTH, height)];
        homeItem.lbRestaurantName.text = gourmet.restaurantName;
        [homeItem.imvView setImageWithURL:[NSURL URLWithString:gourmet.backgroundImg] placeholderImage:nil];
        homeItem.delegate = self;
        if(isUserLoggedIn()){
            [homeItem.btnAddFaviote setSelected:gourmet.isInterested];
        }else{
            homeItem.btnAddFaviote.hidden = YES;
        }
        homeItem.restaurantID = gourmet.restaurantID;
        homeItem.tag = (index + i);
        [self.pvRecommendItemView addSubview:homeItem];
        [pRecommendedViews addObject:homeItem];
        
        positionY += height;
    }
    int totalHeightItem =(int)pArrRecommended.count*height;
    self.pHeightItemRecom.constant = totalHeightItem;
    self.pHeightRecomm.constant =totalHeightItem+130;
    self.pHeightMain.constant = (672*SCREEN_SCALE)+totalHeightItem+130;
}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(SCREEN_SCALE<0.9)
        [self.pScrollView setContentOffset:CGPointMake(0, 160) animated:YES];
    else if(SCREEN_SCALE<1)
        [self.pScrollView setContentOffset:CGPointMake(0, 80) animated:YES];
    else
        [self.pScrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    
    _icRequestMsgError.hidden = YES;
    
    [self stopFlingPlaceHolder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
        self.btnArrow.hidden = YES;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(scrollView.contentOffset.y==0)
        self.btnArrow.hidden = NO;
    else
        self.btnArrow.hidden = YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    //float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if(scrollView.contentOffset.y==0)
        self.btnArrow.hidden = NO;
    else
        self.btnArrow.hidden = YES;
    /*if (bottomEdge >= scrollView.contentSize.height) {
        // we are at the end
        if(!isConnecting && pArrRecommended.count<60){
            isConnecting = YES;
            [wsGourmetList nextPage];
        }
    }*/
    
}

-(void)onFaviotesClick:(int)position{
    [self showToast];
}

-(void)onFaviotesDone:(int)position withValues:(BOOL)isLike{
    [self stopToast];
    GourmetObject* gourmet = [pArrRecommended objectAtIndex:position];
    gourmet.isInterested = isLike;
    [pArrRecommended replaceObjectAtIndex:position withObject:gourmet];
}

-(void)onItemClick:(int)position{
    if(position == 0){
        GourmetRequestController* controller = (GourmetRequestController*) [self controllerByIdentifier:@"GourmetRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    } else if(position == 1){
        HotelRequestController* controller = (HotelRequestController*) [self controllerByIdentifier:@"HotelRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    } else if(position == 2){
        EventRequestController* controller = (EventRequestController*) [self controllerByIdentifier:@"EventRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    } else if(position == 3){
        CarRentalTransferRequestController* controller = (CarRentalTransferRequestController*) [self controllerByIdentifier:@"CarRentalTransferRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    }else if(position == 4){
        GolfRequestController* controller = (GolfRequestController*) [self controllerByIdentifier:@"GolfRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    }else if(position == 5){
        OrthersBookingController* controller = (OrthersBookingController*) [self controllerByIdentifier:@"OrthersBookingController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn] == false){
        [[SlideNavigationController sharedInstance]  closeMenuWithCompletion:^{
            UIAlertAction *signInAction = [UIAlertAction actionWithTitle:SIGN_IN
                                                                   style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {setRootViewLogout();
                                                                   }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:true];
            }];
            showAlertTopOpenLoginInRequest([SlideNavigationController sharedInstance], signInAction, cancelAction);
        }];
        
    }
}

-(void)onFaviotesFail{
    [self stopToast];
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    //TODO: display data to View
    if(ws.task == WS_GET_GOURMET_LIST){
        /*NSLog(@"%@",wsGourmetList.restaurants);
        isConnecting = NO;
        if(pArrRecommended==0){
            pArrRecommended = [[NSMutableArray alloc] init];
            [pArrRecommended addObjectsFromArray:wsGourmetList.restaurants];
        }else{
            [pArrRecommended addObjectsFromArray:wsGourmetList.restaurants];
        }
        [self addRecommendItem:wsGourmetList.restaurants];*/
    } else if(ws.task == WS_BOOKING_ORTHERS){
        ThankYouBookingController* controller = (ThankYouBookingController*) [self controllerByIdentifier:@"ThankYouBooking"];
        controller.thankYouMessage = [BOOKING_THANK_YOU_MSG stringByReplacingOccurrencesOfString:@"PHONE_NUMBER_PLACEHOLDER" withString:[AppConfig shared].companyPhoneNumber];
        [self.navigationController pushViewController:controller animated:YES];
        self.tfChatAsk.text = @"";
    }
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(result!=nil && result.message!=nil){
        
    }

    [self stopToast];
}
- (IBAction)actionSubmit:(id)sender
{
    if (isNetworkAvailable())
    {
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        if([appdele isLoggedIn]){
            if (![[stringFromObject(self.tfChatAsk.text) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] isEqualToString:@""])
            {
                //Submit Other request
                _icRequestMsgError.hidden = YES;
                trackGAICategory(@"Home",@"Send concierge request",@"Home",nil);
                
                [self showToast];
                UserObject *user = (UserObject*)[appdele getLoggedInUser];
                OrthersRequestObj* request = [[OrthersRequestObj alloc] init];
                request.ortherString = self.tfChatAsk.text;
                request.isContactEmail = true;
                request.reservationName = user.fullName;
                request.EditType = EDIT_ADD;
                wsCreateOtherRequest = [[WSCreateOrthersRequest alloc] init];
                wsCreateOtherRequest.delegate = self;
                [wsCreateOtherRequest bookingOrthers:request];
            } else {
                _icRequestMsgError.hidden = NO;
                [self stopFlingPlaceHolder];
            }
            
            
        } else {
            [[SlideNavigationController sharedInstance]  closeMenuWithCompletion:^{
                UIAlertAction *signInAction = [UIAlertAction actionWithTitle:SIGN_IN
                                                                       style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {setRootViewLogout();
                                                                       }];
                showAlertTopOpenLogin([SlideNavigationController sharedInstance] , signInAction);            }];
        }
    }
    else
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:NO_INTERNET_MSG
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK",nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)actionCancel:(id)sender
{
    _icRequestMsgError.hidden = YES;
    [self.tfChatAsk setText:nil];
    [self.tfChatAsk resignFirstResponder];
    
    [self startFlingPlaceHolder];
}


#pragma mark Add Function Of ChatBot - Huy Truong

-(NSString *)encodeUrlString:(NSString *)string {
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *result = [string stringByAddingPercentEncodingWithAllowedCharacters:set];
    return result;
}

- (void) callChatBotForText:(NSString*)textSearch
{
    NSMutableDictionary* dictionParam = [[NSMutableDictionary alloc] init];
    [dictionParam setObject:[NSNumber numberWithInteger:1] forKey:@"bot_id"];
    
    [dictionParam setObject:@"json" forKey:@"format"];
    [dictionParam setObject:[self encodeUrlString:textSearch] forKey:@"say"];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn])
    {
        UserObject* currentUser = getUser();
        [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.userId)]forKey:@"uid"];
        [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.fullName)]forKey:@"usName"];
    }
    
    [self showToast];
    
    callAPINoHeaderForGET(API_CALL_CHAT,dictionParam,self,@selector(getBotResponseComplete:),@selector(getBotResponseFail:));
}


#pragma mark Response API

-(void) getBotResponseComplete:(NSData*)data
{
    [self stopToast];
    id jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    if ([jsonObject isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"%@",jsonObject);
        NSString* status = stringFromObject([jsonObject objectForKey:@"status"]);
        if ([status isEqualToString:@"200"])
        {
            NSDictionary* dataRes = [jsonObject objectForKey:@"botsay"];
            if (dataRes && [dataRes isKindOfClass:[NSDictionary class]])
            {
                NSString* typeParser = stringFromObject([dataRes objectForKey:@"type"]);
                if ([typeParser isEqualToString:@"nonrelated"])
                {
                    ThankYouBookingController* controller = (ThankYouBookingController*) [self controllerByIdentifier:@"ThankYouBooking"];
                    controller.thankYouMessage = CHAT_BOX_THANK_YOU_MSG;
                    [self.navigationController pushViewController:controller animated:YES];
                }
                else
                {
                    ChatBotViewController* vwViewChat = [[ChatBotViewController alloc] initWithNibName:@"ChatBotViewController" bundle:nil];
                    vwViewChat.firstText = self.tfChatAsk.text;
                    [self.navigationController pushViewController:vwViewChat animated:YES];
                }
            }
        }
        else
        {
            ChatBotViewController* vwViewChat = [[ChatBotViewController alloc] initWithNibName:@"ChatBotViewController" bundle:nil];
            vwViewChat.firstText = self.tfChatAsk.text;
            [self.navigationController pushViewController:vwViewChat animated:YES];
        }
    }
    else
    {
        ChatBotViewController* vwViewChat = [[ChatBotViewController alloc] initWithNibName:@"ChatBotViewController" bundle:nil];
        vwViewChat.firstText = self.tfChatAsk.text;
        [self.navigationController pushViewController:vwViewChat animated:YES];
    }
}

-(void) getBotResponseFail:(NSError*)error
{
    [self stopToast];
    NSLog(@"%@",error);
    //cal again
    ChatBotViewController* vwViewChat = [[ChatBotViewController alloc] initWithNibName:@"ChatBotViewController" bundle:nil];
    vwViewChat.firstText = self.tfChatAsk.text;
    [self.navigationController pushViewController:vwViewChat animated:YES];
    return;
}


- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        NSLog(@"Trust:%@",challenge.protectionSpace.host);
        if([challenge.protectionSpace.host isEqualToString:@"chatbot.s3corp.com.vn"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}


#pragma Fake Recommendation list ofr requests

-(void)addFakeRecommendItem:(NSMutableArray*)array{
   
    pArrRecommended = array;
    int height =195*SCREEN_SCALE;
    ExperienceItem* item;
    for (int i = 0; i<array.count; i++) {
        item = [array objectAtIndex:i];
        
        HomeGourmetItem* homeItem = [[HomeGourmetItem alloc] initWithFrame:CGRectMake(0, positionY, SCREEN_WIDTH, height)];
        resetScaleViewBaseOnScreen(homeItem);
        homeItem.lbRestaurantName.text = NSLocalizedString(item.title, nil);
        homeItem.imvView.image = [UIImage imageNamed:item.imageURL];
        homeItem.delegate = self;
        homeItem.btnAddFaviote.hidden = YES;
        homeItem.tag =  i;
        [self.pvRecommendItemView addSubview:homeItem];
        [pRecommendedViews addObject:homeItem];
        
        positionY += height;
    }
    int totalHeightItem =(int)array.count*height;
    self.pHeightItemRecom.constant = totalHeightItem;
    self.pHeightRecomm.constant =totalHeightItem+130;
    self.pHeightMain.constant = (672*SCREEN_SCALE)+totalHeightItem + 130 - 60;
}


-(NSMutableArray*) createFakeExperienceRecommend{
    return [[NSMutableArray alloc] initWithObjects:
            [[ExperienceItem alloc] initWithType:@"DINING" andImage:@"rsz_concierge_service_-_restaurant_and_bars.jpg"],
            [[ExperienceItem alloc] initWithType:@"HOTEL" andImage:@"rsz_concierge_services_-_hotels.jpg"],
            [[ExperienceItem alloc] initWithType:@"ENTERTAINMENT" andImage:@"rsz_concierge_services_-_entertainment.jpg"],
            [[ExperienceItem alloc] initWithType:@"TRANSPORTATION"  andImage:@"rsz_concierge_service_-_car_rental.jpg"],
            [[ExperienceItem alloc] initWithType:@"GOLF"  andImage:@"rsz_concierge_service_-_golf.jpg"],
            [[ExperienceItem alloc] initWithType:@"OTHERS"  andImage:@"Image_6.png"],
            nil];
}

-(void) startFlingArrow{
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [self.btnArrow.imageView startAnimating];
    });
}

-(void) stopFlingArrow{
    [self.btnArrow.imageView stopAnimating];
}

-(void) startFlingPlaceHolder{
    if([[stringFromObject(self.tfChatAsk.text) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] isEqualToString:@""]){
        [UIView setAnimationsEnabled:YES];
        [_icRequestMsgError setHidden:YES];
        [self.lbPlaceHolder setHidden:NO];
        if(placeHolderTimer){
            [placeHolderTimer invalidate];
            placeHolderTimer = nil;
        }
        
        placeHolderTextIndex = 0;
        [self.lbPlaceHolder setText:PLACEHOLDER_TEXTS[placeHolderTextIndex]];
        placeHolderTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                            target:self
                                                          selector:@selector(fadeToPlaceholder)
                                                          userInfo:nil
                                                           repeats:YES];
    }
    
}

-(void) stopFlingPlaceHolder{
    if(placeHolderTimer){
        [placeHolderTimer invalidate];
        placeHolderTimer = nil;
    }
    [self.lbPlaceHolder setHidden:YES];
}

-(void) fadeToPlaceholder{
    [UIView transitionWithView:self.lbPlaceHolder
                          duration:1.0f
                           options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.lbPlaceHolder.text = @"";
                        } completion:^(BOOL finished){
                            [UIView transitionWithView:self.lbPlaceHolder
                                              duration:1.0f
                                               options:UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionTransitionCrossDissolve
                                            animations:^{
                                                if(placeHolderTextIndex >= PLACEHOLDER_TEXTS.count - 1){
                                                    placeHolderTextIndex = 0;
                                                } else {
                                                    placeHolderTextIndex = placeHolderTextIndex + 1;
                                                }
                                               self.lbPlaceHolder.text = PLACEHOLDER_TEXTS[placeHolderTextIndex];
                                                //NSLog(self.lbPlaceHolder.text);
                                            } completion:NULL];
                            
                        }];

}



@end
