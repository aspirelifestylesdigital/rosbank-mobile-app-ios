//
//  HambugerController.h
//  ALC
//
//  Created by Hai NguyenV on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HambugerController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSNumber *INDEX_HOME;
    NSNumber *INDEX_ACCOUNT;
    NSNumber *INDEX_MY_REQUESTS;
    NSNumber *INDEX_NOTIFICATIONS;
    NSNumber *INDEX_SEND_CONCIERGE;
    NSNumber *INDEX_CITY_GUIDE;
    NSNumber *INDEX_EXPERIENCES;
    NSNumber *INDEX_ABOUT;
    NSNumber *INDEX_SIGNOUT;
}
@property (weak, nonatomic) IBOutlet UITableView *pTableView;
-(void) redisplayHome;
@end
