//
//  OrtherPreferenceController.m
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "OrtherPreferenceController.h"
#import "BaseResponseObject.h"

@interface OrtherPreferenceController ()

@end

@implementation OrtherPreferenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
//    [self showToast];
//    wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//    wsGetMyPreference.delegate = self;
//    [wsGetMyPreference getMyPreference];
}

-(void) setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"MY PREFERENCES", nil) withColor:HEADER_COLOR];
    self.otherPreferTitleLbl.text = NSLocalizedString(@"Other Preferences", @"");
    self.otherHelpLbl.text = NSLocalizedString(@"What would you like to share with us?", @"");
    self.lbHolder.text =  NSLocalizedString(@"Please enter comments, if any.", @"");
    [self.saveBtn setTitle:NSLocalizedString(@"SAVE", @"") forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
}


-(void)tapInView:(UITapGestureRecognizer *)tapGuesture{
    [_tfOrtherPreference resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionSave:(id)sender {
    NSString* ortherCuisine = self.tfOrtherPreference.text;
    [self showToast];
    wsCreateOthers = [[WSCreateOrtherPre alloc] init];
    wsCreateOthers.delegate = self;
    wsCreateOthers.others = ortherCuisine;
    wsCreateOthers.preferenceID = _preferenceID;
    [wsCreateOthers createOthersPre];
}

- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    _lbHolder.hidden = YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if(textView.text.length==0){
        _lbHolder.hidden = NO;
    }else{
        _lbHolder.hidden = YES;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Other];
        _tfOrtherPreference.text = [dict stringForKey:VALUE];
        _preferenceID = [dict stringForKey:MYPREFERENCESID];
        if(self.tfOrtherPreference.text.length>0){
            _lbHolder.hidden = YES;
        }
    }else if(ws.task == WS_CREATE_OTHERPRE){
        [self stopToast];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(errorCode == 600){
        return;
    }
    if(result!=nil && result.message!=nil){
        showAlertOneButton(self,@"", result.message, @"OK");
    } else {
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_API_MSG, @"OK");
    }
}

- (void)dealloc
{
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
    if(wsCreateOthers){
        [wsCreateOthers cancelRequest];
    }
}
@end
