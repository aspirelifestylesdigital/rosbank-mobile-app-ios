//
//  CancelRequestViewController.h
//  ALC
//
//  Created by Tho Nguyen on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"

@class MyRequestObject;

@protocol CancelRequestViewControllerDelegate;

@interface CancelRequestViewController : MainViewController

@property (weak, nonatomic) id<CancelRequestViewControllerDelegate> delegate;
@property (strong, nonatomic) id requestDetails;
@property (strong, nonatomic) MyRequestObject *myRequest;
@property (nonatomic) NSInteger requestType;

@end

@protocol CancelRequestViewControllerDelegate <NSObject>

- (void)didCancelRequestSuccessfully:(MyRequestObject *)myRequest;

@end