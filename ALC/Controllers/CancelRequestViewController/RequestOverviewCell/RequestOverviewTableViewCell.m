//
//  RequestOverviewTableViewCell.m
//  ALC
//
//  Created by Tho Nguyen on 10/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "RequestOverviewTableViewCell.h"
#import "HotelRequestDetailsObject.h"
#import "GourmetRequestDetailsObj.h"
#import "GolfDetailsObj.h"
#import "CarRentalDetailsObject.h"
#import "OrtherRequestDetailsObj.h"
#import "MyRequestObject.h"
#import "NSDate+Category.h"
#import "GouetmetRecommendDetailsObj.h"
#import "EntertainmentRequestDetailsObject.h"

@interface RequestOverviewTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblRequestTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestContent;
@property (weak, nonatomic) IBOutlet UILabel *lblCaseID;
@property (weak, nonatomic) IBOutlet UILabel *lblCaseIDValue;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestStatusValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDateOfRequest;
@property (weak, nonatomic) IBOutlet UILabel *lblDateOfRequestValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeOfRequest;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeOfRequestValue;

@end

@implementation RequestOverviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setUp];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUp {
    resetScaleViewBaseOnScreen(self);
    self.lblCaseID.text = NSLocalizedString(@"CASE ID", nil);
    self.lblRequestStatus.text = NSLocalizedString(@"REQUEST STATUS", nil);
    self.lblDateOfRequest.text = NSLocalizedString(@"DATE OF REQUEST", nil);
    self.lblTimeOfRequest.text = NSLocalizedString(@"TIME OF REQUEST", nil);
}

- (void)loadDataForCell:(id)requestDetail andRequestType:(NSInteger)requestType andMyRequest:(MyRequestObject *)myRequest{
    self.lblCaseIDValue.text = ((BaseBookingDetailsObject*)requestDetail).bookingName;
    self.lblRequestStatusValue.text = myRequest.requestStatus;
    
    self.lblDateOfRequestValue.text = formatDateToString(((BaseBookingDetailsObject*)requestDetail).createDate, DATE_FORMAT_DD_MMMM_YYYY);
    self.lblTimeOfRequestValue.text = formatDateToString(((BaseBookingDetailsObject*)requestDetail).createDate, TIME_FORMAT);
    
    switch (requestType) {
        case OTHER_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"OTHER REQUEST", nil);
            OrtherRequestDetailsObj *obj = (OrtherRequestDetailsObj *)requestDetail;
            self.lblRequestContent.text = obj.ortherString;
        }
            break;
        case SPA_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"SPA REQUEST", nil);
            OrtherRequestDetailsObj *obj = (OrtherRequestDetailsObj *)requestDetail;
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@, %@", obj.spaName, obj.city];
            }
            break;

            
        case GOURMET_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"DINING REQUEST", nil);
            GourmetRequestDetailsObj *obj = (GourmetRequestDetailsObj *)requestDetail;
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@, %@", obj.restaurantName, obj.city];
        }
            break;
            
        case CAR_RENTAL_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"CAR RENTAL REQUEST", nil);
            CarRentalDetailsObject *obj = (CarRentalDetailsObject *)requestDetail;
            self.lblRequestContent.text = obj.guestName;
        }
            break;
        case CAR_TRANSFER_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"CAR TRANSFER REQUEST", nil);
            CarRentalDetailsObject *obj = (CarRentalDetailsObject *)requestDetail;
            self.lblRequestContent.text = obj.guestName;
        }
            break;
            
        case HOTEL_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"HOTEL REQUEST", nil);
            HotelRequestDetailsObject *obj = (HotelRequestDetailsObject *)requestDetail;
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@, %@", obj.hotelName, obj.city];
        }
            break;
            
        case GOLF_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"GOLF REQUEST", nil);
            GolfDetailsObj *obj = (GolfDetailsObj *)requestDetail;
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@, %@", obj.GolfCourse, obj.city];
        }
            break;
        case HOTEL_RECOMMEND_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"HOTEL RECOMMEND REQUEST", nil);
            HotelRequestDetailsObject *obj = (HotelRequestDetailsObject *)requestDetail;
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Recommend a Hotel", nil), obj.city];
        }
            break;
        case GOURMET_RECOMMEND_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"DINING RECOMMEND REQUEST", nil);
            GouetmetRecommendDetailsObj *obj = (GouetmetRecommendDetailsObj *)requestDetail;
            NSString* situtationValue = convertArrayToString(obj.occasionValues);
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@%@%@", situtationValue, (situtationValue.length > 0 ? @", " : @""), obj.city];
        }
            break;
            
        case ENTERTAINMENT_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"ENTERTAINMENT REQUEST", nil);
            EntertainmentRequestDetailsObject *obj = (EntertainmentRequestDetailsObject *)requestDetail;
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@, %@", obj.eventName, obj.city];
        }
            break;
        case ENTERTAINMENT_RECOMMEND_REQUEST: {
            self.lblRequestTitle.text = NSLocalizedString(@"ENTERTAINMENT RECOMMEND REQUEST", nil);
            EntertainmentRequestDetailsObject *obj = (EntertainmentRequestDetailsObject *)requestDetail;
            self.lblRequestContent.text = [NSString stringWithFormat:@"%@, %@", convertArrayToString(obj.eventCategory), obj.city];
        }
            break;
        default:
            break;
    }
}

@end
