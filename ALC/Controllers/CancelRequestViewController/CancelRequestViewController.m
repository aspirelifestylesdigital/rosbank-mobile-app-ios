//
//  CancelRequestViewController.m
//  ALC
//
//  Created by Tho Nguyen on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CancelRequestViewController.h"
#import "RequestDetailsTableViewCell.h"
#import "RequestOverviewTableViewCell.h"
#import "SectionHeaderRequestDetailView.h"
#import "WSCancelRequest.h"
#import "HotelRequestDetailsObject.h"
#import "GourmetRequestDetailsObj.h"
#import "GolfDetailsObj.h"
#import "CarRentalDetailsObject.h"
#import "OrtherRequestDetailsObj.h"
#import "NSDate+Category.h"
#import "BaseBookingDetailsObject.h"
#import "MyRequestObject.h"
#import "ThankYouBookingController.h"
#import "GouetmetRecommendDetailsObj.h"
#import "HotelRequestDetailsObject.h"
#import "EntertainmentRequestDetailsObject.h"

#import "WSCreateGolfRequest.h"
#import "WSCreateHotelRequest.h"
#import "WSGourmetBooking.h"
#import "WSCreateOrthersRequest.h"
#import "WSGourmetRecommendRequest.h"
#import "WSCreateHotelRecommendRequest.h"
#import "WSCreateCarTransferRequest.h"
#import "WSCreateRentalRequest.h"
#import "WSCreateEventRequest.h"
#import "WSCreateRecommentRequest.h"

typedef NS_ENUM(NSInteger, SectionType) {
    SectionType_Overview,
    SectionType_Details
};

static NSString *const kRequestDetailTableViewCell = @"RequestDetailsTableViewCell";
static NSString *kRequestDetailCell = @"requestDetailCell";
static NSString *const kRequestOverviewTableViewCell = @"RequestOverviewTableViewCell";
static NSString *kRequestOverviewCell = @"requestOverviewCell";
static CGFloat const kHeightOfRowInSectionDetails = 50.0f;
static CGFloat const kHeightOfRowInSectionOverview = 235.0f;
static CGFloat const kHeightOfHeaderSectionDetails = 50.0f;
static CGFloat const kHeightDefaultOfRequestDetailTableView = 285.0f;
static NSInteger const kNumberOfSectionInRequestDetailTableView = 2;

@interface CancelRequestViewController ()<UITableViewDataSource, UITableViewDelegate, DataLoadDelegate>{
    WSCreateGolfRequest* wsCreateGolfRequest;
    WSCreateHotelRequest* wsCreateHotelRequest;
    WSCreateRentalRequest* wsCreateRentalRequest;
    WSCreateOrthersRequest* wsCreateOrthersRequest;
    WSCreateCarTransferRequest* wsCreateCarTransferRequest;
    WSCreateHotelRecommendRequest* wsCreateHotelRecommendRequest;
    WSGourmetRecommendRequest* wsGourmetRecommendRequest;
    WSGourmetBooking* wsGourmetBooking;
    WSCreateEventRequest *wsCreateEventRequest;
    WSCreateRecommentRequest *wsCreateEventRecommendRequest;
}

@property (weak, nonatomic) IBOutlet UITableView *requestDetailTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelRequest;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintRequestDetailTableView;

@property (nonatomic, strong) NSArray *requestDetailTitles;
@property (nonatomic, strong) NSArray *requestDetailValues;
@property (nonatomic) BOOL isOpenRequestDetailsCell;

@end

@implementation CancelRequestViewController {
    WSCancelRequest* wsCancel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private Methods

- (void)setUp {
    [self addTitleToHeader:NSLocalizedString(@"CANCEL REQUEST", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    [self.requestDetailTableView registerNib:[UINib nibWithNibName:kRequestDetailTableViewCell bundle:nil] forCellReuseIdentifier:kRequestDetailCell];
    [self.requestDetailTableView registerNib:[UINib nibWithNibName:kRequestOverviewTableViewCell bundle:nil] forCellReuseIdentifier:kRequestOverviewCell];
    self.requestDetailTableView.rowHeight = UITableViewAutomaticDimension;
    self.requestDetailTableView.estimatedRowHeight = 44.0;
    
    self.requestDetailTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    [self.btnCancelRequest setTitle:NSLocalizedString(@"CANCEL REQUEST", nil) forState:UIControlStateNormal];
    self.isOpenRequestDetailsCell = NO;
    self.requestDetailTitles = [self willCreateRequestDetailTitles];
    self.requestDetailValues = [self willCreateRequestDetailValues];
    
    [self openRequestDetailsCell:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self
    //                                         selector:@selector(openRequestDetailsCell:)
    //                                             name:kNotificationToOpenRequestDetailCell
    //                                           object:nil];
}

- (NSArray *)willCreateRequestDetailTitles {
    NSArray *titles;
    
    switch (self.requestType) {
        case OTHER_REQUEST:
            titles = @[NSLocalizedString(@"OTHER REQUEST_Cancel request", nil)];
            break;
        case SPA_REQUEST:
            titles = @[NSLocalizedString(@"Spa Name", nil)];
            break;
        case GOURMET_REQUEST:
            titles = @[NSLocalizedString(@"Name of restaurant", nil),
                       NSLocalizedString(@"Reservation name_cancel", nil),
                       NSLocalizedString(@"Reservation date_dining", nil)];
            break;
            
        case CAR_RENTAL_REQUEST:
            titles = @[NSLocalizedString(@"Name of driver", nil),
                       NSLocalizedString(@"Age of driver", nil),
                       NSLocalizedString(@"Pick up location_rental", nil),
                       NSLocalizedString(@"Drop off location_rental", nil)];
            break;
        case CAR_TRANSFER_REQUEST:
            titles = @[NSLocalizedString(@"Pick up location transfer_cancel", nil),
                       NSLocalizedString(@"Drop off location transfer", nil)];
            break;
            
        case HOTEL_REQUEST:
            titles = @[NSLocalizedString(@"Guest", nil),
                       NSLocalizedString(@"Check in date", nil),
                       NSLocalizedString(@"Check out date", nil)];
            break;
            
        case GOLF_REQUEST:
            titles = @[NSLocalizedString(@"Name of golf course", nil),
                       NSLocalizedString(@"Date", nil),
                       NSLocalizedString(@"City", nil)];
            break;
        case HOTEL_RECOMMEND_REQUEST:
            titles = @[NSLocalizedString(@"Guest", nil),
                       NSLocalizedString(@"Room preference_cancel", nil),
                       NSLocalizedString(@"Check in date", nil),
                       NSLocalizedString(@"Check out date", nil)];
            
            break;
        case GOURMET_RECOMMEND_REQUEST:
            titles = @[NSLocalizedString(@"Reservation date_dining request", nil)];
            break;
        case ENTERTAINMENT_REQUEST:
            titles = @[NSLocalizedString(@"Event name_cancel", nil),
                       NSLocalizedString(@"City", nil),
                       NSLocalizedString(@"Reservation date_ENTERTAINMENT", nil)];
            break;
        case ENTERTAINMENT_RECOMMEND_REQUEST:
            titles = @[NSLocalizedString(@"City", nil),
                       NSLocalizedString(@"Reservation date_ENTERTAINMENT", nil)];
            break;
            
            
            
        default:
            titles = nil;
            break;
    }
    
    return titles;
}

- (NSArray *)willCreateRequestDetailValues {
    NSArray *values;
    
    switch (self.requestType) {
        case OTHER_REQUEST: {
            OrtherRequestDetailsObj *obj = (OrtherRequestDetailsObj *)self.requestDetails;
            values = @[obj.ortherString];
        }
            break;
        case SPA_REQUEST: {
            OrtherRequestDetailsObj *obj = (OrtherRequestDetailsObj *)self.requestDetails;
            values = @[obj.spaName];
        }
            break;
            
        case GOURMET_REQUEST: {
            GourmetRequestDetailsObj *obj = (GourmetRequestDetailsObj *)self.requestDetails;
            values = @[obj.restaurantName,
                       obj.reservationName,
                       [obj.reservationDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY]];
        }
            break;
            
        case CAR_RENTAL_REQUEST: {
            CarRentalDetailsObject *obj = (CarRentalDetailsObject *)self.requestDetails;
            values = @[obj.driverName,
                       [NSString stringWithFormat:@"%li", (long)obj.driverAge],
                       obj.pickUpLocation,
                       obj.dropOffLocation];
        }
            break;
        case CAR_TRANSFER_REQUEST: {
            CarRentalDetailsObject *obj = (CarRentalDetailsObject *)self.requestDetails;
            values = @[obj.pickUpLocation,
                       obj.dropOffLocation];
        }
            break;
            
        case HOTEL_REQUEST: {
            HotelRequestDetailsObject *obj = (HotelRequestDetailsObject *)self.requestDetails;
            NSString *guests = [NSString stringWithFormat:@"%li %@", (long)obj.adulsPax,
                                ((long)obj.adulsPax > 1 ? NSLocalizedString(@"Adults",nil) : NSLocalizedString(@"Adult",nil))];
            if (obj.kidsPax > 0 && obj.adulsPax > 0) {
                guests = [NSString stringWithFormat:@"%li %@ + %li %@", (long)obj.adulsPax,        ((long)obj.adulsPax > 1 ? NSLocalizedString(@"Adults",nil) : NSLocalizedString(@"Adult",nil)),
                    (long)obj.kidsPax,
                    ((long)obj.kidsPax > 1 ? NSLocalizedString(@"Children",nil) : NSLocalizedString(@"Child",nil))];
            }
            values = @[guests,
                       obj.checkInDate ? [obj.checkInDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY] : @"",
                       obj.checkOutDate ? [obj.checkOutDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY] : @""] ;
        }
            break;
            
        case GOLF_REQUEST: {
            GolfDetailsObj *obj = (GolfDetailsObj *)self.requestDetails;
            values = @[obj.GolfCourse,
                       [obj.GolfDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY],
                       obj.city];
        }
            break;
        case HOTEL_RECOMMEND_REQUEST: {
            HotelRequestDetailsObject *obj = (HotelRequestDetailsObject *)self.requestDetails;
            NSString *guests = [NSString stringWithFormat:@"%li %@", (long)obj.adulsPax,
                                ((long)obj.adulsPax > 1 ? NSLocalizedString(@"Adults",nil) : NSLocalizedString(@"Adult",nil))];
            if (obj.kidsPax > 0) {
                guests = [NSString stringWithFormat:@"%li %@ + %li %@", (long)obj.adulsPax,        ((long)obj.adulsPax > 1 ? NSLocalizedString(@"Adults",nil) : NSLocalizedString(@"Adult",nil)),
                          (long)obj.kidsPax,
                          ((long)obj.kidsPax > 1 ? NSLocalizedString(@"Children",nil) : NSLocalizedString(@"Child",nil))];
            }
            values = @[guests,
                       obj.roomTypeValue,
                       [obj.checkInDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY],
                       [obj.checkOutDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY]];
        }
            break;
        case GOURMET_RECOMMEND_REQUEST: {
            GouetmetRecommendDetailsObj *obj = (GouetmetRecommendDetailsObj *)self.requestDetails;
            values = @[
                       [obj.reservationDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY]];
        }
            break;
        case ENTERTAINMENT_REQUEST: {
            EntertainmentRequestDetailsObject *obj = (EntertainmentRequestDetailsObject *)self.requestDetails;
            values = @[
                       obj.eventName,
                       obj.city,
                       [obj.eventDateTime stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY]];
        }
            break;
            
        case ENTERTAINMENT_RECOMMEND_REQUEST: {
            EntertainmentRequestDetailsObject *obj = (EntertainmentRequestDetailsObject *)self.requestDetails;
            values = @[
                       obj.city,
                       [obj.preferDate stringFromDateWithDateFormat:DATE_FORMAT_DD_MMMM_YYYY]];
        }
            break;

            
        default:
            values = nil;
            break;
    }
    
    return values;
}

- (NSInteger)numberOfRequestDetailTitle {
    NSInteger amount = 0;
    if (self.isOpenRequestDetailsCell) {
        amount = [self.requestDetailTitles count];
    }
    
    return amount;
}

- (void)reloadSection {
    NSIndexSet *section = [NSIndexSet indexSetWithIndex:1];
    [self.requestDetailTableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
}

- (void)openRequestDetailsCell:(NSNotification *)noti {
    self.isOpenRequestDetailsCell = !self.isOpenRequestDetailsCell;
    if (self.isOpenRequestDetailsCell) {
        [self reloadSection];
        self.heightConstraintRequestDetailTableView.constant = kHeightDefaultOfRequestDetailTableView + [self numberOfRequestDetailTitle] * kHeightOfRowInSectionDetails;
    } else {
        [self reloadSection];
        self.heightConstraintRequestDetailTableView.constant = kHeightDefaultOfRequestDetailTableView;
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kNumberOfSectionInRequestDetailTableView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == SectionType_Overview) {
        
        return 1;
    }
    
    return [self numberOfRequestDetailTitle];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case SectionType_Overview: {
            RequestOverviewTableViewCell *cell = [self.requestDetailTableView dequeueReusableCellWithIdentifier:kRequestOverviewCell];
            [cell loadDataForCell:self.requestDetails andRequestType:self.requestType andMyRequest:self.myRequest];
            
            return cell;
        }
            break;
        case SectionType_Details: {
            
            RequestDetailsTableViewCell *cell = [self.requestDetailTableView dequeueReusableCellWithIdentifier:kRequestDetailCell];
            cell.backgroundColor = [UIColor clearColor];
            
            NSString *title = [self.requestDetailTitles objectAtIndex:indexPath.row];
            NSString *value = [self.requestDetailValues objectAtIndex:indexPath.row];
            [cell loadDataForCell:title andValue:value];
            
            return cell;
        }
            break;
            
        default:
            break;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 0.0f;
    
    switch (indexPath.section) {
        case SectionType_Overview:
            height = kHeightOfRowInSectionOverview;
            break;
        case SectionType_Details:
            height = UITableViewAutomaticDimension;////kHeightOfRowInSectionDetails;
            break;
            
        default:
            break;
    }
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == SectionType_Details) {
        
        return kHeightOfHeaderSectionDetails;
    }
    
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == SectionType_Details) {
        SectionHeaderRequestDetailView *view = [[SectionHeaderRequestDetailView alloc] initWithFrame:CGRectMake(0, 0, self.requestDetailTableView.frame.size.width, kHeightOfHeaderSectionDetails)];
        view.isRotate = self.isOpenRequestDetailsCell;
        [view setTitleForHeaderView:self.requestType];
        
        return view;
    }
    
    return nil;
}

#pragma mark - DataLoadDelegate

-(void)loadDataDoneFrom:(WSBase*)ws{
    [self stopToast];
    if(ws.task ==  WS_BOOKING_GOURMET ||
       ws.task ==WS_BOOKING_ORTHERS ||
       ws.task ==WS_BOOKING_CAR_RENTAL ||
       ws.task ==WS_BOOKING_CAR_TRANSFER ||
       ws.task ==WS_BOOKING_HOTEL ||
       ws.task ==WS_BOOKING_GOLF ||
       ws.task == WS_BOOKING_GOURMET_RECOMMEND ||
       ws.task == WS_BOOKING_HOTEL_RECOMMEND ||
       ws.task == WS_BOOKING_ENTERTAINMENT ||
       ws.task == WS_BOOKING_ENTERTAINMENT_RECOMMEND ){
        
        /*if (self.delegate && [self.delegate respondsToSelector:@selector(didCancelRequestSuccessfully:)]) {
            [self.delegate didCancelRequestSuccessfully:self.myRequest];
        }*/
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_A_REQUEST_IS_AMENDED
         object:self];
    }
    
    [self.navigationController popViewControllerAnimated:NO];
    
    if(!appdelegate){
        appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    ThankYouBookingController* controller = (ThankYouBookingController*) [self controllerByIdentifier:@"ThankYouBooking"];
    controller.subHeaderMessage = NSLocalizedString(@"We hope you will find something that suits your palate from our other offerings", nil);
    [appdelegate.rootVC pushViewController:controller animated:YES];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    
}


#pragma mark - Action Methods

- (IBAction)tappedAtCancelRequestButton:(id)sender {
    [self showToast];
    
    switch (self.requestType) {
        case SPA_REQUEST:
        case OTHER_REQUEST: {
            OrtherRequestDetailsObj *obj = (OrtherRequestDetailsObj *)self.requestDetails;
            OrthersRequestObj *requestData = [[OrthersRequestObj alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateOrthersRequest = [[WSCreateOrthersRequest alloc] init];
            wsCreateOrthersRequest.delegate = self;
            [wsCreateOrthersRequest bookingOrthers:requestData];
        }
             break;            
        case GOURMET_REQUEST: {
            GourmetRequestDetailsObj *obj = (GourmetRequestDetailsObj *)self.requestDetails;
            GourmetRequestObject *requestData = [[GourmetRequestObject alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsGourmetBooking = [[WSGourmetBooking alloc] init];
            wsGourmetBooking.delegate = self;
            [wsGourmetBooking bookingGourmet:requestData];
        }
            
            break;
            
        case CAR_RENTAL_REQUEST: {
            CarRentalDetailsObject *obj = (CarRentalDetailsObject *)self.requestDetails;
            CarCreateRequestObject *requestData = [[CarCreateRequestObject alloc] initRentalFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateRentalRequest = [[WSCreateRentalRequest alloc] init];
            wsCreateRentalRequest.delegate = self;
            [wsCreateRentalRequest bookingRental:requestData];
        }
            break;
            
        case CAR_TRANSFER_REQUEST: {
            CarTransferDetailsObject *obj = (CarTransferDetailsObject *)self.requestDetails;
            CarCreateRequestObject *requestData = [[CarCreateRequestObject alloc] initTransferFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateCarTransferRequest = [[WSCreateCarTransferRequest alloc] init];
            wsCreateCarTransferRequest.delegate = self;
            [wsCreateCarTransferRequest bookingTransfer:requestData];
        }
            break;
            
        case HOTEL_REQUEST: {
            HotelRequestDetailsObject *obj = (HotelRequestDetailsObject *)self.requestDetails;
            HotelRequestObject *requestData = [[HotelRequestObject alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateHotelRequest = [[WSCreateHotelRequest alloc] init];
            wsCreateHotelRequest.delegate = self;
            [wsCreateHotelRequest bookingHotel:requestData];
        }
            break;
        case GOLF_REQUEST: {
            GolfDetailsObj *obj = (GolfDetailsObj *)self.requestDetails;
            GolfRequestObj *requestData = [[GolfRequestObj alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateGolfRequest = [[WSCreateGolfRequest alloc] init];
            wsCreateGolfRequest.delegate = self;
            [wsCreateGolfRequest bookingGolf:requestData];
        }
            break;
            
        case HOTEL_RECOMMEND_REQUEST: {
            HotelRequestDetailsObject *obj = (HotelRequestDetailsObject *)self.requestDetails;
            HotelRequestObject *requestData = [[HotelRequestObject alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateHotelRecommendRequest = [[WSCreateHotelRecommendRequest alloc] init];
            wsCreateHotelRecommendRequest.delegate = self;
            [wsCreateHotelRecommendRequest bookingHotelRecommend:requestData];
            
        }            break;
        case GOURMET_RECOMMEND_REQUEST: {
            GouetmetRecommendDetailsObj *obj = (GouetmetRecommendDetailsObj *)self.requestDetails;
            GourmetRecommendRequestObj *requestData = [[GourmetRecommendRequestObj alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsGourmetRecommendRequest = [[WSGourmetRecommendRequest alloc] init];
            wsGourmetRecommendRequest.delegate = self;
            [wsGourmetRecommendRequest bookingRecommend:requestData];
        }
            break;
            
        case ENTERTAINMENT_REQUEST: {
            EntertainmentRequestDetailsObject *obj = (EntertainmentRequestDetailsObject *)self.requestDetails;
            EventRequestObj *requestData = [[EventRequestObj alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateEventRequest = [[WSCreateEventRequest alloc] init];
            wsCreateEventRequest.delegate = self;
            [wsCreateEventRequest bookingEvent:requestData];
            }
            break;
        case ENTERTAINMENT_RECOMMEND_REQUEST: {
            EntertainmentRequestDetailsObject *obj = (EntertainmentRequestDetailsObject *)self.requestDetails;
            EventRecommendRequestObj *requestData = [[EventRecommendRequestObj alloc] initFromDetails:obj];
            requestData.EditType = @"CANCEL";
            wsCreateEventRecommendRequest = [[WSCreateRecommentRequest alloc] init];
            wsCreateEventRecommendRequest.delegate = self;
            [wsCreateEventRecommendRequest bookingEvent:requestData];
            }
            break;
            
        default:
            
            break;
    }
    
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationToOpenRequestDetailCell
                                                  object:nil];
    if(wsCreateCarTransferRequest){
        [wsCreateCarTransferRequest cancelRequest];
    }
    
    if(wsGourmetBooking){[wsGourmetBooking cancelRequest];
    }
    
    if(wsCreateGolfRequest){ [wsCreateGolfRequest cancelRequest];
    }
    
    if(wsCreateRentalRequest){ [wsCreateRentalRequest cancelRequest];
    }
    
    if(wsGourmetRecommendRequest){ [wsGourmetRecommendRequest cancelRequest];
    }
    
    if(wsCreateHotelRequest){[wsCreateHotelRequest cancelRequest];
    }
    
    if(wsCreateHotelRecommendRequest){[ wsCreateHotelRecommendRequest cancelRequest];
    }
    
    if(wsCreateHotelRequest){ [wsCreateHotelRequest cancelRequest];
    }
    
    if(wsCreateEventRecommendRequest){ [wsCreateEventRecommendRequest cancelRequest];
    }
    
    if(wsCreateEventRequest){ [wsCreateEventRequest cancelRequest];
    }
    
}

@end
