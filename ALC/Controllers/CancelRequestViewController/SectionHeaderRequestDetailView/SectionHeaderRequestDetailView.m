//
//  SectionHeaderRequestDetailView.m
//  ALC
//
//  Created by Tho Nguyen on 10/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "SectionHeaderRequestDetailView.h"

static NSString *const kSectionHeaderRequestDetailView = @"SectionHeaderRequestDetailView";
NSString *const kNotificationToOpenRequestDetailCell = @"NotificationToOpenRequestDetailCell";

@interface SectionHeaderRequestDetailView ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnOpen;

@end

@implementation SectionHeaderRequestDetailView

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.isRotate) {
        self.isRotate = NO;
        [self rotateOpenButton];
    }
}

- (void)setUp {
    UIView *view = nil;
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:kSectionHeaderRequestDetailView
                                                     owner:self
                                                   options:nil];
    for (id object in objects) {
        if ([object isKindOfClass:[UIView class]]) {
            view = object;
            break;
        }
    }
    if (view) {
        [self addSubview:view];
        view.frame = self.frame;
    }
}

- (void)setTitleForHeaderView:(NSInteger)requestType {
    switch (requestType) {
        case OTHER_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Other Request Details", nil);
        }
            break;
        case SPA_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Spa Request Details", nil);
        }
            break;
            
        case GOURMET_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Dining Request Details", nil);
        }
            break;
            
        case CAR_RENTAL_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Car Rental Request Details", nil);
        }
            break;
        case CAR_TRANSFER_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Car Transfer Request Details", nil);
        }
            break;
        case HOTEL_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Hotel Request Details", nil);
        }
            break;
            
        case GOLF_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Golf Request Details", nil);
        }
            break;
        case HOTEL_RECOMMEND_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Hotel Recommend Request Details", nil);
        }
            break;
        case GOURMET_RECOMMEND_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Dining Recommend Request Details", nil);
        }
            break;
        case ENTERTAINMENT_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Entertainment Request Details", nil);
        }
            break;
        case ENTERTAINMENT_RECOMMEND_REQUEST: {
            self.lblTitle.text = NSLocalizedString(@"Entertainment Recommend Request Details", nil);
        }
            break;

            
        default:
            break;
    }
}

- (void)rotateOpenButton {
    [UIView beginAnimations:@"rotate" context:nil];
    [UIView setAnimationDuration:.5f];
    if( CGAffineTransformEqualToTransform( self.btnOpen.imageView.transform, CGAffineTransformIdentity ) )
    {
        self.btnOpen.imageView.transform = CGAffineTransformMakeRotation(M_PI);
    } else {
        self.btnOpen.imageView.transform = CGAffineTransformIdentity;
    }
    [UIView commitAnimations];
}

- (IBAction)tappedAtOpenButton:(id)sender {
    //[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationToOpenRequestDetailCell object:nil];
}

@end
