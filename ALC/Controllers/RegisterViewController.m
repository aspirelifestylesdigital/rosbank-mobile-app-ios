//
//  RegisterViewController.m
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "RegisterViewController.h"
#import "CCNumberController.h"
#import "CustomTextField.h"
#import "CommonUtils.h"
#import "NewUserObject.h"
#import "ForgotPasswordController.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "Constant.h"
#import "BaseResponseObject.h"
#import "UserObject.h"
#import "StoreUserData.h"
#import "WebController.h"
#import "OopsErrorPageController.h"
#import "ALCQC-Swift.h"

@interface RegisterViewController (){
    WSCreateUser *wsCreateUser;
    WSUpdateProfile* wsUpdateUser;
    NSMutableDictionary* dictIconErrorManager;
}
@property (strong, nonatomic) IBOutlet CustomTextField *txtAuthCode;
@property (strong, nonatomic) IBOutlet CustomTextField *txtRetypePassword;
@property (strong, nonatomic) IBOutlet CustomTextField *txtPassword;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCardNumber;
@property (strong, nonatomic) IBOutlet CustomTextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btCCNumber;
@property (strong, nonatomic) IBOutlet CustomTextField *txtNoNumber;
@property (strong, nonatomic) IBOutlet CustomTextField *txtLastName;
@property (strong, nonatomic) IBOutlet CustomTextField *txtFirstName;
@property (strong, nonatomic) IBOutlet CustomTextField *txtPostalCode;
@property (strong, nonatomic) IBOutlet UIButton *btSalutation;

@property (strong, nonatomic) IBOutlet UIButton *btCheck;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icSalutationError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icFirstNameError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icLastNameError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icPhoneNumberError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icEmailError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icRetypePassError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icPasswordError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCardError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icPostalError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icAuthCodeError;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *lbMobileHint;
@property (strong, nonatomic) IBOutlet UILabel *lbEmailHint;
@property (strong, nonatomic) IBOutlet UILabel *lbCardhint;
@property (strong, nonatomic) IBOutlet UILabel *lbPasswordHint;
@property (strong, nonatomic) IBOutlet UIWebView *lbConfimHint;
@property (strong, nonatomic) IBOutlet UIView *viewBtGroup;
@property (weak, nonatomic) IBOutlet UILabel *salutationLbl;
@property (weak, nonatomic) IBOutlet UILabel *mandatoryLbl;
@property (weak, nonatomic) IBOutlet UILabel *firstNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *mobileNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *passwordLbl;
@property (weak, nonatomic) IBOutlet UILabel *authCodeLbl;
@property (weak, nonatomic) IBOutlet UILabel *retypePasswordLbl;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *ccBtn;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];    // Do any additional setup after loading the view.
    [self customButtonBack];
    
    self.lbHeaderTitle.text = NSLocalizedString(@"Registration form", nil);
    
    if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
    }
    
    [self setUpView];
    
    if(self.isEdit){
        UserObject* user = getUser();
        [_lbHeaderTitle setText:NSLocalizedString(@"Need to make an update? Update your profile information here.", nil)];
        [_btSalutation setTitle:user.salutation forState:UIControlStateNormal];
        [_btSalutation setSelected:NO];
        _txtFirstName.text = user.firstName;
        _txtLastName.text = user.lastName;
       // [_btCCNumber setTitle:user. forState:UIControlStateNormal];
        _txtEmail.text = user.email;
        _txtEmail.enabled = NO;
        _txtCardNumber.text = user.memberCardId;
        _txtNoNumber.text = user.shortMobileNumber;
        _txtPostalCode.text = user.zipCode;
        [self.btCCNumber setTitle:[@"+" stringByAppendingString:user.countryCode] forState:UIControlStateNormal];
        [_btCCNumber setSelected:NO];
        
        int height= 0;
        CGSize size= self.scrollView.contentSize;
        height = size.height - self.pHeightPass.constant ;
        [self.scrollView setContentSize:CGSizeMake(size.width, height)];
        
        self.pHeightMain.constant = self.pHeightMain.constant - self.pHeightPass.constant;
        self.pHeightPass.constant = 0;
        self.navigationItem.title = NSLocalizedString(@"MY PROFILE", nil);
    }
    else
    {
        [self addTitleToHeader:NSLocalizedString(@"SIGN UP", nil) withColor:HEADER_COLOR];
    }
}


-(void)setTextForViews
{
    self.lbHeaderTitle.text = NSLocalizedString(@"Register and let Aspire Lifestyles Concierge fulfil your every request.", @"");
    self.salutationLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Salutation*", @"")];
    if(self.isEdit)
    {
        self.mandatoryLbl.attributedText = [self setStyleForString:NSLocalizedString(@"*Mandatory_ChangePW", @"")];
        self.mobileNoLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Mobile no_MyProfile*", @"")];
    }
    else
    {
        self.mandatoryLbl.attributedText = [self setStyleForString:NSLocalizedString(@"*Mandatory", @"")];
        self.mobileNoLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Mobile no*", @"")];
    }
    
    [self.btSalutation setTitle:NSLocalizedString(@"Please select your salutation", @"") forState:UIControlStateNormal];
    self.firstNameLbl.attributedText = [self setStyleForString:NSLocalizedString(@"First name*", @"")];
    self.lastNameLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Last name*", @"")];
    self.txtFirstName.placeholder = NSLocalizedString(@"How  should we address you?", @"");
    self.txtLastName.placeholder = NSLocalizedString(@"How  should we address you?", @"");
    
    self.txtNoNumber.placeholder = NSLocalizedString(@"No.", @"");
    self.lbMobileHint.text = NSLocalizedString(@"We'll need your mobile number so our concierge can contact you for your request", @"");
    
    self.emailLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Email address*", @"")];
    self.txtEmail.placeholder = NSLocalizedString(@"Please enter a valid email address", @"");
    self.lbEmailHint.text =  NSLocalizedString(@"The email address entered will your log-in ID", @"");
    self.lbPasswordHint.text =  NSLocalizedString(@"Enter maximum of 5 numbers or letters", @"");
    self.passwordLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Password*", @"")];
    self.retypePasswordLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Retype password*", @"")];
    self.authCodeLbl.attributedText = [self setStyleForString:[NSString stringWithFormat:@"%@*", NSLocalizedString(@"Code", nil)]];
    self.txtAuthCode.placeholder = NSLocalizedString(@"Input authorization code", nil);
    
    [self.submitBtn setTitle:NSLocalizedString(@"SUBMIT", @"") forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
    [self.ccBtn setTitle:NSLocalizedString(@"CC", @"") forState:UIControlStateNormal];
    // Set default value for CC Phone Number
    [self pickCCNumber:@"+7" forType:2];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//------- Show Concierge button -----------
//---- return NO if you don't want to show it ----
//---- return YES if you want to show it -----------------
-(BOOL)isShowConciergeButton{
    if(self.isEdit)
        return YES;
    else
        return NO;
}
//--------------------------

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}
-(void) touchinDetailsView{
    [self closeAllKeyboardAndPicker];
}
-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
}

-(NSInteger) actualScrollViewHeight{
    return _viewBtGroup.frame.size.height + countRightPositionInScrollView(_viewBtGroup);
}

- (void) setUpView{
    
    [_btCheck setBackgroundImage:[UIImage imageNamed:@"CheckBox_UnChecked.png"] forState:UIControlStateNormal];
    [_btCheck setBackgroundImage:[UIImage imageNamed:@"CheckBox_Checked.png"] forState:UIControlStateSelected];
    [_btCheck setSelected:NO];
        
    _txtRetypePassword.delegate = self;
    _txtCardNumber.delegate = self;
    _txtFirstName.delegate = self;
    _txtLastName.delegate = self;
    _txtPassword.delegate = self;
    _txtNoNumber.delegate = self;
    _txtEmail.delegate = self;
    _txtPostalCode.delegate = self;
    _txtAuthCode.delegate = self;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
     NSURL *url = [[NSBundle mainBundle]
                  URLForResource: @"RegAcknowledgement" withExtension:@"html"];
    [_lbConfimHint  loadRequest:[NSURLRequest requestWithURL:url]];
    _lbConfimHint.scrollView.bounces = NO;
    _lbConfimHint.scrollView.scrollEnabled = NO;
    _lbConfimHint.delegate = self;
    
    
    [self collectErrorIcons];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    int oldHeight = self.pHeightPass.constant;
    self.pHeightPass.constant = _lbConfimHint.frame.origin.y + _lbConfimHint.frame.size.height;
    self.pHeightMain.constant = _viewBtGroup.frame.origin.y + _viewBtGroup.frame.size.height - (oldHeight - self.pHeightPass.constant);
    CGSize size= self.scrollView.contentSize;
    int height = self.pHeightMain.constant ;
    [self.scrollView setContentSize:CGSizeMake(size.width, height)];
}
- (IBAction)actionShowPass:(UIButton *)sender {
    _txtPassword.secureTextEntry = !_txtPassword.secureTextEntry;
    _txtRetypePassword.secureTextEntry = !_txtRetypePassword.secureTextEntry;
    [sender setSelected:!sender.selected];
}
-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_icSalutationError forKey:[NSNumber numberWithInteger:_icSalutationError.tag]];
    [dictIconErrorManager setObject:_icFirstNameError forKey:[NSNumber numberWithInteger:_icFirstNameError.tag]];
    [dictIconErrorManager setObject:_icLastNameError forKey:[NSNumber numberWithInteger:_icLastNameError.tag]];
    [dictIconErrorManager setObject:_icEmailError forKey:[NSNumber numberWithInteger:_icEmailError.tag]];
    [dictIconErrorManager setObject:_icPhoneNumberError forKey:[NSNumber numberWithInteger:_icPhoneNumberError.tag]];
    [dictIconErrorManager setObject:_icCardError forKey:[NSNumber numberWithInteger:_icCardError.tag]];
    [dictIconErrorManager setObject:_icPasswordError forKey:[NSNumber numberWithInteger:_icPasswordError.tag]];
    [dictIconErrorManager setObject:_icRetypePassError forKey:[NSNumber numberWithInteger:_icRetypePassError.tag]];
    [dictIconErrorManager setObject:_icPostalError forKey:[NSNumber numberWithInteger:_icPostalError.tag]];
    [dictIconErrorManager setObject:_icAuthCodeError forKey:[NSNumber numberWithInteger:_icAuthCodeError.tag]];
    for (ErrorToolTip *item in [dictIconErrorManager allValues]) {
        item.delegate = self;
    }
}

-(void)closeAllKeyboardAndPicker{
    [_txtPassword resignFirstResponder];
    [_txtEmail resignFirstResponder];
    [_txtCardNumber resignFirstResponder];
    [_txtRetypePassword resignFirstResponder];
    [_txtLastName resignFirstResponder];
    [_txtFirstName resignFirstResponder];
    [_txtNoNumber resignFirstResponder];
    [_txtPostalCode resignFirstResponder];
    [_txtAuthCode resignFirstResponder];
}

- (void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + [self scrollViewPositionInSuperView] - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}

-(NSInteger) scrollViewPositionInSuperView{
    return _scrollView.frame.origin.y + countRightPositionInScrollView([_scrollView superview]) - 30;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{    
    UITextField *txt = [self.view viewWithTag:textField.tag + 1];
    if(txt!=nil){
        [txt becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)sender {
    [self updateScrollViewToSelectedView:sender];
    if(sender.tag != 0){
        UIView *errorIcon = [self.view viewWithTag:100+sender.tag];
        if(errorIcon!=nil){
            errorIcon.hidden = YES;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField.tag == _txtCardNumber.tag){
        //Card number tag
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
    
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 6;
    } else if(textField.tag == _txtLastName.tag || textField.tag == _txtFirstName.tag){
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 255;
    }
    return YES;
}


- (IBAction)checkBoxClicked:(id)sender {
    if([_btCheck isSelected]==YES)
    {
        [_btCheck setSelected:NO];
    }
    else {
        [_btCheck setSelected:YES];
    }
}

- (IBAction)salutationClicked:(id)sender {
    CCNumberController* controller = (CCNumberController*) [self controllerByIdentifier:@"SelectionList"];
    controller.data = getSalutationList();
    controller.dataType = 1;
    controller.delegate = self;
    controller.selectedValue = _btSalutation.currentTitle;
    [self.navigationController pushViewController:controller animated:YES];
    
    
    _icSalutationError.hidden = YES;
}

- (IBAction)ccNumberClicked:(id)sender {
    CCNumberController* controller = (CCNumberController*) [self controllerByIdentifier:@"SelectionList"];
    controller.data = getCCNumberList();
    controller.dataType = 2;
    controller.delegate = self;
    controller.selectedValue = _btCCNumber.currentTitle;
    [self.navigationController pushViewController:controller animated:YES];
    
    _icPhoneNumberError.hidden = YES;
}

- (IBAction)submitClicked:(id)sender {
    if(!_isEdit){
        NewUserObject *user = [self validateValues];
        if(user != nil){
            [self createNewUser:user];
        }
    }else{
        NewUserObject *user = [self validateValuesEdit];
        if(user != nil){
            [self updateUserProfile:user];
        }
    }
}

- (IBAction)cancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
-(void)updateUserProfile:(NewUserObject*) user{
    [self showToast];
    
    //[[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:API_URL parameters:parameters error:nil];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
   // [userDict setValue:user.userID forKey:@"UserID"];
    [userDict setValue:convertSalutationToEnglish(user.salutation) forKey:@"Salutation"];
    [userDict setValue:user.firstName forKey:@"FirstName"];
    [userDict setValue:user.lastName forKey:@"LastName"];
    [userDict setValue:user.email forKey:@"Email"];
    [userDict setValue:[user.ccNumber stringByAppendingString:user.phoneNumber] forKey:@"Mobile"];
    [userDict setValue:user.postalCode forKey:@"PostalCode"];
    
    wsUpdateUser = [[WSUpdateProfile alloc] init];
    wsUpdateUser.delegate = self;
    [wsUpdateUser updateUser:userDict];
}
-(void) createNewUser:(NewUserObject*) user{
    [self showToast];
    
    [[AppConfig shared] setConciergeCode:_txtAuthCode.text];
    //[[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:API_URL parameters:parameters error:nil];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:user.salutation forKey:@"Salutation"];
    [userDict setValue:user.firstName forKey:@"FirstName"];
    [userDict setValue:user.lastName forKey:@"LastName"];
    [userDict setValue:[user.ccNumber stringByAppendingString:user.phoneNumber] forKey:@"MobileNumber"];
    [userDict setValue:user.email forKey:@"EmailAddress"];
    [userDict setValue:user.cardNumber forKey:@"CreditCardNumber"];
    [userDict setValue:user.password forKey:@"Password"];
    [userDict setValue:user.postalCode forKey:@"PostalCode"];
   
    wsCreateUser = [[WSCreateUser alloc] init];
    wsCreateUser.delegate = self;
    [wsCreateUser createUser:userDict];
}



-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_CREATE_USER){
        [self stopToast];
        [[LPChatProvider shared] registerRemoteNotifications];
        [self setRootViewAfterLogin];
    }else if(ws.task == WS_UPDATE_USER){
        [self stopToast];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if (result.task == WS_CREATE_USER) {
        [[AppConfig shared] reset];
    }
    if(result!=nil && result.b2cErrorCode!=nil){
        if([result.b2cErrorCode isEqualToString:B2C_INVALID_USER_EMAIL]){
            _icEmailError.hidden = false;
            [_icEmailError setErrorMessage:NSLocalizedString(@"Please enter a valid email address", nil)];
        } else if ([result.b2cErrorCode isEqualToString:B2C_INVALID_PHONE_NUMBER]){
            _icPhoneNumberError.hidden = false;
            _icPhoneNumberError.errorMessage = NSLocalizedString(@"Please provide a valid number", nil);
        } else if ([result.b2cErrorCode isEqualToString:B2C_UNAVAILABLE_USER]){
            showAlertOneButton(self,@"", NSLocalizedString(@"User profile has been registered before.",nil), @"OK");
        }else {
            showAlertOneButton(self,@"", result.message, @"OK");
        }
    } else if (errorCode == kNO_INTERNET_CONNECTION){
        showAlertOneButton(self,ERROR_ALERT_TITLE,NSLocalizedString(@"There is no internet connection.", nil), @"OK");
    }
    else {
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_API_MSG, @"OK");
    }
}

- (NewUserObject*) validateValues{
    NewUserObject *user = nil;
    Boolean valid = true;
    _icSalutationError.hidden = true;
    _icFirstNameError.hidden = true;
    _icLastNameError.hidden = true;
    _icPhoneNumberError.hidden = true;
    _icEmailError.hidden = true;
    _icCardError.hidden = true;
    _icPasswordError.hidden = true;
    _icRetypePassError.hidden = true;
    _icAuthCodeError.hidden = true;
    NSString* localeSalutation = NSLocalizedString(@"Please select your salutation", @"");
    NSString *salutation = _btSalutation.currentTitle;
    if([localeSalutation isEqualToString:salutation]){
        salutation = nil;
        valid = false;
        _icSalutationError.hidden = NO;
    }
    
    NSString* firstName = _txtFirstName.text;
    if(firstName==nil || [firstName length]==0){
        valid = false;
        _icFirstNameError.hidden = false;
    }
    
    NSString* lastName = _txtLastName.text;
    if(lastName==nil || [lastName length]==0){
        valid = false;
        _icLastNameError.hidden = false;

    }
    
    NSString* ccNumber = [_btCCNumber.currentTitle stringByReplacingOccurrencesOfString:@"+" withString:@""];
    if([ccNumber isEqualToString:@"CC"]){
        ccNumber = nil;
        valid = false;
        _icPhoneNumberError.hidden = false;
        //_icPhoneNumberError.errorMessage = NSLocalizedString(@"Please select the country code", nil);
    }
    
    NSString* phoneNumber = _txtNoNumber.text;
    if(!checkPhoneValid(phoneNumber)){
        valid = false;
        if(_icPhoneNumberError.hidden){
            _icPhoneNumberError.hidden = false;
            _icPhoneNumberError.layer.zPosition = 1;
            //_icPhoneNumberError.errorMessage = NSLocalizedString(@"Please provide a valid number", nil);
        }
        if(phoneNumber.length==0){
            _icPhoneNumberError.errorMessage = MANDATORY_BLANK;
        }else{
            _icPhoneNumberError.errorMessage = NSLocalizedString(@"Please provide a valid number", nil);
        }
    }
    
    /* // Remove Postal (Zip) code
    NSString* postalCode = _txtPostalCode.text;
    if(postalCode==nil || [postalCode length]==0){
        postalCode = @"";
        valid = false;
        _icPostalError.hidden = false;
    }*/
    
    NSString* email = _txtEmail.text;
    if(email==nil || [email length]==0){
        valid = false;
        _icEmailError.hidden = false;
    } else if ( !checkEmailValid(email)){
        valid = false;
        _icEmailError.hidden = false;
        _icEmailError.errorMessage = NSLocalizedString(@"Please enter a valid email address", nil);
    }
    
    /* //Remove card number
    NSString* cardNumber = _txtCardNumber.text;
    if(cardNumber==nil || cardNumber.length == 0){
        valid = false;
        _icCardError.hidden = false;
    } else if([cardNumber length]< 6 || ![cardNumber isEqualToString:LuxuryCardID]){
        valid = false;
        _icCardError.hidden = false;
        _icCardError.errorMessage = NSLocalizedString(@"Value is invalid", nil);
    }*/
    
    NSString* password = _txtPassword.text;
    if(password==nil || [password length]==0){
        valid = false;
        _icPasswordError.hidden = false;
    } else if(!checkPasswordValid(password)){
        valid = false;
        _icPasswordError.hidden = false;
        _icPasswordError.errorMessage = NSLocalizedString(@"Password must be in correct format", nil);
    }
    
    NSString* authCode = _txtAuthCode.text;
    if(authCode==nil || [authCode length]==0){
        valid = false;
        _icAuthCodeError.hidden = false;
        _icAuthCodeError.errorMessage = NSLocalizedString(@"Input authorization code", nil);
    } else if(![[AppConfig shared]  isConciergeCodeValid:authCode]){
        valid = false;
        _icAuthCodeError.hidden = false;
        _icAuthCodeError.errorMessage = NSLocalizedString(@"Authorization code is invalid", nil);
    }
    
    NSString* retypePass = _txtRetypePassword.text;
    if(retypePass==nil || [retypePass length]==0 ){
        valid = false;
        _icRetypePassError.hidden = false;
    } else if (!checkPasswordValid(retypePass)){
        valid = false;
        _icRetypePassError.hidden = false;
        _icRetypePassError.errorMessage = NSLocalizedString(@"Password must be in correct format", nil);
    } else if (![retypePass isEqualToString:password]){
        valid = false;
        _icRetypePassError.hidden = false;
        _icRetypePassError.errorMessage = NSLocalizedString(@"Passwords do not match. Please try again", nil);
        if(_icPasswordError.hidden){
            _icPasswordError.hidden = false;
            _icPasswordError.errorMessage = NSLocalizedString(@"Passwords do not match. Please try again", nil);
        }
    }
    
    if(![_btCheck isSelected]){
        if(valid){
            OopsErrorPageController* controller = (OopsErrorPageController*) [self controllerByIdentifier:@"OopsErrorPageController"];
            controller.contentError =NSLocalizedString(@"Please read and agree to the T&C.", nil);
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        valid = false;
    }
    
    if(valid){
        user = [[NewUserObject alloc] init];
        user.salutation = salutation;
        user.firstName = firstName;
        user.lastName = lastName;
        user.ccNumber = ccNumber;
        user.phoneNumber = phoneNumber;
        //user.cardNumber = cardNumber;
        user.password = password;
        user.email = email;
        //user.postalCode = postalCode;
    }
    /*else if(!salutation || !firstName || firstName.length ==0 || !lastName || lastName.length == 0 || !ccNumber || !phoneNumber || phoneNumber.length == 0 || !cardNumber || cardNumber.length ==0 || !password || password.length == 0 || !retypePass || retypePass.length == 0 || !email || email.length == 0){
        
        showAlertOneButton(self ,@"Error", NSLocalizedString(@"Field is required", nil)
                                                             , @"OK");
    }*/
    
    return user;
}
- (NewUserObject*) validateValuesEdit{
    NewUserObject *user = nil;
    Boolean valid = true;
    _icSalutationError.hidden = true;
    _icFirstNameError.hidden = true;
    _icLastNameError.hidden = true;
    _icPhoneNumberError.hidden = true;

    NSString* localeSalutation = NSLocalizedString(@"Please select your salutation", @"");
    NSString *salutation = _btSalutation.currentTitle;
    if([localeSalutation isEqualToString:salutation]){
        valid = false;
        _icSalutationError.hidden = false;
    }
    
    NSString* firstName = _txtFirstName.text;
    if(firstName==nil || [firstName length]==0){
        valid = false;
        _icFirstNameError.hidden = false;
    }
    
    NSString* lastName = _txtLastName.text;
    if(lastName==nil || [lastName length]==0){
        valid = false;
        _icLastNameError.hidden = false;
        
    }
    
    NSString* email = _txtEmail.text;
    if(email==nil || [email length]==0 || !checkEmailValid(email)){
        valid = false;
        _icEmailError.hidden = false;
        _icEmailError.errorMessage = NSLocalizedString(@"Please enter a valid email address", nil);
    }
    
    
    NSString* ccNumber = _btCCNumber.currentTitle;
    if([ccNumber isEqualToString:@"CC"]){
        valid = false;
        
    }
    
    NSString* phoneNumber = _txtNoNumber.text;
    if(phoneNumber==nil || [phoneNumber length]==0){
        valid = false;
        _icPhoneNumberError.hidden = false;
    }
    
    /* //Remove postal code
    NSString* postalCode = _txtPostalCode.text;
    if(postalCode==nil || [postalCode length]==0){
        postalCode = @"";
         valid = false;
        _icPostalError.hidden = false;
    }*/
    
    if(valid){
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        user = [[NewUserObject alloc] init];
        user.userID =((UserObject*)[appdele getLoggedInUser]).userId;
        user.salutation = salutation;
        user.firstName = firstName;
        user.lastName = lastName;
        user.ccNumber = ccNumber;
        user.email = email;
        user.phoneNumber = phoneNumber;
        //user.postalCode = postalCode;
    }
    
    return user;
}
- (void) pickCCNumber:(NSString*) number forType:(NSInteger) type{
    //Type = 1: Salutation
    //Type = 2: CC number
    if(type == 1){
        [_btSalutation setSelected:NO];
        [_btSalutation setTitle:number forState:UIControlStateNormal];
    } else if (type == 2){
        if(number!=nil && number.length>0){
            [_btCCNumber setSelected:NO];
            [_btCCNumber setTitle:number forState:UIControlStateNormal];
        }else {
            [_btCCNumber setSelected:YES];
            [_btCCNumber setTitle:@"CC" forState:UIControlStateNormal];
        }
    }
}

- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
        }
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if([[[request URL] relativeString] containsString:@"terms_of_use"]){
        NSURL *url = [[NSBundle mainBundle]
                      URLForResource: NSLocalizedString(@"TermsOfUse", nil) withExtension:@"html"];
        WebController* controller =  (WebController*)[self controllerByIdentifier:@"WebController"];
        controller.urlWebsite = [url absoluteString];
        controller.pageTitle = NSLocalizedString(@"Terms & Conditions", nil);
        [self.navigationController pushViewController:controller animated:YES];
    } else if([[[request URL] absoluteString] containsString:@"privacy_policy"]){
        NSURL *url = [[NSBundle mainBundle]
                      URLForResource: NSLocalizedString(@"PrivacyPolicy", nil) withExtension:@"html"];
        WebController* controller =  (WebController*)[self controllerByIdentifier:@"WebController"];
        controller.urlWebsite = [url absoluteString];
        controller.pageTitle = NSLocalizedString(@"PRIVACY POLICY", nil);
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    return YES;
    
}

@end
