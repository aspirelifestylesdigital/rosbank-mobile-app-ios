//
//  GolfRequestController.m
//  ALC
//
//  Created by Hai NguyenV on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GolfRequestController.h"
#import "ThankYouBookingController.h"
#import "ManagerEventCalendar.h"
#import "ALCQC-Swift.h"

#define COUNTRY_HINT_TEXT NSLocalizedString(@"Please select country of preferred golf course", nil)
#define STATE_HINT_TEXT NSLocalizedString(@"Please enter state of preferred golf course", nil)
#define CITY_HINT_TEXT NSLocalizedString(@"Please enter city of preferred golf course", nil)

@interface GolfRequestController (){
    NSMutableDictionary* dictIconErrorManager;
}
@property (strong, nonatomic) IBOutlet ErrorToolTip *icGolfNameError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCityError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icStateError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCountryError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icBallerError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icTimeError;




@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbGolfDate;
@property (weak, nonatomic) IBOutlet UILabel *lbGolfTime;
@property (weak, nonatomic) IBOutlet UILabel *lbGolfCourse;
@property (weak, nonatomic) IBOutlet UILabel *lbHolePreferences;
@property (weak, nonatomic) IBOutlet UILabel *lbNoOffBaller;


@property (weak, nonatomic) IBOutlet UIButton *btCancel;



@end




@implementation GolfRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTitleToHeader:NSLocalizedString(@"GOLF", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    [self collectErrorIcons];
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    ballersNumber =1;
    _optionalDetailsView.delegate = self;
    _optionalDetailsView.lbKidsUnder12.text = NSLocalizedString(@"Adults (12 years and above)", nil);
    [_optionalDetailsView applyRequestType:GOLF_REQUEST];
    
    selectedCountry = NSLocalizedString(@"Russia", nil);
    [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    _pvState.hidden = YES;
    [_btCountry setSelected:NO];
    [_btCountry setTitle:selectedCountry forState:UIControlStateNormal];
    [_txtCity setPlaceholder:CITY_HINT_TEXT];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    
    _txtGolfDate.inputAccessoryView = toolbar;
    _txtGolfTimes.inputAccessoryView = toolbar;
    _txtCity.delegate = self;
    
    datepickerGolf = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerGolf setDatePickerMode:UIDatePickerModeDate];
    datepickerGolf.datePickerMode = UIDatePickerModeDate;
    [datepickerGolf setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [datepickerGolf addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerGolf.tag = _txtGolfDate.tag*10;
    _txtGolfDate.inputView = datepickerGolf;
    _txtGolfDate.delegate = self;
    datepickerGolf.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    _txtGolfDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerGolf.date, DATE_FORMAT)];
    //end
    
    //Check out date picker
    timepickerGolf = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [timepickerGolf setDatePickerMode:UIDatePickerModeTime];
    timepickerGolf.datePickerMode = UIDatePickerModeTime;
    [timepickerGolf addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    timepickerGolf.tag = _txtGolfTimes.tag*10;
    _txtGolfTimes.inputView = timepickerGolf;
    _txtGolfTimes.delegate = self;
    timepickerGolf.minuteInterval = 5;
    timepickerGolf.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*5];
    _txtGolfTimes.text = [self getTimebyDate:timepickerGolf.date];
    
    [self getRequestDetails];
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}
-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}
-(void) touchinDetailsView{
    [self closeAllKeyboardAndPicker];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}
-(void) getRequestDetails{
    if(_bookingId){
        [self showToast];
        [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
        
        wsGetGolfDetails = [[WSGetGolfRequestDetails alloc] init];
        wsGetGolfDetails.delegate = self;
        [wsGetGolfDetails getRequestDetails:_bookingId];
    }
//    else{
//        [self showToast];
//        wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//        wsGetMyPreference.delegate = self;
//        [wsGetMyPreference getMyPreference];
//    }
}
-(void) dateTextField:(id)sender
{
    if(((UIView*)sender).tag == _txtGolfDate.tag*10){
        _txtGolfDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerGolf.date, DATE_FORMAT)];
    } else if(((UIView*)sender).tag == _txtGolfTimes.tag*10){
        _txtGolfTimes.text = [self getTimebyDate:timepickerGolf.date];
    }
    
    if(!isValidDateTimeWithin24h(datepickerGolf.date, timepickerGolf.date)){
        _icTimeError.hidden = NO;
        timepickerGolf.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*5];
        _txtGolfTimes.text = [self getTimebyDate:timepickerGolf.date];
    }
}
- (void)dismiss
{
    [self closeAllKeyboardAndPicker];
}
-(NSString*)getTimebyDate:(NSDate*)pdate{
    NSString* time = @"";
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    [outputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *temp = [outputFormatter stringFromDate:pdate];
    
    NSDate *date = [outputFormatter dateFromString:temp];
    
    outputFormatter.dateFormat = @"hh:mm a";
    time = [outputFormatter stringFromDate:date];
    
    return  time;
}
-(NSDate*)getDateByTee:(NSString*)time{
    NSDate* result = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:DATE_FORMAT];
    [outputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *temp = [outputFormatter stringFromDate:result];
    NSString* timedate= [NSString stringWithFormat:@"%@ %@",temp,time];
    [outputFormatter setDateFormat:[NSString stringWithFormat:@"%@ %@", DATE_FORMAT, @"hh:mm a"]];
    NSDate *date = [outputFormatter dateFromString:timedate];
    
    return date;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark ---------- UITextField Delegate --------------------
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if([self.view viewWithTag:textField.tag+1]){
        if([[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextView class]])
            [(UITextView*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        else if( [[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextField class]]){
            [(UITextField*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        }
        else{
            [textField resignFirstResponder];
        }
    } else {
        [textField resignFirstResponder];
    }
    
    [self closeAllKeyboardAndPicker];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return  YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)sender{
    [self updateScrollViewToSelectedView:sender];
    UIView *errorIcon = [self.view viewWithTag:100+sender.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon) setErrorHidden:YES];
    }
}

#pragma mark ---- Optional Details Delegate
#pragma mark ------ Optional Detail Delegate ------------------
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
    
}
-(void) textFieldBeginEdit:(UIView*) textField{
    [self updateScrollViewToSelectedView:textField];
}
-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + 90 - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}


-(void) displayballersNumber:(NSInteger) pax{
    ballersNumber = pax;
    _txtBallers.text = [NSString stringWithFormat:@"%ld ", ballersNumber];
}
-(void) closeAllKeyboardAndPicker{
    [_txtGolfCourse resignFirstResponder];
    [_txtGolfDate endEditing:YES];
    [_txtGolfTimes endEditing:YES];
    [_txtCity resignFirstResponder];
}

- (IBAction)actionCoutryClicked:(id)sender {
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = COUNTRY;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btCountry.currentTitle, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedCountry, nil];
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)actionStateClicked:(id)sender {
    [_icStateError setHidden:YES];
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = STATE;
    controller.delegate = self;
    controller.maxCount = 1;
    if([selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        controller.countryType = USA;
    }else if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)]){
        controller.countryType = CANADA;
    }
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btState.currentTitle, nil] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedState, nil];
    [self.navigationController pushViewController:controller animated:YES];
}

//Pick sub filter delegate
#pragma mark ---------- Sub Filter Delegate --------------------
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(key && values && key.count>0 && values.count>0){
        if(type == COUNTRY){
            NSString* newCountry = [key objectAtIndex:0];
            if([newCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [newCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
                _pvState.hidden = NO;
            }else{
                _pvState.hidden = YES;
            }
            selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
            
            selectedCountry = newCountry;
            [_btCountry setSelected:NO];
            [_btCountry setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        } else if(type == STATE){
            NSString* newState = [key objectAtIndex:0];
            selectedState = newState;
            [_btState setSelected:NO];
            [_btState setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        }
    } else {
        if(type == COUNTRY){
            //_btCity.enabled = NO;
            selectedCity = nil;
            selectedCountry = nil;
            [_btCountry setSelected:YES];
            [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
        }else if(type == STATE){
            selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}
- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionSubmitRequest:(id)sender{
    [sender setUserInteractionEnabled:false];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
        [sender setUserInteractionEnabled:true];
    });
    GolfRequestObj* object = [self validateAndCollectData];
    if(object){
        // send request
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Golf",nil);
        
        if(_handicap.length>0){
            object.Handicap = _handicap;
        }
        
        [self showToast];
        wsCreateRequest = [[WSCreateGolfRequest alloc] init];
        wsCreateRequest.delegate = self;
        [wsCreateRequest bookingGolf:object];
    } else {
        //  showAlertOneButton(self,@"Warning", @"Please input correct values.", @"OK");
    }
}
-(GolfRequestObj*) validateAndCollectData{
    for (UIView *item in [dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }
    
    [_optionalDetailsView exportValues];
    
    GolfRequestObj* result = [[GolfRequestObj alloc] init];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_handicap.length>0){
        result.Handicap = _handicap;
    }
    if(_bookingId){
        result.BookingId = _bookingId;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    value = _txtGolfCourse.text;
    if(isValidValue(value)){
        result.GolfCourse = value;
    } else {
        isEnoughInfo = NO;
        if(value.length==0){
            _icGolfNameError.errorMessage = MANDATORY_BLANK;
        }
        _icGolfNameError.hidden = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.MobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.EmailAddress = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    result.GolfDate = formatDate(datepickerGolf.date, DATE_SEND_REQUEST_FORMAT);
    result.GolfTime = formatDate(timepickerGolf.date, TIME_SEND_REQUEST_FORMAT);
    if (!isValidDateTimeWithin24h(datepickerGolf.date, timepickerGolf.date)) {
        _icTimeError.hidden = NO;
        isEnoughInfo = NO;
    }
    value =_txtBallers.text;
    if(isValidValue(value)){
        result.noOfBaler = value;
    } else {
        if(value.length==0){
            _icBallerError.errorMessage = MANDATORY_BLANK;
        }
        [_icBallerError setHidden:NO];
        isEnoughInfo = NO;
    }
    
    if(_btRadio9Hole.selected){
        result.hole = _btRadio9Hole.currentTitle;
    } else if (_btRadio18Hole.selected){
        result.hole = _btRadio18Hole.currentTitle;
    }
    
    value = _txtCity.text;
    if(isValidValue(value)){
        result.City = value;
    } else {
        if(value.length==0){
            _icCityError.errorMessage = MANDATORY_BLANK;
        }
        [_icCityError setHidden:NO];
        isEnoughInfo = NO;
    }
    
    value = selectedCountry;
    if(isValidValue(value)){
        result.Country = value;
    } else {
        isEnoughInfo = NO;
        if(value.length==0){
            _icCountryError.errorMessage = MANDATORY_BLANK;
        }
        _icCountryError.hidden = NO;
    }
    
    if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        value = selectedState;
        if(isValidValue(value)){
            result.State = value;
        }else{
            isEnoughInfo = NO;
            if(value.length==0){
                _icStateError.errorMessage = MANDATORY_BLANK;
            }
            [_icStateError setHidden:NO];
        }
    }
    
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.SpecialRequirements = value;
        } else {
            isEnoughInfo = NO;
        }
    }
    
    result.photo = _optionalDetailsView.selectedPhoto;
    
    if(isEnoughInfo){
        return result;
    } else {
        GolfRequestObj *temp;
        return temp;
    }
}

#pragma mark ------ API Services Delegate ------------------
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_BOOKING_GOLF){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_A_REQUEST_IS_AMENDED
         object:self];
        
        [self.navigationController popViewControllerAnimated:NO];
        ThankYouBookingController* controller = (ThankYouBookingController*) [self controllerByIdentifier:@"ThankYouBooking"];
        controller.thankYouMessage = [BOOKING_THANK_YOU_MSG stringByReplacingOccurrencesOfString:@"PHONE_NUMBER_PLACEHOLDER" withString:[AppConfig shared].companyPhoneNumber];
        [appdelegate.rootVC pushViewController:controller animated:YES];
    } else if (ws.task == WS_GET_GOLF_REQUEST){
        if(wsGetGolfDetails.bookingDetails){
            //show data
            [self showBookingDetails:wsGetGolfDetails.bookingDetails];
        }
    }else if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Golf];
        
        NSString* cuisine = [dict stringForKey:VALUE1];
        NSString* other = [dict stringForKey:VALUE];
        NSString* gofthandicap = [dict stringForKey:VALUE2];
        
        if(gofthandicap.length>0){
            _handicap = gofthandicap;
        }
        
        if(other.length>0)
            _txtGolfCourse.text = other;
        
        if(cuisine.length>0){
            NSDate* date= [self getDateByTee:cuisine];
            timepickerGolf.date = date;
            _txtGolfTimes.text = [self getTimebyDate:timepickerGolf.date];
        }
    }
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result.task == WS_BOOKING_GOLF){
        if(errorCode !=0)
            displayError(self, errorCode);
        else
        {
            showAlertOneButton(self,ERROR_ALERT_TITLE, result.message, @"OK");
        }
    }
}

-(void) showBookingDetails:(GolfDetailsObj*) details{
    _handicap = details.handicaf;
    _txtGolfCourse.text = details.GolfCourse;
    datepickerGolf.date = details.GolfDate;
    timepickerGolf.date = details.GolfDate;
    _bookingId = details.bookingId;
    _txtGolfDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerGolf.date, DATE_FORMAT)];
    _txtGolfTimes.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerGolf.date, TIME_FORMAT)];
    
    [_btCountry setTitle:details.country forState:UIControlStateNormal];
    selectedCountry = details.country;
    selectedCity = details.city;
    _txtCity.text = details.city;
    
    if([details.country isEqualToString:NSLocalizedString(@"Canada", nil)] || [details.country isEqualToString:NSLocalizedString(@"United States", nil)]){
        _pvState.hidden = NO;
        [_btState setSelected:NO];
        [_btState setTitle:details.stateValue forState:UIControlStateNormal];
    }else{
        _pvState.hidden = YES;
    }
    
    _txtBallers.text = details.noOfBaler;
    ballersNumber = [details.noOfBaler intValue];
    
    if([details.hole isEqualToString:NSLocalizedString(@"18-hole",nil)]){
        _btRadio9Hole.selected = NO;
        _btRadio18Hole.selected = YES;
    }else{
        _btRadio9Hole.selected = YES;
        _btRadio18Hole.selected = NO;
    }
    //show data to optional View
    [_optionalDetailsView showDetailsView:details];
}

-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_icGolfNameError forKey:[NSNumber numberWithInteger:_icGolfNameError.tag]];
    [dictIconErrorManager setObject:_icCountryError forKey:[NSNumber numberWithInteger:_icCountryError.tag]];
    [dictIconErrorManager setObject:_icCityError forKey:[NSNumber numberWithInteger:_icCityError.tag]];
    [dictIconErrorManager setObject:_icTimeError forKey:[NSNumber numberWithInteger:_icTimeError.tag]];
    [dictIconErrorManager setObject:_icBallerError forKey:[NSNumber numberWithInteger:_icBallerError.tag]];
    for (UIView *item in [dictIconErrorManager allValues]) {
        if([item isKindOfClass:[ErrorToolTip class]]){
            ((ErrorToolTip*)item).delegate = self;
        }
    }
    
}

- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
        }
    }
}

- (IBAction)action9HoleClicked:(id)sender {
    _btRadio9Hole.selected = YES;
    _btRadio18Hole.selected = NO;
}

- (IBAction)action18HoleClicked:(id)sender {
    _btRadio9Hole.selected = NO;
    _btRadio18Hole.selected = YES;
}
- (IBAction)actionDecreaseBallers:(id)sender {
    if(ballersNumber>1){
        [self displayballersNumber:ballersNumber-1];
    }
}
- (IBAction)actionIncreaseBallers:(id)sender {
    [self displayballersNumber:ballersNumber+1];
}


- (void)dealloc
{
    if(wsCreateRequest){
        [wsCreateRequest cancelRequest];
    }
    
    if(wsGetGolfDetails){
        [wsGetGolfDetails cancelRequest];
    }
    
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
}

-(void)setTextForViews {
   
    _lbCountry.attributedText = [self setRequireString:NSLocalizedString(@"Country", nil)];
    [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
    
    
    _lbCity.attributedText = [self setRequireString:NSLocalizedString(@"City", nil)];
    _txtCity.placeholder = CITY_HINT_TEXT;
    
    _lbState.attributedText = [self setRequireString: NSLocalizedString(@"State", nil)];
    [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    
  
    
    _lbGolfCourse.attributedText = [self setRequireString:NSLocalizedString(@"Golf course", nil)];
    _txtGolfCourse.placeholder = NSLocalizedString(@"Please enter name of golf course", nil);
    
    _lbGolfDate.attributedText = [self setRequireString:NSLocalizedString(@"Date of play", nil)];
    _lbGolfTime.attributedText = [self setRequireString:NSLocalizedString(@"Tee time", nil)];
    
    _lbHolePreferences.attributedText = [self setRequireString:NSLocalizedString(@"Hole preference", nil)];
    [_btRadio9Hole setTitle:NSLocalizedString(@"9-hole", nil) forState:UIControlStateNormal];
    [_btRadio18Hole setTitle:NSLocalizedString(@"18-hole", nil) forState:UIControlStateNormal];
    
    _lbNoOffBaller.attributedText = [self setRequireString:NSLocalizedString(@"No. of Ballers", nil)];
    
    
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_btCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}
@end
