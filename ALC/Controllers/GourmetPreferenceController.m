//
//  GourmetPreferenceController.m
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetPreferenceController.h"
#import "BaseResponseObject.h"

@interface GourmetPreferenceController ()

@end

@implementation GourmetPreferenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
//    [self showToast];
//    
//    wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//    wsGetMyPreference.delegate = self;
//    [wsGetMyPreference getMyPreference];
}

-(void)setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"MY PREFERENCES", nil) withColor:HEADER_COLOR];
    self.diningTitleLbl.text = NSLocalizedString(@"Dining", @"");
    self.cuisinePreferLbl.text = NSLocalizedString(@"Cuisine preferences", @"");
    [self.btCuisine setTitle:NSLocalizedString(@"Please choose cuisine types", @"") forState:UIControlStateNormal];
    self.otherCuisineLbl.text = NSLocalizedString(@"Other cuisines", @"");
    self.tfCuisine.placeholder = NSLocalizedString(@"Please enter cuisine types", @"");
    [self.saveBtn setTitle:NSLocalizedString(@"SAVE", @"") forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
}


-(void)tapInView:(UITapGestureRecognizer *)tapGuesture{
    [_tfCuisine resignFirstResponder];
    [_tfOrtherFood resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionSave:(id)sender {
    NSString* ortherCuisine = self.tfCuisine.text;
    NSString* ortherFood = self.tfOrtherFood.text;

    [self showToast];
    wsCreateGourmet = [[WSCreateGourmetPre alloc] init];
    wsCreateGourmet.delegate = self;
    wsCreateGourmet.preferenceID = _preferenceID;
    wsCreateGourmet.arrCuisine = selectedCuisinesKey;
    wsCreateGourmet.otherCuisine = ortherCuisine;
    wsCreateGourmet.foodAllergies = ortherFood;
    [wsCreateGourmet createGourmet];

}

- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)selectCuisine:(id)sender {
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = CUISINES;
    controller.delegate = self;
    controller.selectedValues = [[NSMutableArray alloc] initWithArray:selectedCuisines];
    controller.selectedKey =[[NSMutableArray alloc] initWithArray:selectedCuisinesKey];
    [self.navigationController pushViewController:controller animated:YES];
}
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    selectedCuisines = values;
    selectedCuisinesKey = key;
    [self.btCuisine setSelected:NO];
    [self.btCuisine setTitle:convertArrayToString(values) forState:UIControlStateNormal];
    if(self.btCuisine.currentTitle.length == 0){
        [self.btCuisine setSelected:YES];
        [self.btCuisine setTitle:NSLocalizedString(@"Please choose cuisine types", nil) forState:UIControlStateNormal];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    float offset = textField.frame.size.height + countRightPositionInScrollView(textField) + 90 - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.pScrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    CGRect frame = self.pScrollView.frame;
    frame.size.height = self.view.frame.size.height ;
    [self.pScrollView setFrame:frame];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    _lbHolder.hidden = YES;
    float offset =  countRightPositionInScrollView(textView) + 80+ textView.frame.size.height;
    if(offset > 0){
        [self.pScrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if(textView.text.length==0){
        _lbHolder.hidden = NO;
    }else{
        _lbHolder.hidden = YES;
    }
    [self.pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    [self stopToast];
    if(ws.task == WS_GET_MY_PREFERENCE){
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Dining];
        NSString* cuisine = [dict stringForKey:VALUE];
        NSString* other = [dict stringForKey:VALUE1];
        NSString* foodallegic = [dict stringForKey:VALUE2];
        self.tfCuisine.text = other;
        _preferenceID = [dict stringForKey:MYPREFERENCESID];
        
        if(foodallegic.length>0){
            _tfOrtherFood.text = foodallegic;
            _lbHolder.hidden = YES;
        }
        
        if(cuisine.length>0){
            NSArray* arr= convertStringToArray(cuisine);
            if(arr.count>0){
                selectedCuisines = [[NSMutableArray alloc] initWithArray:arr];
                selectedCuisinesKey = [[NSMutableArray alloc] initWithArray:arr];
                [_btCuisine setSelected:NO];
                [self.btCuisine setTitle:convertArrayToString(selectedCuisines) forState:UIControlStateNormal];
            }
        }
    }else if (ws.task == WS_CREATE_GOURMETPRE){
        [self.navigationController popViewControllerAnimated:YES];
    }    
}
-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
     [self stopToast];
    if(errorCode == 600){
        return;
    }
    if(result!=nil && result.message!=nil){
        showAlertOneButton(self,@"", result.message, @"OK");
    } else {
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_API_MSG, @"OK");
    }
}

- (void)dealloc
{
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
    if(wsCreateGourmet){
        [wsCreateGourmet cancelRequest];
    }
}
@end
