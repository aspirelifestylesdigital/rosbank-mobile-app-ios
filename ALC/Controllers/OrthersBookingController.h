//
//  OrthersBookingController.h
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "OptionalDetailsView.h"
#import "WSCreateOrthersRequest.h"
#import "WSGetOtherRequestDetails.h"
#import "WSGetMyPreferences.h"
#import "RequestFormBaseController.h"
@interface OrthersBookingController : RequestFormBaseController<UITextViewDelegate,DataLoadDelegate>
@property (strong, nonatomic) NSString* bookingId;



@end
