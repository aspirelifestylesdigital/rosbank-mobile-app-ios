//
//  AboutController.m
//  ALC
//
//  Created by Anh Tran on 9/21/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "AboutController.h"
#import "WebController.h"
#import "FAQController.h"
#import "AboutDetailsController.h"
#import "ALCQC-Swift.h"

@interface AboutController ()
@property (weak, nonatomic) IBOutlet UILabel *lbAsprite;
@property (weak, nonatomic) IBOutlet UILabel *lbTermCodition;
@property (weak, nonatomic) IBOutlet UILabel *lbPrivacy;

@end

@implementation AboutController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
    [self addTitleToHeader:NSLocalizedString(@"ABOUT", nil) withColor:HEADER_COLOR];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)termsConditionClicked:(id)sender {
    //NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"TermsOfUse" ofType:@"html" inDirectory:@"html"]];
    NSURL *url = [[NSBundle mainBundle]
                    URLForResource: NSLocalizedString(@"TermsOfUse", nil) withExtension:@"html"];
    WebController* controller =  (WebController*)[self controllerByIdentifier:@"WebController"];
    controller.urlWebsite = [url absoluteString];
    controller.pageTitle = NSLocalizedString(@"Terms & Conditions", nil);
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)privacyPolicyClicked:(id)sender {
    NSURL *url = [[NSBundle mainBundle]
                  URLForResource: NSLocalizedString(@"PrivacyPolicy", nil) withExtension:@"html"];
    WebController* controller =  (WebController*)[self controllerByIdentifier:@"WebController"];
    controller.urlWebsite = [url absoluteString];
    controller.pageTitle = NSLocalizedString(@"PRIVACY POLICY", nil);
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)luxuryCardClicked:(id)sender {
    AboutDetailsController* controller =  (AboutDetailsController*)[self controllerByIdentifier:@"AboutDetailsController"];
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)faqClicked:(id)sender {
    /*FAQController* controller =  (FAQController*)[self controllerByIdentifier:@"FAQController"];
    [self.navigationController pushViewController:controller animated:YES];*/
}


-(void)setTextForViews {
    _lbAsprite.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Concierge service", nil), [AppConfig shared].conciergeServiceName];
    _lbTermCodition.text = NSLocalizedString(@"Terms & Conditions", nil);
    _lbPrivacy.text = NSLocalizedString(@"Privacy Policy", nil);
}

@end
