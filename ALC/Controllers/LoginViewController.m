//
//  LoginViewController.m
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "LoginViewController.h"
#import "CommonUtils.h"
#import "StoreUserData.h"
#import "AppDelegate.h"
#import "Common.h"
#import "BaseResponseObject.h"
#import "WebController.h"
#import "ALCQC-Swift.h"


@interface LoginViewController (){
    WSB2CGetUserDetails *wsUserDetails;
    CGSize originalSize;
    NSMutableDictionary* dictIconErrorManager;
}
@property (strong, nonatomic) IBOutlet UILabel *lbPasswordHint;
@property (strong, nonatomic) IBOutlet UIView *errorView;
@property (strong, nonatomic) IBOutlet UILabel *lbError;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIWebView *lbTermAndPolicy;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtCode;
@property (strong, nonatomic) IBOutlet UILabel *lbCode;

@property (strong, nonatomic) IBOutlet UIButton *btLogo;
@property (strong, nonatomic) IBOutlet UIButton *btGetStarted;
@property (strong, nonatomic) IBOutlet UIButton *btSignUp;
@property (strong, nonatomic) IBOutlet UIButton *btForgotPassword;
@property (strong, nonatomic) IBOutlet UIButton *btSignIn;
@property (weak, nonatomic) IBOutlet UILabel *lbDescriptionLogin;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewEmail;
@property (strong, nonatomic) IBOutlet UIView *viewPassword;
@property (strong, nonatomic) IBOutlet ErrorToolTip *emailErrorToolTip;
@property (strong, nonatomic) IBOutlet ErrorToolTip *passwordErrorToolTip;
@property (strong, nonatomic) IBOutlet ErrorToolTip *codeErrorToolTip;
@property (strong, nonatomic) IBOutlet UIButton *btEyePassword;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *passwordLbl;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideErrorMsg];
    self.btGetStarted.hidden = true;
    self.btGetStarted.enabled = false;
    _txtPassword.delegate = self;
    _txtEmail.delegate = self;
    _txtCode.delegate = self;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
    }
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        [self setRootViewAfterLogin];
    }
    /*
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapGesture:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
    */
    originalSize = _scrollView.contentSize;
    [_btGetStarted.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_btSignUp.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_btForgotPassword.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_btSignIn.titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    _txtPassword.font = nil;
    _txtPassword.font = [UIFont fontWithName:@"AvenirNextLTPro-Medium" size:FONT_SIZE_15*SCREEN_SCALE];
    
    [self collectErrorIcons];
    
    [_lbTermAndPolicy setOpaque:NO];
    [_lbTermAndPolicy setBackgroundColor:[UIColor clearColor]];
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LogAcknowledgement" ofType:@"html"]
                                                  encoding:NSUTF8StringEncoding
                                                     error:nil];
    html = [html stringByReplacingOccurrencesOfString:@"PHONE_NUMBER_PLACEHOLDER" withString:[AppConfig shared].conciergePhoneNumber];
    [_lbTermAndPolicy loadHTMLString:html baseURL:nil];
    _lbTermAndPolicy.scrollView.bounces = NO;
    _lbTermAndPolicy.scrollView.scrollEnabled = NO;
    _lbTermAndPolicy.delegate = self;
}

-(void) setTextForViews
{
    [self.btGetStarted setTitle:NSLocalizedString(@"GET STARTED LOGIN VIEW", @"") forState:UIControlStateNormal];
    self.emailLbl.text = NSLocalizedString(@"Email", @"");
    self.lbCode.text = NSLocalizedString(@"Code", @"");
    self.txtCode.placeholder = NSLocalizedString(@"Input authorization code", @"");
    self.passwordLbl.text = NSLocalizedString(@"Password", @"");
    self.txtEmail.placeholder = NSLocalizedString(@"name@example.com", @"");
    [self.btSignIn setTitle:NSLocalizedString(@"SIGN IN", @"") forState:UIControlStateNormal];
    [self.btForgotPassword setTitle:NSLocalizedString(@"Forgot password", @"") forState:UIControlStateNormal];
    [self.btSignUp setTitle:NSLocalizedString(@"Sign Up Login View", @"") forState:UIControlStateNormal];
    self.lbError.text = NSLocalizedString(@"You've entered invalid login details. Please try again.", @"");
    self.lbDescriptionLogin.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"This is a members-only Concierge Service.", @""), NSLocalizedString(@"company_name", @"")];
    
}


//------- Show Concierge button -----------
//---- return NO if you don't want to show it ----
//---- return YES if you want to show it -----------------
-(BOOL)isShowConciergeButton{
    return NO;
}
//--------------------------

-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_emailErrorToolTip forKey:[NSNumber numberWithInteger:_emailErrorToolTip.tag]];
    [dictIconErrorManager setObject:_passwordErrorToolTip forKey:[NSNumber numberWithInteger:_passwordErrorToolTip.tag]];
    for (ErrorToolTip *item in [dictIconErrorManager allValues]) {
        item.delegate = self;
    }
    
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [_txtPassword resignFirstResponder];
    [_txtEmail resignFirstResponder];
    [_scrollView setContentSize:originalSize];
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    //Send login
    if(textField.tag == 2){
        [_txtCode becomeFirstResponder];
        [_scrollView setContentSize:originalSize];
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    } else if (textField.tag == 1){
        [_txtPassword becomeFirstResponder];
    } else if(textField.tag == 3){
        [self sendLogin];
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField.tag == 2){
        float offset = (_viewPassword.frame.origin.y) - (SCREEN_HEIGHT - 216) + _viewPassword.frame.size.height*3;
        if(offset > 0){
            [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
        }
    }
    
    UIView *errorIcon = [self.view viewWithTag:100+textField.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon)  setErrorHidden:YES];
    }
    _btEyePassword.hidden = !_passwordErrorToolTip.hidden;
    
    [_scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT + _lbPasswordHint.frame.size.height + _btSignIn.frame.size.height + 30)];
}


- (IBAction)getStartClicked:(id)sender {
    [[AppConfig shared] reset];
    [self setRootViewAfterLogin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) sendLogin{
    [self hideErrorMsg];
    [_passwordErrorToolTip setErrorHidden:YES];
    [_emailErrorToolTip setErrorHidden:YES];
    if (!checkEmailValid(_txtEmail.text)) {
        [_emailErrorToolTip  setErrorHidden:false];
    }
    if(!checkPasswordValid(_txtPassword.text)){
        [_passwordErrorToolTip setErrorHidden:false];
    }
    
    [self.codeErrorToolTip setErrorHidden:[[AppConfig shared] isConciergeCodeValid:self.txtCode.text]];
    
    _btEyePassword.hidden = !_passwordErrorToolTip.hidden;
    
    if(_emailErrorToolTip.hidden && _passwordErrorToolTip.hidden && self.codeErrorToolTip.hidden){
        [[AppConfig shared] setConciergeCode:self.txtCode.text];
        [self showToast];
        [self getUserDetails:_txtEmail.text andPassword:_txtPassword.text];
    }
}

- (IBAction)actionShowPass:(UIButton *)sender {
    _txtPassword.secureTextEntry = !_txtPassword.secureTextEntry;
    [sender setSelected:!sender.selected];
    [_txtPassword setFont:nil];
    [_txtPassword setFont:[UIFont fontWithName:@"AvenirNextLTPro-Medium" size:FONT_SIZE_15*SCREEN_SCALE]];
}

- (IBAction)signInClicked:(id)sender {
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [appdele logoutUser];
    
    [self sendLogin];
}

- (void)handleDoubleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        //TODO: back to Splash
    }
}

- (void) getUserDetails:(NSString*) email andPassword:(NSString*) password{
    wsLogin = [[WSB2CLogin alloc] init];
    wsLogin.delegate = self;
    [wsLogin onLogin:email andPass:password];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(nullable id)sender{
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark --- Login Delegate ------
- (void) showErrorMessage:(NSString*) errorMsg{
//    errorMsg = NSLocalizedString(@"You've entered invalid login details. Please try again.", nil);
    _lbError.text = errorMsg;
    _lbError.hidden = NO;
    CGRect frame = _lbError.frame;
    frame.size = getSizeWithFontAndFrame(errorMsg, [UIFont fontWithName:@"AvenirNextLTPro-Medium" size:11.0], frame.size.width, 400);
    _lbError.frame = frame;
    
    CGRect frameView = _errorView.frame;
    frameView.size.height = frame.size.height + 14;
    _errorView.frame = frameView;
    _errorView.hidden = NO;
    
    
}

- (void) hideErrorMsg{
    CGRect frameView = _errorView.frame;
    frameView.size.height = 0;
    _errorView.frame = frameView;
    _errorView.hidden = YES;
    
    CGRect frameLabel = _lbError.frame;
    frameLabel.size.height = 0;
    _lbError.frame = frameLabel;
    _lbError.hidden = YES;

}

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_AUTHENTICATION_LOGIN){
//        wsUserDetails = [[WSB2CGetUserDetails alloc] init];
//        wsUserDetails.delegate = self;
//        [wsUserDetails getUserDetails];
        [self stopToast];
        [[LPChatProvider shared] registerRemoteNotifications];
        [self setRootViewAfterLogin];
    }
//    else if(ws.task == WS_B2C_GET_USER_DETAILS){
//        [self stopToast];
//        [self setRootViewAfterLogin];
//    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    [[AppConfig shared] reset];
    if(result!=nil && result.b2cErrorCode!=nil){
        if([result.b2cErrorCode isEqualToString:B2C_LOGIN_FAIL]){
            [self showErrorMessage:NSLocalizedString(@"You've entered invalid login details. Please try again.", nil)];
        } else {
            if ([result.b2cErrorCode isEqualToString:@"ENR88-1"]) {
//                showAlertOneButton(self,@"", NSLocalizedString(@"You've entered invalid login details. Please try again.", @""), NSLocalizedString(@"OK", nil));
                [self showErrorMessage:NSLocalizedString(@"You've entered invalid login details. Please try again.", nil)];
            }else{
                showAlertOneButton(self,@"", result.message, NSLocalizedString(@"OK", nil));
            }
        }
    } else if (errorCode == kNO_INTERNET_CONNECTION){
        showAlertOneButton(self,ERROR_ALERT_TITLE,NSLocalizedString(@"There is no internet connection.", nil), @"OK");
    } else {
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_API_MSG, @"OK");
    }
}

- (IBAction)backSplashScreen:(id)sender {
    [self setShowSplashScreen];
}
- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
        }
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if([[[request URL] relativeString] containsString:@"terms_of_use"]){
        NSURL *url = [[NSBundle mainBundle]
                      URLForResource: NSLocalizedString(@"TermsOfUse", nil) withExtension:@"html"];
        WebController* controller =  (WebController*)[self controllerByIdentifier:@"WebController"];
        controller.urlWebsite = [url absoluteString];
        controller.pageTitle = NSLocalizedString(@"Terms & Conditions", nil);
        [self.navigationController pushViewController:controller animated:YES];
    } else if([[[request URL] absoluteString] containsString:@"privacy_policy"]){
        NSURL *url = [[NSBundle mainBundle]
                      URLForResource: NSLocalizedString(@"PrivacyPolicy", nil) withExtension:@"html"];
        WebController* controller =  (WebController*)[self controllerByIdentifier:@"WebController"];
        controller.urlWebsite = [url absoluteString];
        controller.pageTitle = NSLocalizedString(@"PRIVACY POLICY", nil);
        [self.navigationController pushViewController:controller animated:YES];
    } else if([[[request URL] absoluteString] containsString:@"tell"]) {
        NSString *phoneURL = [NSString stringWithFormat:@"tel:%@", [AppConfig shared].conciergePhoneNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneURL]];
    }
    
    return YES;
    
}


@end
