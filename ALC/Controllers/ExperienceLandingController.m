//
//  ExperienceLandingController.m
//  ALC
//
//  Created by Anh Tran on 8/22/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ExperienceLandingController.h"
#import "ExperienceViewCell.h"
#import "DCRItemSearchController.h"
//#import "MapController.h"
#import "ExperienceItem.h"


@interface ExperienceLandingController (){
    NSArray *data;
    int cellHeight;
}
@end

@implementation ExperienceLandingController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleToHeader:NSLocalizedString(@"EXPERIENCES", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    data = [self experienceLandingList];
    cellHeight = self.view.frame.size.width / (320.0/185.0);
    // Do any additional setup after loading the view.
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ExperienceViewCell";
    
    ExperienceViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib;
    nib = [[NSBundle mainBundle] loadNibNamed:@"ExperienceViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    ExperienceItem *item = ((ExperienceItem*)[data objectAtIndex:indexPath.row]);
    [cell.lbExperienceName setFont:[UIFont fontWithName:@"AvenirNextLTPro-Demi" size:FONT_SIZE_16*SCREEN_SCALE]];
    cell.lbExperienceName.text = NSLocalizedString(item.title, nil);
    cell.imvImage.image = [UIImage imageNamed:item.imageURL];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DCRItemSearchController *controller = [[DCRItemSearchController alloc] init];
    if(indexPath.row == 0){
        controller.filterType = DINING_ITEM;
    } else if(indexPath.row == 1){
        controller.filterType = ACCOMMODATION_ITEM;
    } else if(indexPath.row == 2){
        controller.filterType = SPA_ITEM;
    }
    [self.navigationController pushViewController:controller animated:YES];
}

-(NSArray*) experienceLandingList{
    return [[NSArray alloc] initWithObjects:
            [[ExperienceItem alloc] initWithType:@"DINING" andImage:@"rsz_concierge_service_-_restaurant_and_bars.jpg"],
            [[ExperienceItem alloc] initWithType:@"HOTEL" andImage:@"rsz_concierge_services_-_hotels.jpg"],
            [[ExperienceItem alloc] initWithType:@"SPA" andImage:@"ImgSPLURGE.png"],
            nil];
}

@end

