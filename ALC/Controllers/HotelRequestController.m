//
//  HotelRequestController.m
//  ALC
//
//  Created by Anh Tran on 9/27/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HotelRequestController.h"
#import "OptionalDetailsView.h"
#import "BaseResponseObject.h"
#import "WSGetHotelRequestDetails.h"
#import "HotelRequestDetailsObject.h"
#import "ThankYouBookingController.h"
#import "WSCreateHotelRecommendRequest.h"
#import "WSGetHotelRecommendDetails.h"
#import "ManagerEventCalendar.h"

#define COUNTRY_HINT_TEXT NSLocalizedString(@"Please select country of preferred hotel", nil)
#define CITY_HINT_TEXT NSLocalizedString(@"Please enter city of preferred hotel", nil)
#define ROOM_PREFERENCE_HINT_TEXT NSLocalizedString(@"Please select room preference", nil)
#define STATE_HINT_TEXT NSLocalizedString(@"Please enter state of preferred hotel", nil)

@interface HotelRequestController (){
    UIDatePicker *datepickerCheckIn;
    UIDatePicker *datepickerCheckOut;
    NSInteger paxNumber;
    WSCreateHotelRequest* wsCreateRequest;
    WSGetHotelRequestDetails* wsGetRequestDetails;
    WSCreateHotelRecommendRequest* wsCreateRecommendRequest;
    WSGetHotelRecommendDetails* wsGetHotelRecommendDetails;
    NSMutableDictionary* dictIconErrorManager;
    
    WSGetMyPreferences* wsGetMyPreference;
}
@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;
@property (strong, nonatomic) IBOutlet UITextField *txtPax;
@property (strong, nonatomic) IBOutlet UITextField *txtEnternHotelName;
@property (strong, nonatomic) IBOutlet UITextField *txtCheckOutDate;
@property (strong, nonatomic) IBOutlet UITextField *txtCheckInDate;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCity;
@property (strong, nonatomic) IBOutlet UIButton *btCountry;
@property (strong, nonatomic) IBOutlet UIStackView *topNavigation;

@property (strong, nonatomic) IBOutlet UIView *topOrangeUcomingBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowUpcoming;
@property (strong, nonatomic) IBOutlet UIView *topOrangeHistoryBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowHistory;
@property (strong, nonatomic) IBOutlet UIButton *btBook;
@property (strong, nonatomic) IBOutlet UIButton *btRecommend;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topSpaceToSuperView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topRecommendSpaceToSuperView;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (strong, nonatomic) IBOutlet HotelRecommendView *viewRecommend;
@property (weak, nonatomic) IBOutlet UIView *pvState;
@property (strong, nonatomic) IBOutlet UIButton *btState;

@property (strong, nonatomic) IBOutlet ErrorToolTip *icHotelNameError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCityError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icStateError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCountryError;


@property (strong, nonatomic) IBOutlet UIView *viewHotelName;
@property (strong, nonatomic) IBOutlet UIStackView *stackBookForm;
@property (strong, nonatomic) IBOutlet UIView *viewCountry;
@property (strong, nonatomic) IBOutlet UIView *viewCity;




@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbHotelName;
@property (weak, nonatomic) IBOutlet UILabel *lbCheckInDate;
@property (weak, nonatomic) IBOutlet UILabel *lbCheckOutDate;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;

- (IBAction)actionStateClicked:(id)sender;

@end

@implementation HotelRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTextForViews];
    [self getRequestDetails];
    
}

-(void) setUpSubView{
    [self addTitleToHeader:NSLocalizedString(@"HOTEL", nil) withColor:HEADER_COLOR];
    
    //Display Optional Details View
    _optionalDetailsView.delegate = self;
    [_optionalDetailsView applyRequestType:HOTEL_REQUEST];
    //end
    
    [_txtCity setPlaceholder:CITY_HINT_TEXT];
    [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    _pvState.hidden = YES;
    [_btCountry setSelected:NO];
    [_btCountry setTitle:self.selectedCountry forState:UIControlStateNormal];
    
    _viewRecommend.delegate = self;
    if(_requestType == HOTEL_RECOMMEND_REQUEST){
        [self showRecommendView];
    } else {
        [self showBookView];
    }
    
    [self setUpCheckInCheckOutDate];
    [self displayPaxNumber:1];
    
    _txtPax.delegate = self;
    _txtCity.delegate = self;
    
    
    [self collectErrorIcons];
    
    //[_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
}

- (void) hideTopHeader{
    _topNavigation.hidden = YES;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    _topSpaceToSuperView.constant = -_scrollView.frame.origin.y;
    _topRecommendSpaceToSuperView.constant = -   _viewRecommend.frame.origin.y;
}

-(void) setUpCheckInCheckOutDate{
    UIToolbar *toolbar= [self setUpToolBar];
    
    _txtCheckInDate.inputAccessoryView = toolbar;
    _txtCheckOutDate.inputAccessoryView = toolbar;
    
    
    //Check in date picker
    datepickerCheckIn = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerCheckIn setDatePickerMode:UIDatePickerModeDate];
    datepickerCheckIn.datePickerMode = UIDatePickerModeDate;
    [datepickerCheckIn setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [datepickerCheckIn addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerCheckIn.tag = _txtCheckInDate.tag*10;
    _txtCheckInDate.inputView = datepickerCheckIn;
    _txtCheckInDate.delegate = self;
    datepickerCheckIn.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    _txtCheckInDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckIn.date, DATE_FORMAT)];
    //end
    
    //Check out date picker
    datepickerCheckOut = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerCheckOut setDatePickerMode:UIDatePickerModeDate];
    datepickerCheckOut.datePickerMode = UIDatePickerModeDate;
    [datepickerCheckOut setMinimumDate:datepickerCheckIn.date];
    [datepickerCheckOut addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerCheckOut.tag = _txtCheckOutDate.tag*10;
    _txtCheckOutDate.inputView = datepickerCheckOut;
    _txtCheckOutDate.delegate = self;
    datepickerCheckOut.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*2];
    _txtCheckOutDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckOut.date, DATE_FORMAT)];
    //end
    
}


-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) displayPaxNumber:(NSInteger) pax{
    paxNumber = pax;
    _txtPax.text = [NSString stringWithFormat:@"%ld ", paxNumber];
}

-(void) displayUserInfo{
    AppDelegate*  appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    UserObject *user = [appDelegate getLoggedInUser];
    if(user){
        //_txtGuestName.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    }
}

-(void) getRequestDetails{
    if(_bookingId){
        if(_requestType == HOTEL_RECOMMEND_REQUEST){
            [self showToast];
            wsGetHotelRecommendDetails = [[WSGetHotelRecommendDetails alloc] init];
            wsGetHotelRecommendDetails.delegate = self;
            [wsGetHotelRecommendDetails getRequestDetails:_bookingId];
        } else {
            [self showToast];
            wsGetRequestDetails = [[WSGetHotelRequestDetails alloc] init];
            wsGetRequestDetails.delegate = self;
            [wsGetRequestDetails getRequestDetails:_bookingId];
        }
    }
//    else{
//        [self showToast];
//        wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//        wsGetMyPreference.delegate = self;
//        [wsGetMyPreference getMyPreference];
//    }
}

#pragma textfield

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField.tag == 0 || textField.tag == 1003 || textField.tag==1004){
        return NO;
    }
    return  YES;
}

#pragma end

- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionDecreasePax:(id)sender {
    if(paxNumber>1){
        [self displayPaxNumber:paxNumber-1];
    }
}

- (IBAction)actionIncreasePax:(id)sender {
    [self displayPaxNumber:paxNumber+1];
}

- (IBAction)actionCoutryClicked:(id)sender {
    [_icCountryError setHidden:YES];
    [self openFilterCountry:self];
}
- (IBAction)actionStateClicked:(id)sender {
    [_icStateError setHidden:YES];
    [self openFilterState:self];
}


-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + [self scrollViewPositionInSuperView] - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}


-(NSInteger) scrollViewPositionInSuperView{
    return _scrollView.frame.origin.y + countRightPositionInScrollView([_scrollView superview]) - 30;
}

-(void) closeAllKeyboardAndPicker{
    [_txtEnternHotelName resignFirstResponder];
    [_txtCheckInDate endEditing:YES];
    [_txtCheckOutDate endEditing:YES];
    [_txtPax resignFirstResponder];
    //[_txtGuestName resignFirstResponder];
    [_txtCity resignFirstResponder];
    
    [self adjustCheckOutDateDueToCheckIn];
}

- (IBAction)actionSubmitRequest:(id)sender {
    [sender setUserInteractionEnabled:false];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
        [sender setUserInteractionEnabled:true];
    });
    [self showToast];
    [self closeAllKeyboardAndPicker];
    HotelRequestObject* object = [self validateAndCollectData];
    if(object){
        // send request
        
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Hotel",nil);
        
        wsCreateRequest = [[WSCreateHotelRequest alloc] init];
        wsCreateRequest.delegate = self;
        [wsCreateRequest bookingHotel:object];
    } else {
        [self stopToast];
        //showAlertOneButton(self,@"Warning", @"Please input correct values.", @"OK");
    }
}

// Submit from Recommend View
-(void) submitRequest:(HotelRequestObject*) requestObject forType:(enum REQUEST_TYPE) type{
    if(type == HOTEL_RECOMMEND_REQUEST){
        if(_bookingId){
            requestObject.bookingId = _bookingId;
        }
        
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Hotel Recommend",nil);
        
        [self showToast];
        wsCreateRecommendRequest = [[WSCreateHotelRecommendRequest alloc] init];
        wsCreateRecommendRequest.delegate = self;
        [wsCreateRecommendRequest bookingHotelRecommend:requestObject];
    }
}
//end

-(HotelRequestObject*) validateAndCollectData{
    for (ErrorToolTip *item in [dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }
    
    [_optionalDetailsView exportValues];
    
    HotelRequestObject* result = (HotelRequestObject*)[self addPrivilegeData:[[HotelRequestObject alloc] init]];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.bookingId = _bookingId;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    value = _txtEnternHotelName.text;
    if(isValidValue(value)){
        result.hotelName = value;
        result.bookingItemId = value;
    } else {
        [_icHotelNameError setHidden:NO];
        if(value.length==0){
            _icHotelNameError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.mobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.email = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    result.checkInDate = formatDate(datepickerCheckIn.date, DATETIME_SEND_REQUEST_FORMAT);
    result.checkOutDate = formatDate(datepickerCheckOut.date, DATETIME_SEND_REQUEST_FORMAT);
    
    value = _optionalDetailsView.roomType;
    if(isValidValue(value)){
        result.roomType = value;
    }else {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.loyaltyMembershipProgram;
    if(isValidValue(value)){
        result.loyyaltyProgram = value;
    }
    
    value= _optionalDetailsView.loyaltyMembershipNo;
    if(isValidValue(value))
    {
        result.membershipNo = value;
    }
    result.adultPax = _optionalDetailsView.adultPax;
    result.kidsPax = _optionalDetailsView.kidsPax;
    /*if(_optionalDetailsView.normalPax > 0){
        result.noOfRooms = _optionalDetailsView.normalPax;
    }*/
    
    
    value = _txtCity.text;
    if(isValidValue(value)){
        result.city = value;
    } else {
        if(value.length==0){
            _icCityError.errorMessage = MANDATORY_BLANK;
        }
        [_icCityError setHidden:NO];
        isEnoughInfo = NO;
    }
    
    value = self.selectedCountry;
    if(isValidValue(value)){
        result.country = value;
    } else {
        [_icCountryError setHidden:NO];
        if(value.length==0){
            _icCountryError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    if([self isSpecialCountries]){
        value = self.selectedState;
        if(isValidValue(value)){
            result.State = value;
        }else{
            isEnoughInfo = NO;
            if(value.length==0){
                _icStateError.errorMessage = MANDATORY_BLANK;
            }
            [_icStateError setHidden:NO];
        }
    }
    
    result.isSmoking = _optionalDetailsView.isSmoking;
    //result.isWithBreakfast = _optionalDetailsView.isWithBreakfast;
    //result.isWithWifi = _optionalDetailsView.isWithWifi;
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.specialMessage = value;
        } else {
            isEnoughInfo = NO;
        }
    }
    
    //result.photo = _optionalDetailsView.selectedPhoto;
    
    if(isEnoughInfo){
        return result;
    } else {
        HotelRequestObject *temp;
        return temp;
    }
}


-(void) dateTextField:(id)sender
{
    if(((UIView*)sender).tag == _txtCheckInDate.tag*10){
        _txtCheckInDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckIn.date, DATE_FORMAT)];
    } else if(((UIView*)sender).tag == _txtCheckOutDate.tag*10){
        _txtCheckOutDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckOut.date, DATE_FORMAT)];
    }
    
}

-(void) adjustCheckOutDateDueToCheckIn
{
    //adjust CheckOut date due to Checkin
    if([datepickerCheckOut.date compare:datepickerCheckIn.date] != NSOrderedDescending
       || ([formatDateToString(datepickerCheckOut.date, DATE_FORMAT) isEqualToString:formatDateToString(datepickerCheckIn.date, DATE_FORMAT)])){
        datepickerCheckOut.date = [datepickerCheckIn.date dateByAddingTimeInterval:60*60*24*1];
        _txtCheckOutDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckOut.date, DATE_FORMAT)];
    }
    [datepickerCheckOut setMinimumDate:[datepickerCheckIn.date dateByAddingTimeInterval:60*60*24*1]];
}

//Pick sub filter delegate
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(key && values && key.count>0 && values.count>0){
        if(type == COUNTRY){
            //_btCity.enabled = YES;
            NSString* newCountry = [key objectAtIndex:0];
            self.selectedCountry = newCountry;
            [_btCountry setSelected:NO];
            [_btCountry setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
            
            if([self isSpecialCountries]){
                _pvState.hidden = NO;
            }else{
                _pvState.hidden = YES;
            }
            self.selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
            
        } else if(type == STATE){
            NSString* newState = [key objectAtIndex:0];
            self.selectedState = newState;
            [_btState setSelected:NO];
            [_btState setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        }
    } else {
        if(type == COUNTRY){
            self.selectedCountry = nil;
            [_btCountry setSelected:YES];
            [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
        } else if(type == STATE){
            self.selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}

#pragma OptionalDetailsDelegate
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
    [self closeAllKeyboardAndPicker];
}

-(void) textFieldBeginEdit:(UIView*) selectedView{
    [self updateScrollViewToSelectedView:selectedView];
}


-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_BOOKING_HOTEL || ws.task == WS_BOOKING_HOTEL_RECOMMEND){
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_A_REQUEST_IS_AMENDED
         object:self];
        
        if(_bookingId==nil){
            if(ws.task == WS_BOOKING_HOTEL){
                [self trackEcommerceProduct:@"Generic - Book" withCate:@"Hotel"];
            }else if(ws.task == WS_BOOKING_HOTEL_RECOMMEND){
                [self trackEcommerceProduct:@"Generic - Recommend" withCate:@"Recommend Hotel"];
            }
        }
        [self openThankYouPage];
        
    }else if (ws.task == WS_GET_HOTEL_REQUEST){
        if(wsGetRequestDetails.bookingDetails){
            //show data
            [self showBaseBookingDetails:wsGetRequestDetails.bookingDetails];
        }
    }else if (ws.task == WS_GET_HOTEL_RECOMMEND_REQUEST){
        if(wsGetHotelRecommendDetails.bookingDetails){
            //show data
            [self.viewRecommend showRecommendDetails:wsGetHotelRecommendDetails.bookingDetails];
        }
    }else if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Hotel];
        
        NSString* value = [dict stringForKey:VALUE3];
        if(isEqualIgnoreCase(value,@"yes")){
            _optionalDetailsView.swSmoking.on = YES;
        }else{
            _optionalDetailsView.swSmoking.on = NO;
        }
        
        NSString* room = [dict stringForKey:VALUE1];
        _anyRequest = [dict stringForKey:VALUE6];
        
        [_optionalDetailsView setRoomPreferen:room];
        [_optionalDetailsView setLoyalty:[dict stringForKey:VALUE4] withNo:[dict stringForKey:VALUE5]];
        [_viewRecommend setMyPreference:dict];
    }
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result.task == WS_BOOKING_HOTEL || result.task == WS_BOOKING_HOTEL_RECOMMEND){
        if(errorCode !=0)
            displayError(self, errorCode);
        else
        {
            showAlertOneButton(self,ERROR_ALERT_TITLE, result.message, @"OK");
        }
    }
    
}


-(void) showBookingDetails:(HotelRequestDetailsObject*) details{
    _anyRequest = details.anyRequest;
    _txtEnternHotelName.text = details.hotelName;
    //_txtGuestName.text = details.guestName;
    datepickerCheckIn.date = details.checkInDate;
    datepickerCheckOut.date = details.checkOutDate;
    _txtCheckInDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckIn.date, DATE_FORMAT)];
    _txtCheckOutDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckOut.date, DATE_FORMAT)];
    
    //[_btCity setTitle:details.city forState:UIControlStateNormal];
    self.selectedCountry = details.country;
    self.selectedCity = details.city;
    _txtCity.text = details.city;
    [_btCountry setTitle:(details.countryValue ? details.countryValue : self.selectedCountry) forState:UIControlStateNormal];
    [_btCountry setSelected:NO];
    
    if([details.country isEqualToString:NSLocalizedString(@"Canada", nil)] || [details.country isEqualToString:NSLocalizedString(@"United States", nil)]){
        _pvState.hidden = NO;
        [_btState setSelected:NO];
        self.selectedState = details.stateValue;
        [_btState setTitle:details.stateValue forState:UIControlStateNormal];
    }else{
        _pvState.hidden = YES;
    }
    
    [self displayPaxNumber:details.adulsPax];
    
    //show data to optional View
    [_optionalDetailsView showDetailsView:details];
}


-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_icHotelNameError forKey:[NSNumber numberWithInteger:_icHotelNameError.tag]];
    [dictIconErrorManager setObject:_icCountryError forKey:[NSNumber numberWithInteger:_icCountryError.tag]];
    [dictIconErrorManager setObject:_icCityError forKey:[NSNumber numberWithInteger:_icCityError.tag]];
    for (ErrorToolTip *item in [dictIconErrorManager allValues]) {
        item.delegate = self;
    }
    
}

- (IBAction)actionBookView:(id)sender {
    [self showBookView];
}

- (IBAction)actionRecommendView:(id)sender {
    [self showRecommendView];
}

-(void) showBookView{
    [self addTitleToHeader:NSLocalizedString(@"HOTEL", nil) withColor:HEADER_COLOR];
    _scrollView.hidden = NO;
    _viewRecommend.hidden = YES;
    [_btBook setSelected:YES];
    [_topOrangeUcomingBar setHidden:NO];
    [_bottomArrowUpcoming setHidden:NO];
    
    [_btRecommend setSelected:NO];
    [_topOrangeHistoryBar setHidden:YES];
    [_bottomArrowHistory setHidden:YES];
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) showRecommendView{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
    [self addTitleToHeader:NSLocalizedString(@"HOTEL", nil) withColor:HEADER_COLOR];
    _scrollView.hidden = YES;
    _viewRecommend.hidden = NO;
    [_btBook setSelected:NO];
    [_topOrangeUcomingBar setHidden:YES];
    [_bottomArrowUpcoming setHidden:YES];
    
    [_btRecommend setSelected:YES];
    [_topOrangeHistoryBar setHidden:NO];
    [_bottomArrowHistory setHidden:NO];
    
}

- (void)dealloc
{
    if(wsCreateRequest){
        [wsCreateRequest cancelRequest];
    }
    
    if(wsCreateRecommendRequest){
        [wsCreateRecommendRequest cancelRequest];
    }
    
    if(wsGetRequestDetails){
        [wsGetRequestDetails cancelRequest];
    }
    
    if(wsGetHotelRecommendDetails){
        [wsGetHotelRecommendDetails cancelRequest];
    }
    
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
}

#pragma SetUp Privilege
- (void) hideAndFillPrivilegeToForm{
    NSString* value = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressCountry isValid]){
        value = self.privilegeDetails.addressCountry.Name;
    }
    _viewCountry.hidden = YES;
    self.selectedCountry = value;
    [_btCountry setTitle:value forState:UIControlStateNormal];
    
    value = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressCity isValid]){
        value = self.privilegeDetails.addressCity.Name;
    }
    _viewCity.hidden = YES;
    [_txtCity setText:value];
    self.selectedCity = value;
    
    value = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressState isValid]){
        value = self.privilegeDetails.addressState.Name;
    }
    [_btState setTitle:value forState:UIControlStateNormal];
    self.selectedState = value;
    _pvState.hidden = YES;
    
    _viewHotelName.hidden = YES;
    [_txtEnternHotelName setText:self.privilegeDetails.itemName];
}

- (UIStackView*) parentStack{
    return _stackBookForm;
}


-(void)setTextForViews {
    
    
    
    [_btBook setTitle:NSLocalizedString(@"Book", nil) forState:UIControlStateNormal];
    [_btBook setTitle:NSLocalizedString(@"Book", nil) forState:UIControlStateSelected];
    [_btRecommend setTitle:NSLocalizedString(@"Get Recommendation", nil) forState:UIControlStateNormal];
    [_btRecommend setTitle:NSLocalizedString(@"Get Recommendation", nil) forState:UIControlStateSelected];
    _lbCountry.attributedText = [self setRequireString:NSLocalizedString(@"Country", nil)];
    //[_btCountry setTitle:NSLocalizedString(@"Please select country of preferred hotel", nil) forState:UIControlStateNormal];
    
    
    
    _lbCity.attributedText = [self setRequireString:NSLocalizedString(@"City", nil)];
    _txtCity.placeholder = NSLocalizedString(@"Please enter city of preferred hotel", nil);
    
    _lbState.attributedText = [self setRequireString: NSLocalizedString(@"State", nil)];
    [_btState setTitle:NSLocalizedString(@"Please enter state of preferred hotel", nil) forState:UIControlStateNormal];
    
    _lbHotelName.attributedText = [self setRequireString:NSLocalizedString(@"Hotel name", nil)];
    _txtEnternHotelName.placeholder = NSLocalizedString(@"Please enter name of hotel", nil);
    
    _lbCheckInDate.attributedText = [self setRequireString:NSLocalizedString(@"Check in date", nil)];
    _lbCheckOutDate.attributedText = [self setRequireString:NSLocalizedString(@"Check out date", nil)];
    
   
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_btCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}

@end
