//
//  CityGuideDetailsController.m
//  ALC
//
//  Created by Anh Tran on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CityGuideDetailsController.h"
#import "UIImageView+AFNetworking.h"
#import "CityGuideObject.h"
#import "BaseResponseObject.h"
#import "DCRItemSearchController.h"

#import "HotelRequestController.h"

enum CATEGORY_INDEX
{
    
    CAT_EAT = 1,
    CAT_PLAY = 2,
    CAT_SHOP = 3,
    CAT_STAY = 4,
};

@interface CategoryItem : NSObject
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* imageURL;
@property (nonatomic)  enum CATEGORY_INDEX categoryIndex;
@end

@implementation CategoryItem

-(id) initWithType:(enum CATEGORY_INDEX)type title:(NSString*)title andImage:(NSString*) image{
    self = [super init];
    if(self){
        self.title = title;
        self.imageURL = image;
        self.categoryIndex = type;
    }
    return self;
}

@end



@interface CityGuideDetailsController (){
    WSGetCityGuideDetails *wsDetails;
    CityGuideObject *cityGuideObject;
}
@property (strong, nonatomic) IBOutlet UIImageView *imvMainPhoto;
@property (strong, nonatomic) IBOutlet UILabel *lbCityTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbCitySubTitle;
@property (strong, nonatomic) IBOutlet UIButton *btFavourite;
@property (strong, nonatomic) IBOutlet UIStackView *stCategories;
@property (strong, nonatomic) IBOutlet UILabel *lbDescription;

@end

@implementation CityGuideDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleToHeader:NSLocalizedString(@"CITY GUIDES", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    [self getCityGuideDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) getCityGuideDetails{
    [self showToast];
    wsDetails = [[WSGetCityGuideDetails alloc] init];
    wsDetails.delegate = self;
    [wsDetails getCityGuideDetails:_cityGuideId];
}


-(void) showCity:(CityGuideObject *) data{
    cityGuideObject = data;
    
    [self addTitleToHeader:data.cityName withColor:HEADER_COLOR];
    
    [_imvMainPhoto setImageWithURL:[NSURL URLWithString:data.bannerUrl] placeholderImage:nil];
    
    [_lbCityTitle setFont:[UIFont fontWithName:@"AvenirNext-DemiBold" size:FONT_SIZE_23*SCREEN_SCALE]];
    [_lbCityTitle setText:data.title];
    
    [_lbCitySubTitle setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:FONT_SIZE_16*SCREEN_SCALE]];
    [_lbCitySubTitle setText:data.shortDescription];
    
    [_lbDescription setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:FONT_SIZE_16*SCREEN_SCALE]];
    [_lbDescription setText:data.cityDescription];
    
    NSArray *categories = [self createCategoryList];
    CityGuideCategoryView *subView;
    for (CategoryItem *item in categories) {
        subView = [[CityGuideCategoryView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 63)];
        subView.delegate = self;
        [subView showView:item.categoryIndex withTitle:item.title andImage:item.imageURL];
        [_stCategories addArrangedSubview:subView];
    }
    
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_CITY_GUIDE_DETAILS){
        [self showCity:((WSGetCityGuideDetails*)ws).cityDetails];
    }
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result.task == WS_GET_GOURMET_DETAILS){
        //TODO show error
    }
}

-(NSArray*) createCategoryList{
    return [NSArray arrayWithObjects:
            [[CategoryItem alloc] initWithType:CAT_EAT title:@"Eat" andImage:@"eat.png"],
            [[CategoryItem alloc] initWithType:CAT_PLAY title:@"Play" andImage:@"play.png"],
            [[CategoryItem alloc] initWithType:CAT_SHOP title:@"Shop" andImage:@"shop.png"],
            [[CategoryItem alloc] initWithType:CAT_STAY title:@"Stay" andImage:@"stay.png"],
            nil];
}

- (void) clickOnCategory:(NSInteger)index{
    MainViewController* controller;
    
    switch (index) {
        case CAT_EAT:
            controller = [[DCRItemSearchController alloc] init];
            //((GourmetLandingViewController*)controller).filterCityName = cityGuideObject.cityName;
            break;
        
        default:
            break;
    }
    if(controller!=nil){
        [self.navigationController pushViewController:controller animated:YES];
    }
}
@end
