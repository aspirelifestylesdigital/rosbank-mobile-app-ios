//
//  MyPreferenceController.m
//  ALC
//
//  Created by Hai NguyenV on 9/8/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MyPreferenceController.h"

@interface MyPreferenceController ()
@property (weak, nonatomic) IBOutlet UILabel *introductionMsgLbl;
@property (weak, nonatomic) IBOutlet UILabel *transportationLbl;
@property (weak, nonatomic) IBOutlet UILabel *diningLbl;
@property (weak, nonatomic) IBOutlet UILabel *golfLbl;
@property (weak, nonatomic) IBOutlet UILabel *hotelLbl;
@property (weak, nonatomic) IBOutlet UILabel *otherPreferencesLbl;

@end

@implementation MyPreferenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self customButtonBack];
}

-(void) setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"MY PREFERENCES", nil) withColor:HEADER_COLOR];
    self.introductionMsgLbl.text = NSLocalizedString(@"The more we know about you, the better we will be able to provide you with recommendations aligned with your preferences. Please take a few minutes to fill out this profile. This information will help us ensure that we provide a more personalised interaction each time we assist you.", @"");
    self.transportationLbl.text = NSLocalizedString(@"Transportation", @"");
    self.diningLbl.text = NSLocalizedString(@"Dining", @"");
    self.golfLbl.text = NSLocalizedString(@"Golf", @"");
    self.hotelLbl.text = NSLocalizedString(@"Hotel", @"");
    self.otherPreferencesLbl.text = NSLocalizedString(@"Other Preferences", @"");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionHotel:(id)sender {
}

- (IBAction)actinDinning:(id)sender {
}

- (IBAction)actionCarRetal:(id)sender {
}

- (IBAction)actionOther:(id)sender {
}
@end
