//
//  ThankYouBookingController.h
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"

@interface ThankYouBookingController : MainViewController
@property (nonatomic, strong) NSString* thankYouMessage;
@property (nonatomic, strong) NSString* subHeaderMessage;
@end
