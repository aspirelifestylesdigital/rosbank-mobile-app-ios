//
//  OrthersBookingController.m
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "OrthersBookingController.h"
#import "ThankYouBookingController.h"
#import "OrtherRequestDetailsObj.h"

@interface OrthersBookingController (){
    WSCreateOrthersRequest* wsCreateRequest;
    WSGetOtherRequestDetails* wsGetOrthersDetails;
    WSGetMyPreferences* wsGetMyPreference;
    
    //These are used for Privilege spa
    NSString* Country;
    NSString* City;
    NSString* State;
    NSString* spaNameFromPrivilege;
    
}
@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lbOtherMessage;
@property (strong, nonatomic) IBOutlet UITextView *txtorthersMessage;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;
@property (strong, nonatomic) IBOutlet UIStackView *stackBookForm;

- (IBAction)actionSubmitRequest:(id)sender;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icTextError;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;

@end

@implementation OrthersBookingController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getRequestDetails];
    
}

-(void) setUpSubView{
    // Do any additional setup after loading the view.
    [self addTitleToHeader:NSLocalizedString(@"OTHER REQUEST", nil) withColor:HEADER_COLOR];

    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
    _optionalDetailsView.delegate = self;
    [_optionalDetailsView applyRequestType:OTHER_REQUEST];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];

}

-(void) getRequestDetails{
    if(_bookingId){
        [self showToast];
        [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
        
        wsGetOrthersDetails = [[WSGetOtherRequestDetails alloc] init];
        wsGetOrthersDetails.delegate = self;
        [wsGetOrthersDetails getRequestDetails:_bookingId];
    }
//    else{
//        [self showToast];
//        wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//        wsGetMyPreference.delegate = self;
//        [wsGetMyPreference getMyPreference];
//    }
}

- (void) hideTopHeader{
}

- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}
-(void) closeAllKeyboardAndPicker{
    [_txtorthersMessage resignFirstResponder];
}
#pragma mark ------ Optional Detail Delegate ------------------
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
    
}
-(void) touchinDetailsView{
    [self closeAllKeyboardAndPicker];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _icTextError.hidden = YES;
    [self updateScrollViewToSelectedView:textView];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    _icTextError.hidden = YES;
    return YES;
}

-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + 90 - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)actionSubmitRequest:(id)sender{
    [sender setUserInteractionEnabled:false];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
        [sender setUserInteractionEnabled:true];
    });
    OrthersRequestObj* object = [self validateAndCollectData];
    if(object){
        // send request
        
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Others",nil);
        
        [self showToast];
        wsCreateRequest = [[WSCreateOrthersRequest alloc] init];
        wsCreateRequest.delegate = self;
        [wsCreateRequest bookingOrthers:object];
    } else {
        //showAlertOneButton(self,@"Warning", @"Please input correct values.", @"OK");
    }
}
-(OrthersRequestObj*) validateAndCollectData{
    
    [_optionalDetailsView exportValues];
    
    OrthersRequestObj* result = (OrthersRequestObj*)[self addPrivilegeData:[[OrthersRequestObj alloc] init]];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.BookingId = _bookingId;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    value = _txtorthersMessage.text;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.ortherString = value;
            _icTextError.hidden = YES;
        } else {
            isEnoughInfo = NO;
            _icTextError.hidden = NO;
            _icTextError.errorMessage = INCLUDE_UNSUPPORT_CHAR_ERR;
        }
    } else {
        if(value.length==0){
            _icTextError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
        _icTextError.hidden = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.MobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.EmailAddress = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    // Add Spa Name if has Privilege details
    if(isValidValue(spaNameFromPrivilege)){
        result.spaName = spaNameFromPrivilege;
        result.Country = Country;
        result.City = City;
        result.State = State;
    }
    
    if(isEnoughInfo){
        return result;
    } else {
        OrthersRequestObj *temp;
        return temp;
    }
}

#pragma mark ------ API Services Delegate ------------------
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_BOOKING_ORTHERS){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_A_REQUEST_IS_AMENDED
         object:self];
        
         if(_bookingId==nil){
            if(ws.task == WS_BOOKING_ORTHERS){
                [self trackEcommerceProduct:@"Generic - Book" withCate:@"Other"];
            }
        }
        
        [self openThankYouPage];
    } else if (ws.task == WS_GET_OTHER_REQUEST){
        if(wsGetOrthersDetails.bookingDetails){
            //show data
            [self showBaseBookingDetails:wsGetOrthersDetails.bookingDetails];
        }
    }else if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Other];
        _txtorthersMessage.text = [dict stringForKey:VALUE];
    }
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result.task == WS_BOOKING_ORTHERS){
        if(errorCode !=0)
            displayError(self, errorCode);
        else
        {
            showAlertOneButton(self, ERROR_ALERT_TITLE,  result.message, @"OK");
        }
    }
}

-(void) showBookingDetails:(OrtherRequestDetailsObj*) details{
    _txtorthersMessage.text = details.ortherString;
    
    spaNameFromPrivilege = details.spaName;
    Country = details.country;
    City = details.city;
    State = details.stateValue;
    
    //show data to optional View
    [_optionalDetailsView showDetailsView:details];
}

- (void)dealloc
{
    if(wsGetOrthersDetails){
        [wsGetOrthersDetails cancelRequest];
    }
    
    if(wsCreateRequest){
        [wsCreateRequest cancelRequest];
    }
    
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
}

#pragma SetUp Privilege
- (void) hideAndFillPrivilegeToForm{
    spaNameFromPrivilege = self.privilegeDetails.itemName;
    
    Country = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressCountry isValid]){
        Country = self.privilegeDetails.addressCountry.Name;
    }
    
    City = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressCity isValid]){
        City = self.privilegeDetails.addressCity.Name;
    }
    
    State = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressState isValid]){
        State = self.privilegeDetails.addressState.Name;
    }
}

- (UIStackView*) parentStack{
    return _stackBookForm;
}

-(void)setTextForViews {
    _lbOtherMessage.attributedText = [self setRequireString:NSLocalizedString(@"What can we do for you?", nil)];
    [_btCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
}

-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}

@end
