//
//  GolfPreferenceController.m
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GolfPreferenceController.h"
#import "BaseResponseObject.h"

@interface GolfPreferenceController ()

@end

@implementation GolfPreferenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
//    [self showToast];
//    wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//    wsGetMyPreference.delegate = self;
//    [wsGetMyPreference getMyPreference];
}

-(void) setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"MY PREFERENCES", nil) withColor:HEADER_COLOR];
    self.golfTitleLbl.text = NSLocalizedString(@"Golf", @"");
    self.golfCourseLbl.text = NSLocalizedString(@"Preferred golf course", @"");
    self.tfCourseName.placeholder = NSLocalizedString(@"Please enter golf course", @"");
    self.teetimesLbl.text = NSLocalizedString(@"Preferred tee times", @"");
    [self.btnTee setTitle:NSLocalizedString(@"Please choose preferred tee times", @"") forState:UIControlStateNormal];
    [self.saveBtn setTitle:NSLocalizedString(@"SAVE", @"") forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture{
    [_tfCourseName resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionTee:(id)sender {
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = TEE;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.selectedValues = [[NSMutableArray alloc] initWithArray:selectedTee];;
    controller.selectedKey =[[NSMutableArray alloc] initWithArray:selectedTeeKey];
    [self.navigationController pushViewController:controller animated:YES];
}
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    selectedTee = values;
    selectedTeeKey = key;
    [self.btnTee setTitle:convertArrayToString(values) forState:UIControlStateNormal];
    [self.btnTee setSelected:NO];
    if(self.btnTee.currentTitle.length == 0){
        [self.btnTee setSelected:YES];
        [self.btnTee setTitle:NSLocalizedString(@"Please choose preferred tee times", nil) forState:UIControlStateNormal];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)actionSave:(id)sender {
    NSString* ortherCuisine = self.tfCourseName.text;
    NSString* handicap = self.tfHandicap.text;
        [self showToast];
        wsCreateGolf = [[WSCreateGoltPre alloc] init];
        wsCreateGolf.delegate = self;
        wsCreateGolf.courseName = ortherCuisine;
        wsCreateGolf.arrTeeTimes = selectedTeeKey;
        wsCreateGolf.preferenceID = _preferenceID;
    wsCreateGolf.handicap = [handicap stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [wsCreateGolf createGolf];

}

- (IBAction)actionCancel:(id)sender {
     [self.navigationController popViewControllerAnimated:true];
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Golf];
        
        NSString* cuisine = [dict stringForKey:VALUE1];
        NSString* other = [dict stringForKey:VALUE];
        NSString* handicap = [dict stringForKey:VALUE2];
        
        if(handicap.length>0){
            _tfHandicap.text = handicap;
            ballersNumber = [handicap integerValue];
        }
        self.tfCourseName.text = other;
        _preferenceID = [dict stringForKey:MYPREFERENCESID];
        
        NSArray* arr= convertStringToArray(cuisine);
        if(arr.count>0 && cuisine.length>0){
            selectedTee = [[NSMutableArray alloc] initWithArray:arr];
            selectedTeeKey = [[NSMutableArray alloc] initWithArray:arr];
            
            [self.btnTee setTitle:convertArrayToString(selectedTee) forState:UIControlStateNormal];
            [self.btnTee setSelected:NO];
        }
        
    }else if(ws.task == WS_CREATE_GOLTPRE){
        [self stopToast];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(errorCode == 600){
        return;
    }
    if(result!=nil && result.message!=nil){
        showAlertOneButton(self,@"", result.message, @"OK");
    } else {
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_API_MSG, @"OK");
    }
}
- (IBAction)actionDecreaseBallers:(id)sender {
    if(ballersNumber>0){
        [self displayballersNumber:ballersNumber-1];
    }
}
- (IBAction)actionIncreaseBallers:(id)sender {
    [self displayballersNumber:ballersNumber+1];
}
-(void) displayballersNumber:(NSInteger) pax{
    ballersNumber = pax;
    _tfHandicap.text = [NSString stringWithFormat:@"%ld ", ballersNumber];
}
- (void)dealloc
{
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
    if(wsCreateGolf){
        [wsCreateGolf cancelRequest];
    }
}
@end
