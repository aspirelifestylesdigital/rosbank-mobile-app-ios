//
//  AboutDetailsController.m
//  ALC
//
//  Created by Anh Tran on 9/21/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "AboutDetailsController.h"
#import "ALCQC-Swift.h"

@interface AboutDetailsController ()
@property (strong, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbWelcome;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbVersionInfo;
@property (weak, nonatomic) IBOutlet UILabel *lbVersion;
@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;

@end

@implementation AboutDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
    [self addTitleToHeader:NSLocalizedString(@"ABOUT", nil) withColor:HEADER_COLOR];
    self.imgLogo.image = [AppConfig shared].companyDarkLogo;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setTextForViews {
    _lbWelcome.text = NSLocalizedString(@"WELCOME", nil);
    _lbTitle.text = NSLocalizedString(@"Make every experience a story worth telling", nil);
    _lbDescription.text = NSLocalizedString(@"At Aspire Lifestyles, we believe every individual has their own interpretation of \"extraordinary\" and a unique idea of their \"ultimate\" experience. That's why we're here to help design extraordinary interactions and deliver the ultimate to inspire and delight.\nFrom everyday requests to the most extraordinary wishes, Aspire Lifestyles aims to leave you with a story you can't wait to share with your loved ones. So wherever you find yourself in the world, you can rely on our global team to make the exceptional happen without exception.", nil);
    _lbVersionInfo.text = NSLocalizedString(@"VERSION", nil);
}

@end
