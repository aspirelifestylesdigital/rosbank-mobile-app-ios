//
//  InAppBrowserViewController.m
//  ALC
//
//  Created by Nhat Huy Truong  on 9/15/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "InAppBrowserViewController.h"

@interface InAppBrowserViewController ()

@end

@implementation InAppBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isLiveChat)
    {
        [self addTitleToHeader:NSLocalizedString(@"LIVE CHAT", nil) withColor:HEADER_COLOR];
        self.urlBooking = @"https://lpcdn.lpsnmedia.net/le_unified_window/8.9.0.0-release_2605/index.html?lpUnifiedWindowConfig=%7B%22accountId%22%3A%2252075030%22%2C%22env%22%3A%22prod%22%2C%22external%22%3Atrue%2C%22supportBlockCCPattern%22%3Afalse%2C%22engConf%22%3A%7B%22async%22%3Afalse%2C%22scid%22%3A%2244%22%2C%22cid%22%3A338340451%2C%22eid%22%3A639676451%2C%22lang%22%3A%22ru-RU%22%2C%22svid%22%3A%22Y1YTYxNWMxY2Y3MWI1MzJi%22%2C%22ssid%22%3A%229hKRmWUAR9OorDSJ862yEA%22%2C%22lewid%22%3A639706251%7D%7D";
        
    } else if(_headerTitle){
         [self addTitleToHeader:_headerTitle withColor:HEADER_COLOR];
    }
    else
    {
        [self addTitleToHeader:NSLocalizedString(@"BOOKING", nil) withColor:HEADER_COLOR];
    }
    
    [self customButtonBack];
    
    [self showToast];
    [self.wbShowContent loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlBooking]]];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL) isShowConciergeButton
{
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - WebView Controller

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopToast];
}

//- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
//{
//    [self stopToast];
//}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
   
    NSString *requestString = [[[request URL] absoluteString] stringByRemovingPercentEncoding];
    NSLog(@"%@",requestString);
    
    if ([requestString containsString:@"snapwidget.com"]) {
        return NO;
    }
    return YES;
    
}

- (void) buttonBack{
    if(![_wbShowContent canGoBack]){
        [super buttonBack];
    } else {
        [_wbShowContent goBack];
    }
}

@end
