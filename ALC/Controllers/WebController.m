//
//  WebController.m
//  ALC
//
//  Created by Anh Tran on 9/9/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WebController.h"
#import "StoreUserData.h"
#import "UserObject.h"

@interface WebController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation WebController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(!_pageTitle){
        [self addTitleToHeader:NSLocalizedString(@"WEB", nil) withColor:HEADER_COLOR];
    } else {
        [self addTitleToHeader:_pageTitle withColor:HEADER_COLOR];
    }
    [self customButtonBack];
    
    if(self.urlWebsite  !=nil){
        [self showToast];
        [_webView  loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlWebsite]]];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    self.navigationController.navigationBar.hidden = NO;
}
-(BOOL)isShowConciergeButton{
    if(isUserLoggedIn())
        return YES;
    else
        return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma  mark webview
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self stopToast];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopToast];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
   // [self showToast];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        
        NSURL *url = request.URL;
        if ([[url absoluteString] hasPrefix:@"http"]) {
            WebController* controller =  (WebController*)[self controllerByIdentifier:@"WebController"];
            controller.urlWebsite = [url absoluteString];
            controller.pageTitle = NSLocalizedString(@"PRIVACY POLICY", nil);
            [self.navigationController pushViewController:controller animated:YES];
            return NO;
        }
        
    }
    
    return YES;
    
}

@end
