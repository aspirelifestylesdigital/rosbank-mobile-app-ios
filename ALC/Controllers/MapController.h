//
//  MapController.h
//  ALC
//
//  Created by Anh Tran on 9/8/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "DCRItemDetails.h"

@interface MapController : MainViewController
@property (strong, nonatomic) DCRItemDetails* addressObject;
@end
