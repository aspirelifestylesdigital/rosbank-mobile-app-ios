//
//  EventRequestController.m
//  ALC
//
//  Created by Hai NguyenV on 11/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "EventRequestController.h"
#import "ThankYouBookingController.h"
#import "WSGetEntertainmentRequestDetails.h"
#import "ALCQC-Swift.h"

#define COUNTRY_HINT_TEXT NSLocalizedString(@"Please select country of preferred event", nil)
#define CITY_HINT_TEXT NSLocalizedString(@"Please enter city of preferred event", nil)
#define EVENT_HINT_TEXT NSLocalizedString(@"Please select event category", nil)
#define STATE_HINT_TEXT NSLocalizedString(@"Please enter state of preferred event", nil)
@interface EventRequestController (){
    WSGetEntertainmentRequestDetails* wsGetRequestDetails;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topBookSpaceToSuperView;
@property (strong, nonatomic) IBOutlet UIStackView *topNavigator;
@property (strong, nonatomic) IBOutlet UIView *viewContent;




@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbEventName;
@property (weak, nonatomic) IBOutlet UILabel *lbEventCategory;

@property (weak, nonatomic) IBOutlet UILabel *lbEventTime;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;

@end

@implementation EventRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTitleToHeader:NSLocalizedString(@"ENTERTAINMENT", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    [_txtCity setPlaceholder:CITY_HINT_TEXT];
    selectedCountry = NSLocalizedString(@"Russia", nil);
    [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    selectedCountryRecom = NSLocalizedString(@"Russia", nil);
    _pvState.hidden = YES;
    [_btCountry setSelected:NO];
    [_btCountry setTitle:selectedCountry forState:UIControlStateNormal];
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    
    _txtReservationDate.inputAccessoryView = toolbar;
    _txtReservationTime.inputAccessoryView = toolbar;
    
    datepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerReservation setDatePickerMode:UIDatePickerModeDate];
    datepickerReservation.datePickerMode = UIDatePickerModeDate;
    [datepickerReservation setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [datepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerReservation.tag = _txtReservationDate.tag*10;
    _txtReservationDate.inputView = datepickerReservation;
    _txtReservationDate.delegate = self;
    datepickerReservation.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    //end
    
    //Check out date picker
    timepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [timepickerReservation setDatePickerMode:UIDatePickerModeTime];
    timepickerReservation.datePickerMode = UIDatePickerModeTime;
    // [timepickerReservation setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [timepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    timepickerReservation.tag = _txtReservationTime.tag*10;
    _txtReservationTime.inputView = timepickerReservation;
    _txtReservationTime.delegate = self;
    timepickerReservation.minuteInterval = 5;
    timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*10];
    _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    
    _optionalDetailsView.delegate = self;
    [_optionalDetailsView applyRequestType:ENTERTAINMENT_REQUEST];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    if(_requestType == ENTERTAINMENT_RECOMMEND_REQUEST){
        [self showRecommendView];
    } else {
        [self showBookView];
    }
    
    if(_hideTopNavigator || _bookingId){
        if(_bookingId){
            [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
        }
        _topNavigator.hidden = YES;
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        _topBookSpaceToSuperView.constant = -_viewContent.frame.origin.y;
    }
    
    [self getRequestDetails];
    [self setTextForViews];
    
}

-(void) dateTextField:(id)sender
{
    if(((UIView*)sender).tag == _txtReservationDate.tag*10){
        _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    } else if(((UIView*)sender).tag == _txtReservationTime.tag*10){
        _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    }
    
    if(!isValidDateTimeWithin24h(datepickerReservation.date, timepickerReservation.date)){
        _icTimeError.hidden = NO;
        timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1 + 60*5];
        _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    }
}

-(NSString*)getTimebyDate:(NSDate*)pdate{
    NSString* time = @"";
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    [outputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *temp = [outputFormatter stringFromDate:pdate];
    
    NSDate *date = [outputFormatter dateFromString:temp];
    
    outputFormatter.dateFormat = @"hh:mm a";
    time = [outputFormatter stringFromDate:date];
    
    return  time;
}
- (void)dismiss
{
    [self closeAllKeyboardAndPicker];
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}
-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}
-(void) closeAllKeyboardAndPicker{
    [_txtCity resignFirstResponder];
    [_txtEventName resignFirstResponder];
    [_txtReservationDate endEditing:YES];
    [_txtReservationTime endEditing:YES];
}

#pragma mark ---------- UITextField Delegate --------------------
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if([self.view viewWithTag:textField.tag+1]){
        if([[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextView class]])
            [(UITextView*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        else if( [[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextField class]]){
            [(UITextField*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        }
        else{
            [textField resignFirstResponder];
        }
    } else {
        [textField resignFirstResponder];
    }
    
    [self closeAllKeyboardAndPicker];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return  YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self updateScrollViewToSelectedView:textField];
    
    UIView *errorIcon = [self.view viewWithTag:100+textField.tag];
    if(errorIcon!=nil){
        [((ErrorToolTip*) errorIcon) setErrorHidden:YES];
    }
}
-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + 90 - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}

#pragma mark ------ Optional Detail Delegate ------------------
-(void) actionOptionalDetailsOpen:(BOOL) isOpened{
    
}
-(void) textFieldBeginEdit:(UIView*) textField{
    [self updateScrollViewToSelectedView:textField];
}
-(void) touchinDetailsView{
    [self closeAllKeyboardAndPicker];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionCoutryClicked:(id)sender {
    [_icCountryError setHidden:YES];
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = COUNTRY;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btCountry.currentTitle, nil] ;
    if(_requestType == ENTERTAINMENT_REQUEST)
    controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedCountry, nil];
    else
        controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedCountryRecom, nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)actionStateClicked:(id)sender {
    [_icStateError setHidden:YES];
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = STATE;
    controller.delegate = self;
    controller.maxCount = 1;
    if(_requestType == ENTERTAINMENT_REQUEST){
        if([selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
            controller.countryType = USA;
        }else if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)]){
            controller.countryType = CANADA;
        }
        controller.selectedValues =[[NSMutableArray alloc] initWithObjects:selectedState, nil] ;
        controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedState, nil];
    }else{
        if([selectedCountryRecom isEqualToString:NSLocalizedString(@"United States", nil)]){
            controller.countryType = USA;
        }else if([selectedCountryRecom isEqualToString:NSLocalizedString(@"Canada", nil)]){
            controller.countryType = CANADA;
        }
        controller.selectedValues =[[NSMutableArray alloc] initWithObjects:selectedStateRe, nil] ;
        controller.selectedKey =[[NSMutableArray alloc] initWithObjects:selectedStateRe, nil];
    }
    
    
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)actionEventClicked:(id)sender {
    [_icEventCategoryError setHidden:YES];
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = EVENT_CATEGORY;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.selectedValues =selectedEventCategory;
    controller.selectedKey =selectedEventCategoryKey;
    [self.navigationController pushViewController:controller animated:YES];
}
//Pick sub filter delegate
#pragma mark ---------- Sub Filter Delegate --------------------
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(key && values && key.count>0 && values.count>0){
        if(type == COUNTRY){
            //_btCity.enabled = YES;
            NSString* newCountry = [key objectAtIndex:0];
            if([newCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [newCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
                _pvState.hidden = NO;
            }else{
                _pvState.hidden = YES;
            }
            
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
            
            if(_requestType == ENTERTAINMENT_REQUEST){
            selectedCountry = newCountry;
                selectedState = nil;
            }
            else{
                selectedCountryRecom = newCountry;
                selectedStateRe = nil;
            }
            [_btCountry setSelected:NO];
            [_btCountry setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        } else if(type == STATE){
            NSString* newState = [key objectAtIndex:0];
            if(_requestType == ENTERTAINMENT_REQUEST){
                selectedState = newState;
            }else{
                selectedStateRe = newState;
            }
            [_btState setSelected:NO];
            [_btState setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        } else if(type == EVENT_CATEGORY){
            selectedEventCategory = values;
            selectedEventCategoryKey = key;
            [_btEventCategory setSelected:NO];
            [_btEventCategory setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        }
    } else {
        if(type == COUNTRY){
            if(_requestType == ENTERTAINMENT_REQUEST)
            selectedCountry = nil;
            else
                selectedCountryRecom = nil;
            [_btCountry setSelected:YES];
            [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
        }else if(type == STATE){
            if(_requestType == ENTERTAINMENT_REQUEST){
                selectedState = nil;
            }else{
                selectedStateRe = nil;
            }
            
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
        }if(type == EVENT_CATEGORY){
            selectedEventCategory = nil;
            selectedEventCategoryKey = nil;
            [_btEventCategory setSelected:YES];
            [_btEventCategory setTitle:EVENT_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}
#pragma mark ------Button Header Action Handle ---------
- (IBAction)actionBookView:(id)sender {
    [self showBookView];
}

- (IBAction)actionRecommendView:(id)sender {
    [self showRecommendView];
}

-(void) showBookView{
    _requestType = ENTERTAINMENT_REQUEST;
    [self addTitleToHeader:NSLocalizedString(@"ENTERTAINMENT", nil) withColor:HEADER_COLOR];
    _viewEventCategory.hidden = YES;
    _viewEventTime.hidden = NO;
    _pContraintEventDate.constant = 0;
    _viewEventName.hidden = NO;
    
    _icCityError.hidden = YES;
    _icTimeError.hidden = YES;
    _icCountryError.hidden = YES;
    _icEventNameError.hidden = YES;
    _icEventCategoryError.hidden = YES;
    
    [_btBook setSelected:YES];
    [_topOrangeUcomingBar setHidden:NO];
    [_bottomArrowUpcoming setHidden:NO];
    
    [_btRecommend setSelected:NO];
    [_topOrangeHistoryBar setHidden:YES];
    [_bottomArrowHistory setHidden:YES];
    
    selectedCityRe = _txtCity.text;
    _txtCity.text = selectedCity;
    
    if (selectedCountry == nil) {
        [_btCountry setSelected:YES];
        [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
    } else {
        [_btCountry setSelected:NO];
        [_btCountry setTitle:selectedCountry forState:UIControlStateNormal];
    }
    
    
    
    if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        _pvState.hidden = NO;
    }else{
        _pvState.hidden = YES;
    }
    
    if(selectedState){
        [_btState setSelected:NO];
        [_btState setTitle:selectedState forState:UIControlStateNormal];
    }else{
        [_btState setSelected:YES];
        [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    }
    
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) showRecommendView{
    _requestType = ENTERTAINMENT_RECOMMEND_REQUEST;
    [self addTitleToHeader:NSLocalizedString(@"ENTERTAINMENT", nil) withColor:HEADER_COLOR];
    _viewEventCategory.hidden = NO;
    _viewEventTime.hidden = YES;
    _pContraintEventDate.constant = 15;
    _viewEventName.hidden = YES;
    
    _icCityError.hidden = YES;
    _icTimeError.hidden = YES;
    _icCountryError.hidden = YES;
    _icEventNameError.hidden = YES;
    _icEventCategoryError.hidden = YES;
    
    [_btBook setSelected:NO];
    [_topOrangeUcomingBar setHidden:YES];
    [_bottomArrowUpcoming setHidden:YES];
    
    [_btRecommend setSelected:YES];
    [_topOrangeHistoryBar setHidden:NO];
    [_bottomArrowHistory setHidden:NO];
    
    selectedCity = _txtCity.text;
    _txtCity.text = selectedCityRe;
    
    
    if (selectedCountryRecom == nil) {
        [_btCountry setSelected:YES];
        [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
    } else {
        [_btCountry setSelected:NO];
        [_btCountry setTitle:selectedCountryRecom forState:UIControlStateNormal];
    }
        
    if([selectedCountryRecom isEqualToString:NSLocalizedString(@"Canada", nil)] || [selectedCountryRecom isEqualToString:NSLocalizedString(@"United States", nil)]){
        _pvState.hidden = NO;
    }else{
        _pvState.hidden = YES;
    }
    
    if(selectedStateRe){
        [_btState setSelected:NO];
        [_btState setTitle:selectedStateRe forState:UIControlStateNormal];
    }else{
        [_btState setSelected:YES];
        [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    }
    
    
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_icTimeError forKey:[NSNumber numberWithInteger:_icTimeError.tag]];
    [dictIconErrorManager setObject:_icCountryError forKey:[NSNumber numberWithInteger:_icCountryError.tag]];
    [dictIconErrorManager setObject:_icCityError forKey:[NSNumber numberWithInteger:_icCityError.tag]];
    for (UIView *item in [dictIconErrorManager allValues]) {
        if([item isKindOfClass:[ErrorToolTip class]]){
            ((ErrorToolTip*)item).delegate = self;
        }
    }
    
}

- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            if([[dictIconErrorManager objectForKey:icTag] isKindOfClass:[ErrorToolTip class]]){
                [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
            }
        }
    }
}

#pragma mark ------ Action Submit Request/Recommend ------------------
- (IBAction)actionSubmitRequest:(id)sender{
    [sender setUserInteractionEnabled:false];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
        [sender setUserInteractionEnabled:true];
    });
    if(_requestType == ENTERTAINMENT_RECOMMEND_REQUEST){
        EventRecommendRequestObj* object = [self validateAndCollectDataRecommend];
        
        if(object){
            // send request
            trackGAICategory(@"Concierge Request",@"Send concierge request",@"Event Recommend",nil);
            
            [self showToast];
            wsCreateRecommend = [[WSCreateRecommentRequest alloc] init];
            wsCreateRecommend.delegate = self;
            [wsCreateRecommend bookingEvent:object];
        }
    }else{
        EventRequestObj* object = [self validateAndCollectData];
        
        if(object){
            // send request
            trackGAICategory(@"Concierge Request",@"Send concierge request",@"Event",nil);
            
            [self showToast];
            wsCreateRequest = [[WSCreateEventRequest alloc] init];
            wsCreateRequest.delegate = self;
            [wsCreateRequest bookingEvent:object];
        }
    }
    
}
-(EventRequestObj*) validateAndCollectData{
    for (UIView *item in [dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }
    
    [_optionalDetailsView exportValues];
    
    EventRequestObj* result = [[EventRequestObj alloc] init];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.BookingId = _bookingId;
        result.isEdit = YES;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    value = _txtEventName.text;
    if(isValidValue(value)){
        result.EventName = value;
    } else {
        _icEventNameError.hidden = NO;
        if(value.length==0){
            _icEventNameError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.MobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.EmailAddress = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    result.reservationDate = formatDate(datepickerReservation.date, DATE_SEND_REQUEST_FORMAT);
    result.reservationTime = formatDate(timepickerReservation.date, TIME_SEND_REQUEST_FORMAT);
    if (!isValidDateTimeWithin24h(datepickerReservation.date, timepickerReservation.date)) {
        _icTimeError.hidden = NO;
        isEnoughInfo = NO;
    }
    
    result.adultPax = _optionalDetailsView.normalPax;
    
    value = _txtCity.text;
    if(isValidValue(value)){
        result.City = value;
    } else {
        result.City = @"";
        isEnoughInfo = NO;
        _icCityError.hidden = NO;
        if(value.length==0){
            _icCityError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    value = selectedCountry;
    if(isValidValue(value)){
        result.Country = value;
    } else {
        isEnoughInfo = NO;
        _icCountryError.hidden = NO;
        if(value.length==0){
            _icCountryError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    if([selectedCountry isEqualToString:NSLocalizedString(@"Canada", nil)] || [selectedCountry isEqualToString:NSLocalizedString(@"United States", nil)]){
        value = selectedState;
        if(isValidValue(value)){
            result.State = value;
        }else{
            isEnoughInfo = NO;
            if(value.length==0){
                _icStateError.errorMessage = MANDATORY_BLANK;
            }
            [_icStateError setHidden:NO];
        }
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.SpecialRequirements = value;
        } else {
            isEnoughInfo = NO;
        }
    }
    
    if(isEnoughInfo){
        return result;
    } else {
        EventRequestObj *temp;
        return temp;
    }
}
-(EventRecommendRequestObj*) validateAndCollectDataRecommend{
    for (UIView *item in [dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }
    
    [_optionalDetailsView exportValues];
    
    EventRecommendRequestObj* result = [[EventRecommendRequestObj alloc] init];
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.BookingId = _bookingId;
        result.isEdit = YES;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    value = convertArrayToString(selectedEventCategory);
    if(isValidValue(value)){
        result.EventCategory = value;
    } else {
        _icEventCategoryError.hidden = NO;
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.MobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.EmailAddress = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    result.reservationDate = formatDate(datepickerReservation.date, DATETIME_SEND_REQUEST_FORMAT);
    //Recommendation dont have time
    /*if (!isValidDateTimeWithin24h(datepickerReservation.date, timepickerReservation.date)) {
        _icTimeError.hidden = NO;
        isEnoughInfo = NO;
    }*/
    
    result.adultPax = _optionalDetailsView.normalPax;
    
    value = _txtCity.text;
    if(isValidValue(value)){
        result.City = value;
    } else {
        isEnoughInfo = NO;
        _icCityError.hidden = NO;
        if(value.length==0){
            _icCityError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    value = selectedCountryRecom;
    if(isValidValue(value)){
        result.Country = value;
    } else {
        isEnoughInfo = NO;
        _icCountryError.hidden = NO;
    }
    
    if([selectedCountryRecom isEqualToString:NSLocalizedString(@"Canada", nil)] || [selectedCountryRecom isEqualToString:NSLocalizedString(@"United States", nil)]){
        value = selectedStateRe;
        if(isValidValue(value)){
            result.State = value;
        }else{
            isEnoughInfo = NO;
            if(value.length==0){
                _icStateError.errorMessage = MANDATORY_BLANK;
            }
            [_icStateError setHidden:NO];
        }
    }
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        result.SpecialRequirements = value;
    }
    
    if(isEnoughInfo){
        return result;
    } else {
        EventRecommendRequestObj *temp;
        return temp;
    }
}

#pragma mark ------ API Services Delegate ------------------
-(void)loadDataDoneFrom:(WSBase*)ws{
    [self stopToast];
    if(ws.task == WS_BOOKING_ENTERTAINMENT || ws.task == WS_BOOKING_ENTERTAINMENT_RECOMMEND){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_A_REQUEST_IS_AMENDED
         object:self];
        
        [self.navigationController popViewControllerAnimated:NO];
        
        if(_bookingId==nil){
            if(ws.task == WS_BOOKING_ENTERTAINMENT){
                [self trackEcommerceProduct:@"Generic - Book" withCate:@"Event"];
            }else if(ws.task == WS_BOOKING_ENTERTAINMENT_RECOMMEND){
                [self trackEcommerceProduct:@"Generic - Recommend" withCate:@"Recommend Event"];
            }
        }
        
        ThankYouBookingController* controller = (ThankYouBookingController*) [self controllerByIdentifier:@"ThankYouBooking"];
        controller.thankYouMessage = [BOOKING_THANK_YOU_MSG stringByReplacingOccurrencesOfString:@"PHONE_NUMBER_PLACEHOLDER" withString:[AppConfig shared].companyPhoneNumber];
        [appdelegate.rootVC pushViewController:controller animated:YES];
    } else if ((ws.task == WS_GET_BOOKING_ENTERTAINMENT)
               || (ws.task == WS_GET_ENTERTAINMENT_RECOMMEND)){
        if(wsGetRequestDetails.bookingDetails){
            //show data
            [self showBookingDetails:wsGetRequestDetails.bookingDetails];
        }
    }
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result.task == WS_BOOKING_ENTERTAINMENT || result.task == WS_BOOKING_ENTERTAINMENT_RECOMMEND){
        if(errorCode !=0)
            displayError(self, errorCode);
        else
        {
            showAlertOneButton(self,ERROR_ALERT_TITLE, result.message, @"OK");
        }
    }
}


-(void) showBookingDetails:(EntertainmentRequestDetailsObject*) details{
    _txtEventName.text = details.eventName;
    datepickerReservation.date = details.eventDateTime;
    _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    
    //[_btCity setTitle:details.city forState:UIControlStateNormal];
    selectedCountry = details.country;
    selectedCity = details.city;
    _txtCity.text = details.city;
    
    
    if([details.country isEqualToString:NSLocalizedString(@"Canada", nil)] || [details.country isEqualToString:NSLocalizedString(@"United States", nil)]){
        _pvState.hidden = NO;
        [_btState setSelected:NO];
        selectedState = details.stateValue;
        selectedStateRe = details.stateValue;
        
        [_btState setTitle:details.stateValue forState:UIControlStateNormal];
    }else{
        _pvState.hidden = YES;
    }
    
    //show data to optional View
    [_optionalDetailsView showDetailsView:details];
    
    if(_requestType == ENTERTAINMENT_REQUEST){
        selectedCountry = details.country;
        [_btCountry setSelected:NO];
        [_btCountry setTitle:(details.countryValue ? details.countryValue : selectedCountry) forState:UIControlStateNormal];
        
        timepickerReservation.date = details.eventDateTime;
        _txtReservationTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerReservation.date, TIME_FORMAT)];
    } else if(_requestType == ENTERTAINMENT_RECOMMEND_REQUEST && details.eventCategory){
        
        selectedCountryRecom = details.country;
        [_btCountry setSelected:NO];
        [_btCountry setTitle:(details.countryValue ? details.countryValue : selectedCountryRecom) forState:UIControlStateNormal];
        
        selectedEventCategory = [NSMutableArray arrayWithArray:details.eventCategory];
        selectedEventCategoryKey = [NSMutableArray arrayWithArray:details.eventCategory];
        [_btEventCategory setTitle:convertArrayToString(selectedEventCategory) forState:UIControlStateNormal];
        [_btEventCategory setSelected:NO];
        if(_btEventCategory.currentTitle.length == 0){
            [_btEventCategory setSelected:YES];
            [_btEventCategory setTitle:EVENT_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}


- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) getRequestDetails{
    if(_bookingId){
        [self showToast];
        wsGetRequestDetails = [[WSGetEntertainmentRequestDetails alloc] init];
        wsGetRequestDetails.delegate = self;
        [wsGetRequestDetails getRequestDetails:_bookingId withTask:(_requestType == ENTERTAINMENT_RECOMMEND_REQUEST ? WS_GET_ENTERTAINMENT_RECOMMEND : WS_GET_BOOKING_ENTERTAINMENT)];
    }
}

- (void)dealloc
{
    if(wsGetRequestDetails){
        [wsGetRequestDetails cancelRequest];
    }
    
    if(wsCreateRequest){
        [wsCreateRequest cancelRequest];
    }
    
    if(wsCreateRecommend){
        [wsCreateRecommend cancelRequest];
    }
}


-(void)setTextForViews {
    [_btBook setTitle:NSLocalizedString(@"Book", nil) forState:UIControlStateNormal];
    [_btBook setTitle:NSLocalizedString(@"Book", nil) forState:UIControlStateSelected];
    [_btRecommend setTitle:NSLocalizedString(@"Get Recommendation", nil) forState:UIControlStateSelected];
    [_btRecommend setTitle:NSLocalizedString(@"Get Recommendation", nil) forState:UIControlStateNormal];
    _lbCountry.attributedText = [self setRequireString:NSLocalizedString(@"Country", nil)];
    //[_btCountry setTitle:NSLocalizedString(@"Please select country of preferred event", nil) forState:UIControlStateNormal];
    
    
    _lbCity.attributedText = [self setRequireString:NSLocalizedString(@"City", nil)];
    _txtCity.placeholder = NSLocalizedString(@"Please enter city of preferred event", nil);
    
    _lbState.attributedText = [self setRequireString: NSLocalizedString(@"State", nil)];
    [_btState setTitle:NSLocalizedString(@"Please enter state of preferred event", nil) forState:UIControlStateNormal];
    
    _lbEventName.attributedText = [self setRequireString:NSLocalizedString(@"Event name", nil)];
    _txtEventName.placeholder = NSLocalizedString(@"Please enter name of event", nil);
    
    _lbEventDate.attributedText = [self setRequireString:NSLocalizedString(@"Event date", nil)];
    _lbEventTime.attributedText = [self setRequireString:NSLocalizedString(@"Event time", nil)];
    
    _lbEventCategory.attributedText = [self setRequireString:NSLocalizedString(@"Event category", nil)];
    
    [_btEventCategory setTitle:EVENT_HINT_TEXT forState:UIControlStateNormal];
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_btCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}
@end
