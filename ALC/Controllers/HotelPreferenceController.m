//
//  HotelPreferenceController.m
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HotelPreferenceController.h"
#import "BaseResponseObject.h"

#define ROOM_PREFERENCE_PLACEHOLDER NSLocalizedString(@"Please select room preference", nil)
#define BED_PREFERENCE_PLACEHOLDER NSLocalizedString(@"Please select bed preference", nil)
@interface HotelPreferenceController ()

@end

@implementation HotelPreferenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
    self.pStarRating.starImage = [UIImage imageNamed:@"Star_Grey"];
    self.pStarRating.starHighlightedImage = [UIImage imageNamed:@"Star_Orange"];
    self.pStarRating.maxRating = 5.0;
    self.pStarRating.horizontalMargin = 12;
    self.pStarRating.editable=YES;
    self.pStarRating.rating= 5;
    self.pStarRating.displayMode=EDStarRatingDisplayFull;
    
    [self.btnRoom setTitle:ROOM_PREFERENCE_PLACEHOLDER forState:UIControlStateNormal];
    [self.btnBed setTitle:BED_PREFERENCE_PLACEHOLDER forState:UIControlStateNormal];

    
    isSmoking = NO;
  
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    tapGesture.cancelsTouchesInView = NO;
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
//    [self showToast];
//    wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//    wsGetMyPreference.delegate = self;
//    wsGetMyPreference.category = kHotelPreference;
//    [wsGetMyPreference getMyPreference];
}

-(void) setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"MY PREFERENCES", nil) withColor:HEADER_COLOR];
    self.hotelTitleLbl.text = NSLocalizedString(@"Hotel", @"");
    self.starRatingLbl.text = NSLocalizedString(@"Preferred star rating", @"");
    self.roomPreferLbl.text = NSLocalizedString(@"Room preference", @"");
    [self.btnRoom setTitle:NSLocalizedString(@"Please select room preference", @"") forState:UIControlStateNormal];
    self.bedPreferLbl.text = NSLocalizedString(@"Bed preference", @"");
    [self.btnBed setTitle:NSLocalizedString(@"Please select bed preference", @"") forState:UIControlStateNormal];
     self.smokingRoomLbl.text = NSLocalizedString(@"Smoking room", @"");
    self.hintHelpLbl.text = NSLocalizedString(@"We would like to help you take full advantage of your loyalty program benefits when you travel", @"");
    self.loyaltyProgramLbl.text = NSLocalizedString(@"Name of Loyalty Program", @"");
    self.tfLoyalty.placeholder = NSLocalizedString(@"Please enter name of program", @"");
    self.membershipNoLbl.text = NSLocalizedString(@"Membership No.", @"");
    self.tfMembership.placeholder = NSLocalizedString(@"Please enter membership number", @"");
    [self.saveBtn setTitle:NSLocalizedString(@"SAVE", @"") forState:UIControlStateNormal];
    [self.btCancel setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(void)tapInView:(UITapGestureRecognizer *)tapGuesture{
    [self closeAllKeyboard];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss
{
    [self closeAllKeyboard];
}

- (void)closeAllKeyboard{
    [_tfLoyalty resignFirstResponder];
    [_tfMembership resignFirstResponder];
    [_tfOrtherPreference resignFirstResponder];
}

-(NSInteger) actualScrollViewHeight{
    return _btCancel.frame.size.height*2 + countRightPositionInScrollView(_btCancel);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _pScrollView.frame.size;
    size.height = [self actualScrollViewHeight]+216;
    [_pScrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _pScrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_pScrollView setContentSize:size];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionRoom:(id)sender {
    isVehicle = true;
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = ROOM;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.selectedValues = [[NSMutableArray alloc] initWithArray:selectedRoom];
    controller.selectedKey = [[NSMutableArray alloc] initWithArray:selectedRoomKey];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)actionBed:(id)sender {
    isVehicle = false;
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = BED;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.selectedValues =[[NSMutableArray alloc] initWithArray:selectedBed];
    controller.selectedKey =[[NSMutableArray alloc] initWithArray:selectedBedKey];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(isVehicle){
        selectedRoom = values;
        selectedRoomKey = key;
        [self.btnRoom setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        [self.btnRoom setSelected:NO];
        if(self.btnRoom.currentTitle.length == 0){
            [self.btnRoom setSelected:YES];
            [self.btnRoom setTitle:ROOM_PREFERENCE_PLACEHOLDER forState:UIControlStateNormal];
        }
    }else{
        selectedBed = values;
        selectedBedKey = key;
        [self.btnBed setSelected:NO];
        [self.btnBed setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        if(self.btnBed.currentTitle.length == 0){
            [self.btnBed setSelected:YES];
            [self.btnBed setTitle:BED_PREFERENCE_PLACEHOLDER forState:UIControlStateNormal];
        }
    }
}
- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)actionSave:(id)sender {
    int rating = self.pStarRating.rating;
    NSString* ratingID = @"";
    if(rating >0){
        ratingID = [NSString stringWithFormat:@"%d",rating];
    }
    
    NSString* smokingID = @"";
    if(isSmoking){
        smokingID = @"yes";
    }else{
        smokingID = @"no";
    }
    
    NSString* loyalty = self.tfLoyalty.text;
    NSString* member = self.tfMembership.text;
    NSString* anyrequest = self.tfOrtherPreference.text;
    
    [self showToast];
    wsCreateHotel = [[WSCreateHotelPre alloc] init];
    wsCreateHotel.delegate = self;
    wsCreateHotel.preferenceID = _preferenceID;
    wsCreateHotel.loyaltyMem = loyalty;
    wsCreateHotel.loyaltyNo = member;
    wsCreateHotel.BedTypes = selectedBedKey;
    wsCreateHotel.RoomTypes = selectedRoomKey;
        wsCreateHotel.SmokingRoom = smokingID;
        wsCreateHotel.Ratings = ratingID;
    wsCreateHotel.anyRequest = anyrequest;
    [wsCreateHotel createHotelPre];
}

- (IBAction)sbSmoking:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if([mySwitch isOn]){
        isSmoking = YES;
    }else{
        isSmoking = NO;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.pScrollView setContentOffset:CGPointMake(0, textField.tag-100) animated:YES];
    CGRect frame = self.pScrollView.frame;
    frame.size.height = self.view.frame.size.height - 216;
    [self.pScrollView setFrame:frame];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    CGRect frame = self.pScrollView.frame;
    frame.size.height = self.view.frame.size.height ;
    [self.pScrollView setFrame:frame];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _lbHolder.hidden = YES;
    float offset =  countRightPositionInScrollView(textView) + 80+ textView.frame.size.height;
    if(offset > 0){
        [self.pScrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if(textView.text.length==0){
        _lbHolder.hidden = NO;
    }else{
        _lbHolder.hidden = YES;
    }
     [self.pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_CREATE_HOTELPRE){
        [self stopToast];
        [self.navigationController popViewControllerAnimated:YES];
    }else if(ws.task == WS_GET_RATING){
        
    }else if (ws.task == WS_GET_SMOKING){
    
    } else if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Hotel];
        _preferenceID = [dict stringForKey:MYPREFERENCESID];
        
        NSString* strStar = [dict stringForKey:VALUE];
        if(strStar.length>0)
        self.pStarRating.rating = [strStar intValue];
        
        NSString* value = [dict stringForKey:VALUE3];
        if(isEqualIgnoreCase(value,@"yes")){
            isSmoking = YES;
            self.btnSmoking.on = YES;
        }else{
            isSmoking = NO;
            self.btnSmoking.on = NO;
        }
        
        NSString* room = [dict stringForKey:VALUE1];
        NSArray* arr= convertStringToArray(room);
        if(arr.count>0 && room.length>0){
            selectedRoom = [[NSMutableArray alloc] initWithArray:arr];
            selectedRoomKey = [[NSMutableArray alloc] initWithArray:arr];
            [_btnRoom setSelected:NO];
            [self.btnRoom setTitle:convertArrayToString(selectedRoom) forState:UIControlStateNormal];
        }
        
        NSString* bed = [dict stringForKey:VALUE2];
        NSArray* arrCom= convertStringToArray(bed);
        if(arrCom.count>0 && bed.length>0){
            selectedBed = [[NSMutableArray alloc] initWithArray:arrCom];
            selectedBedKey = [[NSMutableArray alloc] initWithArray:arrCom];
            [_btnBed setSelected:NO];
            [self.btnBed setTitle:convertArrayToString(selectedBed) forState:UIControlStateNormal];
        }
        
        self.tfLoyalty.text = [dict stringForKey:VALUE4];
        self.tfMembership.text = [dict stringForKey:VALUE5];
        _tfOrtherPreference.text =[dict stringForKey:VALUE6];
        if(self.tfOrtherPreference.text.length>0){
            _lbHolder.hidden = YES;
        }
    }
}
-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(errorCode == 600){
        return;
    }
    if(result!=nil && result.message!=nil){
        showAlertOneButton(self,@"", result.message, @"OK");
    } else {
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_API_MSG, @"OK");
    }
}

- (void)dealloc
{
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
    if(wsCreateHotel){
        [wsCreateHotel cancelRequest];
    }
}
@end
