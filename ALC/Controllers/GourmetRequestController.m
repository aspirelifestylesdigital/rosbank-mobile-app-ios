//
//  GourmetRequestController.m
//  ALC
//
//  Created by Hai NguyenV on 10/3/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetRequestController.h"
#import "GourmetRequestObject.h"
#import "GourmetRequestDetailsObj.h"
#import "ThankYouBookingController.h"
#import "ManagerEventCalendar.h"

@interface GourmetRequestController (){
    UIDatePicker *datepickerReservation;
    UIDatePicker *timepickerReservation;
    NSMutableArray *selectedOccasion;
    NSMutableArray *selectedOccasionKey;
    
    WSGourmetBooking* wsCreateRequest;
    WSGetGourmetRequestDetails* wsGetGourmet;
    WSGourmetRecommendRequest* wsCreateRecommend;
    WSGetGourmetRecommend* wsGetRecommend;
    WSGetMyPreferences* wsGetMyPreference;
}
@property (weak, nonatomic) IBOutlet UIView *pvContent;
@property (weak, nonatomic) IBOutlet UIStackView *svBookHeader;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topSpaceToSuperView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topRecommendSpaceToSuperView;
@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCity;
@property (strong, nonatomic) IBOutlet UIButton *btCountry;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationDate;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationTime;
@property (strong, nonatomic) IBOutlet UITextField *txtRestaurantName;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;
@property (strong, nonatomic) IBOutlet UIButton *btOccasion;
@property (weak, nonatomic) IBOutlet UIView *pvState;
@property (strong, nonatomic) IBOutlet UIButton *btState;
@property (strong, nonatomic) IBOutlet GourmetRecommendView *viewRecommend;
@property (strong, nonatomic) IBOutlet UIView *topOrangeUcomingBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowUpcoming;
@property (strong, nonatomic) IBOutlet UIView *topOrangeHistoryBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowHistory;
@property (strong, nonatomic) IBOutlet UIButton *btBook;
@property (strong, nonatomic) IBOutlet UIButton *btRecommend;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icRestaurantNameError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCityError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCountryError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icStateError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icTimeError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icOccasionError;

@property (strong, nonatomic) IBOutlet UIView *viewRestaurantName;
@property (strong, nonatomic) IBOutlet UIStackView *stackBookForm;
@property (strong, nonatomic) IBOutlet UIView *viewCountry;
@property (strong, nonatomic) IBOutlet UIView *viewCity;

@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbRestaurantName;
@property (weak, nonatomic) IBOutlet UILabel *lbReservationDate;
@property (weak, nonatomic) IBOutlet UILabel *lbReservationTime;
@property (weak, nonatomic) IBOutlet UILabel *lbOccasion;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;

- (IBAction)actionCoutryClicked:(id)sender;
- (IBAction)actionStateClicked:(id)sender;
- (IBAction)actionSubmitRequest:(id)sender;
- (IBAction)actionBookView:(id)sender;
- (IBAction)actionRecommendView:(id)sender;
@end

#define DATE_FORMAT @"dd MMM yyyy"
#define COUNTRY_HINT_TEXT NSLocalizedString(@"Please select country of preferred restaurant", nil)
#define CITY_HINT_TEXT NSLocalizedString(@"Please enter city of preferred restaurant", nil)
#define CUISINE_HINT_TEXT NSLocalizedString(@"Please select your preferred cuisine", nil)
#define OCCASION_HINT_TEXT NSLocalizedString(@"Please select occasion", nil)
#define STATE_HINT_TEXT NSLocalizedString(@"Please enter state of preferred restaurant", nil)

@implementation GourmetRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTextForViews];
    [self getRequestDetails];
    // Do any additional setup after loading the view.
    
}

- (void) setUpSubView{
    [self addTitleToHeader:NSLocalizedString(@"DINING", nil) withColor:HEADER_COLOR];
    
     _optionalDetailsView.delegate = self;
    [_optionalDetailsView applyRequestType:GOURMET_REQUEST];
    
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [self collectErrorIcons];
    
    [_txtCity setPlaceholder:CITY_HINT_TEXT];
    [_btCountry setSelected:NO];
    [_btCountry setTitle:self.selectedCountry forState:UIControlStateNormal];
    [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
    _pvState.hidden = YES;
    
    
    _txtCity.delegate = self;
    
    _txtReservationDate.inputAccessoryView = _txtReservationTime.inputAccessoryView = [self setUpToolBar];
    
    datepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerReservation setDatePickerMode:UIDatePickerModeDate];
    datepickerReservation.datePickerMode = UIDatePickerModeDate;
//    [datepickerReservation setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24]];
    [datepickerReservation setMinimumDate:[NSDate date]];
    [datepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerReservation.tag = _txtReservationDate.tag*10;
    _txtReservationDate.inputView = datepickerReservation;
    _txtReservationDate.delegate = self;
    datepickerReservation.date = [NSDate date];
    _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    //end
    
    //Check out date picker
    timepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [timepickerReservation setDatePickerMode:UIDatePickerModeTime];
    timepickerReservation.datePickerMode = UIDatePickerModeTime;
    // [timepickerReservation setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [timepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    timepickerReservation.tag = _txtReservationTime.tag*10;
    _txtReservationTime.inputView = timepickerReservation;
    _txtReservationTime.delegate = self;
    timepickerReservation.minuteInterval = 5;
    timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval: 60*30];
    _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    // _txtReservationTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerReservation.date, DATE_FORMAT)];
    if(isValidValue(self.restaurantName)){
        _txtRestaurantName.text = self.restaurantName;
        _txtRestaurantName.enabled = NO;
    }
    _viewRecommend.delegate = self;
    _viewRecommend.hidden = YES;
    
    if(self.country && self.city){
        [_btCountry setTitle:self.country forState:UIControlStateNormal];
        // [_btCity setTitle:self.city forState:UIControlStateNormal];
        self.selectedCountry = self.countryCode;
        self.selectedCity = self.city;
        if(self.selectedCountry && self.selectedCountry.length>0){
            //_btCity.enabled = YES;
        }
    }
    
}

- (void) hideTopHeader{
    _svBookHeader.hidden = YES;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    _topSpaceToSuperView.constant = -_pvContent.frame.origin.y;
    _topRecommendSpaceToSuperView.constant = -_viewRecommend.frame.origin.y;
    if(_bookingId)
        [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
}

-(void) getRequestDetails{
    if(_bookingId){
        if(_requestType == GOURMET_RECOMMEND_REQUEST){
            [self showRecommendView];
            [self showToast];
            wsGetRecommend = [[WSGetGourmetRecommend alloc] init];
            wsGetRecommend.delegate = self;
            [wsGetRecommend getRequestDetails:_bookingId];
        }else{
            [self showToast];
            wsGetGourmet = [[WSGetGourmetRequestDetails alloc] init];
            wsGetGourmet.delegate = self;
            [wsGetGourmet getRequestDetails:_bookingId];
        }
    }
//    else{
//        [self showToast];
//        wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//        wsGetMyPreference.delegate = self;
//        [wsGetMyPreference getMyPreference];
//    }
}

-(NSInteger) actualScrollViewHeight{
    return _viewSubmitButton.frame.size.height + countRightPositionInScrollView(_viewSubmitButton);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight]+266;
    [_scrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _scrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_scrollView setContentSize:size];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) dateTextField:(id)sender
{
    if(((UIView*)sender).tag == _txtReservationDate.tag*10){
        _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    } else if(((UIView*)sender).tag == _txtReservationTime.tag*10){
        _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    }
    
    if(!isValidDateTimeWithinHalfAnHour(datepickerReservation.date, timepickerReservation.date)){
       // _icTimeError.hidden = NO;
        timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval: 60*30];
        _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + 90 - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}

-(void) closeAllKeyboardAndPicker{
    [_txtRestaurantName resignFirstResponder];
    [_txtReservationDate endEditing:YES];
    [_txtReservationTime endEditing:YES];
    [_txtCity resignFirstResponder];
}

- (IBAction)actionCoutryClicked:(id)sender {
    [_icCountryError setHidden:YES];
    [self openFilterCountry:self];
}

- (IBAction)actionStateClicked:(id)sender {
    [_icStateError setHidden:YES];
    [self openFilterState:self];
}

- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionOccasion:(id)sender{
    _icOccasionError.hidden = YES;
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = OCCASION;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.isCloseAfterClick = YES;
    controller.selectedValues =[[NSMutableArray alloc] initWithObjects:_btOccasion.currentTitle,nil] ;
    [self.navigationController pushViewController:controller animated:YES];
}

//Pick sub filter delegate
#pragma mark ---------- Sub Filter Delegate --------------------
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(key && values && key.count>0 && values.count>0){
        if(type == COUNTRY){
            NSString* newCountry = [key objectAtIndex:0];
            self.selectedCountry = newCountry;
            [_btCountry setSelected:NO];
            [_btCountry setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
            
            if([self isSpecialCountries]){
                _pvState.hidden = NO;
            }else{
                _pvState.hidden = YES;
            }
            self.selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
        } else if(type == STATE){
            NSString* newState = [key objectAtIndex:0];
            self.selectedState = newState;
            [_btState setSelected:NO];
            [_btState setTitle:[values objectAtIndex:0] forState:UIControlStateNormal];
        }else if(type == OCCASION){
            selectedOccasion = values;
            selectedOccasionKey = key;
            [_btOccasion setSelected:NO];
            [self.btOccasion setTitle:convertArrayToString(values) forState:UIControlStateNormal];
            if(self.btOccasion.currentTitle.length == 0){
                [_btOccasion setSelected:YES];
                [self.btOccasion setTitle:OCCASION_HINT_TEXT forState:UIControlStateNormal];
            }
        }
    } else {
        if(type == COUNTRY){
            // _btCity.enabled = NO;
            self.selectedCity = nil;
            self.selectedCountry = nil;
            _pvState.hidden = YES;
            [_btCountry setSelected:YES];
            [_btCountry setTitle:COUNTRY_HINT_TEXT forState:UIControlStateNormal];
        }else if(type == OCCASION){
            selectedOccasion = nil;
            selectedOccasionKey = nil;
            [_btOccasion setSelected:YES];
            [_btOccasion setTitle:OCCASION_HINT_TEXT forState:UIControlStateNormal];
        }else if(type == STATE){
            self.selectedState = nil;
            [_btState setSelected:YES];
            [_btState setTitle:STATE_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}


- (IBAction)actionSubmitRequest:(id)sender {
    [sender setUserInteractionEnabled:false];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
        [sender setUserInteractionEnabled:true];
    });
    GourmetRequestObject* object = [self validateAndCollectData];
    if(object){
        // send request
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Dining",nil) ;
        
        [self showToast];
        wsCreateRequest = [[WSGourmetBooking alloc] init];
        wsCreateRequest.delegate = self;
        [wsCreateRequest bookingGourmet:object];
    } else {
        // showAlertOneButton(self,@"Warning", @"Please input correct values.", @"OK");
    }
}

-(GourmetRequestObject*) validateAndCollectData{
    for (UIView *item in [self.dictIconErrorManager allValues]) {
        [item setHidden:YES];
    }
    
    [_optionalDetailsView exportValues];
    
    GourmetRequestObject* result = (GourmetRequestObject*)[self addPrivilegeData:[[GourmetRequestObject alloc] init]];
    
    if(isValidValue(_foodAllergies)){
        result.foodAllergies = _foodAllergies;
    }
    
    NSString *value;
    BOOL isEnoughInfo = YES;
    
    result.isContactPhone = _optionalDetailsView.isContactPhone;
    result.isContactBoth = _optionalDetailsView.isContactBoth;
    result.isContactEmail = _optionalDetailsView.isContactEmail;
    
    if(_bookingId){
        result.BookingId = _bookingId;
        result.isEdit = YES;
        result.EditType = EDIT_AMEND;
    }else{
        result.EditType = EDIT_ADD;
    }
    
    value = _txtRestaurantName.text;
    if(isValidValue(value)){
        result.restaurantName = value;
        if(isValidValue(self.restaurantID))
            result.BookingItemId = self.restaurantID;
        else
            result.BookingItemId = value;
    } else {
        _icRestaurantNameError.hidden = NO;
        if(value.length==0){
            _icRestaurantNameError.errorMessage = MANDATORY_BLANK;
        }
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.phoneNumber;
    if(isValidValue(value)){
        result.MobileNumber = value;
    } else if(result.isContactPhone || result.isContactBoth){
        isEnoughInfo = NO;
    }
    
    value = _optionalDetailsView.email;
    if(isValidValue(value)){
        result.EmailAddress = value;
    } else if(result.isContactEmail || result.isContactBoth) {
        isEnoughInfo = NO;
    }
    
    result.reservationDate = formatDate(datepickerReservation.date, DATE_SEND_REQUEST_FORMAT);
    result.reservationTime = formatDate(timepickerReservation.date, TIME_SEND_REQUEST_FORMAT);
    if (!isValidDateTimeWithinHalfAnHour(datepickerReservation.date, timepickerReservation.date)) {
        //_icTimeError.hidden = NO;
        //isEnoughInfo = NO;
    }
    
    
    value = _optionalDetailsView.reservationName;
    if(isValidValue(value)){
        result.reservationName = value;
    } else {
        isEnoughInfo = NO;
    }
    
    if(selectedOccasionKey.count>0){
        result.Occasion = selectedOccasionKey;
    }else{
        // isEnoughInfo = NO;
        // _icOccasionError.hidden = NO;
    }
    
    result.adultPax = _optionalDetailsView.adultPax;
    result.kidsPax = _optionalDetailsView.kidsPax;
    
    value = _txtCity.text;
    if(isValidValue(value)){
        result.City = value;
    } else {
        if(value.length==0){
            _icCityError.errorMessage = MANDATORY_BLANK;
        }
        [_icCityError setHidden:NO];
        isEnoughInfo = NO;
    }
    
    value = self.selectedCountry;
    if(isValidValue(value)){
        result.Country = value;
    } else {
        isEnoughInfo = NO;
        _icCountryError.hidden = NO;
        if(value.length==0){
            _icCountryError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    if([self isSpecialCountries]){
        value = self.selectedState;
        if(isValidValue(value)){
            result.State = value;
        }else{
            isEnoughInfo = NO;
            if(value.length==0){
                _icStateError.errorMessage = MANDATORY_BLANK;
            }
            [_icStateError setHidden:NO];
        }
    }
    
    
    value = _optionalDetailsView.specialMessage;
    if(isValidValue(value)){
        if(isNotIncludedSpecialCharacters(value)){
            result.SpecialRequirements = value;
        } else {
            isEnoughInfo = NO;
        }
    }
    
    result.photo = _optionalDetailsView.selectedPhoto;
    
    if(isEnoughInfo){
        return result;
    } else {
        GourmetRequestObject *temp;
        return temp;
    }
}


-(void) showBookingDetails:(GourmetRequestDetailsObj*) details{
    _foodAllergies = details.foodAllergies;
    _txtRestaurantName.text = details.restaurantName;
    datepickerReservation.date = details.reservationDate;
    timepickerReservation.date = details.reservationsTime;
    
    _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    _txtReservationTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerReservation.date, TIME_FORMAT)];
    [_btCountry setSelected:NO];
    [_btCountry setTitle:details.country forState:UIControlStateNormal];
    self.selectedCountry = details.country;
    self.selectedCity = details.city;
    _txtCity.text = details.city;
    if([self isSpecialCountries]){
        _pvState.hidden = NO;
        self.selectedState = details.stateValue;
        [_btState setSelected:NO];
        [_btState setTitle:details.stateValue forState:UIControlStateNormal];
    }else{
        _pvState.hidden = YES;
    }
    
    [_btOccasion setSelected:NO];
    [_btOccasion setTitle:convertArrayToString(details.occasion) forState:UIControlStateNormal];
    selectedOccasionKey = [NSMutableArray arrayWithArray:details.occasion];
    
    if(self.btOccasion.currentTitle.length == 0){
        [_btOccasion setSelected:YES];
        [self.btOccasion setTitle:OCCASION_HINT_TEXT forState:UIControlStateNormal];
    }
    //show data to optional View
    [_optionalDetailsView showDetailsView:details];
}

// Submit from Recommend View
-(void) submitRequest:(GourmetRecommendRequestObj*) requestObject forType:(enum REQUEST_TYPE) type{
    if(type == GOURMET_RECOMMEND_REQUEST){
        if(_bookingId){
            requestObject.BookingId = _bookingId;
        }
        
        if(isValidValue(_foodAllergies)){
            requestObject.foodAllergies = _foodAllergies;
        }
        
        [self showToast];
        
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Dining Recommend",nil) ;
        
        wsCreateRecommend = [[WSGourmetRecommendRequest alloc] init];
        wsCreateRecommend.delegate = self;
        [wsCreateRecommend bookingRecommend:requestObject];
    }
}

#pragma mark ------ API Services Delegate ------------------
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_BOOKING_GOURMET || ws.task == WS_BOOKING_GOURMET_RECOMMEND){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_A_REQUEST_IS_AMENDED
         object:self];
        
        if(_bookingId==nil){
            if(ws.task == WS_BOOKING_GOURMET){
                [self trackEcommerceProduct:@"Generic - Book" withCate:@"Dining"];
            }else if(ws.task == WS_BOOKING_GOURMET_RECOMMEND){
                [self trackEcommerceProduct:@"Generic - Recommend" withCate:@"Recommend Dining"];
            }
        }
        
        [self openThankYouPage];
    } else if (ws.task == WS_GET_GOURMET_REQUEST){
        if(wsGetGourmet.bookingDetails){
            //show data
            [self showBaseBookingDetails:wsGetGourmet.bookingDetails];
        }
    }else if(ws.task == WS_GET_GOURMET_RECOMMEND_REQUEST){
        if(wsGetRecommend.bookingDetails){
            _foodAllergies = wsGetRecommend.bookingDetails.foodAllergies;
            [_viewRecommend showRecommendDetails:wsGetRecommend.bookingDetails];
        }
    }else if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Dining];
        
        NSString* cuisine = [dict stringForKey:VALUE];
        NSString* other = [dict stringForKey:VALUE1];
        _foodAllergies = [dict stringForKey:VALUE2];
        
        [_viewRecommend setMyPreferencce:cuisine withOther:other];
    }
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result.task == WS_BOOKING_GOURMET || result.task == WS_BOOKING_GOURMET_RECOMMEND){
        if(errorCode !=0)
            displayError(self, errorCode);
        else
        {
            showAlertOneButton(self,ERROR_ALERT_TITLE, result.message, @"OK");
        }
    }
}

#pragma mark ------Button Header Action Handle ---------
- (IBAction)actionBookView:(id)sender {
    [self showBookView];
}

- (IBAction)actionRecommendView:(id)sender {
    [self showRecommendView];
    [self.viewRecommend setTimeToCurrentTime];
}

-(void) showBookView{
    [self addTitleToHeader:NSLocalizedString(@"DINING", nil) withColor:HEADER_COLOR];
    _viewRecommend.hidden = YES;
    [_btBook setSelected:YES];
    [_topOrangeUcomingBar setHidden:NO];
    [_bottomArrowUpcoming setHidden:NO];
    
    [_btRecommend setSelected:NO];
    [_topOrangeHistoryBar setHidden:YES];
    [_bottomArrowHistory setHidden:YES];
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) showRecommendView{
    [self addTitleToHeader:NSLocalizedString(@"DINING", nil) withColor:HEADER_COLOR];
    _viewRecommend.hidden = NO;
    [_btBook setSelected:NO];
    [_topOrangeUcomingBar setHidden:YES];
    [_bottomArrowUpcoming setHidden:YES];
    
    [_btRecommend setSelected:YES];
    [_topOrangeHistoryBar setHidden:NO];
    [_bottomArrowHistory setHidden:NO];
    [self closeAllKeyboardAndPicker];
    [_optionalDetailsView closeAllKeyboardAndPicker];
}

-(void) collectErrorIcons{
    self.dictIconErrorManager = [NSMutableDictionary dictionary];
    [self.dictIconErrorManager setObject:_icRestaurantNameError forKey:[NSNumber numberWithInteger:_icRestaurantNameError.tag]];
    [self.dictIconErrorManager setObject:_icTimeError forKey:[NSNumber numberWithInteger:_icTimeError.tag]];
    [self.dictIconErrorManager setObject:_icCountryError forKey:[NSNumber numberWithInteger:_icCountryError.tag]];
    [self.dictIconErrorManager setObject:_icOccasionError forKey:[NSNumber numberWithInteger:_icOccasionError.tag]];
    [self.dictIconErrorManager setObject:_icCityError forKey:[NSNumber numberWithInteger:_icCityError.tag]];
    for (UIView *item in [self.dictIconErrorManager allValues]) {
        if([item isKindOfClass:[ErrorToolTip class]]){
            ((ErrorToolTip*)item).delegate = self;
        }
    }
    
}


- (void)dealloc
{
    if(wsCreateRequest){
        [wsCreateRequest cancelRequest];
    }
    
    if(wsCreateRecommend){
        [wsCreateRecommend cancelRequest];
    }
    
    if(wsGetGourmet){
        [wsGetGourmet cancelRequest];
    }
    
    if(wsGetRecommend){
        [wsGetRecommend cancelRequest];
    }
    
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
}

#pragma SetUp Privilege
- (void) hideAndFillPrivilegeToForm{
    
    NSString* value = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressCountry isValid]){
        value = self.privilegeDetails.addressCountry.Name;
    }
    _viewCountry.hidden = YES;
    self.selectedCountry = value;
    [_btCountry setTitle:value forState:UIControlStateNormal];
    
    value = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressCity isValid]){
        value = self.privilegeDetails.addressCity.Name;
    }
    _viewCity.hidden = YES;
    [_txtCity setText:value];
    self.selectedCity = value;
    
    value = NOT_AVAILABLE_LOCATION_VALUE;
    if([self.privilegeDetails.addressState isValid]){
        value = self.privilegeDetails.addressState.Name;
    }
    [_btState setTitle:value forState:UIControlStateNormal];
    self.selectedState = value;
    _pvState.hidden = YES;
    
    _viewRestaurantName.hidden = YES;
    [_txtRestaurantName setText:self.privilegeDetails.itemName];
}

- (UIStackView*) parentStack{
    return _stackBookForm;
}
-(void)setTextForViews {
    [_btBook setTitle:NSLocalizedString(@"Book", nil) forState:UIControlStateNormal];
    [_btBook setTitle:NSLocalizedString(@"Book", nil) forState:UIControlStateSelected];
    [_btRecommend setTitle:NSLocalizedString(@"Get Recommendation", nil) forState:UIControlStateNormal];
    [_btRecommend setTitle:NSLocalizedString(@"Get Recommendation", nil) forState:UIControlStateSelected];
    _lbCountry.attributedText = [self setRequireString:NSLocalizedString(@"Country", nil)];
    //[_btCountry setTitle:NSLocalizedString(@"Please select country of preferred restaurant", nil) forState:UIControlStateNormal];
    
  
    
    
    _lbCity.attributedText = [self setRequireString:NSLocalizedString(@"City", nil)];
    _txtCity.placeholder = NSLocalizedString(@"Please enter city of preferred restaurant", nil);
    
    _lbState.attributedText = [self setRequireString: NSLocalizedString(@"State", nil)];
    [_btState setTitle:NSLocalizedString(@"Please enter state of preferred restaurant", nil) forState:UIControlStateNormal];
    
    _lbRestaurantName.attributedText = [self setRequireString:NSLocalizedString(@"Restaurant name", nil)];
    _txtRestaurantName.placeholder = NSLocalizedString(@"Please enter name of the restaurant", nil);
    
    _lbReservationDate.attributedText = [self setRequireString:NSLocalizedString(@"Reservation date book_dinging", nil)];
    _lbReservationTime.attributedText = [self setRequireString:NSLocalizedString(@"Reservation time", nil)];
    
    _lbOccasion.text = NSLocalizedString(@"Occasion", nil);
    
    [_btOccasion setTitle:NSLocalizedString(@"Please select occasion book_dinging", nil) forState:UIControlStateNormal];
    [_btSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_btCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}

@end
