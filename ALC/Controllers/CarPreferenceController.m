//
//  CarPreferenceController.m
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CarPreferenceController.h"
#import "BaseResponseObject.h"

@interface CarPreferenceController ()

@end

@implementation CarPreferenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
    
    isVehicle = false;
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
//    [self showToast];
//    wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//    wsGetMyPreference.delegate = self;
//    [wsGetMyPreference getMyPreference];
}

-(void) setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"MY PREFERENCES", nil) withColor:HEADER_COLOR];
    self.transportationTitleLbl.text = NSLocalizedString(@"TRANSPORTATION", @"");
    self.vehicleTypeLbl.text = NSLocalizedString(@"Preferred rental vehicle types", @"");
    [self.btnVehicle setTitle:NSLocalizedString(@"Please select rental vehicles", @"") forState:UIControlStateNormal];
    self.companiesLbl.text = NSLocalizedString(@"Preferred rental companies", @"");
    self.tfRentalCompany.placeholder = NSLocalizedString(@"Please enter rental companies", @"");
    self.hintHelpLbl.text = NSLocalizedString(@"We would like to help you take full advantage of your loyalty program benefits when you travel", @"");
    self.nameLoyaltyProgramLbl.text = NSLocalizedString(@"Name of Loyalty Program", @"");
    self.tfLoyalty.placeholder = NSLocalizedString(@"Please enter name of program", @"");
    self.membershipNoLbl.text = NSLocalizedString(@"Membership No.", @"");
    self.tfMembership.placeholder = NSLocalizedString(@"Please enter membership number", @"");
    [self.saveBtn setTitle:NSLocalizedString(@"SAVE", @"") forState:UIControlStateNormal];
    [self.btCancel setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
}



- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(void)tapInView:(UITapGestureRecognizer *)tapGuesture{
    [self closeAllKeyboard];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss
{
    [self closeAllKeyboard];
}

- (void)closeAllKeyboard{
    [_tfLoyalty resignFirstResponder];
    [_tfMembership resignFirstResponder];
    //[_tfOtherCompany resignFirstResponder];
}

-(NSInteger) actualScrollViewHeight{
    return _btCancel.frame.size.height*2 + countRightPositionInScrollView(_btCancel);
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize size = _pScrollView.frame.size;
    size.height = [self actualScrollViewHeight]+216;
    [_pScrollView setContentSize:size];
}

-(void)keyboardWillHide {
    CGSize size = _pScrollView.frame.size;
    size.height = [self actualScrollViewHeight];
    [_pScrollView setContentSize:size];
}

- (IBAction)selectVehicle:(id)sender {
    isVehicle = true;
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = RETAL_VERTICLE;
    controller.delegate = self;
    controller.selectedValues =[[NSMutableArray alloc] initWithArray:selectedVerhicle] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithArray:selectedVerhicleKey];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void) updateScrollViewToSelectedView:(UIView*) selectedView{
    float offset = selectedView.frame.size.height + countRightPositionInScrollView(selectedView) + [self scrollViewPositionInSuperView] - (SCREEN_HEIGHT - 64 - 250);
    if(offset > 0){
        [self.pScrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}

-(NSInteger) scrollViewPositionInSuperView{
    return _pScrollView.frame.origin.y + countRightPositionInScrollView([_pScrollView superview]) - 30;
}

- (IBAction)actionSave:(id)sender {
    NSString* loyalty = self.tfLoyalty.text;
    NSString* member = self.tfMembership.text;
    //NSString* orther = self.tfOtherCompany.text;
    [self showToast];
    wsCreateCarPre = [[WSCreateCarPre alloc] init];
    wsCreateCarPre.delegate = self;
    wsCreateCarPre.preferenceID = _preferenceID;
    wsCreateCarPre.arrVehicle = selectedVerhicleKey;
    
    wsCreateCarPre.arrCompany = _tfRentalCompany.text;
    //wsCreateCarPre.otherCompany = orther;
    wsCreateCarPre.loyaltyMem = loyalty;
    wsCreateCarPre.loyaltyNo = member;
    [wsCreateCarPre createCarPre];
    
}

- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(isVehicle){
        selectedVerhicle = values;
        selectedVerhicleKey = key;
        [self.btnVehicle setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        [self.btnVehicle setSelected:NO];
        if(self.btnVehicle.currentTitle.length == 0){
            [self.btnVehicle setSelected:YES];
            [self.btnVehicle setTitle:NSLocalizedString(@"Please select rental vehicles", nil) forState:UIControlStateNormal];
        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self updateScrollViewToSelectedView:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Car_Rental];
        _preferenceID = [dict stringForKey:MYPREFERENCESID];
        
        NSString* vehicle = [dict stringForKey:VALUE];
        if(vehicle.length>0){
            NSArray* arr= convertStringToArray(vehicle);
            if(arr.count>0){
                selectedVerhicle = [[NSMutableArray alloc] initWithArray:arr];
                selectedVerhicleKey = [[NSMutableArray alloc] initWithArray:arr];
                [self.btnVehicle setSelected:NO];
                [self.btnVehicle setTitle:convertArrayToString(selectedVerhicle) forState:UIControlStateNormal];
            }
        }
        self.tfRentalCompany.text =[dict stringForKey:VALUE1];
        //self.tfOtherCompany.text =[dict stringForKey:VALUE2];
        self.tfLoyalty.text = [dict stringForKey:VALUE3];
        self.tfMembership.text = [dict stringForKey:VALUE4];
        
    }else if(ws.task == WS_CREATE_CARPRE){
        [self stopToast];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(errorCode == 600){
        return;
    }
    if(result!=nil && result.message!=nil){
        showAlertOneButton(self,@"", result.message, @"OK");
    } else {
        showAlertOneButton(self ,ERROR_ALERT_TITLE, ERROR_API_MSG, @"OK");
    }
}

- (void)dealloc
{
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
    if(wsCreateCarPre){
        [wsCreateCarPre cancelRequest];
    }
}
@end
