//
//  HotelRequestController.h
//  ALC
//
//  Created by Anh Tran on 9/27/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "RequestFormBaseController.h"
#import "OptionalDetailsView.h"
#import "WSCreateHotelRequest.h"
#import "SubFilterController.h"
#import "ErrorToolTip.h"
#import "HotelRecommendView.h"
#import "WSGetMyPreferences.h"
@interface HotelRequestController : RequestFormBaseController<OptionalDetailsViewDelegate, DataLoadDelegate, SubFilterControllerDelegate,ErrorToolTipDelegate, HotelRecommendViewDelegate>
{
}
@property (strong, nonatomic) NSString* bookingId;
@property (strong, nonatomic) NSString* anyRequest;
@property (nonatomic) enum REQUEST_TYPE requestType;




@end
