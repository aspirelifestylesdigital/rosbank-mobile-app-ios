//
//  ThankYouBookingController.m
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ThankYouBookingController.h"
#import "ALCQC-Swift.h"

@interface ThankYouBookingController ()
@property (strong, nonatomic) IBOutlet UILabel *lbThanksMsg;
@property (weak, nonatomic) IBOutlet UILabel *lbSubHeader;
@property (weak, nonatomic) IBOutlet UILabel *lbSincerely;
@property (weak, nonatomic) IBOutlet UILabel *lblSign;

@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@end

@implementation ThankYouBookingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customButtonBack];
    
    [_lbThanksMsg setFont:[UIFont fontWithName:@"AvenirNextLTPro-Medium" size:FONT_SIZE_16*SCREEN_SCALE]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)homeClicked:(id)sender {
    [self setRootViewAfterLogin];
}

- (void)setTextForViews{
    [self addTitleToHeader:NSLocalizedString(@"THANK YOU", nil) withColor:HEADER_COLOR];
    self.lbThanksMsg.text = _thankYouMessage ? _thankYouMessage : @"";
    self.lbSubHeader.text = _subHeaderMessage ? _subHeaderMessage :  NSLocalizedString(@"Thank you for booking with us", nil);    
    self.lbSincerely.text = [NSString stringWithFormat:@"%@,", NSLocalizedString(@"Sincerely", nil)];
    self.lblSign.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Concierge service", nil), [AppConfig shared].conciergeServiceName];
}

@end
