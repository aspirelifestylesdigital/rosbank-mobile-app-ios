//
//  CarRentalTransferRequestController.m
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CarRentalTransferRequestController.h"
#import "ThankYouBookingController.h"
#import "WSCreateRentalRequest.h"
#import "WSGetCarRentalRequestDetails.h"
#import "WSGetCarTransferRequestDetails.h"
#import "ManagerEventCalendar.h"
#import "ALCQC-Swift.h"

@interface CarRentalTransferRequestController (){
    WSCreateRentalRequest* wsCreateRental;
    WSCreateCarTransferRequest* wsCreateTransfer;
    WSGetCarRentalRequestDetails* wsGetRentalRequest;
    WSGetCarTransferRequestDetails* wsGetTransferRequest;
}

@property (strong, nonatomic) IBOutlet CarRentalTransferView *viewRental;
@property (strong, nonatomic) IBOutlet CarRentalTransferView *viewTransfer;
@property (strong, nonatomic) IBOutlet UIStackView *topNavigator;
@property (strong, nonatomic) IBOutlet UIView *topOrangeUcomingBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowUpcoming;
@property (strong, nonatomic) IBOutlet UIView *topOrangeHistoryBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowHistory;
@property (strong, nonatomic) IBOutlet UIButton *btRental;
@property (strong, nonatomic) IBOutlet UIButton *btTransfer;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topRentalSpaceToSuperView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topTransferSpaceToSuperView;
@end

@implementation CarRentalTransferRequestController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customButtonBack];
    
    [self setUpView];
    [self setTextForViews];

}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) setUpView{
    [_viewRental applyRequestType:CAR_RENTAL_REQUEST];
    [_viewTransfer applyRequestType:CAR_TRANSFER_REQUEST];
    
    _viewRental.delegate = self;
    _viewTransfer.delegate = self;
    
    
    if(_hideTopNavigator || _bookingId){
        _topNavigator.hidden = YES;
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        _topRentalSpaceToSuperView.constant = -_viewRental.frame.origin.y;
        _topTransferSpaceToSuperView.constant = -_viewTransfer.frame.origin.y;
    }    
    
    if(_requestType == CAR_TRANSFER_REQUEST){
        [self addTitleToHeader:NSLocalizedString(@"TRANSFER", nil) withColor:HEADER_COLOR];
        [self showTransferView];
    } else if (_requestType == CAR_RENTAL_REQUEST){
        [self addTitleToHeader:NSLocalizedString(@"CAR RENTAL", nil) withColor:HEADER_COLOR];
        [self showRentalView];
    } else {
        [self addTitleToHeader:NSLocalizedString(@"TRANSPORTATION", nil) withColor:HEADER_COLOR];
        [self showRentalView];
    }
    
    if(_bookingId){
        [self showToast];
        if(_requestType == CAR_TRANSFER_REQUEST){
            wsGetTransferRequest = [[WSGetCarTransferRequestDetails alloc] init];
            wsGetTransferRequest.delegate = self;
            [wsGetTransferRequest getRequestDetails:_bookingId];
        } else {
            wsGetRentalRequest = [[WSGetCarRentalRequestDetails alloc] init];
            wsGetRentalRequest.delegate = self;
            [wsGetRentalRequest getRequestDetails:_bookingId];
        }
    }
//    else{
//        [self showToast];
//        wsGetMyPreference = [[WSGetMyPreferences alloc] init];
//        wsGetMyPreference.delegate = self;
//        [wsGetMyPreference getMyPreference];
//    }

}

- (IBAction)actionRentalView:(id)sender {
    [self showRentalView];
    [_viewRental resetTimeValue];
}

- (IBAction)actionTransferView:(id)sender {
    [self showTransferView];
    [_viewTransfer resetTimeValue];
}

-(void) showRentalView{
    _viewRental.hidden = NO;
    _viewTransfer.hidden = YES;
    [_btRental setSelected:YES];
    [_topOrangeUcomingBar setHidden:NO];
    [_bottomArrowUpcoming setHidden:NO];
    
    [_btTransfer setSelected:NO];
    [_topOrangeHistoryBar setHidden:YES];
    [_bottomArrowHistory setHidden:YES];
    [_viewTransfer closAllkeyboard];
}

-(void) showTransferView{
    _viewRental.hidden = YES;
    _viewTransfer.hidden = NO;
    [_btRental setSelected:NO];
    [_topOrangeUcomingBar setHidden:YES];
    [_bottomArrowUpcoming setHidden:YES];
    
    [_btTransfer setSelected:YES];
    [_topOrangeHistoryBar setHidden:NO];
    [_bottomArrowHistory setHidden:NO];
    [_viewRental closAllkeyboard];
}

-(void) submitRequest:(CarCreateRequestObject*) requestObject forType:(enum REQUEST_TYPE) type{
    [self showToast];
    if(_bookingId){
        requestObject.bookingId = _bookingId;
    }
    if(type == CAR_TRANSFER_REQUEST){
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Car Transfer",nil);
        
        wsCreateTransfer = [[WSCreateCarTransferRequest alloc] init];
        wsCreateTransfer.delegate = self;
        [wsCreateTransfer bookingTransfer:requestObject];
    } else if(type == CAR_RENTAL_REQUEST){
        
        trackGAICategory(@"Concierge Request",@"Send concierge request",@"Car Rental",nil);
        
        wsCreateRental = [[WSCreateRentalRequest alloc] init];
        wsCreateRental.delegate = self;
        [wsCreateRental bookingRental:requestObject];
    }
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_BOOKING_CAR_RENTAL || ws.task == WS_BOOKING_CAR_TRANSFER){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_A_REQUEST_IS_AMENDED
         object:self];
        
        [self.navigationController popViewControllerAnimated:NO];
        
        if(_bookingId==nil){
            if(ws.task == WS_BOOKING_CAR_RENTAL){
                [self trackEcommerceProduct:@"Generic - Book" withCate:@"Car Rental"];
            }else if(ws.task == WS_BOOKING_CAR_TRANSFER){
                [self trackEcommerceProduct:@"Generic - Book" withCate:@"Car Transfer"];
            }
        }
        
        ThankYouBookingController* controller = (ThankYouBookingController*) [self controllerByIdentifier:@"ThankYouBooking"];
        controller.thankYouMessage = [BOOKING_THANK_YOU_MSG stringByReplacingOccurrencesOfString:@"PHONE_NUMBER_PLACEHOLDER" withString:[AppConfig shared].companyPhoneNumber];
        [appdelegate.rootVC pushViewController:controller animated:YES];
    } else if (ws.task == WS_GET_CAR_RENTAL_REQUEST){
        if(wsGetRentalRequest.bookingDetails){
            //show data
            [_viewRental showRentalDetails:wsGetRentalRequest.bookingDetails];
        }
    } else if (ws.task == WS_GET_CAR_TRANSFER_REQUEST){
        if(wsGetTransferRequest.bookingDetails){
            //show data
            [_viewTransfer showTransferDetails:wsGetTransferRequest.bookingDetails];
        }
    }else if(ws.task == WS_GET_MY_PREFERENCE){
        [self stopToast];
        NSDictionary* dict = [wsGetMyPreference.pData objectForKey:Preference_Car_Rental];
        [_viewRental setMyPreference:dict];
        [_viewTransfer setMyPreference:dict];
    }
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result.task == WS_BOOKING_CAR_RENTAL || result.task == WS_BOOKING_CAR_TRANSFER){
        if(errorCode !=0)
            displayError(self, errorCode);
        else
        {
            showAlertOneButton(self,ERROR_ALERT_TITLE, result.message, @"OK");
    }
    }
}


- (void)dealloc
{
    if(wsCreateRental){
        [wsCreateRental cancelRequest];
    }
    
    if(wsCreateTransfer){
        [wsCreateTransfer cancelRequest];
    }
    
    if(wsGetRentalRequest){
        [wsGetRentalRequest cancelRequest];
    }
    
    if(wsGetTransferRequest){
        [wsGetTransferRequest cancelRequest];
    }
    
    if(wsGetMyPreference){
        [wsGetMyPreference cancelRequest];
    }
    
}

-(void)setTextForViews {
    [_btRental setTitle:NSLocalizedString(@"Car Rental", nil) forState:UIControlStateNormal];
    [_btRental setTitle:NSLocalizedString(@"Car Rental", nil) forState:UIControlStateSelected];
    [_btTransfer setTitle:NSLocalizedString(@"Transfer", nil) forState:UIControlStateSelected];
    [_btTransfer setTitle:NSLocalizedString(@"Transfer", nil) forState:UIControlStateNormal];

}

@end
