//
//  ExperiencesFilterController.m
//  ALC
//
//  Created by Anh Tran on 8/24/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ExperiencesFilterController.h"
#import "CustomTextField.h"

@interface ExperiencesFilterController (){
    NSMutableArray *selectedCuisines;
    NSMutableArray *selectedOccasions;
    CGSize originalSize;
}

   @property (strong, nonatomic)  IBOutlet UIView *viewZipCode;
   @property (strong, nonatomic)  IBOutlet CustomTextField *txtZipCode;
   @property (strong, nonatomic)  IBOutlet UIView *viewPrice;
   @property (strong, nonatomic)  IBOutlet UIButton *btOneDollar;
   @property (strong, nonatomic)  IBOutlet UIButton *btTwoDollar;
   @property (strong, nonatomic)  IBOutlet UIButton *btThreeDollar;
   @property (strong, nonatomic)  IBOutlet UIButton *btFourDollar;
   @property (strong, nonatomic)  IBOutlet UIView *viewCuisine;
   @property (strong, nonatomic)  IBOutlet UIButton *btCuisine;
   @property (strong, nonatomic)  IBOutlet UIView *viewStars;
   @property (strong, nonatomic)  IBOutlet UIButton *btFirstStar;
   @property (strong, nonatomic)  IBOutlet UIButton *btSecondStar;
   @property (strong, nonatomic)  IBOutlet UIButton *btThirdStar;
   @property (strong, nonatomic)  IBOutlet UIView *viewSpecialBenefits;
   @property (strong, nonatomic)  IBOutlet UISwitch *swBenefits;
   @property (strong, nonatomic)  IBOutlet UIView *viewPopularity;
   @property (strong, nonatomic)  IBOutlet UISwitch *swPopularity;
   @property (strong, nonatomic)  IBOutlet UIView *viewOccasions;
   @property (strong, nonatomic)  IBOutlet UIButton *btOccasion;
   @property (strong, nonatomic)  IBOutlet UIView *viewActivities;
   @property (strong, nonatomic)  IBOutlet UISwitch *swActivities;
   @property (strong, nonatomic)  IBOutlet UIView *viewEvent;
   @property (strong, nonatomic)  IBOutlet UISwitch *swEvent;
   @property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
    

@end

@implementation ExperiencesFilterController
@synthesize filterType;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleToHeader:NSLocalizedString(@"FILTER", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    // Do any additional setup after loading the view.
    
    [self checkFilterTypeToView];
    [self setUpView];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize temp = originalSize;
    temp.height = originalSize.height+216;
    [_scrollView setContentSize:temp];
}

-(void)keyboardWillHide {
    [_scrollView setContentSize:originalSize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) checkFilterTypeToView{
    _viewZipCode.hidden = ! ((filterType == FILTER_GOURMET) || (filterType == FILTER_TRAVEL)
                            || (filterType == FILTER_ENTERTAIN));
    _viewPrice.hidden = ! ((filterType == FILTER_GOURMET) || (filterType == FILTER_TRAVEL)
                          || (filterType == FILTER_ENTERTAIN) || (filterType == FILTER_SLURGE));
    _viewCuisine.hidden = ! (filterType == FILTER_GOURMET);
    _viewStars.hidden = ! ((filterType == FILTER_GOURMET) || (filterType == FILTER_TRAVEL));
    _viewOccasions.hidden = ! ((filterType == FILTER_GOURMET) || (filterType == FILTER_TRAVEL)
                              || (filterType == FILTER_ENTERTAIN));
    _viewSpecialBenefits.hidden = ! ((filterType == FILTER_TRAVEL) || (filterType == FILTER_ENTERTAIN)
                                   || (filterType == FILTER_SLURGE));
    _viewActivities.hidden = ! (filterType == FILTER_ENTERTAIN);
    _viewEvent.hidden = ! (filterType == FILTER_ENTERTAIN);
    _viewPopularity.hidden = ! (filterType == FILTER_SLURGE);
}

- (void) setUpView{
    [_btOneDollar setSelected:NO];
    [_btTwoDollar setSelected:NO];
    [_btThreeDollar setSelected:NO];
    [_btFourDollar setSelected:NO];
    
    [_btFirstStar setSelected:NO];
    [_btSecondStar setSelected:NO];
    [_btThirdStar setSelected:NO];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];

    [_scrollView setNeedsLayout];
    [_scrollView layoutIfNeeded];
    originalSize = _scrollView.contentSize;
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [_txtZipCode resignFirstResponder];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [_txtZipCode resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) clearStarStatus{
    [_btFirstStar setSelected:NO];
    [_btSecondStar setSelected:NO];
    [_btThirdStar setSelected:NO];
}

- (IBAction)oneDollarClicked:(id)sender {
    if(_btOneDollar.isSelected){
        [_btOneDollar setSelected:NO];
    } else {
        [_btOneDollar setSelected:YES];
    }
}
- (IBAction)twoDollarClicked:(id)sender {
    if(_btTwoDollar.isSelected){
        [_btTwoDollar setSelected:NO];
    } else {
        [_btTwoDollar setSelected:YES];
    }
}
- (IBAction)threeDollarClicked:(id)sender {
    if(_btThreeDollar.isSelected){
        [_btThreeDollar setSelected:NO];
    } else {
        [_btThreeDollar setSelected:YES];
    }
}
- (IBAction)fourDollarClicked:(id)sender {
    if(_btFourDollar.isSelected){
        [_btFourDollar setSelected:NO];
    } else {
        [_btFourDollar setSelected:YES];
    }
}


- (IBAction)cuisineClicked:(id)sender {
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = CUISINES;
    controller.delegate = self;
    controller.selectedValues = [NSMutableArray arrayWithArray:selectedCuisines];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)firstStarClicked:(id)sender {
    [self clearStarStatus];
    [_btFirstStar setSelected:YES];
}

- (IBAction)secondStarClicked:(id)sender {
    [self clearStarStatus];
    [_btFirstStar setSelected:YES];
    [_btSecondStar setSelected:YES];
}

- (IBAction)thirdStarClicked:(id)sender {
    [_btFirstStar setSelected:YES];
    [_btSecondStar setSelected:YES];
    [_btThirdStar setSelected:YES];
}

- (IBAction)occasionClicked:(id)sender {
    SubFilterController* controller = (SubFilterController*) [self controllerByIdentifier:@"SubFilter"];
    controller.dataType = OCCASION;
    controller.delegate = self;
    controller.selectedValues = [NSMutableArray arrayWithArray:selectedOccasions];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)applyClicked:(id)sender {
    
    NSMutableDictionary* options = [NSMutableDictionary dictionary];
    
    if(!_viewZipCode.hidden && [_txtZipCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length >0){
        [options setObject:[_txtZipCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:ZipCode];
    }
    
    if(!_viewPrice.hidden){
        NSMutableArray *range = [NSMutableArray array];
        if(_btOneDollar.isSelected){
            [range addObject:_btOneDollar.currentTitle];
        }
        
        if(_btTwoDollar.isSelected){
            [range addObject:_btTwoDollar.currentTitle];
        }
        
        if(_btThreeDollar.isSelected){
            [range addObject:_btThreeDollar.currentTitle];
        }
        
        if(_btFourDollar.isSelected){
            [range addObject:_btFourDollar.currentTitle];
        }
        
        [options setObject:range forKey:PriceRange];
    }
    
    if(!_viewCuisine.hidden && selectedCuisines!=nil && selectedCuisines.count>0){
        [options setObject:selectedCuisines forKey:Cuisine];
    }
    
    if(!_viewOccasions.hidden && selectedOccasions!=nil && selectedOccasions.count>0){
        [options setObject:selectedOccasions forKey:Occasion];
    }
    
    if(!_viewPopularity.hidden && _swPopularity.isOn){
        [options setObject:[NSNumber numberWithBool:_swPopularity.isOn] forKey:Popularity];
    }
    
    if(!_viewActivities.hidden && _swActivities.isOn){
        [options setObject:[NSNumber numberWithBool:_swActivities.isOn] forKey:Activities];
    }
    
    if(!_viewEvent.hidden && _swEvent.isOn){
        [options setObject:[NSNumber numberWithBool:_swEvent.isOn] forKey:Event];
    }
    
    if(!_viewSpecialBenefits.hidden && _swBenefits.isOn){
        [options setObject:[NSNumber numberWithBool:_swBenefits.isOn] forKey:Benefit];
    }
    
    if(!_viewStars.hidden){
        if(_btThirdStar.isSelected){
            [options setObject:[NSNumber numberWithInteger:3] forKey:Star];
        } else if(_btSecondStar.isSelected){
            [options setObject:[NSNumber numberWithInteger:2] forKey:Star];
        } else if (_btFirstStar.isSelected){
            [options setObject:[NSNumber numberWithInteger:1] forKey:Star];
        }
    }
    
    if(_delegate!=nil){
        [_delegate selectedFilterOptions:options];
    }
    
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)cancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void) pickOption:(NSMutableArray*) values forType:(NSInteger) type{
    if(type == 1){
        selectedCuisines = values;
        [_btCuisine setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        if(_btCuisine.currentTitle.length == 0){
            [_btCuisine setTitle:@"Please select" forState:UIControlStateNormal];
        }
    } else if (type == 2){
        selectedOccasions = values;
        [_btOccasion setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        if(_btOccasion.currentTitle.length == 0){
            [_btOccasion setTitle:@"Please select" forState:UIControlStateNormal];
        }
    }
}

@end
