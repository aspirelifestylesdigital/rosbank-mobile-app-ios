//
//  ConfirmForgotPasswordController.m
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ConfirmForgotPasswordController.h"
#import "LoginViewController.h"

@interface ConfirmForgotPasswordController ()
@property (strong, nonatomic) IBOutlet UIButton *btCheckEmail;

@end

@implementation ConfirmForgotPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTitleToHeader:NSLocalizedString(@"FORGOT PASSWORD", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    [_btCheckEmail.titleLabel setAdjustsFontSizeToFitWidth:YES];
}
//- (void) buttonBack{
//    [self.navigationController popToViewController:(LoginViewController*) [self controllerByIdentifier:@"LoginViewController"] animated:YES];
//}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
//------- Show Concierge button -----------
//---- return NO if you don't want to show it ----
//---- return YES if you want to show it -----------------
-(BOOL)isShowConciergeButton{
    return NO;
}
//--------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)checkEmailClicked:(id)sender {
    NSURL* mailURL = [NSURL URLWithString:@"message://"];
    if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
        [[UIApplication sharedApplication] openURL:mailURL];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
