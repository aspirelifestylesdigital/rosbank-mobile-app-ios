//
//  LocationPopUpViewController.m
//  ALC
//
//  Created by Tho Nguyen on 10/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "LocationPopUpViewController.h"
#import "LocationSelectionViewController.h"

static NSString *const kLocationSelectionViewController = @"LocationSelectionViewController";

@interface LocationPopUpViewController () <CLLocationManagerDelegate, LocationSelectionViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UILabel *lblShareYourLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblYourLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectManually;
@property (weak, nonatomic) IBOutlet UILabel *lblYourCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblYourCity;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnRemindMeLater;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSArray *countries;
@property (strong, nonatomic) NSArray *cities;
@property (strong, nonatomic) NSString *yourCountry;
@property (strong, nonatomic) NSString *yourCity;
@property (nonatomic) NSInteger amountInvokeData;

@end

@implementation LocationPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUp {
    self.popupView.layer.cornerRadius = 3.0f;
    
    self.lblShareYourLocation.text = NSLocalizedString(@"SHARE YOUR LOCATION", nil);
    self.lblSelectManually.text = NSLocalizedString(@"OR SELECT MANUALLY", nil);
    self.lblYourCountry.text = NSLocalizedString(@"Country", nil);
    self.lblYourCity.text = NSLocalizedString(@"City", nil);
    [self.btnSaveLocation setTitle:NSLocalizedString(@"SAVE LOCATION", nil) forState:UIControlStateNormal];
    self.btnSaveLocation.layer.cornerRadius = 3.0f;
    [self.btnRemindMeLater setTitle:NSLocalizedString(@"Remind me later", nil) forState:UIControlStateNormal];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    
    //[self getData];
}

- (void)willAddLocationPopupViewControllerInViewController:(UIViewController *)vc {
    [vc addChildViewController:self];
    [vc.view addSubview:self.view];
}

- (void)willRemoveLocationPopupInView {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)willOpenLocationSelectionViewControllerWithData:(NSArray *)datas andIndexSelection:(NSInteger)indexSelection andLocationSelectionType:(LocationSelectionType)type {
    LocationSelectionViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kLocationSelectionViewController];
    vc.datas = datas;
    vc.indexSelection = indexSelection;
    vc.type = type;
    vc.delegate = self;
    
    [self presentViewController:vc animated:YES completion:nil];
}
/*
- (void)getCountriesListFormSever {
    self.wsGetCountries = [[WSGetCountriesList alloc] init];
    self.wsGetCountries.delegate = self;
    [self.wsGetCountries getList];
}

- (void)getCitiesListFormSever {
    self.wsGetCities = [[WSGetCitiesList alloc] init];
    self.wsGetCities.delegate = self;
    [self.wsGetCities getList];
}

- (void)getData {
    [self showToast];
    [self getCountriesListFormSever];
    [self getCitiesListFormSever];
}*/

-(void)showToast{
    [self.navigationController.view setUserInteractionEnabled:NO];
    [self.view setUserInteractionEnabled:NO];
    [CSToastManager setTapToDismissEnabled:NO];
    [self.view makeToastActivity:CSToastPositionCenter];
}
-(void)stopToast{
    [self.view hideToastActivity];
    [self.navigationController.view setUserInteractionEnabled:YES];
    [self.view setUserInteractionEnabled:YES];
}

- (void)willShowData {
    self.amountInvokeData ++;
    if (self.amountInvokeData >= 3) {
        [self stopToast];
        self.amountInvokeData = 0;
        self.lblYourLocation.text = [NSString stringWithFormat:@"%@, %@", self.yourCity, self.yourCountry];
    }
}

- (NSInteger)indexString:(NSString *)string inArray:(NSArray *)array {
    NSInteger count = 0;
    for (NSString *temp in array) {
        if ([temp containsString:string]) {
            
            return count;
        }
        count ++;
    }
    
    return -1;
}

#pragma mark - Action Methods

- (IBAction)tappedAtCloseButton:(id)sender {
    [self willRemoveLocationPopupInView];
}

- (IBAction)tappedAtCountrySelectionButton:(id)sender {
    NSInteger index = 0;
    if ([self.lblYourCountry.text isEqualToString:NSLocalizedString(@"Country", nil)]) {
        index = [self indexString:self.yourCountry inArray:self.countries];
    } else {
        index = [self.countries indexOfObject:self.lblYourCountry.text];
    }
    [self willOpenLocationSelectionViewControllerWithData:self.countries andIndexSelection:index andLocationSelectionType:LocationSelectionType_Country];
}

- (IBAction)tappedAtCitySelectionButton:(id)sender {
    NSInteger index = 0;
    if ([self.lblYourCity.text isEqualToString:NSLocalizedString(@"City", nil)]) {
        index = [self indexString:self.yourCity inArray:self.cities];
    } else {
        index = [self.cities indexOfObject:self.lblYourCity.text];
    }
    [self willOpenLocationSelectionViewControllerWithData:self.cities andIndexSelection:index andLocationSelectionType:LocationSelectionType_City];
}

- (IBAction)tappedAtSaveLocationButton:(id)sender {
    NSString *country = self.yourCountry;
    NSString *city = self.yourCity;
    if (![self.lblYourCountry.text isEqualToString:NSLocalizedString(@"Country", nil)]
        && ![self.lblYourCity.text isEqualToString:NSLocalizedString(@"City", nil)]) {
        country = self.lblYourCountry.text;
        city = self.lblYourCity.text;
    }
    [[NSUserDefaults standardUserDefaults] setObject:country forKey:USER_DEFAULT_YOUR_COUNTRY];
    [[NSUserDefaults standardUserDefaults] setObject:city forKey:USER_DEFAULT_YOUR_CITY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self willRemoveLocationPopupInView];
}

- (IBAction)tappedAtRemindMeLaterButton:(id)sender {
    [self willRemoveLocationPopupInView];
}

#pragma mark - LocationSelectionViewControllerDelegate

- (void)didSelectItem:(NSInteger)indexSelection andLocationSelectionType:(LocationSelectionType)type {
    if (indexSelection >= 0) {
        switch (type) {
            case LocationSelectionType_Country:
                self.lblYourCountry.text = [self.countries objectAtIndex:indexSelection];
                break;
            case LocationSelectionType_City:
                self.lblYourCity.text = [self.cities objectAtIndex:indexSelection];
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - DataLoadDelegate
/*
- (void)loadDataDoneFrom:(WSBase *)ws {
    switch (ws.task) {
        case WS_GET_COUNTRY:
            self.countries = ((WSGetCountriesList *)ws).countriesList;
            break;
        case WS_GET_CITY:
            self.cities = ((WSGetCitiesList *)ws).citiesList;
            break;
            
        default:
            break;
    }
    [self willShowData];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    if(result!=nil && result.message!=nil){
        
    }
    [self willShowData];
}
*/
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self.locationManager stopUpdatingLocation];
    CLLocation *location = [locations lastObject];
    CLGeocoder *geo = [[CLGeocoder alloc] init];
    [geo reverseGeocodeLocation:location
              completionHandler:
     ^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
         if (error == nil && [placemarks count] > 0) {
             CLPlacemark *place = [placemarks lastObject];
             self.yourCity = place.locality;
             self.yourCountry = place.country;
             [self willShowData];
         }
    }];
}

@end
