//
//  ForgotPasswordController.m
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "BaseResponseObject.h"
#import "ConfirmForgotPasswordController.h"
#import "OopsErrorPageController.h"
#import "StoreUserData.h"
#import "ALCQC-Swift.h"

@interface ForgotPasswordController (){
    WSForgotPassword *wsForgotPassword;
    CGSize originalSize;
    BOOL isForgotPwsswordSuccessful;
}
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btSend;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewEmail;
@property (strong, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (strong, nonatomic) IBOutlet UIView *viewAuthCode;
@property (strong, nonatomic) IBOutlet UILabel *lbAuthCode;
@property (strong, nonatomic) IBOutlet UITextField *txtAuthCode;
@property (strong, nonatomic) IBOutlet ErrorToolTip *authCodeErrorToolTip;

@end

@implementation ForgotPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
    }
    
    [self customButtonBack];
    
    [_btSend.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_scrollView setNeedsLayout];
    [_scrollView layoutIfNeeded];
    originalSize = _scrollView.contentSize;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    isForgotPwsswordSuccessful = NO;

    [self collectErrorIcons];
}

-(void) setTextForViews
{
   [self addTitleToHeader:NSLocalizedString(@"FORGOT PASSWORD", nil) withColor:HEADER_COLOR];
    self.messageLbl.text = NSLocalizedString(@"Not to worry, we will send you an email to assist you with password retrieval.", @"");
    self.lbEmail.text = NSLocalizedString(@"E-mail", @"");
    self.lbAuthCode.text = NSLocalizedString(@"Code", nil);
    self.txtAuthCode.placeholder = NSLocalizedString(@"Input authorization code", nil);
    [self.btSend setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    if(isForgotPwsswordSuccessful){
        [self.navigationController popViewControllerAnimated:NO];
    }

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void) clickedOnErrorIcon:(NSInteger) tag{
    for (NSNumber* icTag in [dictIconErrorManager allKeys]) {
        if(tag != [icTag integerValue]){
            [((ErrorToolTip*)[dictIconErrorManager objectForKey:icTag]) toggleToolTips:NO];
        }
    }
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize temp = originalSize;
    temp.height = originalSize.height+100;
    [_scrollView setContentSize:temp];
}

-(void)keyboardWillHide {
    [_scrollView setContentSize:originalSize];
     [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [_txtEmail resignFirstResponder];
    [_txtAuthCode resignFirstResponder];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField == _txtEmail){
        [_txtAuthCode becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }    
    return YES;
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)sender {
    
    float offset = (_viewEmail.frame.origin.y) - (SCREEN_HEIGHT - 64 - 250) + _viewEmail.frame.size.height*2;
    if(offset > 0){
        [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
    _emailErrorToolTip.hidden = true;
    _authCodeErrorToolTip.hidden = true;
}


//------- Show Concierge button -----------
//---- return NO if you don't want to show it ----
//---- return YES if you want to show it -----------------
-(BOOL)isShowConciergeButton{
    return NO;
}
//--------------------------

-(void) collectErrorIcons{
    dictIconErrorManager = [NSMutableDictionary dictionary];
    [dictIconErrorManager setObject:_emailErrorToolTip forKey:[NSNumber numberWithInteger:_emailErrorToolTip.tag]];
    [dictIconErrorManager setObject:_authCodeErrorToolTip forKey:[NSNumber numberWithInteger:_authCodeErrorToolTip.tag]];
    for (ErrorToolTip *item in [dictIconErrorManager allValues]) {
        item.delegate = self;
    }
    
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextClicked:(id)sender {
    //send forgot password api
    Boolean valid = true;
    _emailErrorToolTip.hidden = true;
    _authCodeErrorToolTip.hidden = true;
    
    NSString* email = _txtEmail.text;
    if(email==nil || [email length]==0){
        valid = false;
        _emailErrorToolTip.hidden = false;
    } else if ( !checkEmailValid(email)){
        valid = false;
        _emailErrorToolTip.hidden = false;
        _emailErrorToolTip.errorMessage = NSLocalizedString(@"Please enter a valid email address", nil);
    }
    
    NSString* authCode = _txtAuthCode.text;
    if(authCode==nil || [authCode length]==0){
        valid = false;
        _authCodeErrorToolTip.hidden = false;
        _authCodeErrorToolTip.errorMessage = NSLocalizedString(@"Input authorization code", nil);
    } else if(![[AppConfig shared]  isConciergeCodeValid:authCode]){
        valid = false;
        _authCodeErrorToolTip.hidden = false;
        _authCodeErrorToolTip.errorMessage = NSLocalizedString(@"Authorization code is invalid", nil);
    }
    
    if(valid){
        [self showToast];
        [self forgotPassword:_txtEmail.text];
    }
}


-(void) forgotPassword:(NSString*) email{
    [self showToast];
    [[AppConfig shared] setConciergeCode:_txtAuthCode.text];
    wsForgotPassword = [[WSForgotPassword alloc] init];
    wsForgotPassword.delegate = self;
    [wsForgotPassword forgotPassword:email];
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    [self stopToast];
    [[AppConfig shared] reset];
    saveUserForgot([_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
    
    isForgotPwsswordSuccessful = YES;
    ConfirmForgotPasswordController* controller = (ConfirmForgotPasswordController*) [self controllerByIdentifier:@"ConfirmForgotPassword"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    [[AppConfig shared] reset];
    if(result!=nil && result.message!=nil){
        if([result.b2cErrorCode isEqualToString:@"ENR72-1"] || [result.b2cErrorCode isEqualToString:@"ENR008-3"]){
            OopsErrorPageController* controller = (OopsErrorPageController*) [self controllerByIdentifier:@"OopsErrorPageController"];
            controller.contentError =NSLocalizedString(@"That email address does not exist in our records. Please check if you have entered it correctly.", nil);
            [self.navigationController pushViewController:controller animated:YES];
        }else{
            OopsErrorPageController* controller = (OopsErrorPageController*) [self controllerByIdentifier:@"OopsErrorPageController"];
            controller.contentError = result.message;
            [self.navigationController pushViewController:controller animated:YES];
        }
    } else if (errorCode == kNO_INTERNET_CONNECTION){
        showAlertOneButton(self,ERROR_ALERT_TITLE,NSLocalizedString(@"There is no internet connection.", nil), @"OK");
    } else {
        OopsErrorPageController* controller = (OopsErrorPageController*) [self controllerByIdentifier:@"OopsErrorPageController"];
        controller.contentError =NSLocalizedString(@"That email address does not exist in our records. Please check if you have entered it correctly.", nil);
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
