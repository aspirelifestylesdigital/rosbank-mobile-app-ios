//
//  Common.h
//  Reservations
//
//  Created by Linh Le on 10/22/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum
{
    THREE_DOT_FIVE_INCH,
    FOUR_INCH,
    UNDEFINED
}ScreenSize;

typedef enum
{
    GET,
    POST,
    PUT
}MethodType;

enum EXPERIENCE_FILTER_TYPE
{
    FILTER_GOURMET = 1,
    FILTER_TRAVEL = 2,
    FILTER_ENTERTAIN = 3,
    FILTER_SLURGE = 4,
};

@interface Common : NSObject

extern int TABBAR_HEIGHT;
extern int TABBAR_WIDTH ;
extern int THREE_DOT_FIVE_INCH_VIEW_HEIGHT;
extern int FOUR_INCH_VIEW_HEIGHT;

extern int NUMBER_ROW_UPDATED;

extern ScreenSize screenSize;
extern ScreenSize getScreenSize();
extern NSString* urlencode(NSString* url);
extern NSString* getUniqueIdentifier();

// flattent html
extern NSString* flattenHTML(NSString *html);

UIImage* scaleImageWithSize(UIImage *imagetemp ,CGSize newSize);
// format date
extern NSString* formatDateToString(NSDate* date, NSString*format);
extern NSString* getDistanceDay(NSDate* starDay);

extern NSString* base64forData(NSData* theData);
extern NSString* getDistance(float item);
extern float iosVersion();

BOOL isSameDay(NSDate* date1, NSDate*date2);
extern UIImageView* createCircleMask(UIImageView *imageView,int radiusConner);
extern NSString* getMonthName(NSDate *date);
extern NSString* getMonthNamePromotion(NSDate *date);
extern CGSize getSizeWithFont(NSString *string,UIFont *font);
extern CGSize getSizeWithFontAndFrame(NSString *string,UIFont *font,int width,int height);
BOOL isTopCenterViewController(UINavigationController *centerNav,  Class checkedClass);

extern NSString* formatDate(NSDate* date, NSString *format);
extern NSString* stringByStrippingHTML(NSString* data);
extern NSInteger countRightPositionInScrollView(UIView* selectedView);
extern void resetScaleViewBaseOnScreen(UIView* view);
extern NSDate* convertStringToDate(NSString* dateString, NSString* dateFormat);
extern BOOL isEqualIgnoreCase(NSString* string1, NSString* string2);
extern BOOL isValidDateTimeWithin24h(NSDate* date, NSDate* time);
extern BOOL isValidDateTimeWithinHalfAnHour(NSDate* date, NSDate* time);
extern void displayError(UIViewController* controller, NSInteger errorcode );
extern NSDate* convertStringToDateWithTimeZone(NSString* dateString, NSString* dateFormat, NSString* timezone);
extern BOOL isWithin24h(NSDate* date1, NSDate* date2);
extern NSString* replaceFontForRussian(NSString* oldFont);
@end
