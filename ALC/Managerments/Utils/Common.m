//
//  Common.m
//  Reservations
//
//  Created by Linh Le on 10/22/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import "Common.h"
#import "NSString+MD5.h"
#import "Constant.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "NSString+HTML.h"
#import "GourmetRequestController.h"
@implementation Common


int TABBAR_HEIGHT = 51;
int TABBAR_WIDTH = 110;

int THREE_DOT_FIVE_INCH_VIEW_HEIGHT = 480;
int FOUR_INCH_VIEW_HEIGHT = 568;

int NUMBER_ROW_UPDATED = 20;

static NSArray *SHORT_MONTH;
ScreenSize screenSize = UNDEFINED;

ScreenSize getScreenSize()
{
    if (screenSize == UNDEFINED) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 568) {
            // code for 4-inch screen
            screenSize = FOUR_INCH;
        }
        else if(screenBounds.size.height == 480) {
            // code for 3.5-inch screen
            screenSize = THREE_DOT_FIVE_INCH;
        }        
    }
    
    return screenSize;
}

NSString* getDistance(float item)
{
    // Get distance
    NSString *result;
    
    if (item >= 1000)
    {
        result = [[NSString alloc] initWithFormat:@"%.1f km", item /1000 ] ;
    }
    else
    {
        int intDistance = roundf(item);
        result = [[NSString alloc] initWithFormat:@"%d m", intDistance] ;
        
    }

    return result;
}

NSString* urlencode(NSString* url)
{
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[url UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

NSString* getUniqueIdentifier()
{
    NSString *uDID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UDID"];
    if (uDID == nil) {
        // create UDID
        CFUUIDRef theUUID = CFUUIDCreate(NULL);
        CFStringRef string = CFUUIDCreateString(NULL, theUUID);
        CFRelease(theUUID);
        uDID = (__bridge  NSString *)string;
        // store to user default
        [[NSUserDefaults standardUserDefaults] setObject:uDID forKey:@"UDID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    return uDID;
}

NSString* getPhotoUrlBySizeAndSecret(NSString * oldUrl, int width, int height, NSString *secret)
{
    if (!oldUrl || !secret) {
        return nil;
    }
    
    NSString *baseUrl = [oldUrl stringByDeletingLastPathComponent];
    NSString *photoName = [oldUrl lastPathComponent];
    NSRange dotRange = [photoName rangeOfString:@"."];
    NSString *photoBaseName = [photoName substringToIndex:dotRange.location];
    NSString *photoExt = [photoName substringFromIndex:dotRange.location+1];

    NSString *photoCombineInfo = [NSString stringWithFormat:@"%@_%dx%d.%@%@", photoBaseName, width, height, photoExt, secret];
    NSString *sig = [[photoCombineInfo MD5] substringToIndex:10];
    
    return [NSString stringWithFormat:@"%@/%@_%dx%d_%@.%@", baseUrl, photoBaseName, width, height, sig, photoExt];
}


NSString* reverseString(NSString *inputString)
{
    int len =(int) [inputString length];
    NSMutableString *reverseName = [[NSMutableString alloc] initWithCapacity:len];
    for(int i=len-1;i>=0;i = i - 2)
    {
        [reverseName appendString:[NSString stringWithFormat:@"%c%c",[inputString characterAtIndex:i - 1], [inputString characterAtIndex:i]]];
    }
    return reverseName;
}

// flatent html
NSString* flattenHTML(NSString *html)
{
	
    NSScanner *theScanner;
    NSString *text = nil;
	
    theScanner = [NSScanner scannerWithString:html];
	
    while ([theScanner isAtEnd] == NO) {
		
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
		
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
		
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        
        
        html = [html stringByReplacingOccurrencesOfString:
				[ NSString stringWithFormat:@"%@>", text]
											   withString:@" "];
        
        
		
    }
    
    return html;
}

//scale image
UIImage* scaleImageWithSize(UIImage *imagetemp ,CGSize newSize)
{
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [imagetemp drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

NSString* formatDateToString(NSDate* date, NSString*format)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:date];
}

NSString* formatDate(NSDate* date, NSString *format){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:date];

}

NSString* formatTimeForSearch(NSDate *date){
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *time = [formatter stringFromDate:date];
    if(time.length > 0){
        NSString *firstChar = [time substringToIndex:1];
        if([firstChar isEqualToString:@"0"]){
            time = [time substringFromIndex:1];
        }
    }
    return time;
}

// return distance day
NSString* getDistanceDay(NSDate* starDay)
{
    int interval = [[NSDate date] timeIntervalSinceDate:starDay];
    
    NSString *timeUnit;
    int timeValue = 0;
    if(timeValue<0){
        timeValue = interval;
        timeUnit = @"just now";
    }else  if (interval < 60)
    {
        timeValue = interval;
        timeUnit = @"seconds";
    }
    else if (interval< 3600)
    {
        timeValue = round(interval / 60);
        
        if (timeValue == 1) {
            timeUnit = @"minute";
        } else {
            timeUnit = @"minutes";
        }
    }
    else if (interval< 86400) {
        timeValue = round(interval / 60 / 60);
        
        if (timeValue == 1) {
            timeUnit = @"hour";
            
        } else {
            timeUnit = @"hours";
        }
    }
    else if (interval < 604800)
    {
        int diff = round(interval / 60 / 60 / 24);
        if (diff == 1)
        {
            timeUnit = @"yesterday";
        }
        else if (diff == 7)
        {
            timeUnit = @"last week";
        }
        else
        {
            timeValue = diff;
            timeUnit = @"days";
        }
    }
    else
    {
        int days = round(interval / 60 / 60 / 24);
        
        if (days < 7) {
            
            timeValue = days;
            
            if (timeValue == 1) {
                timeUnit = @"day";
            } else {
                timeUnit = @"days";
            }
            
        } else if (days < 30) {
            int weeks = days / 7;
            
            timeValue = weeks;
            
            if (timeValue == 1) {
                timeUnit = @"week";
            } else {
                timeUnit = @"weeks";
            }
            
        } else if (days < 365) {
            
            int months = days / 30;
            timeValue = months;
            
            if (timeValue == 1) {
                timeUnit = @"month";
            } else {
                timeUnit = @"months";
            }
            
        } else if (days < 30000)
        {
            // this is roughly 82 years. After that, we'll say 'forever'
            int years = days / 365;
            timeValue = years;
            
            if (timeValue == 1) {
                timeUnit = @"year";
            } else {
                timeUnit = @"years";
            }
            
        } else {
            return @"forever ago";
        }
    }
    
    if(timeValue == 0)
    {
        return [NSString stringWithFormat:@"%@", timeUnit];
    }
    return [NSString stringWithFormat:@"%d %@ ago", timeValue, timeUnit];
}

// return 64 base encode
NSString* base64forData(NSData* theData)
{
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

float iosVersion()
{
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    float ver_float = [ver floatValue];
    return ver_float;
}

BOOL isSameDay(NSDate* date1, NSDate*date2)
{
    if (date1 == nil || date2 == nil) {
        return NO;
    }
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}


UIImageView* createCricleMask(UIImageView *img,int radiusConner)
{
    img.layer.cornerRadius = radiusConner;
    img.layer.masksToBounds = YES;
    return img;
}

NSString* getMonthName(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"JANUARY",@"FEBRUARY",@"MARCH",@"APRIL",@"MAY", @"JUNE", @"JULY", @"AUGUST",@"SEPTEMBER", @"OCTOBER",@"NOVEMBER", @"DECEMBER", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSLog(@"%@",[NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]]);
    return [NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]];
    
    
}
NSString* getMonthNamePromotion(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"January",@"February",@"March",@"April",@"May", @"June", @"July", @"August",@"September", @"October",@"November", @"December", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSLog(@"%@",[NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]]);
    return [NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]];
    
    
}

CGSize getSizeWithFont(NSString *string,UIFont *font)
{
    CGSize contentSize;
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,NSLineBreakByWordWrapping,NSParagraphStyleAttributeName, nil];
    
    CGRect rect = [string boundingRectWithSize:CGSizeMake(320, 999) options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil ];
    contentSize =rect.size;
    return contentSize;
}

CGSize getSizeWithFontAndFrame(NSString *string,UIFont *font,int width,int height)
{
    CGSize contentSize;
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,NSFontAttributeName,NSLineBreakByWordWrapping,NSParagraphStyleAttributeName, nil];
    
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width, height) options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil ];
    contentSize =rect.size;
    
    return contentSize;
}

BOOL isTopCenterViewController(UINavigationController *centerNav,  Class checkedClass){
    return [centerNav.topViewController isKindOfClass:checkedClass];
}


NSString* stringByStrippingHTML(NSString* data) {
    NSRange r;
    NSString *s = data;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound){
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    }
    
    s = [s stringByDecodingHTMLEntities];
    return s;
}

NSInteger countRightPositionInScrollView(UIView* selectedView){
    NSInteger result;
    if(selectedView==nil){
        return 0;
    }
    else if([selectedView isKindOfClass:[UIScrollView class]]){
        return 0;
    } else {
        return result = selectedView.frame.origin.y + countRightPositionInScrollView([selectedView superview]);
    }
}

void resetScaleViewBaseOnScreen(UIView* view){
    if(view == nil){
        return;
    }
    
    if([view isKindOfClass:[UITextField class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        UIFont* font = ((UITextField*) view).font;
        NSString* fontName = replaceFontForRussian(font.fontName);
        [((UITextField*) view) setFont:[UIFont fontWithName:fontName size:font.pointSize*SCREEN_SCALE]];
    } else if([view isKindOfClass:[UITextView class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        UIFont* font = ((UITextField*) view).font;
         NSString* fontName = replaceFontForRussian(font.fontName);
        [((UITextView*) view) setFont:[UIFont fontWithName:fontName size:font.pointSize*SCREEN_SCALE]];
    }else if([view isKindOfClass:[UIButton class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        UIFont* font = ((UIButton*) view).titleLabel.font;
         NSString* fontName = replaceFontForRussian(font.fontName);
        [((UIButton*) view).titleLabel setFont:[UIFont fontWithName:fontName size:font.pointSize*SCREEN_SCALE]];
        [((UIButton*) view) setTintColor:[UIColor clearColor]];
    }else if([view isKindOfClass:[UILabel class]]){
        UIFont* font = ((UITextField*) view).font;
         NSString* fontName = replaceFontForRussian(font.fontName);
        [((UILabel*) view) setFont:[UIFont fontWithName:fontName size:font.pointSize*SCREEN_SCALE]];
    }  else if(([view isKindOfClass:[UIView class]] || [view isKindOfClass:[UIScrollView class]] || [view isKindOfClass:[UIStackView class]])){
        for (UIView* sub in [view subviews]) {
            resetScaleViewBaseOnScreen(sub);
        }
    }
}

NSString* replaceFontForRussian(NSString* oldFont){
    if([oldFont isEqualToString:@"AvenirNext-Medium"]){
        return [oldFont stringByReplacingOccurrencesOfString:@"AvenirNext-Medium" withString:@"AvenirNextLTPro-Medium"];
    } else if([oldFont isEqualToString:@"AvenirNext-DemiBold"]) {
        return [oldFont stringByReplacingOccurrencesOfString:@"AvenirNext-DemiBold" withString:@"AvenirNextLTPro-Demi"];
    } else if([oldFont isEqualToString:@"AvenirNext-Bold"]) {
        return [oldFont stringByReplacingOccurrencesOfString:@"AvenirNext-Bold" withString:@"AvenirNextLTPro-Bold"];
    }
    return oldFont;
}

NSLayoutConstraint* getHeightConstranint(UIView* view){
    NSLayoutConstraint *heightConstraint;
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            return constraint;
        }
    }
    return heightConstraint;
}

NSDate* convertStringToDate(NSString* dateString, NSString* dateFormat){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter dateFromString:dateString];
}

NSDate* convertStringToDateWithTimeZone(NSString* dateString, NSString* dateFormat, NSString* timezone){
    if(dateString!=nil && dateString.length > 0){
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:timezone];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:timeZone];
        [dateFormatter setDateFormat:dateFormat];
        return [dateFormatter dateFromString:dateString];
    }
    return nil;
}

BOOL isEqualIgnoreCase(NSString* string1, NSString* string2){
    return [[string1 lowercaseString] isEqualToString:[string2 lowercaseString]];
}

BOOL isValidDateTimeWithin24h(NSDate* date, NSDate* time){
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:time];
    NSInteger hour = [components1 hour];
    NSInteger minute = [components1 minute];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
    [components setHour: hour];
    [components setMinute: minute];
    [components setSecond: 0];
    NSDate *fullReservation = [gregorian dateFromComponents: components];
    
    if([fullReservation compare:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]] == NSOrderedAscending){
        return NO;
    }
    return YES;
}
    
BOOL isValidDateTimeWithinHalfAnHour(NSDate* date, NSDate* time) {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:time];
    NSInteger hour = [components1 hour];
    NSInteger minute = [components1 minute];
        
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
    [components setHour: hour];
    [components setMinute: minute];
    [components setSecond: 0];
    NSDate *fullReservation = [gregorian dateFromComponents: components];
    
    if([fullReservation compare:[[NSDate date] dateByAddingTimeInterval:60*30]] == NSOrderedAscending){
        return NO;
    }
    return YES;
}

BOOL isWithin24h(NSDate* date1, NSDate* date2){
    if([date1 compare:[date2 dateByAddingTimeInterval:60*60*24*1]] == NSOrderedAscending){
        return YES;
    }
    return NO;
}


void displayError(UIViewController* controller, NSInteger errorcode ){
    NSString *message;
    switch (errorcode) {
        case 600:
            message = NSLocalizedString(@"User does not exist from the system or Restaurant does not exist", nil);
            break;
        case 602:
            message = NSLocalizedString(@"Booking successfully but unable to send mail", nil);
            break;
        case 500:
            message = NSLocalizedString(@"Unfortunately, a booking request that is less than 24 hours from reservation time is not allowed. Please call concierge for assistance", nil);
            break;
        case 501:
            message = NSLocalizedString(@"Unable to booking", nil);
            break;
        case 402:
            message = NSLocalizedString(@"Missing required data", nil);
            break;
        default:
            break;
    }
    
    if(message){
        showAlertOneButton(controller,ERROR_ALERT_TITLE, message, @"OK");
    }
}

@end
