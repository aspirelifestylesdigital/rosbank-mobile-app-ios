//
//  StoreUserData.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "StoreUserData.h"
#import "ALCQC-Swift.h"

#define EMAIL @"email"
#define USER_ID @"userId"
#define FIRST_NAME @"firstName"
#define LAST_NAME @"lastName"
#define FULL_NAME @"fullName"
#define CARD_ID @"cardID"
#define PHONE @"phone"
#define NAME_ON_CARD @"nameOnCard"
#define SALUSTATION @"Salustation"
#define AVATARRUL @"AvatarURL"
#define ONLINE_MEMBER_USER_ID @"onlineMemberUserId"
#define ONLINEMEMBER_DETAILID @"ONLINEMEMBERDETAILID"
#define VERIFICATION_CODE @"VERIFICATIONCODE"
#define ZIP_CODE @"ZIPCODE"
#define FORGOT_CACHE @"FORGOTCACHE"

@implementation StoreUserData

void storeUserDetails(UserObject* user){
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref setObject:user.userId forKey:USER_ID];
    [pref setObject:user.email forKey:EMAIL];
    [pref setObject:user.mobileNumber forKey:PHONE];
    [pref setObject:user.firstName forKey:FIRST_NAME];
    [pref setObject:user.lastName forKey:LAST_NAME];
    [pref setObject:user.memberCardId  forKey:CARD_ID];
    [pref setObject:user.fullName  forKey:FULL_NAME];
    [pref setObject:user.salutation  forKey:SALUSTATION];
    [pref setObject:user.avatarUrl  forKey:AVATARRUL];
    [pref setObject:user.ONLINEMEMBERDETAILID  forKey:ONLINEMEMBER_DETAILID];
    [pref setObject:user.VERIFICATIONCODE  forKey:VERIFICATION_CODE];
    //[pref setObject:user.zipCode  forKey:ZIP_CODE];
    //[pref setObject:user.password forKey:PASSWORD];
    
    //[pref setObject:user.nameOnCard forKey:NAME_ON_CARD];
    
    [pref synchronize];
}
void updateUserDetails(NSDictionary* user){
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref setObject:[user stringForKey:@"MobileNumber"] forKey:PHONE];
    [pref setObject:[user stringForKey:@"FirstName"] forKey:FIRST_NAME];
    [pref setObject:[user stringForKey:@"LastName"] forKey:LAST_NAME];
    [pref setObject:convertSalutationToRussian([user stringForKey:@"Salutation"])  forKey:SALUSTATION];
   // [pref setObject:[user stringForKey:@"PostalCode"]  forKey:ZIP_CODE];
    [pref setObject:[NSString stringWithFormat:@"%@ %@", [user stringForKey:@"FirstName"], [user stringForKey:@"LastName"]] forKey:FULL_NAME];
    [pref synchronize];
}
extern NSMutableDictionary* getUserDetailsObj(){
    NSMutableDictionary* dict= [[NSMutableDictionary alloc] init];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    
    [dict setObject:[pref objectForKey:ONLINEMEMBER_DETAILID] forKey:@"OnlineMemberDetailId"];
    [dict setObject:[AppConfig shared].verificationCode forKey:@"VerificationCode"];
    
    return dict;
}

UserObject* getUser(){
    UserObject* user = [[UserObject alloc] init];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    user.userId = [pref objectForKey:USER_ID];
    user.email = [pref objectForKey:EMAIL];
    user.lastName = [pref objectForKey:LAST_NAME];
    user.firstName = [pref objectForKey:FIRST_NAME];
    user.mobileNumber = [pref objectForKey:PHONE];
    user.memberCardId = [pref objectForKey:CARD_ID];
    user.fullName = [pref objectForKey:FULL_NAME];
    user.salutation = [pref objectForKey:SALUSTATION];
    user.avatarUrl = [pref objectForKey:AVATARRUL];
    //user.zipCode = [pref objectForKey:ZIP_CODE];
    user.ONLINEMEMBERDETAILID = [pref objectForKey:ONLINEMEMBER_DETAILID];
    user.VERIFICATIONCODE = [pref objectForKey:VERIFICATION_CODE];
    [user isValidMobileNumber:user.mobileNumber];
    
    return user;
}

void saveUserEmail(NSString* email){
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref setObject:email forKey:EMAIL];
    [pref synchronize];
}


BOOL isUserLoggedIn(){
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *userID = [pref objectForKey:USER_ID];
    if(userID!=nil && userID.length>0){
        return YES;
    }
    return NO;
}

void clearUser(){
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref removeObjectForKey:USER_ID];
    [pref removeObjectForKey:EMAIL];
    [pref removeObjectForKey:PHONE];
    [pref removeObjectForKey:FIRST_NAME];
    [pref removeObjectForKey:LAST_NAME];
    [pref removeObjectForKey:CARD_ID];
    [pref removeObjectForKey:FULL_NAME];
    [pref removeObjectForKey:SALUSTATION];
    [pref removeObjectForKey:AVATARRUL];
    [pref removeObjectForKey:ONLINEMEMBER_DETAILID];
    [pref removeObjectForKey:VERIFICATION_CODE];
    [pref synchronize];
}

void    storeUserOnlineMemberId(NSString* onlineMemberId){
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdele.ONLINE_MEMBER_ID = onlineMemberId;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref setObject:onlineMemberId forKey:USER_ID];
    [pref synchronize];
}
void saveUserForgot(NSString* email){
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdele.FORGET_PASS_FROM_USER = email;
}

void removeUserForgot(){
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdele.HAS_FORGOT_PASSWORD = false;
}

void setHasForgotPassword(BOOL isForgot){
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdele.HAS_FORGOT_PASSWORD = isForgot;
}

BOOL isUserForgot(){
    if(isUserLoggedIn()){
        //UserObject* user = getUser();
        
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        if (appdele.HAS_FORGOT_PASSWORD) {
//        if(appdele.FORGET_PASS_FROM_USER !=nil && appdele.FORGET_PASS_FROM_USER .length>0 && [appdele.FORGET_PASS_FROM_USER  isEqualToString:user.email]){
            return YES;
        }
        return NO;
    }
    return NO;
}
@end
