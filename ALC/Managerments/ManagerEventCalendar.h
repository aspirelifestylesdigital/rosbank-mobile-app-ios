//
//  ManagerEventCalendar.h
//  ALC
//
//  Created by Hai NguyenV on 10/20/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ManagerEventCalendar : NSObject


+ (ManagerEventCalendar*)instance;

-(void)removeEventFromCalendar:(NSString*)identifier;
-(void)addEventoCalendar:(NSString*)bookingID withTitle:(NSString*)title withDate:(NSDate*)startDate;
-(void)saveEventId:(NSString*)bookingID withID:(NSString*)eventID;
-(NSArray*)getListEvent;
-(NSString*)getIdentifyByBookingID:(NSString*)bookingID;
@end
