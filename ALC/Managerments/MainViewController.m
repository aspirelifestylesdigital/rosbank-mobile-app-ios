//
//  MainViewController.m
//  ALC
//
//  Created by Hai NguyenV on 8/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "SlideNavigationController.h"
#import "HambugerController.h"
#import "HomeViewController.h"
#import "ChatBotViewController.h"
#import <sys/sysctl.h>
#import "ServiceLandingController.h"
#import "InAppBrowserViewController.h"
#import "ALCQC-Swift.h"

@interface MainViewController (){
    BOOL didUpdateLayout;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
//    if([appdele isLoggedIn] == false){
//        [[SlideNavigationController sharedInstance]  closeMenuWithCompletion:^{
//            UIAlertAction *signInAction = [UIAlertAction actionWithTitle:SIGN_IN
//                                                                   style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {setRootViewLogout();
//                                                                   }];
//            showAlertTopOpenLogin([SlideNavigationController sharedInstance] , signInAction);            }];
//    }
//
    didUpdateLayout = NO;
    if(!didUpdateLayout){
        didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self.view);
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), @"OK");
    }
    
    /*
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }*/
    [self setTextForViews];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication].keyWindow setNeedsLayout];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    
    
    if([self isShowConciergeButton])
        [self createConciergeButton];
    else
    {
        appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        if(appdelegate.socialMenu != nil)
        {
            appdelegate.socialView.hidden = YES;
            appdelegate.socialButton.hidden = YES;
        }
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self stopToast];
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"Dealloc View Controller: %@", NSStringFromClass(self.class));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)customButtonBack{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 9, 15)];
    [leftButton setImage:[UIImage imageNamed:@BACK_IMAGE]  forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(buttonBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
}

- (void) buttonBack{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addTitleToHeader:(NSString*)titleString withColor:(UIColor*)color{
    
    titleString = [titleString uppercaseString];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 210.0*SCREEN_SCALE, 45.0)];
    
    [button setBackgroundColor:[UIColor clearColor]];
    [button setTitle:titleString forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    button.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    button.titleLabel.font = [UIFont fontWithName:@"AvenirNextLTPro-Demi" size:17*SCREEN_SCALE];
    [button setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    self.navigationItem.titleView = (UIView*)button;
    [button addTarget:self action:@selector(scrollToTop) forControlEvents:UIControlEventTouchUpInside];
}
-(void)addTitleToHeader:(NSString*)titleString
{
    titleString = [titleString uppercaseString];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 220.0, 45.0)];
    
    [button setBackgroundColor:[UIColor clearColor]];
    [button setTitle:titleString forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    button.titleLabel.font = [UIFont fontWithName:@"AvenirNextLTPro-Demi" size:17*SCREEN_SCALE];
    [button setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    self.navigationItem.titleView = (UIView*)button;
    [button addTarget:self action:@selector(scrollToTop) forControlEvents:UIControlEventTouchUpInside];
}

- (void) scrollToTop{
    
}

//------- Show Concierge button -----------
//---- return NO if you don't want to show it ----
//---- return YES if you want to show it -----------------
-(BOOL)isShowConciergeButton{
    return YES;
}
//-------------------End--------

- (NSString*) imageBackGroundForToolTip
{
    //    size_t size;
    //    int temp = sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    //    char *machine = malloc(size);
    //    temp = sysctlbyname("hw.machine", machine, &size, NULL, 0);
    //    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    
    CGFloat deviceWidth = [UIScreen mainScreen].bounds.size.width;
    NSString* imageName = @"iphone6plus_tooltip.png";
    if (deviceWidth == 320)
    {
        imageName = @"iphone5_tooltip.png";
    }
    else if (deviceWidth == 375)
    {
        imageName = @"iphone6s_tooltip.png";
    }
    else if (deviceWidth == 414)
    {
        imageName = @"iphone6plus_tooltip.png";
    }
    //    else if ([typeDevice rangeOfString:@"iPhone 6"].location != NSNotFound)
    //    {
    //        imageName = @"iphone6s_tooltip.png";
    //    }
    //    else if ([typeDevice rangeOfString:@"iPhone 5s"].location != NSNotFound)
    //    {
    //        imageName = @"iphone5_tooltip.png";
    //    }
    //    else if ([typeDevice rangeOfString:@"iPhone 5"].location != NSNotFound)
    //    {
    //        imageName = @"iphone5_tooltip.png";
    //    }
    return imageName;
}

-(void)createConciergeButton{
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if(appdelegate.socialMenu == nil)
    {
        appdelegate.socialMenu = [[ALRadialMenu alloc] init];
        appdelegate.socialMenu.delegate = self;
        appdelegate.socialMenu.fadeItems = YES;
        
        appdelegate.socialView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height)];
        appdelegate.socialView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.7f];
        
        appdelegate.socialView.hidden = YES;
        [self.navigationController.view addSubview:appdelegate.socialView];
        
        
        appdelegate.imvBackGround = [[UIImageView alloc] initWithFrame:appdelegate.socialView.frame];
        [ appdelegate.imvBackGround setImage:[UIImage imageNamed:[self imageBackGroundForToolTip]]];
        [appdelegate.socialView addSubview: appdelegate.imvBackGround];
        
        /*NSString* flagShow = [self getObjectFromUserDefaultsForKey:KEY_SHOWTOOLTIP];
         if (!flagShow)
         {
         [self setObjectToUserDefaults:@"1" inUserDefaultsForKey:KEY_SHOWTOOLTIP];
         }
         else if ([flagShow isEqualToString:@"0"])
         {
         [appdelegate.imvBackGround setHidden:YES];
         }*/
        
        int buttonSize  = 45*SCREEN_SCALE;
        CGFloat bottomPadding = 0;
        if ([UIDevice currentDevice].systemVersion.floatValue >= 11){
            bottomPadding = self.navigationController.view.safeAreaInsets.bottom;
        }

        appdelegate.socialButton  = [[UIButton alloc] initWithFrame:CGRectMake(self.navigationController.view.frame.size.width-buttonSize-15, self.navigationController.view.frame.size.height-buttonSize-15-bottomPadding, buttonSize, buttonSize)];
        [appdelegate.socialButton setImage:[UIImage imageNamed:@"icon_concierge"] forState:UIControlStateNormal];
        [appdelegate.socialButton addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
        [appdelegate.socialButton addTarget:self action:@selector(multipleTap:withEvent:)
                           forControlEvents:UIControlEventTouchDownRepeat];
        
        [self.navigationController.view addSubview:appdelegate.socialButton];
        [self.navigationController.view bringSubviewToFront:appdelegate.socialButton];        
    }
    else
    {
        [appdelegate.socialButton setHidden:NO];
    }
}
-(void)showMenu:(id)sender
{
    [appdelegate.socialMenu buttonsWillAnimateFromButton:sender withFrame:appdelegate.socialButton.frame inView:self.navigationController.view];
}

-(void)showToast{
    [CSToastManager setTapToDismissEnabled:NO];
    [self.view makeToastActivity:CSToastPositionCenter];
    [self.navigationController.navigationBar setUserInteractionEnabled:NO];
    [self.view setUserInteractionEnabled:NO];
    
}
-(void)stopToast{
    [self.view hideToastActivity];
    [self.navigationController.navigationBar setUserInteractionEnabled:YES];
    [self.view setUserInteractionEnabled:YES];
}

- (UIViewController*) controllerByIdentifier:(NSString*) identifier {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    return [mainStoryboard instantiateViewControllerWithIdentifier:identifier];
}

-(void)setShowSplashScreen{
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref setObject:@"0" forKey:FLAG_STARTED];
    [pref synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UINavigationController *navigation = (UINavigationController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"BeginNavigationBar"];
    
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdele.socialMenu = nil;
    [UIView transitionWithView:appdele.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        UIApplication.sharedApplication.delegate.window.rootViewController = navigation;
                        
                    }
                    completion:nil];
}

-(void)setRootViewAfterLogin{
    AppDelegate* appdelet=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [appdelet setRootViewAfterLogin:true];
}


#pragma mark - radial menu delegate methods
- (NSInteger) numberOfItemsInRadialMenu:(ALRadialMenu *)radialMenu {
    if (radialMenu == appdelegate.socialMenu) {
        return 3;
    }
    return 0;
}


- (NSInteger) arcSizeForRadialMenu:(ALRadialMenu *)radialMenu {
    if (radialMenu == appdelegate.socialMenu) {
        return 80;
    }
    return 0;
}

- (NSInteger) arcRadiusForRadialMenu:(ALRadialMenu *)radialMenu {
    if (radialMenu == appdelegate.socialMenu) {
        return 90;
    }
    return 0;
}

- (float) buttonSizeForRadialMenu:(ALRadialMenu *)radialMenu{
    return 45;
}

- (NSInteger) arcStartForRadialMenu:(ALRadialMenu *)radialMenu{
    appdelegate.socialView.hidden = NO;
    if (radialMenu == appdelegate.socialMenu) {
        return 190;
    }
    return 0;
}

- (ALRadialButton *) radialMenu:(ALRadialMenu *)radialMenu buttonForIndex:(NSInteger)index {
    ALRadialButton *button = [[ALRadialButton alloc] init];
    if (radialMenu == appdelegate.socialMenu) {
        if (index == 1) {
            [button setImage:[UIImage imageNamed:@"icon_call"] forState:UIControlStateNormal];
        }
        else if (index == 2) {
            [button setImage:[UIImage imageNamed:@"icon_livechat"] forState:UIControlStateNormal];
        }
        else if (index == 3) {
            [button setImage:[UIImage imageNamed:@"icon_plan"] forState:UIControlStateNormal];
        }
    }
    
    if (button.imageView.image) {
        return button;
    }
    
    return nil;
}

- (void) radialMenu:(ALRadialMenu *)radialMenu didSelectItemAtIndex:(NSInteger)index {
    if (radialMenu == appdelegate.socialMenu) {
        appdelegate.socialView.hidden = YES;
        [appdelegate.socialMenu itemsWillDisapearIntoButton:appdelegate.socialButton];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        if([appdelegate isLoggedIn]){
            if (index == 1) {
                trackGAICategory(@"Concierge",@"Call concierge",nil,nil);
                NSString *phoneNumber = [AppConfig shared].companyPhoneNumber;
                NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
                NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
                
                if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
                    [UIApplication.sharedApplication openURL:phoneUrl];
                } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
                    [UIApplication.sharedApplication openURL:phoneFallbackUrl];
                } else {
                    // Show an error message: Your device can not do phone calls.
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@""
                                                  message:@"Your device can not do phone calls."
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"Close",nil)
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             //Do some thing here
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else if (index == 2) {
                NSLog(@"LivePerson chat");
                for (UIViewController* itemViewController in self.navigationController.viewControllers)
                {
                    if ([itemViewController isKindOfClass:[ConversationViewController class]])
                    {
                        [topViewController().navigationController popToViewController:itemViewController animated:YES];
                        return;
                        
                    }
                }
                if (isNetworkAvailable())
                {
                    [appdelegate showLivePersonChatScreen];
                }
                else
                {
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@""
                                                  message:@"Please check your internet"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"Close"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             //Do some thing here
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else if (index == 3) {
                trackGAICategory(@"Concierge",@"Send request",nil,nil);
                ServiceLandingController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceLandingController"];
                
                [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                                         withSlideOutAnimation:YES
                                                                                 andCompletion:nil];
            }
        } else {            
            [[SlideNavigationController sharedInstance]  closeMenuWithCompletion:^{
                UIAlertAction *signInAction = [UIAlertAction actionWithTitle:SIGN_IN
                                                                       style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                                           setRootViewLogout();
                                                                       }];
                showAlertTopOpenLogin([SlideNavigationController sharedInstance] , signInAction);}];
        }
    }
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsAppear:(UIButton *)button {
    appdelegate.socialView.hidden = NO;
    [self showToolTip];
    return NO;
}

- (BOOL)radialMenu:(ALRadialMenu *)radialMenu shouldRotateMenuButtonWhenItemsDisappear:(UIButton *)button {
    appdelegate.socialView.hidden = YES;
    [self showToolTip];
    return NO;
}

- (void) showToolTip{
    if (![appdelegate.socialView isHidden])
    {
        NSString* flagShow = [self getObjectFromUserDefaultsForKey:KEY_SHOWTOOLTIP];
        if (![flagShow isEqualToString:@"0"])
        {
            [self setObjectToUserDefaults:@"0" inUserDefaultsForKey:KEY_SHOWTOOLTIP];
            [appdelegate.imvBackGround setHidden:NO];
        } else {
            [appdelegate.imvBackGround setHidden:YES];
        }
    }
    else
    {
        NSString* flagShow = [self getObjectFromUserDefaultsForKey:KEY_SHOWTOOLTIP];
        if ([flagShow isEqualToString:@"0"])
        {
            [appdelegate.imvBackGround setHidden:YES];
        }
    }
    
}

#pragma mark ------ Slide menu setup
#pragma mark Show Left-Right Menu
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
    return NO;
}
- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return YES;
}

#pragma mark - HUy Truong - Edit Show Menu When Shake Device
-(BOOL)canBecomeFirstResponder
{
    if ([self isShowConciergeButton])
    {
        return YES;
    }
    return NO;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (motion == UIEventSubtypeMotionShake)
    {
        if ([self isShowConciergeButton])
        {
            [self showMenu:appdelegate.socialButton];
        }
    }
}

#pragma mark Huy Truong - Check Show Tool Tip
- (void)setObjectToUserDefaults:(id)object inUserDefaultsForKey:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (id)getObjectFromUserDefaultsForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

#pragma mark Huy Truong Mail Delegat

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)multipleTap:(id)sender withEvent:(UIEvent*)event
{
    UITouch* touch = [[event allTouches] anyObject];
    if (touch.tapCount == 2)
    {
        // do action.
        [self showMenu:sender];
        [self setObjectToUserDefaults:@"1" inUserDefaultsForKey:KEY_SHOWTOOLTIP];
        
    }
}

-(void) showConciergeButtonToolTip{
    [self showMenu:appdelegate.socialButton];
}

-(void) trackEcommerceProduct:(NSString*)name withCate:(NSString*)category{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    GAIEcommerceProduct *product = [[GAIEcommerceProduct alloc] init];
    [product setName:name];
    [product setCategory:category];
    [product setBrand:@"Aspire"];
    [product setQuantity:@1];
    
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:@"Ecommerce"
                                                                           action:@"Checkout"
                                                                            label:nil
                                                                            value:nil];
    
    // Add the step number and additional info about the checkout to the action.
    GAIEcommerceProductAction *action = [[GAIEcommerceProductAction alloc] init];
    [action setAction:kGAIPACheckout];
    [action setCheckoutStep:@1];
    
    [builder addProduct:product];
    [builder setProductAction:action];
    [tracker send:[builder build]];
    
}


- (void) trackCategory:(NSString*)category action:(NSString*)action label:(NSString*)label value:(NSNumber*)value{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:value] build]];
}

-(void) setTextForViews
{
    
}

-(NSRange) rangeAsteriskInString:(NSString *)text
{
    return [text rangeOfString:@"*"];
}
-(NSAttributedString *) setStyleForString:(NSString *)text
{
    NSMutableAttributedString* aString =
    [[NSMutableAttributedString alloc] initWithString:text];
    
    [aString setAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]}
                     range:[self rangeAsteriskInString:text]];
    [aString enumerateAttributesInRange:(NSRange){0,aString.length}
                                options:0
                             usingBlock:
     ^(NSDictionary* attrs, NSRange range, BOOL *stop) {
         
         //unspecific: don't change text color if ANY attributes are set
         /*
          if ([[attrs allKeys] count]==0)
          [aString addAttribute:NSForegroundColorAttributeName
          value:[UIColor colorWithRed:(104.0/255.0) green:(104.0/255.0) blue:(104.0/255.0) alpha:1.0]
          range:range];
          */
         //specific: don't change text color if text color attribute is already set
         if (![[attrs allKeys] containsObject:NSForegroundColorAttributeName])
             [aString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor colorWithRed:(104.0/255.0) green:(104.0/255.0) blue:(104.0/255.0) alpha:1.0]
                             range:range];
         
     }];
    return aString;
}
@end
