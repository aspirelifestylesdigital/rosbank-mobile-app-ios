//
//  CommonUtils.h
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CommonUtils : NSObject
extern BOOL checkPasswordValid(NSString* password);
extern BOOL checkEmailValid(NSString* email);
extern BOOL checkPhoneValid(NSString* email);
extern NSArray* getSalutationList();
extern NSArray* getCCNumberList();
extern void showAlertOneButton(UIViewController* controller ,NSString* title, NSString* message, NSString* buttonName);
extern NSString* convertArrayToString(NSArray* array);
extern NSArray* convertStringToArray(NSString* value);
extern Boolean showAlertTopOpenLogin(UIViewController* controller, UIAlertAction *signInAction);
extern Boolean showAlertTopOpenLoginInRequest(UIViewController* controller, UIAlertAction *signInAction, UIAlertAction *cancelAction);
extern void callAPINoHeaderForGET(NSString* endpoint ,NSMutableDictionary* data ,id target, SEL successAction ,SEL failAction);
extern void callAPINoHeaderForGET(NSString* endpoint ,NSMutableDictionary* data ,id target, SEL successAction ,SEL failAction);
extern NSString* stringFromObject(id object);
#pragma mark - Get Top Controller

extern UIViewController* topViewController();
extern UIViewController* topViewControllerWithRootViewController(UIViewController* rootViewController);
extern NSString* platformType(NSString *platform);
extern BOOL isNetworkAvailable();
extern NSArray* getNormalCurrencyList();
extern NSArray* getPopularCurrencyList();
extern NSMutableDictionary* createCommonBookingObject(NSString* requestType, NSString* funtionality);
extern NSString* convertRequestDetailsToString(NSMutableDictionary* dict);
extern NSDictionary* convertRequestDetailsStringToDict(NSString* requestDetails);
extern void setRootViewLogout();
extern NSString* removeSpecialCharacters(NSString* value);
extern NSMutableDictionary* removeSpecialCharactersfromDict(NSMutableDictionary* dict);
extern BOOL isValidValue(NSString* text);
extern BOOL isNotIncludedSpecialCharacters(NSString* text);
extern void trackGAICategory(NSString* category, NSString* action, NSString* label,NSNumber* value);
extern NSString* convertSalutationToRussian(NSString* enValue);
extern NSString* convertSalutationToEnglish(NSString* ruValue);
extern NSString* getCountryNameByCode(NSString* code);
extern NSString* getCountryCodeByName(NSString* name);
@end
