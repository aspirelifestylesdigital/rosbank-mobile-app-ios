//
//  CommonUtils.m
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CommonUtils.h"
#import "CCNumber.h"
#import "MainViewController.h"
#import <sys/sysctl.h>
#import "Reachability.h"
#import "CurrencyObject.h"
#import "ALCQC-Swift.h"

#define SPECIAL_CHARACTER @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*()-+_=[]:;\",.?/ "

NSArray *countries = nil;

@implementation CommonUtils

NSString* convertArrayToString(NSArray* array){
    NSMutableString *result = [NSMutableString string];
    if(array!=nil && array.count>0){
        for (int i = 0; i < array.count - 1; i++) {
            [result appendString:[array objectAtIndex:i]];
            [result appendString:@", "];
        }
        [result appendString:[array objectAtIndex:array.count-1]];
    }
    return result;
}

NSArray* convertStringToArray(NSString* value){
    NSMutableArray *result = [NSMutableArray array];
    if(value && value.length>0){
        NSArray* splitted = [value componentsSeparatedByString:@","];
        for (NSString* item in splitted) {
            if(item){
                [result addObject:[item stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            }
        }
    }
    return result;
}

BOOL checkEmailValid(NSString* email)
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

BOOL checkPasswordValid(NSString* password)
{
    BOOL valid = NO;
    if([password length]<1)
    {
        valid = NO;
    }
    else
    {
        /*NSMutableString *stringPassWord=[[NSMutableString alloc] initWithFormat:@"%@",password];
         int nCount=0;
         for(int i=0;i<[stringPassWord length];i++)
         {
         
         NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
         NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:[stringPassWord substringWithRange:NSMakeRange(i, 1)]];
         valid = [alphaNums isSupersetOfSet:inStringSet];
         if (valid)
         {
         nCount +=1;
         }
         
         }
         if(nCount >= 2)*/
        valid = YES;
    }
    return valid;
}

BOOL checkPhoneValid(NSString* password){
    BOOL valid = NO;
    if([password length]<4)
    {
        valid = NO;
    }
    else
    {
        valid = YES;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [password length]; i++)
        {
            unichar c = [password characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                valid = NO;
            }
        }
        
    }
    return valid;
}

NSArray* getSalutationList(){
    NSArray *data = [NSArray arrayWithObjects:
                     [[CCNumber alloc] init:NSLocalizedString(@"Ms", nil) withDescription:NSLocalizedString(@"Ms", nil)],
                     //[[CCNumber alloc] init:NSLocalizedString(@"Mr.", nil) withDescription:NSLocalizedString(@"Mr.", nil)],
                     [[CCNumber alloc] init:NSLocalizedString(@"Mrs", nil) withDescription:NSLocalizedString(@"Mrs", nil)],
                     //[[CCNumber alloc] init:NSLocalizedString(@"Miss", nil) withDescription:NSLocalizedString(@"Miss", nil)],
                     //[[CCNumber alloc] init:NSLocalizedString(@"Master", nil) withDescription:NSLocalizedString(@"Master", nil)],
                     //[[CCNumber alloc] init:NSLocalizedString(@"Dr.(Mr)", nil) withDescription:NSLocalizedString(@"Dr.(Mr)", nil)],
                    // [[CCNumber alloc] init:NSLocalizedString(@"Dr.(Mrs)", nil) withDescription:NSLocalizedString(@"Dr.(Mrs)", nil)],
                    // [[CCNumber alloc] init:NSLocalizedString(@"Dr.(Ms)", nil) withDescription:NSLocalizedString(@"Dr.(Ms)", nil)],
                    // [[CCNumber alloc] init:NSLocalizedString(@"Dr.(Miss)", nil) withDescription:NSLocalizedString(@"Dr.(Miss)", nil)],
                     nil];
    
    return data;
}

NSArray* getCCNumberList(){
    NSArray *data = [NSArray arrayWithObjects:
                     [[CCNumber alloc] init:@"+853" withDescription:NSLocalizedString(@"Macao (853)",nil)],
                     [[CCNumber alloc] init:@"+680" withDescription:NSLocalizedString(@"Palau (680)",nil)],
                     [[CCNumber alloc] init:@"+61" withDescription:NSLocalizedString(@"Australia (61)",nil)],
                     [[CCNumber alloc] init:@"+43" withDescription:NSLocalizedString(@"Austria (43)",nil)],
                     [[CCNumber alloc] init:@"+994" withDescription:NSLocalizedString(@"Azerbaijan (994)",nil)],
                     [[CCNumber alloc] init:@"+355" withDescription:NSLocalizedString(@"Albania (355)",nil)],
                     [[CCNumber alloc] init:@"+213" withDescription:NSLocalizedString(@"Algeria (213)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"American Samoa (1)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Anguilla (1)",nil)],
                     [[CCNumber alloc] init:@"+244" withDescription:NSLocalizedString(@"Angola (244)",nil)],
                     [[CCNumber alloc] init:@"+376" withDescription:NSLocalizedString(@"Andorra (376)",nil)],
                     [[CCNumber alloc] init:@"+672" withDescription:NSLocalizedString(@"Antarctica (672)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Antigua and Barbuda (1)",nil)],
                     [[CCNumber alloc] init:@"+54" withDescription:NSLocalizedString(@"Argentina (54)",nil)],
                     [[CCNumber alloc] init:@"+374" withDescription:NSLocalizedString(@"Armenia (374)",nil)],
                     [[CCNumber alloc] init:@"+297" withDescription:NSLocalizedString(@"Aruba (297)",nil)],
                     [[CCNumber alloc] init:@"+93" withDescription:NSLocalizedString(@"Afghanistan (93)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Bahamas (1)",nil)],
                     [[CCNumber alloc] init:@"+880" withDescription:NSLocalizedString(@"Bangladesh (880)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Barbados (1)",nil)],
                     [[CCNumber alloc] init:@"+973" withDescription:NSLocalizedString(@"Bahrain (973)",nil)],
                     [[CCNumber alloc] init:@"+375" withDescription:NSLocalizedString(@"Belarus (375)",nil)],
                     [[CCNumber alloc] init:@"+501" withDescription:NSLocalizedString(@"Belize (501)",nil)],
                     [[CCNumber alloc] init:@"+32" withDescription:NSLocalizedString(@"Belgium (32)",nil)],
                     [[CCNumber alloc] init:@"+229" withDescription:NSLocalizedString(@"Benin (229)",nil)],
                     [[CCNumber alloc] init:@"+225" withDescription:NSLocalizedString(@"Cote D'Ivoire (225)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Bermuda (1)",nil)],
                     [[CCNumber alloc] init:@"+359" withDescription:NSLocalizedString(@"Bulgaria (359)",nil)],
                     [[CCNumber alloc] init:@"+591" withDescription:NSLocalizedString(@"Bolivia (591)",nil)],
                     [[CCNumber alloc] init:@"+387" withDescription:NSLocalizedString(@"Bosnia and Herzegovina (387)",nil)],
                     [[CCNumber alloc] init:@"+267" withDescription:NSLocalizedString(@"Botswana (267)",nil)],
                     [[CCNumber alloc] init:@"+55" withDescription:NSLocalizedString(@"Brazil (55)",nil)],
                     [[CCNumber alloc] init:@"+246" withDescription:NSLocalizedString(@"British Indian Ocean Territory (246)",nil)],
                     [[CCNumber alloc] init:@"+673" withDescription:NSLocalizedString(@"Brunei Darussalam (673)",nil)],
                     [[CCNumber alloc] init:@"+226" withDescription:NSLocalizedString(@"Burkina Faso (226)",nil)],
                     [[CCNumber alloc] init:@"+257" withDescription:NSLocalizedString(@"Burundi (257)",nil)],
                     [[CCNumber alloc] init:@"+975" withDescription:NSLocalizedString(@"Bhutan (975)",nil)],
                     [[CCNumber alloc] init:@"+678" withDescription:NSLocalizedString(@"Vanuatu (678)",nil)],
                     [[CCNumber alloc] init:@"+44" withDescription:NSLocalizedString(@"United Kingdom (44)",nil)],
                     [[CCNumber alloc] init:@"+36" withDescription:NSLocalizedString(@"Hungary (36)",nil)],
                     [[CCNumber alloc] init:@"+58" withDescription:NSLocalizedString(@"Venezuela (58)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Virgin Islands, British (1)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Virgin Islands, U.s. (1)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"United States Minor Outlying Islands (1)",nil)],
                     [[CCNumber alloc] init:@"+84" withDescription:NSLocalizedString(@"Viet Nam (84)",nil)],
                     [[CCNumber alloc] init:@"+241" withDescription:NSLocalizedString(@"Gabon (241)",nil)],
                     [[CCNumber alloc] init:@"+509" withDescription:NSLocalizedString(@"Haiti (509)",nil)],
                     [[CCNumber alloc] init:@"+592" withDescription:NSLocalizedString(@"Guyana (592)",nil)],
                     [[CCNumber alloc] init:@"+220" withDescription:NSLocalizedString(@"Gambia (220)",nil)],
                     [[CCNumber alloc] init:@"+233" withDescription:NSLocalizedString(@"Ghana (233)",nil)],
                     [[CCNumber alloc] init:@"+590" withDescription:NSLocalizedString(@"Guadeloupe (590)",nil)],
                     [[CCNumber alloc] init:@"+502" withDescription:NSLocalizedString(@"Guatemala (502)",nil)],
                     [[CCNumber alloc] init:@"+224" withDescription:NSLocalizedString(@"Guinea (224)",nil)],
                     [[CCNumber alloc] init:@"+245" withDescription:NSLocalizedString(@"Guinea-Bissau (245)",nil)],
                     [[CCNumber alloc] init:@"+49" withDescription:NSLocalizedString(@"Germany (49)",nil)],
                     [[CCNumber alloc] init:@"+350" withDescription:NSLocalizedString(@"Gibraltar (350)",nil)],
                     [[CCNumber alloc] init:@"+504" withDescription:NSLocalizedString(@"Honduras (504)",nil)],
                     [[CCNumber alloc] init:@"+852" withDescription:NSLocalizedString(@"Hong Kong (852)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Grenada (1)",nil)],
                     [[CCNumber alloc] init:@"+299" withDescription:NSLocalizedString(@"Greenland (299)",nil)],
                     [[CCNumber alloc] init:@"+30" withDescription:NSLocalizedString(@"Greece (30)",nil)],
                     [[CCNumber alloc] init:@"+995" withDescription:NSLocalizedString(@"Georgia (995)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Guam (1)",nil)],
                     [[CCNumber alloc] init:@"+45" withDescription:NSLocalizedString(@"Denmark (45)",nil)],
                     [[CCNumber alloc] init:@"+253" withDescription:NSLocalizedString(@"Djibouti (253)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Dominica (1)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Dominican Republic (1)",nil)],
                     [[CCNumber alloc] init:@"+20" withDescription:NSLocalizedString(@"Egypt (20)",nil)],
                     [[CCNumber alloc] init:@"+260" withDescription:NSLocalizedString(@"Zambia (260)",nil)],
                     [[CCNumber alloc] init:@"+212" withDescription:NSLocalizedString(@"Western Sahara (212)",nil)],
                     [[CCNumber alloc] init:@"+263" withDescription:NSLocalizedString(@"Zimbabwe (263)",nil)],
                     [[CCNumber alloc] init:@"+972" withDescription:NSLocalizedString(@"Israel (972)",nil)],
                     [[CCNumber alloc] init:@"+91" withDescription:NSLocalizedString(@"India (91)",nil)],
                     [[CCNumber alloc] init:@"+62" withDescription:NSLocalizedString(@"Indonesia (62)",nil)],
                     [[CCNumber alloc] init:@"+962" withDescription:NSLocalizedString(@"Jordan (962)",nil)],
                     [[CCNumber alloc] init:@"+964" withDescription:NSLocalizedString(@"Iraq (964)",nil)],
                     [[CCNumber alloc] init:@"+98" withDescription:NSLocalizedString(@"Iran, Islamic Republic of (98)",nil)],
                     [[CCNumber alloc] init:@"+353" withDescription:NSLocalizedString(@"Ireland (353)",nil)],
                     [[CCNumber alloc] init:@"+354" withDescription:NSLocalizedString(@"Iceland (354)",nil)],
                     [[CCNumber alloc] init:@"+34" withDescription:NSLocalizedString(@"Spain (34)",nil)],
                     [[CCNumber alloc] init:@"+39" withDescription:NSLocalizedString(@"Italy (39)",nil)],
                     [[CCNumber alloc] init:@"+967" withDescription:NSLocalizedString(@"Yemen (967)",nil)],
                     [[CCNumber alloc] init:@"+238" withDescription:NSLocalizedString(@"Cape Verde (238)",nil)],
                     [[CCNumber alloc] init:@"+7" withDescription:NSLocalizedString(@"Kazakhstan (7)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Cayman Islands (1)",nil)],
                     [[CCNumber alloc] init:@"+855" withDescription:NSLocalizedString(@"Cambodia (855)",nil)],
                     [[CCNumber alloc] init:@"+237" withDescription:NSLocalizedString(@"Cameroon (237)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Canada (1)",nil)],
                     [[CCNumber alloc] init:@"+974" withDescription:NSLocalizedString(@"Qatar (974)",nil)],
                     [[CCNumber alloc] init:@"+254" withDescription:NSLocalizedString(@"Kenya (254)",nil)],
                     [[CCNumber alloc] init:@"+357" withDescription:NSLocalizedString(@"Cyprus (357)",nil)],
                     [[CCNumber alloc] init:@"+996" withDescription:NSLocalizedString(@"Kyrgyzstan (996)",nil)],
                     [[CCNumber alloc] init:@"+686" withDescription:NSLocalizedString(@"Kiribati (686)",nil)],
                     [[CCNumber alloc] init:@"+86" withDescription:NSLocalizedString(@"China (86)",nil)],
                     [[CCNumber alloc] init:@"+61" withDescription:NSLocalizedString(@"Cocos (Keeling) Islands (61)",nil)],
                     [[CCNumber alloc] init:@"+57" withDescription:NSLocalizedString(@"Colombia (57)",nil)],
                     [[CCNumber alloc] init:@"+269" withDescription:NSLocalizedString(@"Comoros (269)",nil)],
                     [[CCNumber alloc] init:@"+242" withDescription:NSLocalizedString(@"Congo (242)",nil)],
                     [[CCNumber alloc] init:@"+243" withDescription:NSLocalizedString(@"Congo, the Democratic Republic of the (243)",nil)],
                     [[CCNumber alloc] init:@"+850" withDescription:NSLocalizedString(@"Korea, Democratic People's Republic of (850)",nil)],
                     [[CCNumber alloc] init:@"+82" withDescription:NSLocalizedString(@"Korea, Republic of (82)",nil)],
                     [[CCNumber alloc] init:@"+506" withDescription:NSLocalizedString(@"Costa Rica (506)",nil)],
                     [[CCNumber alloc] init:@"+53" withDescription:NSLocalizedString(@"Cuba (53)",nil)],
                     [[CCNumber alloc] init:@"+965" withDescription:NSLocalizedString(@"Kuwait (965)",nil)],
                     [[CCNumber alloc] init:@"+856" withDescription:NSLocalizedString(@"Lao People's Democratic Republic (856)",nil)],
                     [[CCNumber alloc] init:@"+371" withDescription:NSLocalizedString(@"Latvia (371)",nil)],
                     [[CCNumber alloc] init:@"+266" withDescription:NSLocalizedString(@"Lesotho (266)",nil)],
                     [[CCNumber alloc] init:@"+231" withDescription:NSLocalizedString(@"Liberia (231)",nil)],
                     [[CCNumber alloc] init:@"+961" withDescription:NSLocalizedString(@"Lebanon (961)",nil)],
                     [[CCNumber alloc] init:@"+218" withDescription:NSLocalizedString(@"Libyan Arab Jamahiriya (218)",nil)],
                     [[CCNumber alloc] init:@"+370" withDescription:NSLocalizedString(@"Lithuania (370)",nil)],
                     [[CCNumber alloc] init:@"+423" withDescription:NSLocalizedString(@"Liechtenstein (423)",nil)],
                     [[CCNumber alloc] init:@"+352" withDescription:NSLocalizedString(@"Luxembourg (352)",nil)],
                     [[CCNumber alloc] init:@"+230" withDescription:NSLocalizedString(@"Mauritius (230)",nil)],
                     [[CCNumber alloc] init:@"+222" withDescription:NSLocalizedString(@"Mauritania (222)",nil)],
                     [[CCNumber alloc] init:@"+261" withDescription:NSLocalizedString(@"Madagascar (261)",nil)],
                     [[CCNumber alloc] init:@"+262" withDescription:NSLocalizedString(@"Mayotte (262)",nil)],
                     [[CCNumber alloc] init:@"+389" withDescription:NSLocalizedString(@"Macedonia, the Former Yugoslav Republic of (389)",nil)],
                     [[CCNumber alloc] init:@"+265" withDescription:NSLocalizedString(@"Malawi (265)",nil)],
                     [[CCNumber alloc] init:@"+60" withDescription:NSLocalizedString(@"Malaysia (60)",nil)],
                     [[CCNumber alloc] init:@"+223" withDescription:NSLocalizedString(@"Mali (223)",nil)],
                     [[CCNumber alloc] init:@"+960" withDescription:NSLocalizedString(@"Maldives (960)",nil)],
                     [[CCNumber alloc] init:@"+356" withDescription:NSLocalizedString(@"Malta (356)",nil)],
                     [[CCNumber alloc] init:@"+212" withDescription:NSLocalizedString(@"Morocco (212)",nil)],
                     [[CCNumber alloc] init:@"+596" withDescription:NSLocalizedString(@"Martinique (596)",nil)],
                     [[CCNumber alloc] init:@"+692" withDescription:NSLocalizedString(@"Marshall Islands (692)",nil)],
                     [[CCNumber alloc] init:@"+52" withDescription:NSLocalizedString(@"Mexico (52)",nil)],
                     [[CCNumber alloc] init:@"+691" withDescription:NSLocalizedString(@"Micronesia, Federated States of (691)",nil)],
                     [[CCNumber alloc] init:@"+258" withDescription:NSLocalizedString(@"Mozambique (258)",nil)],
                     [[CCNumber alloc] init:@"+373" withDescription:NSLocalizedString(@"Moldova, Republic of (373)",nil)],
                     [[CCNumber alloc] init:@"+377" withDescription:NSLocalizedString(@"Monaco (377)",nil)],
                     [[CCNumber alloc] init:@"+976" withDescription:NSLocalizedString(@"Mongolia (976)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Montserrat (1)",nil)],
                     [[CCNumber alloc] init:@"+95" withDescription:NSLocalizedString(@"Myanmar (95)",nil)],
                     [[CCNumber alloc] init:@"+264" withDescription:NSLocalizedString(@"Namibia (264)",nil)],
                     [[CCNumber alloc] init:@"+674" withDescription:NSLocalizedString(@"Nauru (674)",nil)],
                     [[CCNumber alloc] init:@"+977" withDescription:NSLocalizedString(@"Nepal (977)",nil)],
                     [[CCNumber alloc] init:@"+227" withDescription:NSLocalizedString(@"Niger (227)",nil)],
                     [[CCNumber alloc] init:@"+234" withDescription:NSLocalizedString(@"Nigeria (234)",nil)],
                     [[CCNumber alloc] init:@"+599" withDescription:NSLocalizedString(@"Netherlands Antilles (599)",nil)],
                     [[CCNumber alloc] init:@"+31" withDescription:NSLocalizedString(@"Netherlands (31)",nil)],
                     [[CCNumber alloc] init:@"+505" withDescription:NSLocalizedString(@"Nicaragua (505)",nil)],
                     [[CCNumber alloc] init:@"+683" withDescription:NSLocalizedString(@"Niue (683)",nil)],
                     [[CCNumber alloc] init:@"+64" withDescription:NSLocalizedString(@"New Zealand (64)",nil)],
                     [[CCNumber alloc] init:@"+687" withDescription:NSLocalizedString(@"New Caledonia (687)",nil)],
                     [[CCNumber alloc] init:@"+47" withDescription:NSLocalizedString(@"Norway (47)",nil)],
                     [[CCNumber alloc] init:@"+971" withDescription:NSLocalizedString(@"United Arab Emirates (971)",nil)],
                     [[CCNumber alloc] init:@"+968" withDescription:NSLocalizedString(@"Oman (968)",nil)],
                     [[CCNumber alloc] init:@"+55" withDescription:NSLocalizedString(@"Bouvet Island (55)",nil)],
                     [[CCNumber alloc] init:@"+672" withDescription:NSLocalizedString(@"Norfolk Island (672)",nil)],
                     [[CCNumber alloc] init:@"+61" withDescription:NSLocalizedString(@"Christmas Island (61)",nil)],
                     [[CCNumber alloc] init:@"+0" withDescription:NSLocalizedString(@"Heard Island and Mcdonald Islands (0)",nil)],
                     [[CCNumber alloc] init:@"+682" withDescription:NSLocalizedString(@"Cook Islands (682)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Turks and Caicos Islands (1)",nil)],
                     [[CCNumber alloc] init:@"+92" withDescription:NSLocalizedString(@"Pakistan (92)",nil)],
                     [[CCNumber alloc] init:@"+970" withDescription:NSLocalizedString(@"Palestinian Territory, Occupied (970)",nil)],
                     [[CCNumber alloc] init:@"+507" withDescription:NSLocalizedString(@"Panama (507)",nil)],
                     [[CCNumber alloc] init:@"+675" withDescription:NSLocalizedString(@"Papua New Guinea (675)",nil)],
                     [[CCNumber alloc] init:@"+595" withDescription:NSLocalizedString(@"Paraguay (595)",nil)],
                     [[CCNumber alloc] init:@"+51" withDescription:NSLocalizedString(@"Peru (51)",nil)],
                     [[CCNumber alloc] init:@"+64" withDescription:NSLocalizedString(@"Pitcairn (64)",nil)],
                     [[CCNumber alloc] init:@"+48" withDescription:NSLocalizedString(@"Poland (48)",nil)],
                     [[CCNumber alloc] init:@"+351" withDescription:NSLocalizedString(@"Portugal (351)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Puerto Rico (1)",nil)],
                     [[CCNumber alloc] init:@"+262" withDescription:NSLocalizedString(@"Reunion (262)",nil)],
                     [[CCNumber alloc] init:@"+7" withDescription:NSLocalizedString(@"Russian Federation (7)",nil)],
                     [[CCNumber alloc] init:@"+250" withDescription:NSLocalizedString(@"Rwanda (250)",nil)],
                     [[CCNumber alloc] init:@"+40" withDescription:NSLocalizedString(@"Romania (40)",nil)],
                     [[CCNumber alloc] init:@"+503" withDescription:NSLocalizedString(@"El Salvador (503)",nil)],
                     [[CCNumber alloc] init:@"+685" withDescription:NSLocalizedString(@"Samoa (685)",nil)],
                     [[CCNumber alloc] init:@"+378" withDescription:NSLocalizedString(@"San Marino (378)",nil)],
                     [[CCNumber alloc] init:@"+239" withDescription:NSLocalizedString(@"Sao Tome and Principe (239)",nil)],
                     [[CCNumber alloc] init:@"+966" withDescription:NSLocalizedString(@"Saudi Arabia (966)",nil)],
                     [[CCNumber alloc] init:@"+268" withDescription:NSLocalizedString(@"Swaziland (268)",nil)],
                     [[CCNumber alloc] init:@"+39" withDescription:NSLocalizedString(@"Holy See (Vatican City State) (39)",nil)],
                     [[CCNumber alloc] init:@"+290" withDescription:NSLocalizedString(@"Saint Helena (290)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Northern Mariana Islands (1)",nil)],
                     [[CCNumber alloc] init:@"+248" withDescription:NSLocalizedString(@"Seychelles (248)",nil)],
                     [[CCNumber alloc] init:@"+508" withDescription:NSLocalizedString(@"Saint Pierre and Miquelon (508)",nil)],
                     [[CCNumber alloc] init:@"+221" withDescription:NSLocalizedString(@"Senegal (221)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Saint Vincent and the Grenadines (1)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Saint Kitts and Nevis (1)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Saint Lucia (1)",nil)],
                     [[CCNumber alloc] init:@"+381" withDescription:NSLocalizedString(@"Serbia and Montenegro (381)",nil)],
                     [[CCNumber alloc] init:@"+65" withDescription:NSLocalizedString(@"Singapore (65)",nil)],
                     [[CCNumber alloc] init:@"+963" withDescription:NSLocalizedString(@"Syrian Arab Republic (963)",nil)],
                     [[CCNumber alloc] init:@"+421" withDescription:NSLocalizedString(@"Slovakia (421)",nil)],
                     [[CCNumber alloc] init:@"+386" withDescription:NSLocalizedString(@"Slovenia (386)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"United States (1)",nil)],
                     [[CCNumber alloc] init:@"+677" withDescription:NSLocalizedString(@"Solomon Islands (677)",nil)],
                     [[CCNumber alloc] init:@"+252" withDescription:NSLocalizedString(@"Somalia (252)",nil)],
                     [[CCNumber alloc] init:@"+249" withDescription:NSLocalizedString(@"Sudan (249)",nil)],
                     [[CCNumber alloc] init:@"+597" withDescription:NSLocalizedString(@"Suriname (597)",nil)],
                     [[CCNumber alloc] init:@"+232" withDescription:NSLocalizedString(@"Sierra Leone (232)",nil)],
                     [[CCNumber alloc] init:@"+992" withDescription:NSLocalizedString(@"Tajikistan (992)",nil)],
                     [[CCNumber alloc] init:@"+66" withDescription:NSLocalizedString(@"Thailand (66)",nil)],
                     [[CCNumber alloc] init:@"+886" withDescription:NSLocalizedString(@"Taiwan, Province of China (886)",nil)],
                     [[CCNumber alloc] init:@"+255" withDescription:NSLocalizedString(@"Tanzania, United Republic of (255)",nil)],
                     [[CCNumber alloc] init:@"+670" withDescription:NSLocalizedString(@"Timor-Leste (670)",nil)],
                     [[CCNumber alloc] init:@"+228" withDescription:NSLocalizedString(@"Togo (228)",nil)],
                     [[CCNumber alloc] init:@"+690" withDescription:NSLocalizedString(@"Tokelau (690)",nil)],
                     [[CCNumber alloc] init:@"+676" withDescription:NSLocalizedString(@"Tonga (676)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Trinidad and Tobago (1)",nil)],
                     [[CCNumber alloc] init:@"+688" withDescription:NSLocalizedString(@"Tuvalu (688)",nil)],
                     [[CCNumber alloc] init:@"+216" withDescription:NSLocalizedString(@"Tunisia (216)",nil)],
                     [[CCNumber alloc] init:@"+993" withDescription:NSLocalizedString(@"Turkmenistan (993)",nil)],
                     [[CCNumber alloc] init:@"+90" withDescription:NSLocalizedString(@"Turkey (90)",nil)],
                     [[CCNumber alloc] init:@"+256" withDescription:NSLocalizedString(@"Uganda (256)",nil)],
                     [[CCNumber alloc] init:@"+998" withDescription:NSLocalizedString(@"Uzbekistan (998)",nil)],
                     [[CCNumber alloc] init:@"+380" withDescription:NSLocalizedString(@"Ukraine (380)",nil)],
                     [[CCNumber alloc] init:@"+681" withDescription:NSLocalizedString(@"Wallis and Futuna (681)",nil)],
                     [[CCNumber alloc] init:@"+598" withDescription:NSLocalizedString(@"Uruguay (598)",nil)],
                     [[CCNumber alloc] init:@"+298" withDescription:NSLocalizedString(@"Faroe Islands (298)",nil)],
                     [[CCNumber alloc] init:@"+679" withDescription:NSLocalizedString(@"Fiji (679)",nil)],
                     [[CCNumber alloc] init:@"+63" withDescription:NSLocalizedString(@"Philippines (63)",nil)],
                     [[CCNumber alloc] init:@"+358" withDescription:NSLocalizedString(@"Finland (358)",nil)],
                     [[CCNumber alloc] init:@"+500" withDescription:NSLocalizedString(@"Falkland Islands (Malvinas) (500)",nil)],
                     [[CCNumber alloc] init:@"+33" withDescription:NSLocalizedString(@"France (33)",nil)],
                     [[CCNumber alloc] init:@"+594" withDescription:NSLocalizedString(@"French Guiana (594)",nil)],
                     [[CCNumber alloc] init:@"+689" withDescription:NSLocalizedString(@"French Polynesia (689)",nil)],
                     [[CCNumber alloc] init:@"+385" withDescription:NSLocalizedString(@"Croatia (385)",nil)],
                     [[CCNumber alloc] init:@"+236" withDescription:NSLocalizedString(@"Central African Republic (236)",nil)],
                     [[CCNumber alloc] init:@"+235" withDescription:NSLocalizedString(@"Chad (235)",nil)],
                     [[CCNumber alloc] init:@"+420" withDescription:NSLocalizedString(@"Czech Republic (420)",nil)],
                     [[CCNumber alloc] init:@"+56" withDescription:NSLocalizedString(@"Chile (56)",nil)],
                     [[CCNumber alloc] init:@"+41" withDescription:NSLocalizedString(@"Switzerland (41)",nil)],
                     [[CCNumber alloc] init:@"+46" withDescription:NSLocalizedString(@"Sweden (46)",nil)],
                     [[CCNumber alloc] init:@"+47" withDescription:NSLocalizedString(@"Svalbard and Jan Mayen (47)",nil)],
                     [[CCNumber alloc] init:@"+94" withDescription:NSLocalizedString(@"Sri Lanka (94)",nil)],
                     [[CCNumber alloc] init:@"+593" withDescription:NSLocalizedString(@"Ecuador (593)",nil)],
                     [[CCNumber alloc] init:@"+240" withDescription:NSLocalizedString(@"Equatorial Guinea (240)",nil)],
                     [[CCNumber alloc] init:@"+291" withDescription:NSLocalizedString(@"Eritrea (291)",nil)],
                     [[CCNumber alloc] init:@"+372" withDescription:NSLocalizedString(@"Estonia (372)",nil)],
                     [[CCNumber alloc] init:@"+251" withDescription:NSLocalizedString(@"Ethiopia (251)",nil)],
                     [[CCNumber alloc] init:@"+27" withDescription:NSLocalizedString(@"South Africa (27)",nil)],
                     [[CCNumber alloc] init:@"+500" withDescription:NSLocalizedString(@"South Georgia and the South Sandwich Islands (500)",nil)],
                     [[CCNumber alloc] init:@"+262" withDescription:NSLocalizedString(@"French Southern Territories (262)",nil)],
                     [[CCNumber alloc] init:@"+1" withDescription:NSLocalizedString(@"Jamaica (1)",nil)],
                     [[CCNumber alloc] init:@"+81" withDescription:NSLocalizedString(@"Japan (81)",nil)]
                     ,nil];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"longDescription"
                                                 ascending:YES];
    NSArray *sortedArray = [data sortedArrayUsingDescriptors:@[sortDescriptor]];
    return sortedArray;
}


NSArray* getPopularCurrencyList(){
    NSArray *data = [NSArray arrayWithObjects:
                     [[CurrencyObject alloc] init:@"USD" withValue:@"US dollar"],
                     [[CurrencyObject alloc] init:@"EUR" withValue:@"Euro"],
                     [[CurrencyObject alloc] init:@"РУБЛЬ"   withValue:@"Russian ruble"],
                     nil];
    return data;
}

NSArray* getNormalCurrencyList(){
    NSArray *data = [NSArray arrayWithObjects:
                     [[CurrencyObject alloc] init:@"AUD" withValue:@"Australian dollar"],
                     [[CurrencyObject alloc] init:@"CAD" withValue:@"Canadian dollar"],
                     [[CurrencyObject alloc] init:@"CNY" withValue:@"Chinese yuan"],
                     [[CurrencyObject alloc] init:@"EGP" withValue:@"Egyptian pound"],
                     [[CurrencyObject alloc] init:@"HKD" withValue:@"Hong Kong dollar"],
                     [[CurrencyObject alloc] init:@"Rs." withValue:@"Indian rupee"],
                     [[CurrencyObject alloc] init:@"Rp"  withValue:@"Indonesia rupiah"],
                     [[CurrencyObject alloc] init:@"¥"   withValue:@"Japanese yen"],
                     [[CurrencyObject alloc] init:@"KRW" withValue:@"Korean won"],
                     [[CurrencyObject alloc] init:@"KWD" withValue:@"Kuwaiti dinar"],
                     [[CurrencyObject alloc] init:@"MYR" withValue:@"Malaysia ringgit"],
                     [[CurrencyObject alloc] init:@"MXN" withValue:@"Mexican peso"],
                     [[CurrencyObject alloc] init:@"TWD" withValue:@"New Taiwan dollar"],
                     [[CurrencyObject alloc] init:@"£"   withValue:@"Pound sterling"],
                     [[CurrencyObject alloc] init:@"SAR"   withValue:@"Saudi Arabian riyal"],
                     [[CurrencyObject alloc] init:@"SGD" withValue:@"Singapore"],
                     [[CurrencyObject alloc] init:@"ZAR"   withValue:@"South African rand"],
                     [[CurrencyObject alloc] init:@"SEK"   withValue:@"Swedish krona"],
                     [[CurrencyObject alloc] init:@"CHF"   withValue:@"Swiss franc"],
                     [[CurrencyObject alloc] init:@"THB"   withValue:@"Thai baht"],
                     nil];
    return data;
}

void showAlertOneButton(UIViewController* controller ,NSString* title, NSString* message, NSString* buttonName){
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(title, nil)
                                                                   message:NSLocalizedString(message, nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedString(buttonName, nil)
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [alert popoverPresentationController];
                                                          }];
    [alert addAction:firstAction];
    
    [controller presentViewController:alert animated:YES completion:nil];
}

Boolean showAlertTopOpenLogin(UIViewController* controller, UIAlertAction *signInAction){
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if(![appdele isLoggedIn]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", nil)
                                                                       message:NSLocalizedString(@"Please login first before using this feature.", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  [alert popoverPresentationController];
                                                              }];
        
        
        [alert addAction:signInAction];
        [alert addAction:firstAction];
        
        [controller presentViewController:alert animated:YES completion:nil];
        return YES;
    }
    
    return NO;
}


Boolean showAlertTopOpenLoginInRequest(UIViewController* controller, UIAlertAction *signInAction, UIAlertAction *cancelAction) {
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if(![appdele isLoggedIn]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", nil)
                                                                       message:NSLocalizedString(@"Please login first before using this feature.", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
   
        
        
        
        [alert addAction:signInAction];
        [alert addAction:cancelAction];
        
        [alert setPreferredAction:signInAction];
        
        [controller presentViewController:alert animated:YES completion:nil];
        return YES;
    }
    
    return NO;
}

void setRootViewLogout(){
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UINavigationController *navigation = (UINavigationController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"NavigationbarLogin"];
    
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdele.socialMenu = nil;
    [UIView transitionWithView:appdele.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        UIApplication.sharedApplication.delegate.window.rootViewController = navigation;
                        
                    }
                    completion:nil];
}

#pragma mark - HUY Truong GET API

void callAPINoHeaderForGET(NSString* endpoint ,NSMutableDictionary* data ,id target, SEL successAction ,SEL failAction)
{
    NSURL *url = [NSURL URLWithString:endpoint];
    if(data!=nil)
    {
        
        NSInteger currentIndexKey = 0;
        for (NSString* key in data.allKeys) {
            if (currentIndexKey == 0) {
                endpoint = [NSString stringWithFormat:@"%@?%@=%@",endpoint,key,[data objectForKey:key]];
                currentIndexKey += 1;
            } else {
                endpoint = [NSString stringWithFormat:@"%@&%@=%@",endpoint,key,[data objectForKey:key]];
            }
            
            url = [NSURL URLWithString:endpoint];
        }
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    //
    
    //    [request setValue:@"text/html; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:@"gzip" forHTTPHeaderField:@"Content-Encoding"];
    
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:target delegateQueue:Nil];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data.length > 0 && error == nil) {
            if(target!=nil){
                NSInvocation *inv = [NSInvocation invocationWithMethodSignature: [target methodSignatureForSelector:successAction]];
                [inv setSelector:successAction];
                [inv setTarget:target];
                [inv setArgument:&(data) atIndex:2];
                [inv performSelectorOnMainThread:@selector(invoke) withObject:nil waitUntilDone:YES];
            }
        }else{
            if(target!=nil){
                NSInvocation *inv = [NSInvocation invocationWithMethodSignature: [target methodSignatureForSelector:failAction]];
                [inv setSelector:failAction];
                [inv setTarget:target];
                [inv setArgument:&(error) atIndex:2];
                [inv performSelectorOnMainThread:@selector(invoke) withObject:nil waitUntilDone:YES];
            }
        }
    }];
    [task resume];
}

NSString* stringFromObject(id object)
{
    if (object) {
        if ([object isKindOfClass:[NSString class]]) {
            return object;
        } else if ([object isKindOfClass:[NSNull class]]) {
            return @"";
        }
        else if ([object isKindOfClass:[NSNumber class]]) {
            return [object stringValue];
        }
        return [object description];
    }
    return @"";
}


#pragma mark - Get Top Controller

UIViewController* topViewController()
{
    return topViewControllerWithRootViewController([UIApplication sharedApplication].keyWindow.rootViewController);
}

UIViewController* topViewControllerWithRootViewController(UIViewController* rootViewController)
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return  topViewControllerWithRootViewController(tabBarController.selectedViewController);
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return topViewControllerWithRootViewController(navigationController.visibleViewController);
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return topViewControllerWithRootViewController(presentedViewController);
    } else {
        return rootViewController;
    }
}

BOOL isNetworkAvailable()
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable)
    {
        //No internet
        [reachability stopNotifier];
        
        return NO;
    }
    else if (status == ReachableViaWiFi)
    {
        //WiFi
        [reachability stopNotifier];
        
        return YES;
    }
    else if (status == ReachableViaWWAN)
    {
        //3G
        [reachability stopNotifier];
        
        return YES;
    }
    [reachability stopNotifier];
    
    return YES;
}


#pragma mark - Check Device

NSString* platformType(NSString *platform)
{
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4 (Verizon)";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPod6,1"])      return @"iPod Touch 6G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad 1G";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air (China)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G (China)";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,1"])      return @"iPad Mini 4 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad5,2"])      return @"iPad Mini 4 (Cellular)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"iPad6,3"])      return @"iPad Pro 9.7\" (Wi-Fi)";
    if ([platform isEqualToString:@"iPad6,4"])      return @"iPad Pro 9.7\" (Cellular)";
    if ([platform isEqualToString:@"iPad6,7"])      return @"iPad Pro 12.9\" (Wi-Fi)";
    if ([platform isEqualToString:@"iPad6,8"])      return @"iPad Pro 12.9\" (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"AppleTV5,3"])   return @"Apple TV 4";
    if ([platform isEqualToString:@"Watch1,1"])     return @"Apple Watch Series 1 (38mm, S1)";
    if ([platform isEqualToString:@"Watch1,2"])     return @"Apple Watch Series 1 (42mm, S1)";
    if ([platform isEqualToString:@"Watch2,6"])     return @"Apple Watch Series 1 (38mm, S1P)";
    if ([platform isEqualToString:@"Watch2,7"])     return @"Apple Watch Series 1 (42mm, S1P)";
    if ([platform isEqualToString:@"Watch2,3"])     return @"Apple Watch Series 2 (38mm, S2)";
    if ([platform isEqualToString:@"Watch2,4"])     return @"Apple Watch Series 2 (42mm, S2)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return platform;
}


NSMutableDictionary* createCommonBookingObject(NSString* requestType, NSString* funtionality){
    NSMutableDictionary* dictKeyValues = [NSMutableDictionary dictionary];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        UserObject *user = (UserObject*)[appdele getLoggedInUser];
        if(user.userId && user.firstName && user.lastName && user.salutation && user.email
           && user.mobileNumber &&  appdele.ACCESS_TOKEN){
            [dictKeyValues setObject:user.userId  forKey:@"OnlineMemberId"];
            [dictKeyValues setObject:user.firstName forKey:@"FirstName"];
            [dictKeyValues setObject:user.lastName forKey:@"LastName"];
            [dictKeyValues setObject:convertSalutationToEnglish(user.salutation) forKey:@"Salutation"];
            [dictKeyValues setObject:user.email forKey:@"EmailAddress1"];
            [dictKeyValues setObject:user.mobileNumber forKey:@"PhoneNumber"];
            [dictKeyValues setObject:[AppConfig shared].clientCode forKey:@"EPCClientCode"];
            [dictKeyValues setObject:[AppConfig shared].programCode forKey:@"EPCProgramCode"];
            [dictKeyValues setObject:funtionality forKey:@"Functionality"];
            [dictKeyValues setObject:requestType forKey:@"RequestType"];
            [dictKeyValues setObject:[AppConfig shared].consumerKey forKey:@"ConsumerKey"];
            [dictKeyValues setObject:@"Mobile App" forKey:@"Source"];
            [dictKeyValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
            [dictKeyValues setObject:[AppConfig shared].verificationCode forKey:@"VerificationCode"];
        }
    }
    return dictKeyValues;
}

NSString* convertRequestDetailsToString(NSMutableDictionary* dict){
    NSString *result = @"";
    for (NSString* key in [dict allKeys]) {
        NSString *value = dict[key];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@%@%@", (result.length > 0 ? @"|" : @""), key, value]];
    }
    return  result;
}

NSDictionary* convertRequestDetailsStringToDict(NSString* requestDetails){
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSArray* splitedItems = [requestDetails componentsSeparatedByString:@"|"];
    
    NSMutableArray *keys = [NSMutableArray arrayWithArray:ALL_KEYS_IN_REQUESTDETAILS];
    NSString* trimItem;
    for (NSString* item in splitedItems) {
        trimItem = [item stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
        for (NSString* key in keys) {
            if ([trimItem hasPrefix:key]) {
                [dict setObject:[trimItem stringByReplacingOccurrencesOfString:key withString:@""] forKey:key];
                [keys removeObject:key];
                break;
            }
        }
    }
    
    return dict;
}

NSString* removeSpecialCharacters(NSString* value){
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet characterSetWithCharactersInString:SPECIAL_CHARACTER];
    [charactersToKeep formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
    
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];
    
    NSString *trimmedReplacement = [[value componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@"" ];
    
    //NSLog(@"orrigional: %@",trimmedReplacement);
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\u0080-\\u0400]"
                                                                           options:NSRegularExpressionUseUnicodeWordBoundaries
                                                                             error:&error];
    trimmedReplacement = [regex stringByReplacingMatchesInString:trimmedReplacement
                                                         options:0
                                                           range:NSMakeRange(0, [trimmedReplacement length])
                                                    withTemplate:@""];
    //NSLog(@"trimmed 1: %@",trimmedReplacement);
    
    regex = [NSRegularExpression regularExpressionWithPattern:@"[\\u0500-\\uFFFF]"
                                                      options:NSRegularExpressionUseUnicodeWordBoundaries
                                                        error:&error];
    trimmedReplacement = [regex stringByReplacingMatchesInString:trimmedReplacement
                                                         options:0
                                                           range:NSMakeRange(0, [trimmedReplacement length])
                                                    withTemplate:@""];
    //NSLog(@"trimmed 2: %@",trimmedReplacement);
    
    return trimmedReplacement;
}

NSMutableDictionary* removeSpecialCharactersfromDict(NSMutableDictionary* dict){
    for (NSString* key in [dict allKeys]) {
        if([[dict objectForKey:key] isKindOfClass:[NSString class]]){
            [dict setObject:removeSpecialCharacters([dict objectForKey:key]) forKey:key];
        }
    }
    return dict;
}

BOOL isNotIncludedSpecialCharacters(NSString* text){
    NSString *newString = [[text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    return [newString isEqualToString:removeSpecialCharacters(text)];
}

BOOL isValidValue(NSString* text){
    if(text){
        text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if(text==nil || text.length == 0){
            return NO;
        }
    } else {
        return NO;
    }
    return YES;
}

void trackGAICategory(NSString* category, NSString* action, NSString* label,NSNumber* value){
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:value] build]];
}

NSString* convertSalutationToRussian(NSString* enValue){
    if(enValue){
        if(isEqualIgnoreCase(enValue, @"Ms") || isEqualIgnoreCase(enValue, @"Ms.")){
            return @"Уважаемый";
        } else if (isEqualIgnoreCase(enValue, @"Mrs") || isEqualIgnoreCase(enValue, @"Mrs.")){
            return @"Уважаемая";
        }else if(isEqualIgnoreCase(enValue, @"Mr.")){
            return @"Г-н.";
        }
    }
    return enValue;
}

NSString* convertSalutationToEnglish(NSString* ruValue){
    if(ruValue){
        if(isEqualIgnoreCase(ruValue, @"Уважаемый") || isEqualIgnoreCase(ruValue, @"Уважаемый.")){
            return @"Ms";
        } else if (isEqualIgnoreCase(ruValue, @"Уважаемая") || isEqualIgnoreCase(ruValue, @"Уважаемая.")){
            return @"Mrs";
        }else if(isEqualIgnoreCase(ruValue, @"Г-н.")){
            return @"Mr.";
        }
    }
    return ruValue;
}



NSString* getCountryNameByCode(NSString* code){
    loadCountries();
    for (NSDictionary *dict in countries){
        if ([dict[@"code"] isEqual: code]) {
            return dict[@"name"];
        }
    }
    return [countries.firstObject valueForKey:@"name"];
}

NSString* getCountryCodeByName(NSString* name){
    loadCountries();
    for (NSDictionary *dict in countries){
        if ([dict[@"name"] isEqual: name]) {
            return dict[@"code"];
        }
    }
    return [countries.firstObject valueForKey:@"code"];
}

void loadCountries(){
    @synchronized([CommonUtils class]){
        if (countries == nil) {
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"]];
            countries = [NSArray arrayWithContentsOfURL:url];
        }
    }
}

@end
