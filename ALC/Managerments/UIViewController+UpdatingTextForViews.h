//
//  UIViewController+UpdatingTextForViews.h
//  ALC
//
//  Created by Chung Mai on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UpdatingTextForViewsDelegate <NSObject>

@required
-(void) setTextForViews;

@end

@interface UIViewController (UpdatingTextForViews)

@property(nonatomic, weak) id<UpdatingTextForViewsDelegate> updatingTextDelegate;

@end
