//
//  Constant.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

#define API_URL @"http://113.161.77.24:10302/api/"
#define DOMAIN_URL @"http://113.161.77.24:10302"

/*
#define B2C_API_URL @"https://apiservice-stg.aspirelifestyles.com/ConciergeOauthWebAPI/"
#define B2C_VERIFICATION_CODE @"1001"
#define B2C_Callback_Url @"urn:ietf:wg:oauth:2.0:oob"
#define B2C_ConsumerKey @"BCD"
#define B2C_ConsumerSecret @"BCD-CA28FC02-910"
#define B2C_DeviceId @"DeviceID10"
#define B2C_MemberRefNo @"TestMemRef127"
#define LuxuryCardID @"545212"
#define B2C_EPCClientCode @"BCD"
#define B2C_EPCProgramCode @"BCD SINGAPORE"
*/

//DMA
/*
#define B2C_API_URL @"https://apiservice-stg.aspirelifestyles.com/ConciergeOauthWebAPI/"
#define B2C_VERIFICATION_CODE @"Deepa.SHANKAR@internationalsos.com"
#define B2C_Callback_Url @"urn:ietf:wg:oauth:2.0:oob"
#define B2C_ConsumerKey @"DMA"
#define B2C_ConsumerSecret @"DMAConsumerSecret"
#define B2C_DeviceId @"DeciveID10"
#define B2C_MemberRefNo @"TestMemRef127"
#define LuxuryCardID @"545212"
#define B2C_EPCClientCode @"DMA"
#define B2C_EPCProgramCode @"DMA SINGAPORE"
*/


//// DMR api
//
//#define B2C_API_URL @"https://apiservice-stg.aspirelifestyles.com/ConciergeOauthWebAPI/"
//#define B2C_VERIFICATION_CODE @"Deepa.SHANKAR@internationalsos.com"
//#define B2C_Callback_Url @"dmr:ietf:wg:oauth:2.0:oob"
//#define B2C_ConsumerKey @"DMR"
//#define B2C_ConsumerSecret @"DMR-50140C35-K3N"
//#define B2C_DeviceId @"DeviceID10"
//#define B2C_MemberRefNo @"TestMemRef127"
//#define LuxuryCardID @"545212"
//#define B2C_EPCClientCode @"DMR"
//#define B2C_EPCProgramCode @"DMR"
//
////Define DCR api - Dev env dcrstag
//#define DCR_API_URL @"https://api.aspirelifestyles.com/dcrstg/api/v1/"
//#define DCR_SUBCRIPTION_VALUE @"09d2faefb37a43d6a58cc68f46149b35"


////DMR Production

#define B2C_API_URL @"https://mowapiservice.aspirelifestyles.com/ConciergeWebAPIProd/"
//#define B2C_VERIFICATION_CODE @"Deepa.SHANKAR@internationalsos.com"
//#define B2C_Callback_Url @"urn:iekn:wg:oauth:2.0:oob"
//#define B2C_ConsumerKey @"DMR"
//#define B2C_ConsumerSecret @"DMR-BD48ML91-426"
#define B2C_DeviceId @"DeviceID10"
#define B2C_MemberRefNo @"TestMemRef127"
#define LuxuryCardID @"545212"
//#define B2C_EPCClientCode @"DMR"
//#define B2C_EPCProgramCode @"DMR VASP"

/*
#define B2C_API_URL @"https://apiservice-stg.aspirelifestyles.com/ConciergeOauthWebAPI/"
//#define B2C_VERIFICATION_CODE @"Deepa.SHANKAR@internationalsos.com"
//#define B2C_Callback_Url @"dmr:ietf:wg:oauth:2.0:oob"
//#define B2C_ConsumerKey @"DMR"
//#define B2C_ConsumerSecret @"DMR-50140C35-K3N"
#define B2C_DeviceId @"DeviceID10"
#define B2C_MemberRefNo @"TestMemRef127"
#define LuxuryCardID @"545212"
//#define B2C_EPCClientCode @"DMR"
//#define B2C_EPCProgramCode @"DMR"
 */
 


//Define DCR Prod api
#define DCR_API_URL @"https://api.aspirelifestyles.com/dcr/api/v1/"
#define DCR_SUBCRIPTION_VALUE @"299688b8f867458b81ea8dffe4afb05a"


//Define DCR api - Dev env dcrtest
//#define DCR_API_URL @"https://api.aspiredigital.net/dcrtest/api/v1/"
//#define DCR_SUBCRIPTION_VALUE @"299688b8f867458b81ea8dffe4afb05a"

//Define DCR api - Dev env dcrso
//#define DCR_API_URL @"http://api.aspiredigital.net/dcrso/api/v1/"
//#define DCR_SUBCRIPTION_VALUE @"299688b8f867458b81ea8dffe4afb05a"

#define DCR_SUBCRIPTION_KEY @"Ocp-Apim-Subscription-Key"
#define DCR_ITEM_SEARCH_API @"items/search"
#define DCR_ITEM_DETAILS_API @"items"



//define error code
#define B2C_UNAVAILABLE_USER        @"ENR68-2"
#define B2C_INVALID_ACCESS_TOKEN    @"ENR73-2"
#define B2C_INVALID_USER_EMAIL      @"ENR008-3"
#define B2C_INVALID_PHONE_NUMBER    @"ENR007-3"
#define B2C_LOGIN_FAIL    @"ENR70-1"

// define error message
#define MANDATORY_BLANK     NSLocalizedString(@"Field is required", nil)
#define INCLUDE_UNSUPPORT_CHAR_ERR NSLocalizedString(@"Only !@#$%&*()-+_=[]:;\",.?/ special characters are allowed",nil)
#define DROP_OFF_TIME_ERR NSLocalizedString(@"Dropoff time should after Pickup time", nil)
//Request Details Fields
#define EDIT_ADD  @"ADD"
#define EDIT_AMEND  @"AMEND"
#define EDIT_CANCEL @"CANCEL"
#define SPECIAL_REQUIREMENT @"Special Requirement - "
#define CITY_GOLF_REQUIREMENT @"City - "
#define COUNTRY_REQUIREMENT @"Country - "
#define RESERVATION_NAME_REQUIREMENT @"Reservation Name - "
#define GUEST_NAME_REQUIREMENT @"Guest Name - "
#define RESTAURANT_NAME_REQUIREMENT @"Name of Restaurant - "
#define HOTEL_NAME_REQUIREMENT @"Hotel Name - "
#define ROOM_PREF_REQUIREMENT @"Room Preference - "
#define SMOKING_PREF_REQUIREMENT @"Smoking Preference - "
#define MEMBER_SHIP_REQUIREMENT @"Loyalty Program - "
#define MAX_PRICE_REQUIREMENT @"Budget Range per night Currency - "
#define MAX_BUDGET_RENTAL @"Budget Range Currency per day - "
#define MIN_PRICE_REQUIREMENT @"MinPrice - "
#define CUISINE_REQUIREMENT @"Type of Cuisine - "
#define OCCASION_REQUIREMENT @"Ambiance/Occasion - "
#define RATING_PREF_REQUIREMENT @"PrefRating - "
#define PrefCarRentalComp @"Preferred car rental company - "
#define PrefVehicle @"Preferred Vehicle - "
#define DropLocation @"Dropoff Location - "
#define PickLocation @"Pickup Location - "
#define IntLicense @"Does the drive hold a valid international driving licence - "
#define DriverAge @"Age of driver - "
#define DriverName @"Name of driver - "
#define DropoffDate @"Dropoff Date - "
#define DropoffTime @"Dropoff Time - "
#define GolfHandicap @"GolfHandicap - "
#define PrefRegion @"PrefRegion - "
#define SpecificStartTime @"SpecificStartTime - "
#define NoofBallers @"No ballers - "
#define Holes @"Hole preference - "
#define NeedCaddy @"NeedCaddy - "
#define Needbuggy @"Needbuggy - "
#define MemberofGolfClub @"MemberofGolfClub - "
#define Event_Name @"EventName - "
#define Event_Category @"Event Category - "
#define EventDate @"Event Date - "
#define EventTime @"Event Time - "
#define NoofTickets @"No of tickets - "
#define PreferedDate @"Preferred Date - "
#define GolfCourseName @"Golf Course - "
#define MEMBER_NO_REQUIREMENT @"Hotel Loyalty Membership Number - "
#define TransportType @"Transport Type - "
#define NumberPassengers @"No. of Passengers - "
#define GolfDateOfPlay @"Date of play - "
#define TeeTime @"Tee time - "
#define FoodAllergies @"FoodAllergies - "
#define AdditionalPreference @"AdditionalPreference - "
#define PreferredGolfHandicap @"PreferredGolfHandicap - "
#define StateRequirement @"State - "
#define WithBreakfast @"With Breakfast - "
#define WithWifi @"With Wifi - "
#define SpaName @"Spa Name - "
#define FullAddress @"Full Address - "
#define PrivilegeId @"Privilege Id - "
#define NumberOfRooms @"Numbers Of Room - "

#define ALL_KEYS_IN_REQUESTDETAILS @[COUNTRY_REQUIREMENT,SPECIAL_REQUIREMENT, RESERVATION_NAME_REQUIREMENT, RESTAURANT_NAME_REQUIREMENT, HOTEL_NAME_REQUIREMENT, ROOM_PREF_REQUIREMENT, SMOKING_PREF_REQUIREMENT, MEMBER_SHIP_REQUIREMENT, MAX_PRICE_REQUIREMENT, MIN_PRICE_REQUIREMENT, CUISINE_REQUIREMENT, OCCASION_REQUIREMENT, RATING_PREF_REQUIREMENT, PrefCarRentalComp, PrefVehicle, DropLocation, PickLocation, IntLicense, DriverAge, DriverName, GolfHandicap, DropoffDate, PrefRegion, SpecificStartTime, NoofBallers, NeedCaddy, Needbuggy, MemberofGolfClub, Event_Name, GolfCourseName,GolfDateOfPlay, Holes, TeeTime, Event_Category, NoofTickets, PreferedDate,MEMBER_NO_REQUIREMENT,TransportType, DropoffTime, MAX_BUDGET_RENTAL, NumberPassengers, EventDate, EventTime, CITY_GOLF_REQUIREMENT,FoodAllergies,PreferredGolfHandicap,AdditionalPreference,StateRequirement, WithBreakfast, WithWifi, SpaName, FullAddress, PrivilegeId, NumberOfRooms,GUEST_NAME_REQUIREMENT]

//Request Type values
#define RESTAURANT_REQUEST_TYPE @"D RESTAURANT"
#define RESTAURANT_RECOMEND_REQUEST_TYPE @"D RESTAURANT"
#define HOTEL_REQUEST_TYPE @"T HOTEL AND B&B"
#define HOTEL_RECOMMEND_REQUEST_TYPE @"T HOTEL AND B&B"
#define CAR_RENTAL_REQUEST_TYPE @"T CAR RENTAL"
#define TRANSFER_LIMO_REQUEST_TYPE @"T LIMO AND SEDAN"
#define TRANSFER_TAXI_REQUEST_TYPE @"T LIMO AND SEDAN"
#define TRANSFER_TRAIN_REQUEST_TYPE @"T LIMO AND SEDAN"
#define TRANSFER_BUS_REQUEST_TYPE @"T LIMO AND SEDAN"
#define TRANSFER_OTHER_REQUEST_TYPE @"T LIMO AND SEDAN"
#define GOLF_REQUEST_TYPE @"S GOLF"
#define OTHER_REQUEST_TYPE @"Other"
#define EVENT_REQUEST_TYPE @"E CONCERT/THEATER"
#define EVENT_RECOMMEND_REQUEST_TYPE @"E CONCERT/THEATER"

//Funtionality
#define RESTAURANT_BOOK @"Book a Dining"
#define RESTAURANT_RECOMMEND @"Recommend a Dining"
#define HOTEL_BOOK @"Book a Hotel"
#define HOTEL_RECOMMEND @"Recommend a Hotel"
#define GOLF_RESERVATION @"Book a Golf"
#define OTHER_RESERVATION @"Others"
#define CAR_RENTAL_FUNCTION @"CarRental"
#define CAR_TRANSFER_FUNCTION @"Transfer"
#define EVENT_BOOK_FUNCTION @"Book an Event"
#define EVENT_RECOMMEND_FUNCTION @"Recommend an Event"

//Situation
#define RESTAURANT_BOOK_SITUATION @"Reserve Table"
#define EVENT_BOOK_SITUATION @"Book an Event"
#define EVENT_RECOMMEND_SITUATION @"Recommend an Event"

// define My Preference Type
#define Preference_Hotel @"Hotel"
#define Preference_Dining @"Dining"
#define Preference_Golf @"Golf"
#define Preference_Car_Rental @"Car Rental"
#define Preference_Other @"Other"

// define My Preference Key_Value
#define MYPREFERENCESID @"MYPREFERENCESID"
#define MY_PRE_TYPE @"TYPE"
#define VALUE @"VALUE"
#define VALUE1 @"VALUE1"
#define VALUE2 @"VALUE2"
#define VALUE3 @"VALUE3"
#define VALUE4 @"VALUE4"
#define VALUE5 @"VALUE5"
#define VALUE6 @"VALUE6"


#define NO_INTERNET_MSG NSLocalizedString(@"There is no internet connection.", nil)

enum WSTASK
{
    WS_AUTHENTICATION_MULTITASK = 1,
    WS_AUTHENTICATION_LOGIN = 101,
    WS_GET_USER_DETAILS = 2,
    WS_GET_GOURMET_LIST = 3,
    WS_GET_CUISINE = 4,
    WS_GET_OCCASION = 5,
    WS_GET_GOURMET_DETAILS = 6,
    WS_CREATE_USER = 7,
    WS_FORGOT_PASSWORD = 8,
    WS_GET_REQUEST_LIST = 9,
    WS_GET_LIFETYLE_LIST = 21,
    WS_GET_ENTERTAINMENT_LIST = 22,
    WS_GET_TRAVEL_LIST = 23,
    WS_ADD_ITEM_TO_WISHLIST = 24,
    WS_REMOVE_ITEM_FROM_WISHLIST = 25,
    WS_GET_CITY_GUIDE_LIST = 26,
    WS_GET_CITY_GUIDE_DETAILS = 27,
    WS_GET_RETAL_VEHICLE = 41,
    WS_GET_COMPANY = 42,
    WS_GET_TEE_TIMES = 43,
    WS_GET_ROOM = 44,
    WS_GET_BED = 45,
    WS_CREATE_GOURMETPRE = 46,
    WS_CREATE_HOTELPRE = 47,
    WS_CREATE_GOLTPRE = 48,
    WS_CREATE_CARPRE = 49,
    WS_CREATE_OTHERPRE = 50,
    WS_GET_RATING = 51,
    WS_GET_SMOKING = 52,
    WS_GET_MY_PREFERENCE = 53,
    WS_GET_CUISINE_PREFERENCE = 54,
    WS_UPDATE_USER = 55,
    WS_GET_CURRENCY = 30,
    WS_CREATE_CURRENCY = 31,
    WS_CHANGE_PASSWORD = 32,
    WS_CANCEL_REQUEST = 33,
    WS_BOOKING_GOURMET = 56,
    WS_BOOKING_ORTHERS = 57,
    WS_BOOKING_CAR_RENTAL = 58,
    WS_BOOKING_CAR_TRANSFER = 59,
    WS_BOOKING_HOTEL = 60,
    WS_BOOKING_GOLF = 63,
    WS_GET_COUNTRY = 61,
    WS_GET_CITY = 62,
    WS_GET_HOTEL_REQUEST = 70,
    WS_GET_GOLF_REQUEST = 71,
    WS_GET_GOURMET_REQUEST = 72,
    WS_GET_CAR_RENTAL_REQUEST = 73,
    WS_GET_CAR_TRANSFER_REQUEST = 74,
    WS_GET_OTHER_REQUEST = 75,
    WS_GET_ENTERTAINMENT_REQUEST = 76,
    WS_GET_TRANSPORT_TYPE = 80,
    WS_BOOKING_HOTEL_RECOMMEND = 81,
    WS_GET_HOTEL_RECOMMEND_REQUEST = 82,
    WS_BOOKING_GOURMET_RECOMMEND = 83,
    WS_GET_GOURMET_RECOMMEND_REQUEST = 84,
    WS_GET_REQUEST_TOKEN = 101,
    WS_GET_ACCESS_TOKEN = 102,
    WS_GET_REFRESH_ACCESS_TOKEN = 103,
    WS_B2C_GET_USER_DETAILS = 104,
    WS_BOOKING_ENTERTAINMENT = 105,
    WS_BOOKING_ENTERTAINMENT_RECOMMEND = 106,
    WS_GET_BOOKING_ENTERTAINMENT = 107,
    WS_GET_ENTERTAINMENT_RECOMMEND = 108,
    WS_DCR_ITEM_SEARCH = 200,
    WS_DCR_ITEM_DETAILS = 201,
};

enum WS_SUB_TASK
{
    WS_ST_NONE = 0,
    WS_AUTHENTICATION = 1,
};

enum DCR_ITEM_TYPE
{
    DINING_ITEM = 1,
    ACCOMMODATION_ITEM = 2,
    SIGHTSEEING_ITEM = 3,
    VACATION_ITEM = 4,
    EVENT_ITEM = 5,
    PRIVILEGE_ITEM = 6,
    SPA_ITEM = 7,
    UNKNOWN_TYPE = 8,
};
//Position of this values is appropriate to DCR_ITEM_TYPE enum
#define DCR_ITEM_TYPE_VALUE @[@"dining", @"accommodation",@"sightseeing_activity",@"vacation_package",@"event",@"privilege"]
#define DCR_DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ssZZZZ"


/**************************** Development API V1.0  ****************************/

// authen
#define AuthenticationUrl              @"http://113.161.77.24:10204/api/identity/Authenticate"

#define GetMyRequestListUrl            @"ManageRequest/GetRecentRequestList/"
#define PostChangePasswordUrl           @"ManageUsers/ChangePassword"
#define CancelRequestUrl               @"ManageRequest/UpdateConciergeRequest"
#define GetGourmetListIrl              @"Restaurants/GetRestaurants/"
#define GetCuisineListIrl              @"Restaurants/GetCuisines/"
#define GetOccasionListIrl             @"Restaurants/getoccasions/"
#define GetRestaurantDetailsUrl        @"Restaurants/GetRestaurantDetail"
#define GetEntertainmentUrl            @"statictemp/GetEntertainments"
#define GetLifeStyleUrl                @"statictemp/getlifestyles"
#define GetTravelUrl                   @"statictemp/GetTravels"
#define CreateUpdateGourmetPreferences            @"identity/CreateUpdateGourmetPreferences"
#define AddWishList                    @"mywishlist/AddToWishlist"
#define RemoveWishList                 @"mywishlist/RemoveMyWishlist"

#define GetCityGuideList               @"Travel/GetCityGuides"
#define GetCityGuideDetails            @"Travel/GetCityGuideDetail"

#define GetRetalVehicleListIrl         @"common/RentalVehicleTypePreferences"
#define GetRetalCompanyPre             @"common/RentalCompanyPreferences"
#define GetTeeTimesPre                 @"common/TeeTimePreferences"
#define GetRoomPre                     @"common/RoomTypePreferences"
#define GetBedPre                      @"common/BedPreferences"
#define GetRatingPre                   @"common/RatingPreferences"
#define GetSmokingPre                  @"common/SmokingRoomPreferences"
#define GetTransportType               @"common/TransportTypes"
#define CreateGourmetPre               @"identity/CreateUpdateGourmetPreferences"
#define CreateHotelPre                 @"identity/CreateUpdateHotelPreferences"
#define CreateGoltPre                  @"identity/CreateUpdateGolfPreferences"
#define CreateCarPre                   @"identity/CreateUpdateTransportPreferences"
#define CreateOthersPre                @"identity/CreateUpdateOthersPreferences"

#define GetCuisinePreference           @"common/CuisinePreferences"
#define UpdateUserProfile              @"identity/UpdateMyProfile"
#define GetUserCurrencies              @"identity/GetMyCurrency"
#define CreateUserCurrency             @"identity/SaveMyCurrency"
#define CreateGourmetBooking             @"Restaurants/BookRestaurant"
#define CreateGolfBooking             @"LifeStyle/BookGolf"
#define CreateOrthersBooking             @"ManageRequest/CreateConciergeRequest"

#define CreateHotelBook                @"Travel/BookHotel"
#define CreateCarRental                @"Travel/BookCarRental"
#define CreateCarTransfer              @"Travel/BookCarTransfer"
#define CreateHotelRecommend           @"Travel/BookHotelRecommendation"
#define GetCountryList                 @"Geos/GetCountries"
#define GetCityList                    @"Geos/GetCities"
#define GetHotelRequestDetails         @"Travel/GetHotelBookingDetail"
#define GetGolfRequestDetails          @"LifeStyle/GetGolfBookingDetail"
#define GetCarRentalRequestDetails          @"Travel/GetCarRentalBookingDetail"
#define GetCarTransferRequestDetails          @"Travel/GetCarTransferBookingDetail"
#define GetOthersRequestDetails        @"Others/GetOtherBookingDetail"
#define GetGourmetRequestDetails       @"Restaurants/GetRestaurantBookingDetail"
#define GetEntertainmentRequestDetails       @"lifestyle/GetEntertainmentBookingDetail"
#define GetHotelRecommendRequestDetails       @"Travel/GetHotelRecommendationBookingDetail"
#define CreateGourmetRecommend           @"Restaurants/BookRestaurantRecommendation"
#define GetGourmetRecommend           @"Restaurants/GetRestaurantRecommendationBookingDetail"

//B2C API
#define GetRequestToken               @"OAuth/AuthoriseRequestToken"
#define GetAccessToken                @"OAuth/GetAccessToken"
#define RefreshAccessToken            @"OAuth/RefereshToken"
#define PostRegistration              @"ManageUsers/Registration"
#define PostUserLogin                 @"ManageUsers/Login"
#define GetUserDetailsUrl             @"ManageUsers/GetUserDetails"
#define PostForgotPasswordUrl         @"ManageUsers/ForgotPassword"
#define UpdateRegistration              @"ManageUsers/UpdateRegistration"
#define CreateConciergeRequestUrl     @"ManageRequest/CreateConciergeRequest"
#define GetConciergeRequestDetailsUrl @"ManageRequest/GetConciergeRequestDetails"
#define GetMyPreference                @"ManageUsers/GetPreference"
#define AddMyPreference                @"ManageUsers/AddPreference"
#define UpdateMyPreference                @"ManageUsers/UpdatePreference"

//end B2C

// config user authenticate
#define userAuthenticate @"admin"
#define passAuthenticate @"Admin123!@#$"
//

/**
 * Http Methods
 **/
#define kHTTPGET    @"GET"
#define kHTTPPUT    @"PUT"
#define kHTTPPOST   @"POST"
#define kHTTPDEL    @"DELETE"

/**
 * Error Code
 **/
#define kSTATUSOK               200
#define kSTATUSTOKENEXPIRE     401
#define kNO_INTERNET_CONNECTION 503

/**
 * My Preference Category
 **/
#define kHotelPreference           @"Hotel"
#define kGourmetPreference      @"Dining"
#define kCarPreference              @"Car Rental and Transfers"
#define kOrtherPreference          @"Others"
#define kGolfPreference              @"Golf"


/**
 * Request Content Type (also used for return format)
 **/
enum REQUEST_CONTENT_TYPE
{
    RCT_JSON = 0,
    RCT_FORM = 1,
    RCT_XML = 2,
    RCT_IMAGE = 3,
};


/***
 *Define static key
 ***/
#define FLAG_STARTED @"0"
#define stLoginStatus  @"isUserLogin"
#define stSessionToken  @"sessionToken"
#define SCREEN_SCALE  (((float)[UIScreen mainScreen].bounds.size.width)/414.0)
#define SCREEN_WIDTH  (((float)[UIScreen mainScreen].bounds.size.width))
#define SCREEN_HEIGHT (((float)[UIScreen mainScreen].bounds.size.height))
//#define HEADER_COLOR [UIColor colorWithRed:172.0/255.0 green:144.0/255.0 blue:49.0/255.0 alpha:1.1]
#define HEADER_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.1]
#define FONT_SIZE_15 15.0
#define FONT_SIZE_16 16.0
#define FONT_SIZE_23 23.0
#define FONT_SIZE_21 21.0
#define FONT_SIZE_18 18.0
#define FONT_SIZE_14 14.0
#define CATEGORY_LIST_CELL_HEIGHT 240
#define CHAT_BOX_THANK_YOU_MSG NSLocalizedString(@"Thank you! The most sought after experiences are often tailor-made. Please allow our Concierge consultants to get back to you in the 4 hours.", nil)
#define BOOKING_THANK_YOU_MSG NSLocalizedString(@"We will respond to your request within 4 hours, either with your request fulfilled or with an estimated timeline for fulfillment. If your request is received after 10pm Eastern Time, please expect a response by 12 noon Eastern Time the following day. If your request is urgent, we recommend that you call the concierge at phone number", nil)
#define CANCEL_THANK_YOU_MSG NSLocalizedString(@"We are sorry you had to cancel your request. We hope you will find something that suits your palate from our other offerings", nil)
#define AMEND_THANK_YOU NSLocalizedString(@"Please allow some time for our concierge consultants to work on your request. We’ll get back you as soon as we can.", nil)
#define ERROR_API_MSG NSLocalizedString(@"Sorry, we are not able to process your request. Please try again later or contact our concierge.", nil)
#define ERROR_CANCEL_MSG NSLocalizedString(@"Unfortunately, a cancellation request that is less than 24 hours from reservation time is not allowed. Please call concierge for assistance.", nil)
#define ERROR_ADMEND_MSG NSLocalizedString(@"Unfortunately, a modification request that is less than 24 hours from reservation time is not allowed. Please call concierge for assistance.", nil)
#define COMMON_BUTON_HEIGHT 45
#define SIGN_IN NSLocalizedString(@"Sign In", nil)
#define ERROR_ALERT_TITLE  NSLocalizedString(@"Error", nil)


#define KEY_SHOWTOOLTIP @"Show Tool Tips"

#define kJpegPhotoExt                   @"jpg"
#define kPngPhotoExt                    @"png"
#define kPhotoUploadMaxSize             2097152
#define NOTIFY_A_REQUEST_IS_AMENDED     @"DidAmendRequest"
#define NOTIFY_CLICK_HOME     @"DidHomeClick"

#define DATE_FORMAT @"dd MMM yyyy"
#define DATE_SEND_REQUEST_FORMAT @"yyyy-MM-dd"
#define DATETIME_SEND_REQUEST_FORMAT @"yyyy-MM-dd HH:mm:ss"
#define TIME_FORMAT @"hh:mm a"
#define TIME_SEND_REQUEST_FORMAT @"HH:mm:ss"
#define STORYBOARD_ID_LOCATION_POP_UP_VIEW_CONTROLLER @"LocationPopUpViewController"
#define DID_OPEN_LOCATION_POP_UP @"DIDOPENLOCATIONPOPUP"
#define USER_DEFAULT_YOUR_COUNTRY @"USERDEFAULTYOURCOUNTRY"
#define USER_DEFAULT_YOUR_CITY @"USERDEFAULTYOURCITY"
#define UTC_TIMEZONE @"UTC"
enum REQUEST_TYPE{
    NO_REQUEST = 0,
    OTHER_REQUEST = 1,
    GOURMET_REQUEST = 2,
    CAR_RENTAL_REQUEST = 3,
    CAR_TRANSFER_REQUEST = 4,
    HOTEL_REQUEST = 5,
    GOLF_REQUEST = 6,
    ENTERTAINMENT_REQUEST = 7,
    HOTEL_RECOMMEND_REQUEST = 9,
    GOURMET_RECOMMEND_REQUEST = 10,
    ENTERTAINMENT_RECOMMEND_REQUEST = 11,
    SPA_REQUEST = 12,
};

#define USER_SECRET @"Password"
#define keyCurrentSecret        @"l3JdK3PT09XfoxS3LKPwGA=="
#define EncryptKey @"#Deepa.SHANKAR@internationalsos.com#"
@end
